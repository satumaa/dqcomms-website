# Installation instructions

1.  make client folder
1.  fork `mellow-client-3.0` in bitbucket
1.  clone your forked repo to client folder `git clone --depth 1 <repo>`
1.  cd into the created repo folder
1.  run `./install.sh`
1.  create mellow database & database user (via phpmyadmin)
1.  import `mellow-client-3.0-default-db.sql` into the newly created database
1.  add database information to env file `configuration/.environment.env` (file is hidden)
1.  (optional) try to make first deployment via `__management`

# Update project with changes from Mellow-client-3.0

1.  cd into the dev version of your project
1.  make sure it's up to date and has no unstaged or staged changes
1.  run `git pull git@bitbucket.org:satumaa/mellow-client-3.0.git`
1.  deal with potential merge issues
1.  (optional) commit and push via the `__management`

# Commiting changes to mellow-client-3.0

This project uses a pre-commit script to enforce php coding standards.
If the script fails to install correctly (after `composer install` etc), please run the `contrib/setup.sh`. Any changes to the pre-commit script `crontrib/pre-commit.sh` require you to run the `setup.sh` again.
Project: dqcomms
