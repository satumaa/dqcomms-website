var exports = (module.exports = {});

/* these will be populated by parent */
exports.parentConfig = {};
exports.parentModules = {};

exports.modifySettings = function(settings) {
	return Object.assign(settings, {
		// return {
		EXCLUDE_CORE_GLOB: [
			"!" + settings.SOURCE_FOLDER + "/core/**/*",
			"!" + settings.NEW_BUILD_PATH_PUBLIC + "/core/**/*",
			"!" + settings.SOURCE_FOLDER + "/mellow/**/*",
			"!" + settings.NEW_BUILD_PATH_PUBLIC + "/mellow/**/*",
			"!" + settings.SOURCE_FOLDER + "/mellow_dev/**/*",
			"!" + settings.NEW_BUILD_PATH_PUBLIC + "/mellow_dev/**/*",
			"!" + settings.SOURCE_FOLDER + "/mellow_live/**/*",
			"!" + settings.NEW_BUILD_PATH_PUBLIC + "/mellow_live/**/*",
			"!" + settings.SOURCE_FOLDER + "/mellow-dev/**/*",
			"!" + settings.NEW_BUILD_PATH_PUBLIC + "/mellow-dev/**/*",
			"!" + settings.SOURCE_FOLDER + "/mellow-live/**/*",
			"!" + settings.NEW_BUILD_PATH_PUBLIC + "/mellow-live/**/*"
		]
	});
};

/* tasks in here will extend parent's gulpTasks */
exports.gulpTasks = {
	/* add tasks here */
};
