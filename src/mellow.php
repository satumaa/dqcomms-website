<?php

use mellow\App;
use mellow\Pages;

error_reporting(E_ALL);
ini_set('display_errors', '1');

session_start();
set_time_limit(8);

mb_http_output('UTF-8');
mb_internal_encoding('UTF-8');

global $DEBUG;
global $DEBUG_MODULE_COUNT;
global $DB_COUNT;

//opcache_reset();

function error_handler($code, $message, $file, $line)
{
	var_dump($code, $message, $file, $line);
	exit();
}
//set_error_handler('error_handler');

function object_to_array($obj)
{
	if (is_object($obj)) {
		$obj = (array) $obj;
	}
	if (is_array($obj)) {
		$new = [];
		foreach ($obj as $key => $val) {
			$new[$key] = object_to_array($val);
		}
	} else {
		$new = $obj;
	}

	return $new;
}

$rustart = getrusage();

// Script end
/* function rutime($ru, $rus, $index)
{
	return ($ru["ru_$index.tv_sec"] * 1000 + (int) ($ru["ru_$index.tv_usec"] / 1000))
	 - ($rus["ru_$index.tv_sec"] * 1000 + (int) ($rus["ru_$index.tv_usec"] / 1000));
} */

$REQUEST_URI_LOWER = mb_strtolower($_SERVER['REQUEST_URI']);
$reqArr = explode('?', $_SERVER['REQUEST_URI']);

// TODO: replace while -loop with a method that converts // - ///// to /
while (false !== strpos($reqArr[0], '//')) {
	$reqArr[0] = str_replace('//', '/', $reqArr[0]);
}

$_SERVER['REQUEST_URI'] = $reqArr[0];
if (isset($_SERVER['QUERY_STRING'])) {
	$_SERVER['REQUEST_URI'] .= '?'.$_SERVER['QUERY_STRING'];
}

require_once 'mellow_load.php';

try {
	$idstr = \mellow\Timer::getActiveTimerIdsAsString();

	if ((false == \mellow\App::getUser()->canCache() && App::isAPICallWithParams($_SERVER['REQUEST_URI'])) || \mellow\Cache::timerHasChanged()) {
		\mellow\Cache::clear();
	}

	$isAPICall = App::isAPICall($_SERVER['REQUEST_URI']);
	if (false === $isAPICall && \mellow\App::getUser()->canCache() && 'GET' == $_SERVER['REQUEST_METHOD'] && \mellow\Cache::hasCache($_SERVER['REQUEST_URI'])) {
		\mellow\Cache::output($_SERVER['REQUEST_URI']);
		exit();
	}

	require_once 'init.php';

	// $isAPICall = App::isAPIcall($_SERVER['REQUEST_URI']);
	// var_dump('isAPIcall?', $isAPICall, 'idstr', $idstr);

	// If current call is an API CALL:
	if (!empty($isAPICall)) {
		if (isset($_SERVER['CONTENT_TYPE'])) {
			$type = mb_strtolower(trim(explode(';', $_SERVER['CONTENT_TYPE'])[0]));

			if ('application/json' == $type) {
				$postdata = file_get_contents('php://input');
				if (!empty($postdata)) {
					$json = @json_decode($postdata);
					if (!empty($json)) {
						$_POST = object_to_array($json);
					}
				}
			}
		}
	}

	$response = App::route($_SERVER['REQUEST_URI'], $_POST);
	// var_dump($response);

	if (empty($isAPICall)) {
		if ('GET' == $_SERVER['REQUEST_METHOD'] && \mellow\App::getUser()->canCache() && isset($response) && is_object($response) && isset($response->type) && 'html' == $response->type) {
			\mellow\Cache::addToCache($_SERVER['REQUEST_URI'], $response->getHtml());
		}
	}

	if (isset($response) && is_object($response) && isset($response->type) && 'unauthorized' != $response->type) {
		$response->output();
	} else {
		echo return404();
	}

	$ru = getrusage();
	/*echo "This process used " . rutime($ru, $rustart, "utime") .
		" ms for its computations\n";
	echo "It spent " . rutime($ru, $rustart, "stime") .
		" ms in system calls\n";
		//echo "<br>".$DEBUG." instancee<br>";
		echo "<h1>".$COUNTER." </h1>";
		echo "<h1>DB: ".$DB_COUNT." </h1>";

		die();*/

//     if ($response) {
//         if ($isAPICall) {
//             if (isset($_GET['pretty'])) {
//                 header('Content-Type: text/plain; charset=utf-8');

//                 echo json_encode($page, JSON_PRETTY_PRINT);
//             } else {
//                 if (is_string($page)) {
//                     header('Content-Type: text/html; charset=utf-8');
//                     echo $page;
//                 } else {
//                     $page->outputAsAPICall();
//                 }
//             }

//             //echo json_encode($page->json());
//         } else {
//             $page->outputWithHeaders();

//             $ru = getrusage();
//             //echo "This process used " . rutime($ru, $rustart, "utime") .
//   //  " ms for its computations\n";
//         }
//     }
} catch (Exception $e) {
	if ('No page' === $e->getMessage()) {
		echo return404($e);
	} else {
		echo 'Caught exception: ', $e->getMessage(), "\n";
	}
}

function return404()
{
	// try {
	//     // App::clearErrors();
	//     http_response_code(404);
	//     /** @var \client\pages\PageNotFound[] $pageNotFounds */
	//     $pageNotFounds = Pages::getPagesHavingLayout(TEMPLATE_PAGE_NOT_FOUND);

	//     if (count($pageNotFounds) > 0) {
	//         $page = $pageNotFounds[0];
	//         $page->init();
	//         // this is for calming mellow - mellow stresses when the url does not match
	//         \mellow\App::parseUrl($page->getUrl());
	//         return $page->getHtml();
	//     }
	// } catch (Exception $e) {
	//TODO: return 404 page if one exists, otherwise the following:
	$notfound = \mellow\App::getPageFromClass('PageNotFound');
	header('HTTP/1.0 404 Not Found');
	if (empty($notfound)) {
		echo '<html>
            <head>
                <meta http-equiv="Refresh" content="0;url=/" />
            </head><body></body>
          </html>';
	} else {
		return $notfound->html();
	}

	//TODO: should this use http_response_code or vice versa?

	//echo '404! page not found';
	// if (!LIVE) {
	//     echo 'Caught exception: ', $e->getMessage(), "\n";
	// }
	// return $notfound->html();
	// }
}
