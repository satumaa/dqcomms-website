<?php

namespace client\modules\contentselectors;

class Images extends \mellow\ContentSelector
{
	public $useImgsAsHidden = false;

	public function __construct($settings = [])
	{
		$settings['response_edit'] = self::RESPONSE_RELOAD;
		parent::__construct($settings);
	}

	public function getDefaultImageSrc()
	{
		$modules = ($this->__get('modules'));

		for ($i = 0; $i < count($modules); ++$i) {
			if (!$modules[$i]->image->values->isDefault) {
				return $modules[$i]->image->values->square->src;
			}
		}

		return null;
	}

	public function get_selectable_productimage()
	{
		$rivi = new \mellow\Selectable();

		$rivi->translation_base = 'productimage';

		$hidden = $this->useImgsAsHidden;

		$rivi->modules->image = function () use ($hidden) {
			if (isset($hidden) && false === $hidden) {
				$view = 'product-image';
			} else {
				$view = 'product-hidden-image';
			}

			$img = new \mellow\Image(['default_value' => '//placehold.it/600x400', 'view' => $view, 'response_edit' => self::RESPONSE_RELOAD]);
			$img->translation_base = 'productimage';

			$img->sizes->default = new \mellow\ImageSize(600, null, 'scale');

			$img->sizes->square = new \mellow\ImageSize(400, 400, 'scale');

			$img->contextBubble('edit', 1);
			$img->siblingContext('edit');

			return $img;
		};

		return $rivi;
	}
}
