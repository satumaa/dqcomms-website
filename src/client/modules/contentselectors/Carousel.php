<?php

namespace client\modules\contentselectors;

class Carousel extends \mellow\ContentSelector
{
	public function __construct($settings = [])
	{
		$settings['response_edit'] = self::RESPONSE_RELOAD;
		parent::__construct($settings);
	}

	public function get_selectable_block()
	{
		$rivi = new \mellow\Selectable(['view' => 'carousel-slide']);

		$rivi->translation_base = 'carousel';

		/*$rivi->modules->h2div=static function(){ return new \mellow\Textarea(array("view"=>"div","inplace"=>false,"clickable"=>false));};
		$rivi->modules->description=static function(){ return new \mellow\Textarea(array("view"=>"p","inplace"=>false,"clickable"=>false));};
		$rivi->modules->button=static function(){ return new \client\modules\groups\Button(array("view"=>"button-bulky"));};


		$rivi->modules->varit=static function(){

			$b=new \mellow\Boolean(array("label"=>"Näytä oranssina?","true"=>"Kyllä","false"=>"Ei","default_value"=>false));
			$b->contextBubble("setbooleanvalue",2);
			$b->siblingContext("setbooleanvalue");
			return $b;

		};

		*/
		$rivi->modules->h2 = static function () {
			$t = new \mellow\Textarea(['default_value' => '[Title]', 'view' => 'h2', 'clickable' => false, 'inplace' => false]);
			$t->translation_base = 'title';

			return $t;
		};
		$rivi->modules->smalltitle = static function () {
			$t = new \mellow\Textarea(['default_value' => '[Small Title]', 'view' => 'p', 'clickable' => false, 'inplace' => false]);
			$t->translation_base = 'smalltitle';

			return $t;
		};

		// $rivi->modules->buttontext=static function () {
		//     $t= new \mellow\Textarea(array("default_value"=>"[Button text]","view"=>"p","clickable"=>false,"inplace"=>false,"response_edit"=>self::RESPONSE_RELOAD));
		//     $t->translation_base="carouselbutton";
		//     $t->contextBubble("edit", 1);
		//     return $t;
		// };

		$rivi->modules->button = static function () {
			$t = new \client\modules\groups\Button();

			return $t;
		};

		$rivi->modules->picture = static function () {
			$img = new \mellow\Image(['default_value' => '//placehold.it/1920x715', 'view' => 'picture']);

			$img->sizes->default = new \mellow\ImageSize(1920, 580, 'crop');
			$img->sizes->mobile = new \mellow\ImageSize(600, 600, 'crop');

			//1920x580
			$widths = [1920, 1600, 1200, 900, 640];
			$ratio = 1920 / 580;

			for ($i = 0; $i < count($widths); ++$i) {
				$width = $widths[$i];
				$height = (int) round($width / $ratio);
				$k = 's'.$width;
				$img->sizes->$k = new \mellow\ImageSize($width, $height, 'crop');
			}

			$img->contextBubble('edit', 1);
			$img->siblingContext('edit');

			return $img;
		};

		return $rivi;
	}
}
