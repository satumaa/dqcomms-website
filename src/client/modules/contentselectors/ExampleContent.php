<?php

namespace client\modules\contentselectors;

class ExampleContent extends \mellow\ContentSelector
{
	public function __construct($settings = [])
	{
		$settings['group_add'] = 'Add content';
		parent::__construct($settings);
	}

	public function get_selectable_topicandingres1()
	{
		$rivi = new \mellow\Selectable(['view' => 'topic-ingres']);

		$rivi->translation_base = 'topicandingres';

		$rivi->modules->title = static function () {
			return new \mellow\Textarea(['default_value' => '[Title]', 'view' => 'h2', 'clickable' => false, 'inplace' => false]);
		};

		$rivi->modules->text = static function () {
			return new \mellow\RichText(['default_value' => '[Text]', 'view' => 'div']);
		};

		return $rivi;
	}

	public function get_selectable_topicandingres2()
	{
		$rivi = new \mellow\Selectable(['view' => 'topic-ingres']);

		$rivi->translation_base = 'topicandingres';

		$rivi->modules->title = static function () {
			return new \mellow\Textarea(['default_value' => '[Title]', 'view' => 'h2', 'clickable' => false, 'inplace' => false]);
		};

		$rivi->modules->text = static function () {
			return new \mellow\RichText(['default_value' => '[Text]', 'view' => 'div']);
		};

		return $rivi;
	}

	public function get_selectable_topicandingres3()
	{
		$rivi = new \mellow\Selectable(['view' => 'topic-ingres']);

		$rivi->translation_base = 'topicandingres';

		$rivi->modules->title = static function () {
			return new \mellow\Textarea(['default_value' => '[Title]', 'view' => 'h2', 'clickable' => false, 'inplace' => false]);
		};

		$rivi->modules->text = static function () {
			return new \mellow\RichText(['default_value' => '[Text]', 'view' => 'div']);
		};

		return $rivi;
	}

	public function get_selectable_topicandingres4()
	{
		$rivi = new \mellow\Selectable(['view' => 'topic-ingres']);

		$rivi->translation_base = 'topicandingres';

		$rivi->modules->title = static function () {
			return new \mellow\Textarea(['default_value' => '[Title]', 'view' => 'h2', 'clickable' => false, 'inplace' => false]);
		};

		$rivi->modules->text = static function () {
			return new \mellow\RichText(['default_value' => '[Text]', 'view' => 'div']);
		};

		return $rivi;
	}

	public function get_selectable_topicandingres()
	{
		$rivi = new \mellow\Selectable(['view' => 'topic-ingres']);

		$rivi->translation_base = 'topicandingres';

		$rivi->modules->title = static function () {
			return new \mellow\Textarea(['default_value' => '[Title]', 'view' => 'h2', 'clickable' => false, 'inplace' => false]);
		};

		$rivi->modules->text = static function () {
			return new \mellow\RichText(['default_value' => '[Text]', 'view' => 'div']);
		};

		return $rivi;
	}

	public function get_selectable_video()
	{
		$rivi = new \mellow\Selectable(['view' => 'video']);

		$rivi->translation_base = 'video';

		$rivi->modules->image = static function () {
			$img = new \mellow\Image(['default_value' => '//placehold.it/1253x640', 'view' => 'image']);
			$img->sizes->default = new \mellow\ImageSize(1253, 640, 'crop');
			$img->translation_base = 'videocoverimage';
			$img->contextBubble('edit', 1);
			//$img->siblingContext("edit");
			return $img;
		};
		$rivi->modules->video = static function () {
			$v = new \mellow\Youtube(['response_edit' => self::RESPONSE_RELOAD]);
			$v->contextBubble('edit', 1);
			//$v->siblingContext("edit");
			return $v;
		};

		return $rivi;
	}

	public function get_selectable_kolmenostoa()
	{
		$rivi = new \mellow\Selectable(['view' => 'kolmenostoa']);

		$rivi->translation_base = 'kolmenostoa';

		$rivi->modules->n1 = function () {
			return new \client\modules\groups\KolumniNostoille();
		};
		$rivi->modules->n2 = function () {
			return new \client\modules\groups\KolumniNostoille();
		};
		$rivi->modules->n3 = function () {
			return new \client\modules\groups\KolumniNostoille();
		};

		return $rivi;
	}

	public function get_selectable_largeimage()
	{
		$rivi = new \mellow\Selectable(['view' => 'large-image']);

		$rivi->translation_base = 'large-image';
		$rivi->modules->picture = function () {
			$img = new \mellow\Image(['default_value' => '//placehold.it/1920x600', 'view' => 'picture', 'response_edit' => self::RESPONSE_HTML_PARENT]);

			$img->sizes->default = new \mellow\ImageSize(1920, null, 'scale');

			$widths = [1920, 1600, 1200, 900, 640];
			$ratio = 1920 / 424;

			for ($i = 0; $i < count($widths); ++$i) {
				$width = $widths[$i];
				$height = (int) round($width / $ratio);
				$k = 's'.$width;
				$img->sizes->$k = new \mellow\ImageSize($width, null, 'scale');
			}

			$img->contextBubble('edit', 1);
			$img->siblingContext('edit');

			return $img;
		};

		$rivi->modules->fontcolor = function () {
			$cats = [];
			$cats[] = (object) ['ID' => 1, 'name' => 'White', 'label' => 'White', 'cssclass' => 'color-white'];
			$cats[] = (object) ['ID' => 2, 'name' => 'Black', 'label' => 'Black', 'cssclass' => 'color-black'];

			$paikkavalinta = new \mellow\SelectableItems(['response_edit' => self::RESPONSE_RELOAD, 'view' => 'maara', 'default_value' => (object) ['value' => 1], 'group-label' => 'Select font color'], $cats);
			$paikkavalinta->contextBubble('setselectedvalue', 1);

			$paikkavalinta->bubbleForm('edit', 1);

			$paikkavalinta->siblingContext('setselectedvalue');

			return $paikkavalinta;
		};

		$rivi->modules->h2 = static function () {
			return new \mellow\Textarea(['default_value' => '[Title]', 'view' => 'h2', 'clickable' => false, 'inplace' => false]);
		};

		$rivi->modules->p = static function () {
			return new \mellow\Textarea(['default_value' => '[Text]', 'view' => 'p', 'clickable' => false, 'inplace' => false]);
		};

		$rivi->modules->button = static function () {
			return new \client\modules\groups\Button();
		};

		return $rivi;
	}

	public function get_selectable_nosto()
	{
		$rivi = new \mellow\Selectable(['view' => 'nosto']);

		$rivi->translation_base = 'nosto';

		$rivi->modules->image = static function () {
			$img = new \mellow\Image(['default_value' => '//placehold.it/720x720', 'view' => 'image']);
			$img->sizes->default = new \mellow\ImageSize(720, 720, 'crop');

			return $img;
		};
		$rivi->modules->h2 = static function () {
			return new \mellow\Textarea(['default_value' => '[Title]', 'view' => 'h2', 'clickable' => false, 'inplace' => false]);
		};

		$rivi->modules->p = static function () {
			return new \mellow\Textarea(['default_value' => '[Text]', 'view' => 'p', 'clickable' => false, 'inplace' => false]);
		};

		$rivi->modules->reverse = static function () {
			$naistapuhutaan = new \mellow\Boolean(['label' => 'Reverse image?', 'true' => 'Yes', 'false' => 'No', 'default_value' => false, 'response_edit' => self::RESPONSE_HTML_PARENT]);
			$naistapuhutaan->contextBubble('setbooleanvalue', 1);
			$naistapuhutaan->siblingContext('setbooleanvalue');

			return 	$naistapuhutaan;
		};
		$rivi->modules->vari = static function () {
			$maarat = [];

			$maarat[] = (object) ['ID' => 3, 'name' => 3, 'label' => 'Gray', 'color' => '#eee'];
			$maarat[] = (object) ['ID' => 6, 'name' => 6, 'label' => 'White', 'color' => '#ffffff'];

			$maarat[] = (object) ['ID' => 9, 'name' => 9, 'label' => 'Pink', 'color' => '#f1dcea'];

			$maarat = array_reverse($maarat);
			$paikkavalinta = new \mellow\SelectableItems(['response_edit' => self::RESPONSE_HTML_PARENT, 'view' => 'maara', 'default_value' => (object) ['value' => 3], 'group-label' => 'Select color'], $maarat);
			$paikkavalinta->contextBubble('setselectedvalue', 1);

			$paikkavalinta->siblingContext('setselectedvalue');

			return $paikkavalinta;
		};

		return $rivi;
	}

	public function get_selectable_topicandtext()
	{
		$rivi = new \mellow\Selectable(['view' => 'topicandtext']);

		$rivi->translation_base = 'topicandtext';

		$rivi->modules->title = static function () {
			return new \mellow\Textarea(['default_value' => '[Title]', 'view' => 'h2', 'clickable' => false, 'inplace' => false]);
		};

		$rivi->modules->text = static function () {
			return new \mellow\RichText(['default_value' => '[Text]', 'view' => 'div']);
		};

		return $rivi;
	}
}
