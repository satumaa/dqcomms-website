<?php
namespace client\modules\groups;
class ArticlesFrontPage extends \mellow\Group{
		
		
		
		public function __construct($settings=array()){
			
		
			parent::__construct($settings);
			
		}
		public function getArticlesToShow(){
			
			
			$m= "showatfrontpage";
			
			
			$pages=\mellow\App::getPagesFromClass("Article");
			
			shuffle($pages);
			
			$temp=array();
			$limit=3;
			for($i=0;$i<count($pages);$i++){
			
				if($pages[$i]->$m->values->value){
					
					
					$temp[]=$pages[$i];
				}
				if(count($temp)==$limit){
					return $temp;
				}
				
			}
			
			
			return $temp;
			
		}
		
		
}
?>