<?php

namespace client\modules\groups;

class Button extends \mellow\Group
{
	public function __construct($settings = [])
	{
		parent::__construct($settings);
	}

	public function get_text()
	{
		$text = new \mellow\Textarea(['response_edit' => self::RESPONSE_HTML_PARENT]);
		$text->contextBubble('edit', 2);

		$text->global = $this->global;

		$text->translation_base = 'buttontext';

		return $text;
	}

	public function get_link()
	{
		$link = new \mellow\Link(['classes' => @$this->settings['classes']]);
		$link->contextBubble('edit', 2);
		$link->translation_base = 'buttonlink';
		$link->global = $this->global;

		return $link;
	}
}
