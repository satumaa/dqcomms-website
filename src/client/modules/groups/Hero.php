<?php
namespace client\modules\groups;
class Hero extends \mellow\Group{
		
		
		
		public function __construct($settings=array()){
			
			parent::__construct($settings);
			
			
		}
	public static function getFontColor(){
		
		$cats=array();
		$cats[]=(object)array("ID"=>1,"name"=>"White","label"=>"White","cssclass"=>"color-white");
		$cats[]=(object)array("ID"=>2,"name"=>"Black","label"=>"Black","cssclass"=>"color-black");

		
		
		return $cats;
		
	}
	public function get_fontcolor(){
		
		
		
		$maarat=array();
		$maarat=self::getFontColor();
			
			
		
			
		



		//$maarat=array_reverse($maarat);
		$paikkavalinta=new \mellow\SelectableItems(array("response_edit"=>self::RESPONSE_RELOAD,"view"=>"maara","default_value"=>(object)array("value"=>1),"group-label"=>"Select font color"),$maarat);
		$paikkavalinta->contextBubble("setselectedvalue",1); 
		
		
		$paikkavalinta->bubbleForm("edit",1);
		
		$paikkavalinta->siblingContext("setselectedvalue");
		//$paikkavalinta->bubbleForm("create",1);
		
		//$paikkavalinta->siblingContext("setselectedvalue");
		
		return $paikkavalinta;
		
		
		
	}
		public function get_h1(){
			
	
				$text=new \mellow\Textarea(array("view"=>"h1","inplace"=>false,"clickable"=>false));
			
				$text->translation_base="title";
				return $text;
			
		
			
			
		}
		public function get_p(){
			
				$text=new \mellow\Textarea(array("view"=>"p","inplace"=>false,"clickable"=>false));
				$text->translation_base="ingressi";
				return $text;
			
			
			
		}
		public function get_picture(){
					
					$img=new \mellow\Image(array("default_value"=>"//placehold.it/1920x424","view"=>"picture","response_edit"=>self::RESPONSE_HTML_PARENT));
			
					$img->sizes->default=new \mellow\ImageSize(1920,NULL,"scale");
					
					
					
					$widths=array(1920,1600,1200,900,640);
					$ratio=1920/424;
					
					for($i=0;$i<count($widths);$i++){
						
						$width=$widths[$i];
						$height=(int)round($width/$ratio);
						$k="s".$width;
						$img->sizes->$k=new \mellow\ImageSize($width,NULL,"scale");
					}
					$img->sizes->square=new \mellow\ImageSize(600,600,"crop");
					
					$img->contextBubble("edit",1);
					$img->siblingContext("edit");
					return $img; 
					
		}
	
	
		
}
?>