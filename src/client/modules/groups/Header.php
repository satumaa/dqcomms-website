<?php
namespace client\modules\groups;
class Header extends \mellow\Group{
		
		
		
		public function __construct($settings=array()){
			
			
			parent::__construct($settings);
		
		
		
		}
	
		public function get_booking_btn_text(){
			
			$t= new \mellow\Textarea(array("default_value"=>"[Button text]","view"=>"span","clickable"=>false,"inplace"=>false));
			
			return $t;
		}
	
		public function get_booking_title(){
			
			$t= new \mellow\Textarea(array("default_value"=>"[Title]","view"=>"ptitle","clickable"=>false,"inplace"=>false));
			
			
			return $t;
		}
	
		public function get_booking_explained(){
			
				$t= new \mellow\Textarea(array("default_value"=>"[Description]","view"=>"p","clickable"=>false,"inplace"=>false));
			
			
			return $t;
		}
	
		public function get_booking_button(){
			
				$t= new \client\modules\groups\Button();
			
			
			return $t;
		}
		
		
}
?>