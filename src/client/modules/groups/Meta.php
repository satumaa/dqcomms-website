<?php
namespace client\modules\groups;
class Meta extends \mellow\Group{
		
		
		
		public function __construct($settings=array()){
			parent::__construct($settings);
			
		}
	
		public function getShareImgageFb(){
			$curTemplate=$this->getCurrentTemplate();
			$img=$curTemplate->img;
			if($img){
				
				if($img->values->fb && $img->values->isDefault==false){
					return $img->values->fb->src;
					
				}
				
			}
			
			return "/resources/images/share_facebook.png";
			
		}
		public function get_title(){
			
			//<?=$M->getCurrentTemplate()->values->name; - 
			$default_title="";
			$curTemplate=$this->getCurrentTemplate();
			if($curTemplate){
				if($curTemplate->values->default_page){
					
					$default_title=\mellow\Translations::translate("meta.default.title");
				}else{
				$default_title=ucfirst($curTemplate->values->name)." - ".\mellow\Translations::translate("meta.default.title");
			
			//$default_title=($curTemplate->values->name)." - YTK";
				}
				
			}
			 $title= new \mellow\Textarea(array("default_value"=>$default_title,"inplace"=>false,"clickable"=>false,"response_edit"=>self::RESPONSE_RELOAD));
			$title->translation_base="metatitle";
			
			
			// $title->contextBubble("edit",3);
			 return $title;
		}
		public function get_description(){
			
			$def=\mellow\Translations::translate("meta.default.description");
			
			$curTemplate=$this->getCurrentTemplate();
			if($curTemplate){
				
				if($curTemplate->ingressi && $curTemplate->ingressi->values->isDefault==false){
					
					$def= $curTemplate->ingressi->values->raw;
					// $curTemplate->ingressi->values->value;
					
				}
				
			}
			
			
		
			$desc= new \mellow\Textarea(array("default_value"=>$def,"inplace"=>false,"clickable"=>false,"response_edit"=>self::RESPONSE_RELOAD));
			$desc->translation_base="metadesc";
			// $desc->contextBubble("edit",3);
			 return $desc;
			
			
		}
	/*	public function createLayoutModules(){
			
			
			
			$this->modules->title=static function() { 
				return new \mellow\Textarea(array("default_value"=>"YTK"));
				
			};
			
			$this->modules->description=static function() { 
			return new \mellow\Textarea(array("default_value"=>\mellow\Translations::translate("meta.default.description"),"inplace"=>false,"clickable"=>false,"response_edit"=>self::RESPONSE_RELOAD));
				
			};
			parent::createLayoutModules();
			
		}*/
		public function initialize(){
			parent::initialize();
			
			//echo $this->getCurrentTemplate()->values->name;
			//$this->modules->title->setSetting("default_value","YTK - ".$this->getCurrentTemplate()->values->name);
		}
		
		
}
?>