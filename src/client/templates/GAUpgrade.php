<?php

namespace client\templates;

class GAUpgrade extends BaseCampaign
{
    public function __construct($settings = [])
    {
        $settings['table'] = 'form_gaupgrade';
        parent::__construct($settings);
    }

    public function get_title()
    {
        return new \mellow\Textarea(['default_value' => '[Title]', 'view' => 'h1', 'classes' => 'm-title' ]);
    }

    public function get_description()
    {
        return new \mellow\RichText(['default_value' => '[Description]', 'view' => 'div', 'classes' => 'm-description' ]);
    }
    public function get_cta()
    {
        return new \mellow\Textarea(['default_value' => '[CTA]', 'view' => 'p', 'classes'=>'m-cta' ]);
    }

    public function get_bg()
    {
        $kuva = new \mellow\Image(['default_value' => '//placehold.it/1600x900', 'classes'=>'container']);
        $kuva->sizes->default = new \mellow\ImageSize(1600, 900, 'crop', '//placehold.it/1600x900');
        // $kuva->sizes->fb = new \mellow\ImageSize(1200,600 ,'crop', '//placehold.it/1200x600');
        return $kuva;
    }

    public function handleSubmit($data)
    {
        // if (!empty($data['email'])) {
        $PDO = \mellow\App::$PDO;
        // $entry = $this->getEntry($data['email']);
        $entry = false;
            
        if (!empty($entry) && !empty($entry['id'])) {
            $sql = "UPDATE ".$this->table." SET first_name = ?, last_name = ?, email = ?, domain = ?,  json = ? WHERE id = ?";
            $input = [
                    @$data['first_name'],
                    @$data['email'],
                    @$data['last_name'],
                    @$data['domain'],
                    
                    json_encode(@$data['json']),
                    $entry['id']
                ];
        } else {
            $sql = "INSERT INTO ".$this->table." (first_name, last_name, email, domain, message, json, added) VALUES (?,?,?,?,?,?,?)";
            $input = [
                    @$data['first_name'],
                    @$data['email'],
                    @$data['last_name'],
                    @$data['domain'],
                    @$data['message'],
                    json_encode(@$data['json']),
                    date('Y-m-d H:i:s')
                ];
        }
            
        if ($stmt = $PDO->prepare($sql)) {
            if ($stmt->execute($input)) {
                return $PDO->lastInsertId();
            }
        }
        // }
        
        return false;
    }


    public function emailForm($data = "")
    {
        if (!empty($data['id'])) {
            $body = "";
            $hideColumns = ['status', 'json', 'modified'];

            foreach ($data as $col => $value) {
                // $body .= $col.":\n".$value."\n\n";
                if (!in_array($col, $hideColumns)) {
                    $body .= '<p><strong>'.$col.":</strong><br/>".$value."</p>";
                }
            }
            
            // if (LIVE) {
            //     $this->sendHtmlMail("lauri.suopera@dqcomms.com", "GA Upgrade notification", $body);
            // } else {
            $this->sendHtmlMail("hunter.rafuse@dqcomms.com", "GA Upgrade notification", $body);
            // }
            $sql = "UPDATE ".$this->table." SET `status` = 2 WHERE id = ?";
            $PDO = \mellow\App::$PDO;
            if ($stmt = $PDO->prepare($sql)) {
                if ($stmt->execute([$data['id']])) {
                    return true;
                }
            }
        }

        return false;
    }


    public function html()
    {
        if (!empty($_POST)) {
            $id = $this->handleSubmit($_POST);
            $data = $this->getEntryById($id);
            $this->emailForm($data);

            // return json_encode(['success' => !empty($contact_id)]);
            return new \mellow\responses\Json(['success' => !empty($id)]);
        }
        return parent::html();
    }
}
