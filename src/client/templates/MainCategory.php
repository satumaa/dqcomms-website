<?php

namespace client\templates;

class MainCategory extends ProductCategory
{
	public function getAddableChildren()
	{
		return ['subcategory'];
	}
}
