<?php

namespace client\templates;

class SubCategory extends ExampleCommonTemplate
{
	public function __construct($settings = null)
	{
		parent::__construct($settings);

		$this->translation_base = 'subcategory';
	}

	public function get_hero()
	{
		return new \client\modules\groups\Hero();
	}

	public function getListView()
	{
		return '';
	}

	public function get_squareimage()
	{
		// sub cate
		$kuva = new \mellow\Image(['default_value' => '//placehold.it/120x120']);
		$kuva->translation_base = 'categoryimage';
		$kuva->sizes->default = new \mellow\ImageSize(120, 120, 'crop', '//placehold.it/120x120');

		return $kuva;
	}

	public function getAddableChildren()
	{
		$depth = count($this->getUrlPath());

		if ($depth < 3) {
			return ['subcategory'];
		}
		/*if($depth==2){

			return array("product","smallbanner");

		}else{

		return array("subcategory");

		}*/
		return ['subcategory', 'simpleproduct'];
	}
}
