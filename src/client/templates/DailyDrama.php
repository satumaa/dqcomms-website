<?php

namespace client\templates;

class DailyDrama extends DqcBase
{
	public function getAddableChildren(){
		 
		return array("article");
	}
		public function get_h1(){
		
			$t = new \mellow\Textarea(['default_value' => '[Otsikko]', 'view' => 'h1', 'clickable' => false, 'inplace' => false]);
	
		return $t;
	}
	
}
