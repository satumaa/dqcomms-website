<?php

namespace client\templates;

class FinnkinoDa extends BaseCampaign
{
    public function __construct($settings = [])
    {
        $settings['campaign'] = 'FinnkinoDowntownAbbey';
        parent::__construct($settings);
    }

    public function get_title()
    {
        return new \mellow\Textarea(['default_value' => '[Title]', 'view' => 'h1', 'classes' => 'm-title' ]);
    }

    public function get_description()
    {
        return new \mellow\RichText(['default_value' => '[Description]', 'view' => 'div', 'classes' => 'm-description' ]);
    }
    public function get_cta()
    {
        return new \mellow\Textarea(['default_value' => '[CTA]', 'view' => 'p', 'classes'=>'m-cta' ]);
    }

    public function get_bg()
    {
        $kuva = new \mellow\Image(['default_value' => '//placehold.it/1600x900', 'classes'=>'container']);
        $kuva->sizes->default = new \mellow\ImageSize(1600, 900, 'crop', '//placehold.it/1600x900');
        // $kuva->sizes->fb = new \mellow\ImageSize(1200,600 ,'crop', '//placehold.it/1200x600');
        return $kuva;
    }

    public function handleSubmit($data)
    {
        // if (!empty($data['email'])) {
        $PDO = \mellow\App::$PDO;
        // $entry = $this->getEntry($data['email']);
        $entry = false;
        $transport = isset($data['transport']) ? $data['transport'] : 1;
            
        if (!empty($entry) && !empty($entry['id'])) {
            $sql = "UPDATE form_finnkino SET first_name = ?, last_name = ?, email = ?, diet = ?, `location` = ?,  json = ? WHERE campaign = ? AND id = ?";
            $input = [
                    @$data['first_name'],
                    @$data['last_name'],
                    @$data['email'],
                    @$data['diet'],
                    @$data['location'],
                    json_encode(@$data['json']),
                    $this->settings['campaign'],
                    $entry['id']
                ];
        } else {
            $sql = "INSERT INTO form_finnkino (campaign, first_name, last_name, email, diet, `location`, json, added) VALUES (?,?,?,?,?,?,?,?)";
            $input = [
                    $this->settings['campaign'],
                    @$data['first_name'],
                    @$data['last_name'],
                    @$data['email'],
                    @$data['diet'],
                    @$data['location'],
                    json_encode(@$data['json']),
                    'NOW()'
                ];
        }
            
        if ($stmt = $PDO->prepare($sql)) {
            if ($stmt->execute($input)) {
                return $PDO->lastInsertId();
            }
        }
        // }
        
        return false;
    }
}
