<?php

namespace client\templates;

abstract class ExampleCommonTemplate extends \mellow\Template
{
	public function showOnNavigation()
	{
		return true;
	}

	public function showNavigationOverlay()
	{
		return false;
	}

	public static function countProductsOnly($prods = [])
	{
		$c = 0;

		$approved = ['product', 'simpleproduct'];
		for ($i = 0; $i < count($prods); ++$i) {
			$name = mb_strtolower($prods[$i]->getShortNameOfClass());

			if (in_array($name, $approved)) {
				++$c;
			}
		}

		return $c;
	}

	public function get_content()
	{
		return new \client\modules\contentselectors\ExampleContent();
	}

	public function get_head()
	{
		return new \client\modules\groups\Head();
	}

	public function get_header()
	{
		return new \client\modules\groups\Header();
	}

	public function get_footer()
	{
		return new \client\modules\groups\Footer();
	}

	public static function object_array_sort(&$data, $key)
	{
		for ($i = count($data) - 1; $i >= 0; --$i) {
			$swapped = false;

			for ($j = 0; $j < $i; ++$j) {
				$jval = $data[$j]->values->$key;
				$jplusval = $data[$j + 1]->values->$key;
				if ('added' == $key) {
					$jval = strtotime($jval);
					$jplusval = strtotime($jplusval);
				}
				if (($jval) > ($jplusval)) {
					$tmp = $data[$j];
					$data[$j] = $data[$j + 1];
					$data[$j + 1] = $tmp;
					$swapped = true;
				}
			}
			if (!$swapped) {
				return;
			}
		}
	}
}
