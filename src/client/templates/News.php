<?php

namespace client\templates;

class News extends ExampleCommonTemplate
{
	public function getAddableChildren()
	{
		return ['article'];
	}

	public function get_hero()
	{
		return new \client\modules\groups\Hero();
	}
}
