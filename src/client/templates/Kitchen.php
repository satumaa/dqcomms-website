<?php

namespace client\templates;

class Kitchen extends ExampleCommonTemplate
{
	public function __construct($settings = [])
	{
		parent::__construct($settings);
	}

	public function get_carousel()
	{
		return new \client\modules\contentselectors\Carousel();
	}

	public function get_bestsellers()
	{
		return new \client\modules\groups\ProductListView(['show' => 'bestseller']);
	}

	public function get_newin()
	{
		return new \client\modules\groups\ProductListView(['show' => 'newin']);
	}

	public function get_articles()
	{
		return new \client\modules\groups\ArticlesFrontPage([]);
	}

	public function get_hero()
	{
		return new \client\modules\groups\Hero();
	}

	public static function getCategories()
	{
	}

	public function get_showatfrontpage()
	{
		$naistapuhutaan = new \mellow\Boolean(['label' => 'Show at Home- page?', 'true' => 'Yes', 'false' => 'No', 'default_value' => false, 'response_edit' => self::RESPONSE_RELOAD]);
		$naistapuhutaan->contextBubble('setbooleanvalue', 1);

		return    $naistapuhutaan;
	}

	public static function getTags()
	{
		$cats = [];
		$cats[] = (object) ['ID' => 1, 'name' => 'Example Tag 1', 'label' => 'Example Tag 1'];
		$cats[] = (object) ['ID' => 2, 'name' => 'Example Tag 2', 'label' => 'Example Tag 2'];

		$cats[] = (object) ['ID' => 3, 'name' => 'Example Tag 3', 'label' => 'Example Tag 3'];

		return $cats;
	}

	public function get_tags()
	{
		$maarat = [];
		$maarat = self::getTags();

		//$maarat=array_reverse($maarat);
		$paikkavalinta = new \mellow\SelectableItems(['response_edit' => self::RESPONSE_RELOAD, 'view' => 'maara', 'default_value' => (object) ['value' => 1], 'group-label' => 'Select category'], $maarat);
		$paikkavalinta->contextBubble('setselectedvalue', 1);

		$paikkavalinta->bubbleForm('edit', 1);
		$paikkavalinta->bubbleForm('create', 1);

		//$paikkavalinta->siblingContext("setselectedvalue");

		return $paikkavalinta;
	}

	public function get_shortdescription()
	{
		$text = new \mellow\Textarea(['default_value' => '[Short description]', 'view' => 'span', 'clickable' => false, 'inplace' => false]);

		$text->translation_base = 'shortdesc';
		$text->contextBubble('edit', 1);
		$text->bubbleForm('edit', 1);
		$text->bubbleForm('create', 1);

		return $text;
	}

	public function get_listimage()
	{
		$img = new \mellow\Image(['default_value' => '//placehold.it/560x560', 'view' => 'image', 'allow_remove' => true, 'response_edit' => self::RESPONSE_RELOAD]);
		$img->sizes->default = new \mellow\ImageSize(560, 560, 'crop');
		$img->translation_base = 'listimg';
		$img->contextBubble('edit', 1);
		$img->contextBubble('clear', 1);

		return $img;
	}

	public function getListView()
	{
		$img = $this->__get('listimage');

		if (!$img->values->isDefault) {
			$src = $img->values->src;
		} else {
			$picture = $this->__get('hero')->picture;
			$src = '//placehold.it/512x512';
			if (!$picture->values->isDefault) {
				$src = $picture->values->square->src;
			}
		}

		$tag = $this->__get('tags')->values->payload->label;

		return '<li data-tag="'.$tag.'" data-uri="'.$this->uri.'" class="'.$this->values->classes.'"><article><img src="'.$src.'" alt=""><h4>'.$tag.'</h4><h2>'.$this->values->name.'</h2><p>'.$this->__get('shortdescription').'</p><p><a href="'.$this->values->url.'" class="button">'.$this->translate('read more').'</a></p></article></li>';
	}

	public function get_content()
	{
		return new \client\modules\contentselectors\ExampleContent();
	}

	public function getAddableChildren()
	{
		return ['subcategory'];

		return ['simplecategory'];
	}

	public function get_link()
	{
		$link = new \mellow\Link(['response_edit' => self::RESPONSE_HTML_PARENT]);
		$link->contextBubble('edit', 1);

		$link->siblingContext('edit');

		return $link;
	}

	public function get_h4()
	{
		$text = new \mellow\Textarea(['view' => 'h4', 'inplace' => false, 'clickable' => false]);

		return $text;
	}

	public function get_h3()
	{
		$text = new \mellow\Textarea(['view' => 'h3', 'inplace' => false, 'clickable' => false]);

		return $text;
	}

	public function get_image()
	{
		$img = new \mellow\Image(['default_value' => '//placehold.it/370x205', 'view' => 'image']);

		$img->sizes->default = new \mellow\ImageSize(370, 205, 'crop');

		$img->contextBubble('edit', 1);
		$img->siblingContext('edit');

		return $img;
	}

	public function getArticlesToShow()
	{
		$m = 'showatfrontpage';

		$pages = \mellow\App::getPagesFromClass('Article');

		shuffle($pages);

		$temp = [];
		$limit = 3;
		for ($i = 0; $i < count($pages); ++$i) {
			if ($pages[$i]->$m->values->value) {
				$temp[] = $pages[$i];
			}
			if (count($temp) == $limit) {
				return $temp;
			}
		}

		return $temp;
	}
}
