<?php

namespace client\templates;

class BaseCampaign extends ExampleCommonTemplate
{
    protected $table = 'form_signups';

    public function __construct($settings = [])
    {
        parent::__construct($settings);
        if (!empty($settings['table'])) {
            $this->table = $settings['table'];
        }
    }

    public function canBeAddedToRoot()
    {
        return false;
    }

    public function showOnNavigation()
    {
        return false;
    }
    
    public function getEntry($email = "")
    {
        if (!empty($email)) {
            $sql = "SELECT * FROM ".$this->table." WHERE campaign = ? AND email = ?";
            $PDO = \mellow\App::$PDO;

            if ($stmt = $PDO->prepare($sql)) {
                $input = array($this->settings['campaign'], $email);
                if ($stmt->execute($input)) {
                    $row = $stmt->fetch(\PDO::FETCH_ASSOC);
                    return $row;
                }
            }
        }

        return false;
    }

    public function getEntryById($id)
    {
        if (!empty($id)) {
            $sql = "SELECT * FROM ".$this->table." WHERE id = ?";
            $PDO = \mellow\App::$PDO;

            if ($stmt = $PDO->prepare($sql)) {
                $input = array($id);
                if ($stmt->execute($input)) {
                    $row = $stmt->fetch(\PDO::FETCH_ASSOC);
                    return $row;
                }
            }
        }

        return false;
    }


    public function handleSubmit($data)
    {
        return $this->handleSubmitUnique();
    }

    public function handleSubmitAll($data)
    {
        $PDO = \mellow\App::$PDO;
        $status = isset($data['status']) ? $data['status'] : 1;
        
        $sql = "INSERT INTO ".$this->table." (campaign, first_name, last_name, email, status, json, added) VALUES (?,?,?,?,?,?,?)";
        $input = [
            $this->settings['campaign'],
            @$data['first_name'],
            @$data['last_name'],
            @$data['email'],
            $status,
            json_encode(@$data['json']),
            'NOW()'
        ];
    
        if ($stmt = $PDO->prepare($sql)) {
            if ($stmt->execute($input)) {
                return $PDO->lastInsertId();
            }
        }
    
        return false;
    }

    public function handleSubmitUnique($data)
    {
        if (!empty($data['email'])) {
            $PDO = \mellow\App::$PDO;
            $entry = $this->getEntry($data['email']);
            $status = isset($data['status']) ? $data['status'] : 1;
            
            if (!empty($entry) && !empty($entry['id'])) {
                $sql = "UPDATE ".$this->table." SET first_name = ?, last_name = ?, email = ?, status = ?, json = ? WHERE campaign = ? AND id = ?";
                $input = [
                    @$data['first_name'],
                    @$data['last_name'],
                    @$data['email'],
                    $status,
                    json_encode(@$data['json']),
                    $this->settings['campaign'],
                    $entry['id']
                ];
            } else {
                $sql = "INSERT INTO ".$this->table." (campaign, first_name, last_name, email, status, json, added) VALUES (?,?,?,?,?,?,?)";
                $input = [
                    $this->settings['campaign'],
                    @$data['first_name'],
                    @$data['last_name'],
                    @$data['email'],
                    $status,
                    json_encode(@$data['json']),
                    'NOW()'
                ];
            }
            
            if ($stmt = $PDO->prepare($sql)) {
                if ($stmt->execute($input)) {
                    return $PDO->lastInsertId();
                }
            }
        }
        
        return false;
    }

    public static function sendHtmlMail($to, $subject, $txt)
    {
        $headers = "From: form@dqcomms.com" . "\r\n";
        $headers .= "MIME-Version: 1.0\r\n";
        $headers .= "Content-Type: text/html; charset=UTF-8\r\n";
        mail($to, $subject, $txt, $headers);
    }
    
    public static function sendPlainMail($to, $subject, $txt)
    {
        $headers = "From: form@dqcomms.com" . "\r\n";
        $headers .= "MIME-Version: 1.0\r\n";
        $headers .= "Content-Type: text/plain; charset=UTF-8\r\n";
        mail($to, $subject, $txt, $headers);
    }

    public function html()
    {
        if (!empty($_POST)) {
            $contact_id = $this->handleSubmit($_POST);
            // return json_encode(['success' => !empty($contact_id)]);
            return new \mellow\responses\Json(['success' => !empty($contact_id)]);
        }
        return parent::html();
    }
}
