<?php

namespace client\templates;

class Namu extends BaseCampaign
{
    public function __construct($settings = [])
    {
        $settings['campaign'] = 'Namu2019';
        parent::__construct($settings);
    }

    public function get_description()
    {
        return new \mellow\Textarea(['default_value' => '[Description]', 'view' => 'div' ]);
    }


    public function handleSubmit($data)
    {
        // if (!empty($data['email'])) {
        $PDO = \mellow\App::$PDO;
        // $entry = $this->getEntry($data['email']);
        $entry = false;
        $transport = isset($data['transport']) ? $data['transport'] : 1;
            
        if (!empty($entry) && !empty($entry['id'])) {
            $sql = "UPDATE form_signups SET first_name = ?, last_name = ?, diet = ?, transport = ?, json = ? WHERE campaign = ? AND id = ?";
            $input = [
                    @$data['first_name'],
                    @$data['last_name'],
                    @$data['diet'],
                    $transport,
                    json_encode(@$data['json']),
                    $this->settings['campaign'],
                    $entry['id']
                ];
        } else {
            $sql = "INSERT INTO form_signups (campaign, first_name, last_name, diet, transport, json, added) VALUES (?,?,?,?,?,?,?)";
            $input = [
                    $this->settings['campaign'],
                    @$data['first_name'],
                    @$data['last_name'],
                    @$data['diet'],
                    $transport,
                    json_encode(@$data['json']),
                    'NOW()'
                ];
        }
            
        if ($stmt = $PDO->prepare($sql)) {
            if ($stmt->execute($input)) {
                return $PDO->lastInsertId();
            }
        }
        // }
        
        return false;
    }
}
