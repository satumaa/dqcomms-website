<?php

namespace client\templates;

class Variables extends BaseAdmin
{
	public function get_taulukko()
	{
		$taulukko = new \client\modules\contentselectors\Taulukko(['disable_setcellcount' => true, 'disable_createfromtext' => true]);

		$taulukko->setPdo(\mellow\App::$PDO_COMMON);

		return $taulukko;
		//$table=new \mellow\Table(array("disable_setcellcount"=>true,"disable_createfromtext"=>true));
					//$table->=get_selectable_row=static function (){ return new \client\modules\selectables\VariablesRow(); };
					//return $table;
	}

	public function getPrefill()
	{
		$taulukko = ($this->__get('taulukko'));
		$rivit = $taulukko->__get('modules');
		$temp = [];
		foreach ($rivit as $index => $rivi) {
			if (!$rivi->nameofcolor->values->isDefault && !$rivi->rgb->values->isDefault) {
				$temp[] = ['name' => $rivi->nameofcolor->values->value, 'value' => $rivi->rgb->values->value];
			}
		}

		return $temp;
	}

	/*public function createModules(){

			//$this->modules->content=new \client\modules\groups\WikiTietopankkiContent();


			parent::createModules();

			$this->modules->taulukko=static function(){

					$table=		new \mellow\Table(array("disable_setcellcount"=>true,"disable_createfromtext"=>true));
					$table->selectable->row=static function (){ return new \client\modules\selectables\VariablesRow(); };
					return $table;
			};

	}*/
	public function getTranslationTable()
	{
		//return array();
		$temp = [];
		$taulukko = ($this->__get('taulukko'));

		$rivit = $taulukko->__get('modules');

		foreach ($rivit as $index => $rivi) {
			$solu = $rivi->__get('0');
			$arvosolu = $rivi->__get('1');
			$key = ($solu->values->raw);
			$arvo = ($arvosolu->values->raw);

			$temp['{'.mb_strtoupper(trim($key)).'}'] = $arvo;
		}

		return $temp;
	}
}
