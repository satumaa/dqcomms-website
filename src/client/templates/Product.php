<?php

namespace client\templates;

class Product extends ExampleProduct
{
	public function __construct($settings = null)
	{
		parent::__construct($settings);

		$this->translation_base = 'product';
	}

	public function getAddableChildren()
	{
		return ['color'];
	}

	public function get_h1()
	{
		$t = new \mellow\Textarea(['default_value' => '[Topic]', 'view' => 'h1', 'clickable' => false, 'inplace' => false]);

		$t->translation_base = 'topic';

		return $t;
	}

	public function get_inspiration()
	{
		return new \client\modules\groups\Inspiration();
	}

	public function get_features()
	{
		return new \client\modules\contentselectors\Features();
	}

	public function getDefaultImageSrc()
	{
		$lapset = $this->getChildren();

		for ($i = 0; $i < count($lapset); ++$i) {
			$src = ($lapset[$i]->getDefaultImageSrc());

			if (isset($src)) {
				return $src;
			}
		}

		return '//placehold.it/400x400';
	}

	public function getListView()
	{
		$lapset = $this->getChildren();

		$colorcircles = '';
		for ($i = 0; $i < count($lapset); ++$i) {
			$colorcircles .= $lapset[$i]->getColorCircle();
		}

		$src = $this->getDefaultImageSrc();

		return '<li>
						<a href="'.$this->values->url.'" data-uri="'.$this->uri.'" class="'.$this->values->classes.'">
							<div class="image">
								<button class="cta"><span>'.$this->translate('discover').'</span></button>
								<img src="'.$src.'" alt="">
							</div>
							
							<div class="Clickable Colors">'.$colorcircles.'</div>
							<h3>'.$this->values->name.'</h3>
							<p>'.$this->__get('shortdescription').'</p>
						</a>
					</li>';
	}
}
