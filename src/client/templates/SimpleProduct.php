<?php

namespace client\templates;

class SimpleProduct extends ExampleProduct
{
	public function __construct($settings = null)
	{
		parent::__construct($settings);

		$this->translation_base = 'simpleproduct';
	}

	public function getDefaultImageSrc()
	{
		$src = $this->__get('images')->getDefaultImageSrc();

		if (isset($src)) {
			return $src;
		}

		return '//placehold.it/400x400';
	}

	public function get_images()
	{
		return new \client\modules\contentselectors\Images();
	}

	public function getAddableChildren()
	{
		return [];
	}

	public function get_h1()
	{
		$t = new \mellow\Textarea(['default_value' => '[Topic]', 'view' => 'h1', 'clickable' => false, 'inplace' => false]);

		$t->translation_base = 'topic';

		return $t;
	}

	public function get_inspiration()
	{
		return new \client\modules\groups\Inspiration();
	}

	public function get_features()
	{
		return new \client\modules\contentselectors\Features();
	}

	public function getListView()
	{
		$lapset = $this->getChildren();

		$colorcircles = '';

		$src = $this->getDefaultImageSrc();

		return '<li>
						<a href="'.$this->values->url.'" data-uri="'.$this->uri.'" class="'.$this->values->classes.'">
							<div class="image">
								<button class="cta"><span>'.$this->translate('discover').'</span></button>
								<img src="'.$src.'" alt="">
							</div>
							
							<h3>'.$this->values->name.'</h3>
							<p>'.$this->__get('shortdescription').'</p>
						</a>
					</li>';
	}
}
