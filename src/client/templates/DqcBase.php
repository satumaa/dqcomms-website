<?php

namespace client\templates;

abstract class DqcBase extends \mellow\Template
{
	public function showOnNavigation()
	{
		return true;
	}

	
	public function get_head()
	{
		return new \client\modules\groups\Head();
	}

	public function get_header()
	{
		return new \client\modules\groups\Header();
	}

	public function get_footer()
	{
		return new \client\modules\groups\Footer();
	}

	public function get_share()
	{
		return new \client\modules\groups\Share();
	}
	
	
	
}
