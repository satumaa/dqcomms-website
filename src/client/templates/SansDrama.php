<?php

namespace client\templates;

class SansDrama extends ExampleCommonTemplate
{
    public function __construct($settings = [])
    {
        parent::__construct($settings);
        $this->settings['campaign_name'] = 'sansdrama2019';
    }

    public function canBeAddedToRoot()
    {
        return false;
    }

    public function showOnNavigation()
    {
        return false;
    }

    public function get_googleauth()
    {
        return $this->getGoogleSignIn();
    }

    public function getGoogleSignIn()
    {
        return new \mellow\modules\groups\GoogleSignIn(['client_id' => '177889218619-1vccf8e7vhqoo16ldjp7da023bsjg5n7.apps.googleusercontent.com']);
    }

    public function getEntry($email = "")
    {
        if (empty($email) && !empty($_SESSION['_gauth'])) {
            $email = $_SESSION['_gauth']->email;
        }

        if (!empty($email)) {
            $sql = "SELECT * FROM form_google WHERE campaign = ? and email = ?";
            $PDO = \mellow\App::$PDO;

            if ($stmt = $PDO->prepare($sql)) {
                $input = array($this->settings['campaign_name'], $email);
                if ($stmt->execute($input)) {
                    $row = $stmt->fetch(\PDO::FETCH_ASSOC);
                    return $row;
                }
            }
        }


        return false;
    }

    public function handleSubmit($data)
    {
        if (!empty($data['email'])) {
            $PDO = \mellow\App::$PDO;

            $entry = $this->getEntry($data['email']);

            

            $status = isset($data['status']) ? $data['status'] : 1;
            
            if (!empty($entry) && !empty($entry['id'])) {
                $sql = "UPDATE form_google SET status = ?, json = ? WHERE campaign = ? AND id = ?";
                $input = [
                    $status,
                    '',
                    $this->settings['campaign_name'],
                    $entry['id']
                ];
            } else {
                $sql = "INSERT INTO form_google (campaign, first_name, last_name, email, status, json, added) VALUES (?,?,?,?,?,?,?)";
                $input = [
                    $this->settings['campaign_name'],
                    @$data['given_name'],
                    @$data['family_name'],
                    @$data['email'],
                    $status,
                    '',
                    date('Y-m-d H:i:s')
                ];
            }
            
            if ($stmt = $PDO->prepare($sql)) {
                if ($stmt->execute($input)) {
                    return !empty($entry['id']) ? $entry['id'] : $PDO->lastInsertId();
                    // return $PDO->lastInsertId();
                }
            }
        }
        
        return false;
    }



    public function html()
    {
        if (isset($_REQUEST['attend'])) {
            $data = (array) @$_SESSION['_gauth'];
            // var_dump($data);
            $data['status'] = $_REQUEST['attend'];
            $contact_id = $this->handleSubmit($data);

            return json_encode(['success' => !empty($contact_id)]);
        }
        return parent::html();
    }
}
