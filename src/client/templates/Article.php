<?php

namespace client\templates;

class Article extends  DqcBase
{
	
	public function get_h1(){
		
			$t = new \mellow\Textarea(['default_value' => '[Otsikko]', 'view' => 'h1', 'clickable' => false, 'inplace' => false]);
	
		return $t;
	}
	public function get_ingressi(){
		
			$t = new \mellow\Textarea(['default_value' => '[Ingressi]', 'view' => 'ingressi', 'clickable' => false, 'inplace' => false]);
	
		return $t;
	}
	public function getListImageSrc(){
		$img=$this->__get("img");
		$def=$img->values->isDefault;
		if(!$def){
			
			return $img->values->list->src;
		}
		return '//placehold.it/528x297';
	}
	
	
	public function get_img(){
		$kuva = new \mellow\Image(['default_value' => '//placehold.it/1600x900']);
		$kuva->sizes->default = new \mellow\ImageSize(1600,900 ,'crop', '//placehold.it/1600x900');
		
		$kuva->sizes->list = new \mellow\ImageSize(528,297 ,'crop', '//placehold.it/528x297');
		
		$kuva->sizes->fb = new \mellow\ImageSize(1200,600 ,'crop', '//placehold.it/1200x600');

		return $kuva;
	
	}
	public function get_text(){
		return new \mellow\RichText(['default_value' => '[Teksti]', 'view' => 'div']);
	}
}
