<?php

namespace client\templates;

class Hidden extends ExampleCommonTemplate
{
	public function get_hero()
	{
		return new \client\modules\groups\Hero();
	}

	public function showOnNavigation()
	{
		return false;
	}

	public function get_content()
	{
		return new \client\modules\contentselectors\ExampleContent();
	}
}
