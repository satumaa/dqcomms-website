<?php

namespace client\templates;

class Country extends  DqcBase
{
	
	
	public function get_de(){
		
		return new \client\modules\groups\CountryContacts(array("view"=>"de"));
			
			
	}
	public function get_se(){
		
		return new \client\modules\groups\CountryContacts(array("view"=>"se"));
			
			
	}
	public function get_fi(){
		
		return new \client\modules\groups\CountryContacts(array("view"=>"fi"));
			
			
	}
}
