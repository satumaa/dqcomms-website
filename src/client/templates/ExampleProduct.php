<?php

namespace client\templates;

abstract class ExampleProduct extends ExampleCommonTemplate
{
	public function get_squareimage()
	{
		// for nav
		$kuva = new \mellow\Image(['default_value' => '//placehold.it/120x120']);
		$kuva->sizes->default = new \mellow\ImageSize(120, 120, 'crop', '//placehold.it/120x120');

		return $kuva;
	}

	public function get_bestseller()
	{
		$naistapuhutaan = new \mellow\Boolean(['label' => 'Is bestseller?', 'true' => 'Yes', 'false' => 'No', 'default_value' => false, 'response_edit' => self::RESPONSE_RELOAD]);
		$naistapuhutaan->contextBubble('setbooleanvalue', 1);

		return    $naistapuhutaan;
	}

	public function get_related()
	{
		return new \client\modules\groups\Related();
	}

	public function get_newin()
	{
		$naistapuhutaan = new \mellow\Boolean(['label' => 'Is new?', 'true' => 'Yes', 'false' => 'No', 'default_value' => false, 'response_edit' => self::RESPONSE_RELOAD]);
		$naistapuhutaan->contextBubble('setbooleanvalue', 1);

		return    $naistapuhutaan;
	}

	public function get_shortdescription()
	{
		$t = new \mellow\Textarea(['default_value' => '[Short description]', 'view' => 'span', 'clickable' => false, 'inplace' => false]);
		$t->translation_base = 'shorttext';
		$t->bubbleForm('edit', 1);
		$t->bubbleForm('create', 1);

		return $t;
	}

	public function get_longdescription()
	{
		$t = new \mellow\Textarea(['default_value' => '[Long description]', 'view' => 'span-description', 'clickable' => false, 'inplace' => false]);
		$t->translation_base = 'longtext';

		return $t;
	}
}
