<?php

namespace client\templates;

class SimpleCategory extends ExampleCommonTemplate
{
	public function __construct($settings = null)
	{
		parent::__construct($settings);

		$this->translation_base = 'subcategory';
	}

	public function get_hero()
	{
		return new \client\modules\groups\Hero();
	}

	public function get_shortdescription()
	{
		$t = new \mellow\Textarea(['default_value' => '[Short description]', 'view' => 'span', 'clickable' => false, 'inplace' => false]);
		$t->translation_base = 'shorttext';
		$t->bubbleForm('edit', 1);
		$t->bubbleForm('create', 1);

		return $t;
	}

	public function get_squareimage()
	{
		// sub cate
		$kuva = new \mellow\Image(['default_value' => '//placehold.it/120x120']);
		$kuva->translation_base = 'categoryimage';
		$kuva->sizes->default = new \mellow\ImageSize(120, 120, 'crop', '//placehold.it/120x120');

		return $kuva;
	}

	public function getAddableChildren()
	{
		return ['simpleproduct'];
	}
}
