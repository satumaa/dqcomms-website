<?php

namespace client\templates;

class SmallBanner extends ExampleCommonTemplate
{
	public function __construct($settings = null)
	{
		parent::__construct($settings);

		$this->translation_base = 'smallbanner';
	}

	public function getAddableChildren()
	{
		return [];
	}

	public function get_banner()
	{
		return new \client\modules\groups\SmallBanner();
	}

	public function html($m = null)
	{
		header('Location: '.$this->getCurrentTemplate()->getParent()->values->url);
		exit();
	}

	public function getListView()
	{
		return $this->__get('banner');
	}
}
