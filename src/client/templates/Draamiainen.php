<?php

namespace client\templates;

class Draamiainen extends ExampleCommonTemplate
{
	public function __construct($settings = [])
	{
		parent::__construct($settings);
	}

	public function showOnNavigation(){
		return false;
	}

	public function html(){
		if(isset($_POST['submit'])){
			$PDO=\mellow\App::$PDO;
			$input=[
				$_POST['first_name'],
				$_POST['last_name'],
				$_POST['city'],
				$_POST['diet'],
				
			];
			$query="INSERT INTO form(first_name,last_name,city,diet,`date`) VALUES(?,?,?,?,NOW())";
			$prepare=$PDO->prepare($query);
			if($prepare->execute($input)){
				$_SESSION['success']='Tervetuloa!';
				return parent::html();
			}
			
		}else{
			unset($_SESSION['success']);
			return parent::html();
		}

		
	}

	public function getFormData(){
		$sql="SELECT *FROM form";

		$prepare=\mellow\App::$PDO->prepare($sql);
		
		if($response=$prepare->execute()){
			while($data=$prepare->fetchObject()){
				
				print_r($data->first_name);
			}
		}

	}
}
