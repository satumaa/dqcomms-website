<?= $M->head; ?>
<?= $M->header; ?>

<main>
	<div class="c-content m-tight">
		<div class="inner">
			<?= $M->share; ?>
			<article class="c-article g-article">
				<div class="inner">
					<div class="image">
						<?=$M->img;?>
					</div>
					<p class="tag"><?=$M->getParent()->values->name;?></p>
					<div class="text">
						<?=$M->h1;?>
						<?=$M->ingressi;?>
						<?=$M->text;?>
						
					</div>
				</div>
			</article>
		</div>
	</div>
</main>

<?= $M->footer; ?>