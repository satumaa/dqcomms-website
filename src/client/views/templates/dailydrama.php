<?= $M->head; ?>
<?= $M->header; ?>

<main>
	<?= $M->share; ?>
	<div class="c-content m-center">
		<div class="inner">
			<?=$M->h1;?>
			
			<div class="c-drama row">
				
				<?
					$lapset=$M->getChildren();
				
					for($i=0;$i<count($lapset);$i++){
						
						echo '<article>
					<a href="'.$lapset[$i]->values->url.'" class="'.$lapset[$i]->values->classes.'">
						<div class="image">
							<img src="'.$lapset[$i]->getListImageSrc().'" alt="">
						</div>
						<h2>'.$lapset[$i]->values->name.'</h2>
					</a>
				</article>';
						
						
					}
				?>
				
			</div>
		</div>
	</div>
</main>

<?= $M->footer; ?>