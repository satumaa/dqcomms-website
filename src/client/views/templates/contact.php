<?= $M->head; ?>
<?= $M->header; ?>

<main>
	<div class="c-content m-center">
		<div class="inner">
			<?= $M->share; ?>
			<h1>We are located at Copenhagen, Helsinki, Stockholm and Turku.</h1>
			<div class="c-offices">
				<a href="/Contact/finland" class="item m-fi">
					<div class="image"><img src="resources/images/offices/listing_finland.jpg" alt=""></div>
					<div class="inner">
						<h2>Finland</h2>
						<p>Offices & Contacts ›</p>
					</div>
				</a>
				<a href="/Contact/sweden" class="item m-se">
					<div class="image"><img src="resources/images/offices/listing_sweden.jpg" alt=""></div>
					<div class="inner">
						<h2>Sweden</h2>
						<p>Office & Contacts ›</p>
					</div>
				</a>
				<a href="/Contact/denmark" class="item m-dk">
					<div class="image"><img src="resources/images/offices/listing_denmark.jpg" alt=""></div>
					<div class="inner">
						<h2>Denmark</h2>
						<p>Office & Contacts ›</p>
					</div>
				</a>
			</div>
			<!--<p class="c-tagline">Let's create some drama!</p>-->
		</div>
	</div>
</main>

<?= $M->footer; ?>