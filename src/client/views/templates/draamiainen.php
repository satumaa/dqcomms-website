<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, viewport-fit=cover">
		<title>Drama Queen's Breakfast Club</title>
		<meta name="description" content="Dqcomm's Monday breakfast">
		<link href="resources/draamiainen/css/style.css" rel="stylesheet">
		<link rel="apple-touch-icon" sizes="180x180" href="/resources/images/favicon/apple-touch-icon.png">
		<link rel="icon" type="image/png" sizes="32x32" href="/resources/images/favicon/favicon-32x32.png">
		<link rel="icon" type="image/png" sizes="16x16" href="/resources/images/favicon/favicon-16x16.png">
		<link rel="manifest" href="/resources/images/favicon/site.webmanifest">
		<link rel="mask-icon" href="/resources/images/favicon/safari-pinned-tab.svg" color="#5bbad5">
		<meta name="msapplication-TileColor" content="#ffffff">
		<meta name="theme-color" content="#ffffff">
	</head>

	<body>
		<?php if(isset($_SESSION['success']) && !empty($_SESSION['success'])):?>
			<div class="success">
				<?=$_SESSION['success']?>
			</div>
			<?php unset($_SESSION['success'])?>
		<?php endif?>

		<img class="logo" src="resources/draamiainen/images/dq_monogram.svg">
		<div class="cheerios">
			<img class="cheerio cheerio1 " src="resources/draamiainen/images/dramaqueen_breakfast.png">
			<img class="cheerio cheerio2 " src="resources/draamiainen/images/dramaqueen_breakfast.png">
			<img class="cheerio cheerio3 " src="resources/draamiainen/images/dramaqueen_breakfast.png">
			<img class="cheerio cheerio4 " src="resources/draamiainen/images/dramaqueen_breakfast.png">
			<img class="cheerio cheerio5 " src="resources/draamiainen/images/dramaqueen_breakfast.png">
			<img class="cheerio cheerio6 " src="resources/draamiainen/images/dramaqueen_breakfast.png">
			<img class="cheerio cheerio7 " src="resources/draamiainen/images/dramaqueen_breakfast.png">
			<img class="cheerio cheerio8 " src="resources/draamiainen/images/dramaqueen_breakfast.png">
			<img class="cheerio cheerio9 " src="resources/draamiainen/images/dramaqueen_breakfast.png">
			<img class="cheerio cheerio10" src="resources/draamiainen/images/dramaqueen_breakfast.png">
			<img class="cheerio cheerio11" src="resources/draamiainen/images/dramaqueen_breakfast.png">
			<img class="cheerio cheerio12" src="resources/draamiainen/images/dramaqueen_breakfast.png">
			<img class="cheerio cheerio13" src="resources/draamiainen/images/dramaqueen_breakfast.png">
			<img class="cheerio cheerio14" src="resources/draamiainen/images/dramaqueen_breakfast.png">
			<img class="cheerio cheerio15" src="resources/draamiainen/images/dramaqueen_breakfast.png">		
		</div> 
		<div class="ptextcontent">
			<section class="c-ingress">
				<h1>Drama<br>Queen's<br>Breakfast<br>Club</h1>
				<p>"Se päivä viikosta, kun saa ilmaisen koulutuksen eikä tarvitse itse tehdä aamupalaa."</p>
			</section>
			<section class="c-schedule">
				<!--<p class="time">Täytä ilmoittautumiskaavake 18.4. mennessä, kiitos.</p>-->
				<p class="time">Vielä ehdit mukaan!</p>
			</section>
			<section class="c-form">
				<p>Valitse kaupunkisi ja lisää tietosi alle:</p>
				<form method="POST">
					<div class="field radio-group" role="radiogroup" aria-describedby="form-field-city">
						<div class="checkbox-container">
							<div>
								<label class="c-checkbox">
									<input type="radio" name="city" value="Turku" required="">
									<span class="text">
										<span class="title">Turku</span><br>
										<span class="details">Tiistai 14.5.2019 klo 09.00–11.00<br>Radisson Blu Marina Palace / Aava Linnankatu 32, 20100, Turku</span>
									</span>
								</label>
							</div>
							<div>
								<label class="c-checkbox">
									<input type="radio" name="city" value="Helsinki" required="">
									<span class="text">
										<span class="title">Helsinki</span><br>
										<span class="details">Keskiviikko 15.5.2019 klo 09.00–11.00<br>Tony’s deli & Street Bar, Bulevardi 7, 00120, Helsinki</span>
									</span>
								</label>
							</div>
						</div>
						<p class="error-text" id="form-field-city">Valitse kaupunkisi.</p>
					</div>
					<input type="text" id="fname" name="first_name" required placeholder="Etunimi">
					<input type="text" id="lname" name="last_name" required placeholder="Sukunimi">
					<input type="text" id="diet" name="diet" required placeholder="Erityisruokavalio">
					<button class="button" name="submit">Lähetä</button>
					<div class="c-ingress">
						<p>Nähdään hitaan, mutta tehokkaan aamun merkeissä!</p>
					</div>
				</form>
			</section>
		</div>
		<!-- build:js resources/js/breakfast.js async -->
		<script src="resources/js/babel-helpers.js"></script>
		<script src="resources/js/polyfills.js"></script>
		<script src="resources/js/vendor/isMobile.min.js"></script>
		<script src="resources/js/vendor/delfin.js"></script>
		<script src="resources/js/vendor/cookieconsent.js"></script>
		<script src="resources/js/Core.js"></script>
		<script src="resources/js/Cheerios.js"></script>
		<!-- endbuild -->

		<!--<script>-->
			<!--new Cheerios();-->
		<!--</script>-->
	</body>
</html>