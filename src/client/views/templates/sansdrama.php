<?php header("Content-Security-Policy: default-src *; style-src * 'unsafe-inline'; script-src * 'unsafe-inline' 'unsafe-eval';"); ?>
<!DOCTYPE html>
<html lang="en" class="mobile">
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1, maximum-scale=1">
		<title>Sans Drama</title>
		<meta name="description" content="Wednesday, 29th May 15:00 ->">
		<meta http-equiv="Content-Security-Policy" content="default-src *; style-src * 'unsafe-inline'; script-src * 'unsafe-inline' 'unsafe-eval';">

		<link href="resources/sansdrama/style.css" rel="stylesheet">
		<link rel="apple-touch-icon" sizes="180x180" href="/resources/images/favicon/apple-touch-icon.png">
		<link rel="icon" type="image/png" sizes="32x32" href="/resources/images/favicon/favicon-32x32.png">
		<link rel="icon" type="image/png" sizes="16x16" href="/resources/images/favicon/favicon-16x16.png">
		<link rel="manifest" href="/resources/images/favicon/site.webmanifest">
		<link rel="mask-icon" href="/resources/images/favicon/safari-pinned-tab.svg" color="#5bbad5">
		<meta name="msapplication-TileColor" content="#cbe8f3">
		<meta name="theme-color" content="#cbe8f3">
	</head>

	<body class="loading">
		
		<div class="c-arches">
			<video src="resources/sansdrama/loop5.mp4" loop autoplay muted playsinline />
			<!-- <video src="resources/sansdrama/loop2.mp4" loop autoplay muted /> -->
		</div>
		<div class="cheerios">
			<div class=" bottom">
				<img class="mehmet cheerio cheerio1 " src="/resources/sansdrama/mehmet.png" >
			</div>
		</div>	
	
		<img class="logo" src="resources/draamiainen/images/dq_monogram.svg">
		
		<div class="sansdrama-main">
			<div class="sansdrama-logocontainer" data-overlay-button="test-overlay" aria-haspopup="true">
				<!-- <div class="sansdrama-logos">
					<div class="rotater">
						<img class="sansdrama-logo" src="resources/sansdrama/sansdrama2b.svg">
						<img class="otherside" src="resources/sansdrama/sansdrama2-alt.svg">			
					</div>
				</div> -->
				<div class="sansdrama-logos rotater">
					<img class="sansdrama-logo" src="resources/sansdrama/sansdrama2b.svg">
					<img class="otherside" src="resources/sansdrama/sansdrama2-alt.svg">			
				</div>
				<h2 class="info">Wed. &nbsp;29.5 &nbsp;15:00 →</h2>
				<img class="logos" src="/resources/sansdrama/logot2.png">			
			</div>
		</div>
		<div class="info-link">info</div>
	

		<div class="c-overlay m-active" id="test-overlay" data-overlay aria-hidden="true" role="dialog" tabindex="-1">			
			<div class="background">
				<div class="inner">
					<img src="/resources/sansdrama/hand3.png"  class="hand"/>
					<img src="/resources/sansdrama/sansdrama-ps.svg" class="logo-mini"/>

					<h2>The terrace doors are opening for the summer.
					It’s time to have a relapsing evening and party…
					</h2>
					<p>Join us on <br/><strong style="font-weight:normal;">Wednesday, 29th May.</strong><br/>
					<span class="small">(Thursday is an off day)</span></p>


					<div class="attend-section">
						<div class="login button" id="googleSignInButton">Sign up!</div>
						<div class="form">
							<button class="button submit" name="submit" type="button">Attend!</button>
							<br/>
							<br/>							
						</div>
						<h2 class="success hidden">See you there</h2>
						<br/>
						<br/>
						<a href="#" id="googleSignOutButton" class="signout hidden ">Not the right account?</a>
					</div>			

					<p>Cocktails from 15:00 →<br/>
					Food from 17:00 →<br/>
					Followed by more cocktails in Bassi at 22:00 →<br/>
					</p>

					<p><strong style="color:#f9cbcb;">RSVP before the 15th may!</strong></p>
		
				</div>
			</div>
			<div class="close"></div>
		</div>


		<script>				
			var state = {
				loggedIn: <?= !empty($M->googleauth->isLoggedIn()) ? 'true' : 'false' ?>,
				hasEntry: <?= !empty($M->getEntry()) ? 'true' : 'false' ?>,
				profile: <?= !empty($M->googleauth->isLoggedIn()) ? json_encode($_SESSION['_gauth']) : '{}' ?>,
				x: 0.5,
				y: 0.5
			};
		</script>

		<!-- build:js resources/js/sansdrama-bundle.js -->
		<script src="resources/js/babel-helpers.js"></script>
		<script src="resources/js/polyfills.js"></script>
		<script src="resources/js/vendor/isMobile.min.js"></script>
		<script src="resources/js/vendor/delfin.js"></script>
		<script src="resources/js/vendor/cookieconsent.js"></script>
		<script src="resources/js/Core.js"></script>
		<script src="resources/js/SansDrama.js"></script>
		<!-- endbuild -->

		<script>		
			function initGS(cb) {
				gapi.load('client:auth2:signin2', function() {
					var auth2 = gapi.auth2.init({
						client_id:'<?= $M->googleauth->settings['client_id'] ?>',
						hosted_domain: 'dqcomms.com'
					}).then(function() {
						if (cb) cb();
						var auth2 = gapi.auth2.getAuthInstance();
						auth2.attachClickHandler(
							document.getElementById('googleSignInButton'), 
							{}, 
							function(u) {
								var profile = u.getBasicProfile();
								var id_token = u.getAuthResponse().id_token;

								// console.log('login', profile);
								
								if (state.loggedIn == false) {
									var xhr = new XMLHttpRequest();
									xhr.open('POST', '/api/gauth');
									xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
									xhr.onload = function() {
										state.profile.given_name = profile.ofa;
										state.profile.family_name = profile.wea;
										state.profile.email = profile.U3;
										state.loggedIn = true;
										// updateAttendSection();

										setTimeout(function() {
											attend(profile);
										}, 100)
									};
									xhr.send('id_token=' + id_token);
								}
							}, 
							function(e) { 
								console.log('something happened',e);
							}
						);								
					});
				});
			}

			var signOut = function() {
				var auth2 = gapi.auth2.getAuthInstance();
				auth2.signOut();
				var xhr = new XMLHttpRequest();
                xhr.open('POST', '/api/gauth/logout');
                xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
                xhr.onload = function() {
					state.loggedIn = false;
					state.profile = {};
					updateAttendSection();
                };
                xhr.send();
			}

			function _gs_signOut(e) {
				e.preventDefault();
				if (gapi.auth2) {
					initGS(signOut);
				} else {
					signOut();	
				}
			}
		</script>

		<script src="https://apis.google.com/js/platform.js?onload=initGS"></script>
	</body>
</html>