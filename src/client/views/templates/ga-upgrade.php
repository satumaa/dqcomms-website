
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, viewport-fit=cover">
		<title> TITLE </title>
		<meta name="description" content="Dqcomm's Monday breakfast">
		<link href="resources/css/ga-upgrade.css" rel="stylesheet">
		<!-- remove? <link href="resources/css/carousel.css" rel="stylesheet">-->
		<?php if (\mellow\App::getUser()->getLevel() >= 1): ?>
			<link href="/mellow/resources/css/mellow.css" rel="stylesheet">
		<?php endif; ?>
		<link rel="apple-touch-icon" sizes="180x180" href="/resources/images/favicon/apple-touch-icon.png">
		<link rel="icon" type="image/png" sizes="32x32" href="/resources/images/favicon/favicon-32x32.png">
		<link rel="icon" type="image/png" sizes="16x16" href="/resources/images/favicon/favicon-16x16.png">
		<link rel="manifest" href="/resources/images/favicon/site.webmanifest">
		<link rel="mask-icon" href="/resources/images/favicon/safari-pinned-tab.svg" color="#5bbad5">
		<meta name="msapplication-TileColor" content="#ffffff">
		<meta name="theme-color" content="#ffffff">
		<script>var app={queue:[],ready:function(a){this.isReady?a():this.queue.push(a)}}
		</script>
		<?php if (LIVE): ?>
			<script async src="https://www.googletagmanager.com/gtag/js?id=UA-60387522-1"></script>
			<script>
				window.dataLayer = window.dataLayer || [];
				function gtag(){dataLayer.push(arguments);}
				gtag('js', new Date());

				gtag('config', 'UA-60387522-1');
			</script>
		<?php else : ?>
			<script> function ga() {console.log(arguments); } </script>
		<?php endif; ?>
	</head>
	<style>
		h1{
			text-align: center;
			max-width: 20em;
			margin: 0 auto;
		}
		.m-description {
			max-width: 48em !important;
			margin: 0 auto;
			margin-top: 3em;
		}
		.m-description ul {
			list-style-type: none;
		}
		.m-description ul > li {
			text-indent: -3px;
		}
		.m-description ul > li:before {
			content: "-";
			text-indent: -3px;
			margin-right: 5px;
		}
		.m-cta-ga p {
			max-width: 24em;
			margin: 0 auto;
			font-size: 2em;
			font-weight: 400;
		}
		.c-form {
			max-width: 50em !important;
    		margin: 0 auto !important;
		}
		 .container .styleddiv {
			max-width: 8em !important;
		}

		.container-div img{
			max-width: 44em;
			margin: 0em 9em auto;
			margin-bottom: 2em;
		}
	</style>
	<body class="loading">
		<?=$M->header;?>
		<section class="c-schedule">
			<div class="container-div container">
				<?= $M->bg; ?>
			</div>
			<?= $M->title; ?>
			<div class="m-description-ga">
				<?= $M->description; ?>
			</div>
			<div class="m-cta-ga container">
				<?= $M->cta; ?>
			</div>			
		</section>
		<section class="c-form container">
			
			<form class="c-form" method="post" data-form data-action="?" novalidate>
			<p data-fetch-error hidden role="alert">Ilmoittautumisesi epäonnistui, odota hetki ja yritä uudestaan.</p>
			<div class="container styleddiv" data-success hidden role="alert">
				<p style="text-transform: uppercase; text-align: center; font-size: 30px;">Kiitos!</p>
				<img class="afterimage" style="" width="150" height="150" src="/mellow_internal/images/en/generated/58_131_1600_900_crop.svg" alt="">
			</div>
			
			<div class="form-content-ga" data-form-content>
			<label  class="field">
				<input type="text" id="fname" name="first_name" required placeholder="Etunimi*" required>
				<span class="error-text">Ole hyvä ja kirjoita nimesi</span>
			</label>
			<label  class="field">
				<input type="text" id="lname" name="last_name" required placeholder="Sukunimi*" required>
				<span class="error-text">Ole hyvä ja kirjoita nimesi</span>
			</label>
			<label  class="field">
				<input type="text" id="email" name="email" required placeholder="Sähköposti" required>
				<span class="error-text">Ole hyvä ja kirjoita sähköpostiosoitteesi</span>
			</label>
			<label  class="field">
				<input type="text" id="domain" name="domain" placeholder="Verkkotunnus" required>
				<span class="error-text">Ole hyvä ja kirjoita puhelin</span>
			</label>
			<label  class="field">
				<textarea placeholder="Viesti" id="" name="message"></textarea>
				<span class="error-text">Ole hyvä ja kirjoita Viesti</span>
			</label>
				<div class="form-button">
					<button class="c-button" type="submit" data-submit>
						Lähetä
						<div class="c-loader m-reverse">
							<div class="inner"></div>
						</div>
					</button>
				</div>
			</form>
		</section>

		<footer class="c-footer container">
			<img class="logo" src="resources/images/logo2.svg" alt="">
			<div class="social">
				<a href="https://www.facebook.com/DramaQueenComms/" class="facebook" target="_blank" rel="noopener">Facebook</a>
				<a href="https://twitter.com/DramaQueenComms" class="twitter" target="_blank" rel="noopener">Twitter</a>
				<a href="https://www.instagram.com/dramaqueencomms/" class="instagram" target="_blank" rel="noopener">Instagram</a>
				<a href="" style="display:none;" class="linkedin" target="_blank" rel="noopener">Linkedin</a>
				<a href="https://vimeo.com/dramaqueencomms" class="vimeo" target="_blank" rel="noopener">Vimeo</a>
			</div>
			<div class="copyright">
			<p>Copyright &copy; <?=date("Y", time());?> Drama Queen Communications Oy / Ab. All Rights Reserved.</p>
			</div>
		</footer>

		<script>
			app.ready(function() {
				cookieConsent({
					text: "This website uses cookies. By continuing to browse it, you agree to their use. ",
					linkText: "Read more",
					linkUrl: "/privacy-policy",
					buttonText: "OK"
				});
			});
		</script>

		<!-- build:js resources/js/ga-upgrade.js async -->
        <script src="resources/js/babel-helpers.js"></script>
        <script src="resources/js/polyfills.js"></script>
        <script src="resources/js/vendor/isMobile.min.js"></script>
        <script src="resources/js/vendor/delfin.js"></script>
        <script src="resources/js/vendor/cookieconsent.js"></script>
        <script src="resources/js/Core.js"></script>
        <script src="resources/js/utils/Utils.js"></script>
        <script src="resources/js/utils/Validator.js"></script>
        <script src="resources/js/utils/Form.js"></script>
        <!-- endbuild -->
		<?php /*
		remove?
		<script src="resources/js/utils/Overlay.js"></script>
        <script src="resources/js/utils/Scroller.js"></script>
        <script src="resources/js/utils/VideoEmbed.js"></script>
        <script src="resources/js/ImageLoader.js"></script>
        <script src="resources/js/Shape.js"></script>
        <script src="resources/js/Screen.js"></script>
        <script src="resources/js/Pl.js"></script>
        <script src="resources/js/Tukes.js"></script>
        <script src="resources/js/DQCclients.js"></script>
        <script src="resources/js/Contacts.js"></script>
        <script src="resources/js/DQC.js"></script>
        <script src="resources/js/App.js"></script>
        <script src="resources/js/start.js"></script> */ ?>

		<script>
			Form.setup();
		</script>


		<?= $M->footer->mellowfooter; ?>
	</body>
</html>