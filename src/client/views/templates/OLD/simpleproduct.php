<?= $M->head; ?>
<?= $M->header; ?>

<?php
$lapset = $M->getChildren();

?>
<main class="Site-content">
		<div class="container">
			<div class="Breadcrumb">
				<a href="/"><span><?=$M->translate('home'); ?></span></a>

				<?php
					$path = ($M->getCurrentTemplate()->getPath());
					 for ($i = 0; $i < count($path); ++$i) {
					 	if ($i + 1 < count($path)) {
					 		echo '<a href="'.$path[$i]->values->url.'"><span class="'.$path[$i]->values->classes.'" data-uri="'.$path[$i]->uri.'">'.$path[$i]->values->name.'</span></a>';
					 	} else {
					 		echo '<span class="'.$path[$i]->values->classes.'" data-uri="'.$path[$i]->uri.'">'.$path[$i]->values->name.'</span>';
					 	}
					 }

				?>


			</div>
			<section itemscope itemtype="http://schema.org/Product">
				<div class="Product-detail" id="product-detail">


					<?php


							echo "<div class='color-images active'>";
							echo $M->images;

							echo '</div>';

					?>


					<div class="text">
						<h1 data-uri="<?=$M->uri; ?>" class="<?=$M->values->classes; ?>"><?=$M->values->name; ?></h1>

						<p class="subtitle"><?=$M->shortdescription; ?></p>
						<p><?=$M->longdescription; ?></p>




					</div>
				</div>


				<div class="Product-features">

					<?=$M->features; ?>
					<?=$M->inspiration; ?>

				</div>
			</section>
		</div>
		<?=$M->related; ?>

	<?=$M->content; ?>

	</main>

<?= $M->footer; ?>

<script>
	app.ready(function() {

		var productDetail = new ProductDetail(d("#product-detail"));
	});
</script>
