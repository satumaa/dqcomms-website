<?= $M->head; ?>
<?= $M->header; ?>

<?php
$lapset = $M->getChildren();

?>
<main class="Site-content">
		<div class="container">
			<div class="Breadcrumb">
				<a href="/"><span><?=$M->translate('home'); ?></span></a>

				<?php
					$path = ($M->getCurrentTemplate()->getPath());
					 for ($i = 0; $i < count($path); ++$i) {
					 	if ($i + 1 < count($path)) {
					 		echo '<a href="'.$path[$i]->values->url.'"><span class="'.$path[$i]->values->classes.'" data-uri="'.$path[$i]->uri.'">'.$path[$i]->values->name.'</span></a>';
					 	} else {
					 		echo '<span class="'.$path[$i]->values->classes.'" data-uri="'.$path[$i]->uri.'">'.$path[$i]->values->name.'</span>';
					 	}
					 }

				?>


			</div>
			<section itemscope itemtype="http://schema.org/Product">
				<div class="Product-detail" id="product-detail">


					<?php

						$colorName = '';
						for ($i = 0; $i < count($lapset); ++$i) {
							$class = '';
							if (0 == $i) {
								$class = 'active';

								$colorName = $lapset[$i]->values->name;
							}
							echo "<div class='color-images ".$class."' data-hash='".$lapset[$i]->values->urlname."' >";

							if (0 == $i) {
								echo $lapset[$i]->images;
							} else {
								echo $lapset[$i]->getImagesAsHidden();
							}

							echo '</div>';
						}

					?>


					<div class="text">
						<h1 data-uri="<?=$M->uri; ?>" class="<?=$M->values->classes; ?>"><?=$M->values->name; ?></h1>

						<p class="subtitle"><?=$M->shortdescription; ?></p>
						<p><?=$M->longdescription; ?></p>


						<?php

						$names = '';
						$colorcicle = '';

						for ($i = 0; $i < count($lapset); ++$i) {
							$active = false;
							if (0 == $i) {
								$active = true;
							}

							$colorcicle .= $lapset[$i]->getColorCircle($active);
						}
						?>
						<div class="Colors">
							<h4><?=$M->translate('colors'); ?></h4>
							<div class="palette">
								<?=$colorcicle; ?>
							</div>
							<div class="output">
								<span></span>
								<p><?=$colorName; ?></p>
							</div>
						</div>
					</div>
				</div>


				<div class="Product-features">

					<?=$M->features; ?>
					<?=$M->inspiration; ?>

				</div>
			</section>
		</div>
	<?=$M->related; ?>

		<?=$M->content; ?>
		<!--<section class="Image-text-block background article" style="margin-bottom: 0;">
			<div class="container">
				<div class="image">
					<img src="resources/images/temp/article-image.jpg" alt="">
				</div>
				<div class="text">
					<h2>Headline</h2>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Temporibus, consequatur, beatae, aut nobis quaerat eum numquam repellendus dolore ducimus quia laborum officiis. Error vero doloremque accusamus quas dolor aut quaerat.</p>
					<div class="addons">
						<div>
							<img class="something" id="something1" src="../resources/images/something.png" alt="something">
							<h4>Description 1</h4>
						</div>
						<div>
							<img class="something" id="something2" src="../resources/images/something.png" alt="something">
							<h4>Description 2</h4>
						</div>
						<div>
							<img class="something" id="something3" src="../resources/images/something.png" alt="something">
							<h4>Description 3</h4>
						</div>
					</div>
				</div>
			</div>
		</section>-->
	</main>

<?= $M->footer; ?>

<script>
	app.ready(function() {
		var productDetails = [];

		$$(".color-images").forEach(function(i) {
			productDetails.push(new ProductDetail(i));
		});

		// Selector
		var hash = window.location.hash.split("#")[1];
		var colors = d(".Colors", this.el)
		var selector = new ColorSelector(colors, this.el, productDetails)

		var q = ".palette span[data-hash='" + hash + "']";

		if(!hash) {
			q = ".palette span";
		}

		selector.setColor(colors.querySelector(q));

	});
</script>
