<?= $M->head; ?>
<?= $M->header; ?>
<main class="Site-content">

		<?=$M->hero; ?>
		<div class="container">
			<div class="Product-category row">
				<div class="nav">
					<div class="inner">

						<?php
							$path = $M->getCurrentTemplate()->getPath();

							$products = $M->getChildren();

							$subs = $path[0]->getChildren();

						?>
						<p data-uri="<?=$path[0]->uri; ?>"><?=$path[0]->values->name; ?></p>
						<nav>
							<ul class="reset">
								<?php


									for ($i = 0; $i < count($subs); ++$i) {
										$act = '';

										if ($subs[$i]->values->ID == $M->values->ID) {
											$act = 'active';
										}

										echo '<li  class="'.$act.'"><a class="'.$subs[$i]->values->classes.'" data-uri="'.$subs[$i]->uri.'" href="'.$subs[$i]->values->url.'">'.$subs[$i]->values->name.'</a></li>';
									}
								?>

							</ul>
						</nav>
					</div>
				</div>
				<div class="Product-list columns-3">
				<p class="count"><?=$M::countProductsOnly($products); ?> <?=$M->translate('products'); ?></p>
				<ul>

					<?php

						for ($i = 0; $i < count($products); ++$i) {
							echo $products[$i]->getListView();
						}
					?>
					<?php /*
					<li class="banner-width-1" style="background-image: url(resources/images/temp/product-banner.jpg)">
						<a href="">
							<h3>small banner</h3>
							<p>Lorem ipsum dolor sit amet</p>
							<p><button class="button">Optional cta</button></p>
						</a>
					</li>
					<li>
						<a href="">
							<div class="image">
								<button class="cta"><span>Discover</span></button>
								<img src="resources/images/temp/product-image.png" alt="">
							</div>
							<div class="Colors"><span></span><span></span><span></span></div>
							<h3>Product name</h3>
							<p>Your eyes wake-up call, a brighter, younger look.</p>
						</a>
					</li>
					<li>
						<a href="">
							<div class="image">
								<button class="cta"><span>Discover</span></button>
								<img src="resources/images/temp/product-image.png" alt="">
							</div>
							<div class="Colors"><span></span><span></span><span></span></div>
							<h3>Product name</h3>
							<p>Your eyes wake-up call, a brighter, younger look.</p>
						</a>
					</li>
					<li class="banner-width-2" style="background-image: url(resources/images/temp/product-banner.jpg)">
						<a href="">
							<h3>medium banner</h3>
							<p>Lorem ipsum dolor sit amet</p>
							<p><button class="button">Optional cta</button></p>
						</a>
					</li>
					 */?>
				</ul>
			</div>
			</div>
		</div>
	<?=$M->content; ?>


</main>

<?= $M->footer; ?>
