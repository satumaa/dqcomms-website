<?= $M->head; ?>
<?= $M->header; ?>
<?php


?>
<h1>This is the primary heading and there should only be one of these per page</h1>
<main class="Site-content">
  <br />
  <br />
  <h1>This is a carousel</h1>
    <section class="Top-content">
        <?=$M->carousel; ?>
    </section>

    <section class="Featured-products container">
        <h3>This is a Products Header</h3>
        <div class="tab-view" data-tabview>
            <ul class="tab-nav">
                <li class="active" data-id="1">This is a best seller link</li>
                <li data-id="2">This is a transitioning link</li>
            </ul>
            <div class="tab-content active" data-id="1">
                <?=$M->bestsellers; ?>
            </div>
            <div class="tab-content" data-id="2">
                <?=$M->newin; ?>
            </div>
        </div>
    </section>

    <section class="Instagram">
        <h2>This is an Instagram Feed</h2>
        <h3>This is an underlying header</h3>
        <a class="social-item dummy" target="_blank" style="display:none">
            <div class="social-inner"></div>
        </a>

        <div class="social-feed" data-count="12"></div>
    </section>

	<div class="News-block">
        <h3>This is an Articles Selection</h3>
		<?=$M->articles; ?>
		<div class="container show-all">
			<a href="">This is to show all</a>
		</div>
	</div>

</main>


<main class="Site-content">
		<?=$M->hero; ?>


	</main>

  <?php
  $cats = $M->getChildren();

  $products = [];

								for ($i = 0; $i < count($cats); ++$i) {
									$subcat = $cats[$i]->getChildren();
									for ($x = 0; $x < count($subcat); ++$x) {
										$products = array_merge($products, $subcat[$x]->getChildren());
									}
								}

  ?>
  <main class="Site-content">

  		<?=$M->hero; ?>
  		<div class="container">
  			<div class="Category-selector">
  				<?php


					for ($i = 0; $i < count($cats); ++$i) {
						echo '<a data-uri="'.$cats[$i]->uri.'" class="'.$cats[$i]->values->classes.'" href="'.$cats[$i]->values->url.'">'.$cats[$i]->values->name.'</a>';
					}

				?>
  			</div>
  			<div class="Product-list columns-3">
          <h1>This is a product list</h1>
  				<p class="count"><?=$M::countProductsOnly($products); ?> <?=$M->translate('products'); ?></p>
  				<ul>


  					<?php

						for ($i = 0; $i < count($products); ++$i) {
							echo $products[$i]->getListView();
						}
					?>
  					<?php /*
					<li class="banner-width-1" style="background-image: url(resources/images/temp/product-banner.jpg)">
						<a href="">
							<h3>small banner</h3>
							<p>Lorem ipsum dolor sit amet</p>
							<p><button class="button">Optional cta</button></p>
						</a>
					</li>
					<li>
						<a href="">
							<div class="image">
								<button class="cta"><span>Discover</span></button>
								<img src="resources/images/temp/product-image.png" alt="">
							</div>
							<div class="Colors"><span></span><span></span><span></span></div>
							<h3>Product name</h3>
							<p>Your eyes wake-up call, a brighter, younger look.</p>
						</a>
					</li>
					<li>
						<a href="">
							<div class="image">
								<button class="cta"><span>Discover</span></button>
								<img src="resources/images/temp/product-image.png" alt="">
							</div>
							<div class="Colors"><span></span><span></span><span></span></div>
							<h3>Product name</h3>
							<p>Your eyes wake-up call, a brighter, younger look.</p>
						</a>
					</li>
					<li class="banner-width-2" style="background-image: url(resources/images/temp/product-banner.jpg)">
						<a href="">
							<h3>medium banner</h3>
							<p>Lorem ipsum dolor sit amet</p>
							<p><button class="button">Optional cta</button></p>
						</a>
					</li>
					 */?>
  				</ul>
  			</div>
  		</div>
      <?= $M->content; ?>
  </main>

  <style>
  .section-404{
      text-align:center;
  }
  h1{
      font-size:120px;
     font-weight:800;
     padding:0px;
     margin:0px;

  }
  .head-404{
      font-size:25px;
  }

  .section-404 a{
      font-style:italic;
      color:#FB8A72;
      font-size:16px;
  }
  .info-404{
      font-size:20px;

  }

  </style>

    <div class="inspiration"  data-uri="<?=$M->uri; ?>">

    		<?php


		if (\mellow\App::getUser()->canSeeAdminHtml() || false == $M->image->values->isDefault) {
			if (\mellow\App::getUser()->canSeeAdminHtml() || (false == $M->link->values->isDefault)) {
				$target = '';

				if ($M->link->values->target) {
					$target = ' target="_blank" ';
				} ?>

    						<a  href="<?=$M->link->values->value; ?>" <?=$target; ?> class="<?=$M->link->values->classes; ?>" >
    							<?=$M->image; ?>
    							<?=$M->h4; ?>
    							<?=$M->h3; ?>

    						</a>

    	<?php
			} else {
				?>

    				<?=$M->image; ?>
    				<?=$M->h4; ?>
    				<?=$M->h3; ?>

    			<?php
			}
		}

		?>





<?=$M->footer; ?>

<script>
    app.ready(function(){
        /* Carousel */
        var carousel = new Carousel(d("#my-carousel"), {
            auto: 6000,
            callback: function() {
            }
        });

        /* Use getSwipe to get access to swipe methods */
        var numSlides = carousel.getSwipe().getNumSlides();

    });
</script>
