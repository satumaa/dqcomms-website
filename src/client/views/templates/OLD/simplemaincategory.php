<?= $M->head; ?>
<?= $M->header; ?>

<?php
$cats = $M->getChildren();

$products = [];

								for ($i = 0; $i < count($cats); ++$i) {
									//$subcat=$cats[$i]->getChildren();
									//for($x=0;$x<count($subcat);$x++){
									$products = array_merge($products, $cats[$i]->getChildren());

									//}
								}

?>
<main class="Site-content">

		<?=$M->hero; ?>
		<div class="container">
			<div class="Category-selector">
				<?php


					for ($i = 0; $i < count($cats); ++$i) {
						echo '<a data-uri="'.$cats[$i]->uri.'" class="'.$cats[$i]->values->classes.'" href="'.$cats[$i]->values->url.'">'.$cats[$i]->values->name.'</a>';
					}

				?>
			</div>
			<div class="Product-list columns-3">
				<p class="count"><?=$M::countProductsOnly($products); ?> <?=$M->translate('products'); ?></p>
				<ul>

					<?php

						for ($i = 0; $i < count($products); ++$i) {
							echo $products[$i]->getListView();
						}
					?>
					<?php /*
					<li class="banner-width-1" style="background-image: url(resources/images/temp/product-banner.jpg)">
						<a href="">
							<h3>small banner</h3>
							<p>Lorem ipsum dolor sit amet</p>
							<p><button class="button">Optional cta</button></p>
						</a>
					</li>
					<li>
						<a href="">
							<div class="image">
								<button class="cta"><span>Discover</span></button>
								<img src="resources/images/temp/product-image.png" alt="">
							</div>
							<div class="Colors"><span></span><span></span><span></span></div>
							<h3>Product name</h3>
							<p>Your eyes wake-up call, a brighter, younger look.</p>
						</a>
					</li>
					<li>
						<a href="">
							<div class="image">
								<button class="cta"><span>Discover</span></button>
								<img src="resources/images/temp/product-image.png" alt="">
							</div>
							<div class="Colors"><span></span><span></span><span></span></div>
							<h3>Product name</h3>
							<p>Your eyes wake-up call, a brighter, younger look.</p>
						</a>
					</li>
					<li class="banner-width-2" style="background-image: url(resources/images/temp/product-banner.jpg)">
						<a href="">
							<h3>medium banner</h3>
							<p>Lorem ipsum dolor sit amet</p>
							<p><button class="button">Optional cta</button></p>
						</a>
					</li>
					 */?>
				</ul>
			</div>
		</div>

</main>



<?= $M->footer; ?>
