<?= $M->head; ?>
<?= $M->header; ?>
<main class="Site-content">
		<?=$M->hero; ?>

		<div class="container">
			<div class="Category-selector" id="category-selector">

				<?php
				$tags = \client\templates\Article::getTags();

				for ($i = 0; $i < count($tags); ++$i) {
					echo '<a data-tag="'.$tags[$i]->label.'">'.$tags[$i]->label.'</a>';
				}
				?>
			</div>

			<div class="News" id="news"><ul>
				<?php
				$lapset = $M->getChildren();

				for ($i = 0; $i < count($lapset); ++$i) {
					echo $lapset[$i]->getListView();
				}

				?>



				</ul></div>
		</div>


	</main>

	<script>
		app.ready(function() {
			// Show all news
			$$("#news li").forEach(function(j) {
				j.classList.add("active")
			});

			// Filter news
			$$("#category-selector a").forEach(function(i) {
				i.addEventListener("click", function(e) {
					var tag = i.getAttribute("data-tag");

					// Set active category
					$$("#category-selector a").forEach(function(j) {
						j.classList.remove("active");
					});

					i.classList.add("active");

					// Filter news
					$$("#news li").forEach(function(j) {
						if(j.getAttribute("data-tag") == tag) {
							j.classList.add("active")
						}
						else {
							j.classList.remove("active")
						}
					});
				});
			});
		});
	</script>
<?= $M->footer; ?>
