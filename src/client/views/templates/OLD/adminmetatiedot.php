<?=$M->head; ?>

<div class='mellow-variables'>
<h2 style="margin-bottom:0.5em;">Metatiedot<a href='/'>Sulje</a></h2>
<input type='text' placeholder="Etsi sivua" id="hae" style='float:right;'> 
<div style='clear:both;'></div>
<?php

function loop($sites, $DEPTH = 0)
{
	for ($i = 0; $i < count($sites); ++$i) {
		$p = \mellow\App::getPage($sites[$i]->ID);

		if (isset($p->use_custom_meta) && true === $p->use_custom_meta) {
			$title = $p->head->meta->title->values->value;
			$desc = $p->head->meta->description->values->value;

			$is_title_default = $p->head->meta->title->values->isDefault;
			$is_desc_default = $p->head->meta->description->values->isDefault;

			$title_attr = '';
			if ($is_title_default) {
				$title_attr = " style='opacity:.7;'";
			}

			$desc_attr = '';
			if ($is_desc_default) {
				$desc_attr = " style='opacity:.7;'";
			}

			$px = 10 * $DEPTH;

			echo "<tr  data-name='".$sites[$i]->name."'>
			<td ><div style='padding-left:".$px."px;'><a  class='".$p->values->classes."' target='_blank'  href='".$sites[$i]->url."'>".$sites[$i]->name.'</a></div></td>
			<td '.$title_attr." data-uri='".$p->head->meta->title->uri."'>".$title.'</td>
			<td '.$desc_attr." data-uri='".$p->head->meta->description->uri."'>".$desc.'</td>
			</tr>';

			$childs = $sites[$i]->children;

			if (count($childs) > 0) {
				loop($childs, $DEPTH + 1);
			}
		} else {
			$childs = $sites[$i]->children;

			if (count($childs) > 0) {
				loop($childs, $DEPTH);
			}
		}
	}
}

echo '<div id="mellow-admin-sitemap" style="padding-bottom:4em;">';
	echo "<table>
	<tr><th>Sivun nimi</th><th>Otsikko</th><th style='width:390px;'>Kuvaus</th></tr>
	";
loop($M->getSites(0));
	echo '</table>';
echo '</div>';
?>
</div>
<?=$M->footer; ?>
<style type="text/css">
	table{
		font-size:.8em;
		border-spacing: 3px 10px;
	}
	th{
		text-align:left;
		text-transform: uppercase;
	}
	
</style>
<script type="text/javascript">
	
	var DATAT=new Array();
	$("tr[data-name]").each(function(){
		var value=jQuery.trim($(this).attr("data-name").toLowerCase()).split(" ");
		
		DATAT.push({target:$(this),name:value})
		
	});
	function isHit(searches,names){
		
		for(var i in names){
			var n=names[i];
			for(var x in searches){
				
				var s=searches[x];
				
				if(n.search(s)!==-1){
					return true;
				}
			}
		}
		
		return false;
		
	}
function hae(value){
	
	value=jQuery.trim( value );
	value=value.toLowerCase().split(" ");
	
	
	
	for(var i in DATAT){
		
		if(isHit(value,DATAT[i].name)){
						DATAT[i].target.show();
			
		}else{
			
			DATAT[i].target.hide();
		}
		
	}
	
}
	$("#hae").keyup(function(){
		
		hae($(this).val());
	});
</script>