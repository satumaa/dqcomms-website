<?= $M->head; ?>
<?= $M->header; ?>
<main class="Site-content">

		<?=$M->hero; ?>
		<div class="container">
			<div class="Product-category row">
				<!--<button id="change-category">Change category</button>-->
				<div class="nav">
					<div class="inner">

						<?php
							$path = $M->getCurrentTemplate()->getPath();

							$products = [];

							$subs = $path[1]->getChildren();
							if (2 == count($path)) {
								for ($i = 0; $i < count($subs); ++$i) {
									$products = array_merge($products, $subs[$i]->getChildren());
								}
							} else {
								$products = $path[2]->getChildren();
							}
						?>
						<p data-uri="<?=$path[1]->uri; ?>"><?=$path[1]->values->name; ?></p>
						<nav>
							<ul class="reset">
								<?php


									for ($i = 0; $i < count($subs); ++$i) {
										$act = '';

										if ($subs[$i]->values->ID == $M->values->ID) {
											$act = 'active';
										}

										echo '<li class="'.$act.'"><a class="'.$subs[$i]->values->classes.'" data-uri="'.$subs[$i]->uri.'" href="'.$subs[$i]->values->url.'">'.$subs[$i]->values->name.'</a></li>';
									}
								?>

							</ul>
						</nav>
					</div>
				</div>
				<div class="Product-list columns-3">
				<p class="count"><?=$M::countProductsOnly($products); ?> <?=$M->translate('products'); ?></p>
				<ul>

					<?php

						for ($i = 0; $i < count($products); ++$i) {
							echo $products[$i]->getListView();
						}
					?>

				</ul>
			</div>
			</div>
		</div>

		<?=$M->content; ?>


</main>

<?= $M->footer; ?>
