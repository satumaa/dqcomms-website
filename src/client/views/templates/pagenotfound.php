<?= $M->head; ?>
<?= $M->header; ?>

<style>
.section-404{
    text-align:center;
}
h1{
    font-size:120px;
   font-weight:800;
   padding:0px;
   margin:0px;

}
.head-404{
    font-size:25px;
}

.section-404 a{
    font-style:italic;
    color:#FB8A72;
    font-size:16px;
}
.info-404{
    font-size:20px;
    
}

</style>

<main>
    <section class="section-404">
      <h1>404</h1>
      <p class="head-404"><strong>Page not found</strong></p>
      <p class="info-404">The page you are looking for could not be found.<br><a href="/">home</a></p>
    </section>
</main>



<?= $M->footer; ?>