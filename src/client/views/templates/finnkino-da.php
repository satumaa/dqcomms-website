<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, viewport-fit=cover">
		<title>Finnkino | Downton Abbey</title>
		<meta name="description" content="We invite you to join us(change desc)">
		<?php if (\mellow\App::getUser()->getLevel() >= 1): ?>
			<link href="/mellow/resources/css/mellow.css" rel="stylesheet">
		<?php endif; ?>
		<link rel="apple-touch-icon" sizes="180x180" href="/resources/images/favicon/apple-touch-icon.png">
		<link rel="icon" type="image/png" sizes="32x32" href="/resources/images/favicon/favicon-32x32.png">
		<link rel="icon" type="image/png" sizes="16x16" href="/resources/images/favicon/favicon-16x16.png">
		<link rel="manifest" href="/resources/images/favicon/site.webmanifest">
		<link rel="mask-icon" href="/resources/images/favicon/safari-pinned-tab.svg" color="#5bbad5">
		<meta name="msapplication-TileColor" content="#ffffff">
		<meta name="theme-color" content="#ffffff">
		<script>var app={queue:[],ready:function(a){this.isReady?a():this.queue.push(a)}}
		</script>
		<?php if (LIVE): ?>
			<script async src="https://www.googletagmanager.com/gtag/js?id=UA-60387522-1"></script>
			<script>
				window.dataLayer = window.dataLayer || [];
				function gtag(){dataLayer.push(arguments);}
				gtag('js', new Date());

				gtag('config', 'UA-60387522-1');
			</script>
		<?php else : ?>
			<script> function ga() {console.log(arguments); } </script>
		<?php endif; ?>
		<link href="resources/css/downtownab.css" rel="stylesheet" type="text/css">
		<link href="https://use.typekit.net/dfw5dfz.css" rel="stylesheet">
	</head>
	<body class="loading">
		<!--<?=$M->header;?>-->
		<!--<section class="c-schedule">
			<div class="container-div container">
				<?= $M->bg; ?>
			</div>
			<?= $M->title; ?>
			<div class="m-description-ga">
				<?= $M->description; ?>
			</div>
			<div class="m-cta-ga container">
				<?= $M->cta; ?>
			</div>
		</section>-->
		<section class="c-hero">
			<div class="image">
				<img class="m-desktop" src="../resources/images/downtownabb/img-hero.jpg" alt="hero">
				<img class="m-mobile" src="../resources/images/downtownabb/img-hero_mob.jpg" alt="hero_mob">
			</div>
		</section>
		<div class="container m-large">
			<section>
				<!--<div class="image">
					<img src="../resources/images/downtownabb/txt-ingress.svg" alt="ingressi">
				</div>-->
				<div class="text">
					<p>Mrs. Crawley: <br class="m-mobile">"I take that as a compliment."<br>Countess Violet: <br class="m-mobile">"I must've said it wrong."</p>
				</div>
			</section>
			<div class="c-divider">
				<img src="../resources/images/downtownabb/img-section-divider.svg" alt="divider">
			</div>
			<section>
				<div class="text">
					<h1>Welcome to Downton Abbey.</h1>
					<?php /*<p>Odotetussa Downton Abbey -elokuvassa kartano saa kuninkaallisia vieraita, ja tapahtuman valmistelut laittavat talon väen hermot koetukselle.<br><br>Siniveriset ja draama tuntuivat meistä jotenkin niin omalta, että buukkasimme käyttöömme peräti kaksi näytöstä.<br><br><strong style="text-transform: uppercase;">Tule siis kanssamme leffaan.</strong><br><br>Varaa ihmeessä jo vapaaksi jompikumpi iltapäivä:</p>*/?>
					<p>Ilmoittaudu Drama Queenin kanssa leffaan valitsemalla kaupunkisi ja täyttämällä tietosi alle:</p>
					<div class="target-dates">
						<p>17.9. klo 15–18<br><span><strong>Helsinki</strong>, Kinopalatsi 4</span></p>
						<p>18.9. klo 15–18<br><span><strong>Turku</strong>, Kinopalatsi 7</span></p>
					</div>
				</div>
			</section>
			<div class="c-divider m-lower">
				<img src="../resources/images/downtownabb/img-section-divider.svg" alt="divider">
			</div>
			<!--<button class="c-button m-activate-form">Ilmoittaudu</button>-->
			<section class="c-form container">
				<form class="" method="post" data-form data-action="?" novalidate>
					<p data-fetch-error hidden role="alert">Ilmoittautumisesi epäonnistui, odota hetki ja yritä uudestaan.</p>
					<p data-success hidden role="alert">Kiitos, ilmoittautumisesi on vastaanotettu.</p>
					<div data-form-content>
						<div class="field">
							<div class="form-group">
								<?php /*
									<label for="location">Kaupunki:</label>
								<input type="radio" name="location" value="Turku"
									<?php if(isset($location)) {
										if($location='turku') {
											echo 'selected';
										}
									} ?>
								><span>Turku</span>
								<input type="radio" name="location" value="Helsinki"
									<?php if(isset($location)) {
										if($location='helsinki') {
											echo 'selected';
										}
									} ?>
								><span>Helsinki</span> 
								*/ ?>
								<fieldset class="field" role="radiogroup" aria-describedby="form-field-location">
									<!--<legend>Kaupunki:</legend>-->
									<label class="c-checkbox">
										<input type="radio" name="location" value="Turku" required>
										<span>Turku</span>
									</label>
									<label class="c-checkbox">
										<input type="radio" name="location" value="Helsinki" required>
										<span>Helsinki</span>
									</label>
									<span class="error-text" id="form-field-location">Ole hyvä ja valitse kaupunki</span>
								</fieldset>
							</div>
						</div>
						<input type="hidden" id="email" name="email" value="<?= @$_REQUEST['email'] ?>">
						<label class="field">
							<input type="text" id="fname" name="first_name" value="<?= @$_REQUEST['fname'] ?>" placeholder="Etunimi*" required>
							<span class="error-text">Ole hyvä ja kirjoita etunimesi</span>
						</label>
						<label class="field">
							<input type="text" id="lname" name="last_name" value="<?= @$_REQUEST['lname'] ?>" placeholder="Sukunimi*" required>
							<span class="error-text">Ole hyvä ja kirjoita sukunimesi</span>
						</label>
						<label class="field">
							<!--<input type="text" id="diet" name="diet" placeholder="Ruokavalio*" required>
							<span class="error-text">Ole hyvä ja kirjoita allergiat</span>-->
							<textarea type="text" id="diet" name="diet" placeholder="Ruokavalio"></textarea>
							<!--<span class="error-text">Ole hyvä ja kirjoita ruokavaliosi</span>-->
						</label>
						<div class="form-button">
							<button class="c-button" type="submit" data-submit>
								Lähetä
								<div class="c-loader m-reverse">
									<div class="inner"></div>
								</div>
							</button>
						</div>
					</div>
				</form>
			</section>
		</div>
		<footer class="c-footer">
			<div class="container">
				<p>"I'm so looking forward to seeing your mother again.<br>When I'm with her, I'm reminded of the virtues of the English."<br>"But isn't she American?"<br>"Exactly."<br>– Lady Violet –</p>
				<img src="../resources/images/downtownabb/logo-dqc.svg" alt="logo">
				<!--<div class="social">
					<a href="https://www.facebook.com/DramaQueenComms/" class="facebook" target="_blank" rel="noopener">Facebook</a>
					<a href="https://twitter.com/DramaQueenComms" class="twitter" target="_blank" rel="noopener">Twitter</a>
					<a href="https://www.instagram.com/dramaqueencomms/" class="instagram" target="_blank" rel="noopener">Instagram</a>
					<a href="" style="display:none;" class="linkedin" target="_blank" rel="noopener">Linkedin</a>
					<a href="https://vimeo.com/dramaqueencomms" class="vimeo" target="_blank" rel="noopener">Vimeo</a>
				</div>-->
				<!--<div class="copyright">
					<p>Copyright &copy; <?=date("Y", time());?> Drama Queen Communications Oy / Ab. All Rights Reserved.</p>
				</div>-->
			</div>
		</footer>

		<script>
			app.ready(function() {
				cookieConsent({
					text: "This website uses cookies. By continuing to browse it, you agree to their use. ",
					linkText: "Read more",
					linkUrl: "/privacy-policy",
					buttonText: "OK"
				});
			});
		</script>

		
		<?= $M->footer->mellowfooter; ?>

		<!-- build:js resources/js/finnkino.js -->
		<script src="resources/js/babel-helpers.js"></script>
		<script src="resources/js/polyfills.js"></script>
		<script src="resources/js/vendor/isMobile.min.js"></script>
		<script src="resources/js/vendor/delfin.js"></script>
		<script src="resources/js/vendor/cookieconsent.js"></script>
		<script src="resources/js/Core.js"></script>
		<script src="resources/js/utils/Utils.js"></script>
		<script src="resources/js/utils/Validator.js"></script>
		<script src="resources/js/utils/Form.js"></script>		
		<!-- endbuild -->		

		<script>
			Delfin.ready(() => {
			 	Form.setup();
			});
		</script>
		<?php /*
		remove?
		<script src="resources/js/utils/Overlay.js"></script>
		<script src="resources/js/utils/Scroller.js"></script>
		<script src="resources/js/utils/VideoEmbed.js"></script>
		<script src="resources/js/ImageLoader.js"></script>
		<script src="resources/js/Shape.js"></script>
		<script src="resources/js/Screen.js"></script>
		<script src="resources/js/Pl.js"></script>
		<script src="resources/js/Tukes.js"></script>
		<script src="resources/js/DQCclients.js"></script>
		<script src="resources/js/Contacts.js"></script>
		<script src="resources/js/DQC.js"></script>
		<script src="resources/js/App.js"></script>
		<script src="resources/js/start.js"></script> */ ?>

		


		
	</body>
</html>