<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, viewport-fit=cover">
		<title>Naantalin musiikkijuhlien kutsu</title>
		<meta name="description" content="Dqcomm's Monday breakfast">
		<link href="resources/namu/css/style.css" rel="stylesheet">
		<link rel="apple-touch-icon" sizes="180x180" href="/resources/images/favicon/apple-touch-icon.png">
		<link rel="icon" type="image/png" sizes="32x32" href="/resources/images/favicon/favicon-32x32.png">
		<link rel="icon" type="image/png" sizes="16x16" href="/resources/images/favicon/favicon-16x16.png">
		<link rel="manifest" href="/resources/images/favicon/site.webmanifest">
		<link rel="mask-icon" href="/resources/images/favicon/safari-pinned-tab.svg" color="#5bbad5">
		<meta name="msapplication-TileColor" content="#ffffff">
		<meta name="theme-color" content="#ffffff">
		<style>
			.bg {
				position:fixed;
				top:0;
				left:0;
				width:100%;
				min-height:100vh;
				transition: min-height 1000s steps(1);

				/* background:url('/resources/namu/images/bg.jpg') no-repeat center center; */
				background-size:cover;				
			}

			svg {
				max-width: 100%;
			}

			svg path {
				stroke-dasharray: 720 720;
    			
				/* animation: stroke-reveal 10s ease-out infinite; */
			}

			svg polygon {
				stroke-dasharray: 1380 1380;
    			
			}

			svg #stroke-13 {
				stroke-dasharray: 550 550;
			}

			body.loading svg path {
				stroke-dashoffset: 720;
			}

			body.loading svg polygon {
				stroke-dashoffset: 1380;
			}

			body.loading svg #stroke-13 {
				stroke-dashoffset: 550;
			}

			body.loading svg {
				/* transition:opacity 10s ease; */
				opacity:0;
			}
			svg {
				transition:opacity 10s cubic-bezier(0.25, 0.46, 0.45, 0.94);
				opacity:1;
			}
			svg path, svg polygon, svg #stroke-13 {
				stroke-width: 1.5px;
    			stroke: rgba(255,255,255,0.9);
				transition: stroke-dashoffset 8s cubic-bezier(0.25, 0.46, 0.45, 0.94);
				/* transition-delay:0.5s; */
			}

			svg path, svg polygon, svg #stroke-13 {
				stroke-dashoffset: 0;
			}


			<?php /*
            @keyframes stroke-reveal {
                from {
                    stroke-dashoffset: -720;
                }
                to {
                    stroke-dashoffset: 0;
                }
            } */ ?>
		</style>
	</head>

	<body class="loading">
		<img class="logo" src="resources/namu/images/dq_monogram.svg">		
		<div class="bg">
			<img class="img" src="resources/namu/images/bg.jpg"  style="display:none;">		
			<!-- <canvas width="1752" height="1195" style="touch-action: none; cursor: inherit;"></canvas> -->
		</div>
		<div class="ptextcontent">		
			<section class="" style="position: relative; display:flex;flex-direction: column;justify-content:center;align-items:center;width:100%; min-height:100vh;transition: min-height 1000s steps(1);">			
				<!-- <img src="/resources/namu/images/logo.svg" /> -->
				<?php include(MELLOW_BASE_PATH.'/resources/namu/images/logo2.svg') ?>
				<div style="text-align: center;">
					<button class="c-button" id="signup-button" style="opacity: 0;">Ilmoittaudu tästä</button>
				</div>
			</section>			
			<section class="c-schedule">
				<?php /* $M->description */ ?>
				<p>Tervetuloa torstaina 6.6. viettämään kanssamme kesäistä iltaa hyvän musiikin tahdittamana. </p>
				<p>Ilta alkaa klo 17.30 Pinellassa, jossa nautimme lasin kuplivaa. </p>
				<p>Klo 18.15 lähdemme Pinellasta taksikuljetuksella Naantaliin nauttimaan Naantalin Musiikkijuhlien Muistojen tango -illalliskonsertista. </p>
				<p>Illalliskonsertti Naantalin Kaivohuoneella alkaa klo 19. Musiikista vastaavat Mari Palo, Jaakko Kuusisto, Marzi Nyman, Niko Kumpuvaara sekä Ville Herrala. </p>
				<p>Konsertin jälkeen taksikuljetus Turun keskustaan. </p>
				<p>Avec. Ilmoittauduthan 28.5. Mennessä.</p>
			</section>
			<section class="c-form">
				<!-- <form method="POST"> -->
				<form class="c-form" method="post" data-form data-action="?" novalidate>
				<p data-fetch-error hidden role="alert">Ilmoittautumisesi epäonnistui, odota hetki ja yritä uudestaan.</p>
				<p data-success hidden role="alert">Kiitos, ilmoittautumisesi on vastaanotettu.</p>
				<div data-form-content>
					<p>Jätä yhteystietosi:</p>
					<label class="field">
						<input type="text" id="fname" name="first_name" required placeholder="Etunimi*" required>
					</label>
					<label class="field">
						<input type="text" id="lname" name="last_name" required placeholder="Sukunimi*" required>
					</label>
					<label class="field">
						<input type="text" id="diet" name="diet" placeholder="Erityisruokavalio">
					</label>
					<div class="field">
						<label class="c-checkbox">
							<input type="checkbox" name="transport" value="1">
							<span>Tarvitsen kuljetuksen Pinellasta Naantaliin</span>
						</label>
					</div>
					<div class="form-button">
						<button class="c-button" type="submit" data-submit>
							Lähetä
							<div class="c-loader m-reverse">
								<div class="inner"></div>
							</div>
						</button>
					</div>
				</form>
			</section>
		</div>
		<!-- build:js resources/js/breakfast.js async -->
		<script src="resources/js/babel-helpers.js"></script>
		<script src="resources/js/polyfills.js"></script>
		<script src="resources/js/vendor/isMobile.min.js"></script>
		<script src="resources/js/vendor/delfin.js"></script>
		<script src="resources/js/vendor/cookieconsent.js"></script>
		<script src="resources/js/Core.js"></script>
		<script src="resources/js/utils/Utils.js"></script>
		<script src="resources/js/utils/Validator.js"></script>
		<script src="resources/js/utils/Form.js"></script>
		<!-- endbuild -->

		<script src="resources/namu/js/pixi.min.js"></script>
		<script src="resources/namu/js/pixi-filters.js"></script>
		
		<script>
			function $(e){return"string"==typeof e?document.querySelector(e):e||null}function animate(e,t,n){function i(e){return++c<u||e.target!=e.currentTarget?!1:(clearTimeout(b),c=0,void r())}function r(){e.removeEventListener("transitionend",i),e.removeEventListener("webkitTransitionEnd",i),e.style.transition=s,e.style.webkitTransition=a,v&&v.call()}var s=e.style.transition,a=e.style.webkitTransition,o="",l="",u=0,c=0,d=n.ease||"linear",y=n.delay||0,v=n.complete||null;for(var m in n)if("ease"!=m&&"delay"!=m&&"complete"!=m){var f=m.replace(/([a-z])([A-Z])/g,"$1-$2"),g=f.toLowerCase()+" "+t+"s "+d+" "+y+"s, ";o+=g,u++,"transform"==f.slice(0,9)?(f="-webkit-"+f,l+=f.toLowerCase()+" "+t+"s "+d+" "+y+"s, "):l+=g}else delete n[m];e.style.webkitTransition=l.substring(0,l.length-2),e.style.transition=o.substring(0,o.length-2);for(var m in n){var f=m.replace(/([a-z])([A-Z])/g,"$1-$2");e.style[m]=n[m],"transform"==f.slice(0,9)&&(e.style["-webkit-"+m]=n[m])}getComputedStyle(e).display,e.addEventListener("transitionend",i),e.addEventListener("webkitTransitionEnd",i);var b=setTimeout(r,1e3*t+1e3*y+50)}

			/*
			* Easing Functions - inspired from http://gizma.com/easing/
			* only considering the t value for the range [0, 1] => [0, 1]
			*/
			EasingFunctions = {
			// no easing, no acceleration
			linear: function (t) { return t },
			// accelerating from zero velocity
			easeInQuad: function (t) { return t*t },
			// decelerating to zero velocity
			easeOutQuad: function (t) { return t*(2-t) },
			// acceleration until halfway, then deceleration
			easeInOutQuad: function (t) { return t<.5 ? 2*t*t : -1+(4-2*t)*t },
			// accelerating from zero velocity 
			easeInCubic: function (t) { return t*t*t },
			// decelerating to zero velocity 
			easeOutCubic: function (t) { return (--t)*t*t+1 },
			// acceleration until halfway, then deceleration 
			easeInOutCubic: function (t) { return t<.5 ? 4*t*t*t : (t-1)*(2*t-2)*(2*t-2)+1 },
			// accelerating from zero velocity 
			easeInQuart: function (t) { return t*t*t*t },
			// decelerating to zero velocity 
			easeOutQuart: function (t) { return 1-(--t)*t*t*t },
			// acceleration until halfway, then deceleration
			easeInOutQuart: function (t) { return t<.5 ? 8*t*t*t*t : 1-8*(--t)*t*t*t },
			// accelerating from zero velocity
			easeInQuint: function (t) { return t*t*t*t*t },
			// decelerating to zero velocity
			easeOutQuint: function (t) { return 1+(--t)*t*t*t*t },
			// acceleration until halfway, then deceleration 
			easeInOutQuint: function (t) { return t<.5 ? 16*t*t*t*t*t : 1+16*(--t)*t*t*t*t }
			}
		</script>
		<script>
		document.addEventListener("DOMContentLoaded", function() {
			var isMobileViewport = innerWidth < 1024;
			$frontModules = document.querySelectorAll('.bg');
			$frontModulesImages = document.querySelectorAll(".bg .img");

			allFrontModulesImages = ['/resources/namu/images/bg.jpg'];
			frontModulesImages = allFrontModulesImages;
			if (allFrontModulesImages.indexOf('/resources/namu/images/transparent.png') == -1) {
				allFrontModulesImages.push('/resources/namu/images/transparent.png');
			}
			
			var body = document.querySelector('body');
				body.classList.remove('loading');


			var state = 0,
				tick = 0,
				current = 0,
				transition = false,
				transitionEffectTimeout = null,
				moduleImageScale = 1.05,
				displacementScale = 1,
				targetDisplacementScale = 5;
				displacementSpeed = 0.85,
				targetDisplacementSpeed = 0.85;
				transitionDelay = 500,
				transitionSpeed = 80,
				modulesLength = document.querySelectorAll(".bg").length,
				modules = [],
				maskSprites = [],
				canvases = [],
				containers = [];
			PIXI.loader.add(allFrontModulesImages).add("/resources/namu/images/white-pixel.png").add("/resources/namu/images/displacement-map.jpg").load(setup);

			function setup() {
				function getModuleWidthHeight(index) {
					var _image = $frontModulesImages[index],
						textureWidth = _image.naturalWidth,
						textureHeight = _image.naturalHeight,
						textureRatio = textureWidth / textureHeight,
						viewportRatio = innerWidth / innerHeight;
					if (viewportRatio <= textureRatio) {
						moduleWidth = innerHeight * textureRatio;
						moduleHeight = innerHeight;
					} else {
						moduleWidth = innerWidth;
						moduleHeight = innerWidth / textureRatio;
					}
					return {
						width: moduleWidth,
						height: moduleHeight
					}
				}
				var masks = [],
					maskFilters = [],
					displacementFilters = [],
					displacementSprites = [];

				function createCanvases(i) {
					canvases[i] = new PIXI.Application(innerWidth, innerHeight, {
						transparent: true
					}), containers[i] = new PIXI.Container();
					$frontModules[i].insertBefore(canvases[i].view, parent.firstChild);
					canvases[i].stage.addChild(containers[i]);
					displacementSprites[i] = new PIXI.Sprite(PIXI.loader.resources["/resources/namu/images/displacement-map.jpg"].texture);
					displacementSprites[i].texture.baseTexture.wrapMode = PIXI.WRAP_MODES.REPEAT;
					containers[i].addChild(displacementSprites[i]);
				}

				function setupCanvas(isInitial) {
					for (var i = 0; i < modulesLength; i++) {
						createCanvases(i);
						var moduleSizes = getModuleWidthHeight(i);
						modules[i] = new PIXI.Sprite(PIXI.loader.resources[frontModulesImages[i]].texture);
						modules[i].width = moduleSizes.width * moduleImageScale;
						modules[i].height = moduleSizes.height * moduleImageScale;
						modules[i].anchor.set(.5, .5);
						modules[i].position.set(innerWidth / 2, innerHeight / 2);
						if (isInitial) {
							containers[i].addChild(modules[i]);
						}
						// maskSprites[i] = new PIXI.Sprite(PIXI.loader.resources["/resources/namu/images/white-pixel.png"].texture)
						// maskSprites[i].width = innerWidth + 100;
						// maskSprites[i].height = innerHeight;
						// maskSprites[i].anchor.set(.5, .5);
						// maskSprites[i].position.set(innerWidth / 2, innerHeight / 2);
						// if (isInitial) {
						// 	containers[i].addChild(maskSprites[i]);
						// }
						// maskFilters[i] = new PIXI.SpriteMaskFilter(maskSprites[i]);
						displacementFilters[i] = new PIXI.filters.DisplacementFilter(displacementSprites[i]);
						displacementFilters[i].resolution = 0.7;
						displacementFilters[i].scale.x = 0;
						displacementFilters[i].scale.y = 0;
						displacementFilters[i].enabled = false;
						// modules[i].filters = [displacementFilters[i], maskFilters[i]];
						modules[i].filters = [displacementFilters[i]];
					}
				}
				setupCanvas(true);

				function disableFilters(module) {
					displacementFilters[module].scale.x = 0;
					displacementFilters[module].scale.y = 0;
					displacementFilters[module].enabled = false;
				}

				function enableFilters(module) {
					displacementFilters[module].enabled = true;
				}

				function transitionEffect(activeModule) {
					current = activeModule;
					enableFilters(activeModule);
					state = 3;
					tick = 0;
					transition = true;
				}

				transitionEffectTimeout = setTimeout(function() {
					transitionEffect(0);
				}, transitionDelay);



				function ticker() {
					if (transition == true) {
						if (tick < transitionSpeed / 60) {
							tick += 1 / transitionSpeed;
						} else {
							transition = false;
							tick = transitionSpeed / 60;
						}
					}
					// tick = transitionSpeed / 60;
					
					var diff = targetDisplacementScale - displacementScale;
					if (Math.abs(diff) > 0.5) {
						displacementScale += diff * 0.05;
					}
					
					var diffSpeed = targetDisplacementSpeed - displacementSpeed;
					if (Math.abs(diffSpeed) > 0.5) {
						displacementSpeed += diffSpeed * 0.05;
					}
					
					modules[0].alpha = 1- ((displacementScale / 100)*0.6);

					displacementFilters[current].scale.x = displacementScale * state * tick;
					displacementFilters[current].scale.y = displacementScale * state * tick;
					displacementSprites[current].x += displacementSpeed;
					displacementSprites[current].y += displacementSpeed;
					
					requestAnimationFrame(ticker);
				}
				ticker();
				

				function resize() {
					// run only on desktop?

					ww = window.innerWidth;
					wh = window.innerHeight;
					if (ww > 640) {
						for (var i = 0; i < modulesLength; i++) {
							canvases[i].renderer.resize(innerWidth, innerHeight);
							var moduleSizes = getModuleWidthHeight(i);
							modules[i].width = moduleSizes.width * moduleImageScale;
							modules[i].height = moduleSizes.height * moduleImageScale;
							modules[i].position.set(innerWidth / 2, innerHeight / 2);
							// maskSprites[i].width = innerWidth;
							// maskSprites[i].height = innerHeight;
							// maskSprites[i].position.set(innerWidth / 2, innerHeight / 2);
							canvases[i].render(containers[i]);
						}
					}
				}

				function scroll() {
					var sctop = Core.scrollTop();
					var bh = document.body.clientHeight;
					var wh = window.innerHeight;

					var scalar = sctop / (bh - wh);
					targetDisplacementScale = scalar*100;
					targetDisplacementScale = Math.min(Math.max(targetDisplacementScale, 5), 100);
					targetDisplacementSpeed = Math.min(Math.max(scalar, 0.15), 1);
				}

				window.addEventListener('resize', function() { resize(); });
				document.addEventListener('scroll', function(e) {
					scroll();
				});

				scroll();

				document.querySelector("#signup-button").addEventListener("click", function(e) {
					window.scrollTo({
						top: document.querySelector(".c-schedule").getBoundingClientRect().top - 60 + document.documentElement.scrollTop || document.body.scrollTop,
						behavior: "smooth"
					})
				});

				animate(document.querySelector("#signup-button"), 1, {opacity: 1, delay: 2});
			}

			Form.setup();
		});
		</script>
	</body>
</html>