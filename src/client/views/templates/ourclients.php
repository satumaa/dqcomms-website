<?= $M->head; ?>
<?= $M->header; ?>

<main>
	<div class="c-content m-tight">
		<div class="inner">
			<?= $M->share; ?>
			<? /*
			<article class="c-article g-article">
				<div class="inner">
					<div class="image">
						<img src="resources/images/temp/client.jpg" alt="">
					</div>
					<p class="tag">Tag</p>
					<div class="text">
						<h1>The quick brown fox jumps over the lazy dog</h1>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus <a href="">tristique quam</a> sed pharetra consequat. Mauris dictum purus a sem euismod, ac dignissim dui maximus. Etiam fermentum, dui eget malesuada elementum, risus purus consectetur nunc, a cursus mi nisl ac sapien. Mauris felis dui, rhoncus ut nulla nec, pretium facilisis erat. Vestibulum egestas nunc erat, vitae congue arcu rhoncus ac. In id viverra purus, id finibus ante. In hac habitasse platea dictumst.</p>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus tristique quam sed pharetra consequat. Mauris dictum purus a sem euismod, ac dignissim dui maximus. Etiam fermentum, dui eget malesuada elementum, risus purus consectetur nunc, a cursus mi nisl ac sapien. Mauris felis dui, rhoncus ut nulla nec, pretium facilisis erat. Vestibulum egestas nunc erat, vitae congue arcu rhoncus ac. In id viverra purus, id finibus ante. In hac habitasse platea dictumst.</p>
						<div class="c-video-embed" data-video-id="290886731">
							<div class="image">
								<button class="play">
									<span class="inner">
										<span class="icon"></span>
									</span>
								</button>
								<img src="resources/images/temp/video.jpg" alt="">
							</div>
							<div class="embed">
								<iframe src="https://player.vimeo.com/video/" width="640" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
							</div>
						</div>
					</div>
					<div class="c-scroller" data-scroller>
						<div class="inner">
							<div>
								<div><img src="resources/images/temp/scroller.jpg" alt=""></div>
								<div><img src="resources/images/temp/scroller.jpg" alt=""></div>
							</div>
						</div>
					</div>
				</div>
			</article>
			*/ ?>
			<div class="c-clients row" id="clients">
				<div class="nav">
					<h1>Some of our beloved clients</h1>
					<div class="item" data-id="1"><div class="image"><img src="resources/images/clients/mobile/7-eleven.jpg" alt=""></div><span>7-Eleven</span></div>
					<div class="item" data-id="2"><div class="image"><img src="resources/images/clients/mobile/absolut.jpg" alt=""></div><span>Absolut</span></div>
					<div class="item" data-id="3"><div class="image"><img src="resources/images/clients/mobile/admares.jpg" alt=""></div><span>Admares</span></div>
					<div class="item" data-id="4"><div class="image"><img src="resources/images/clients/mobile/tillander.jpg" alt=""></div><span>Atelier Torbjörn Tillander</span></div>
					<div class="item" data-id="5"><div class="image"><img src="resources/images/clients/mobile/cadmatic.jpg" alt=""></div><span>Cadmatic</span></div>
					<div class="item" data-id="6"><div class="image"><img src="resources/images/clients/mobile/carlsberg.jpg" alt=""></div><span>Carlsberg</span></div>
					<div class="item" data-id="7"><div class="image"><img src="resources/images/clients/mobile/turun_kaupunki.jpg" alt=""></div><span>City of Turku</span></div>
					<div class="item" data-id="23"><div class="image"><img src="resources/images/clients/mobile/handelshogskolan.jpg" alt=""></div><span>Handelshögskolan</span></div>
					<div class="item" data-id="8"><div class="image"><img src="resources/images/clients/mobile/hesburger.jpg" alt=""></div><span>Hesburger</span></div>
					<div class="item" data-id="9"><div class="image"><img src="resources/images/clients/mobile/ido.jpg" alt=""></div><span>IDO</span></div>
					<div class="item" data-id="10"><div class="image"><img src="resources/images/clients/mobile/keiju.jpg" alt=""></div><span>Keiju</span></div>
					<div class="item" data-id="11"><div class="image"><img src="resources/images/clients/mobile/loreal.jpg" alt=""></div><span>L'Oréal</span></div>
					<div class="item" data-id="12"><div class="image"><img src="resources/images/clients/mobile/minttu.jpg" alt=""></div><span>Minttu</span></div>
					<div class="item" data-id="13"><div class="image"><img src="resources/images/clients/mobile/muumimaailma.jpg" alt=""></div><span>Muumimaailma</span></div>
					<div class="item" data-id="14"><div class="image"><img src="resources/images/clients/mobile/nestek.jpg" alt=""></div><span>Neste K</span></div>
					<? /*<div class="item" data-id="11"><div class="image"><img src="resources/images/clients/mobile/client.jpg" alt=""></div><span>Naantalin musiikkijuhlat</span></div>*/ ?>
					<div class="item" data-id="15"><div class="image"><img src="resources/images/clients/mobile/orkla.jpg" alt=""></div><span>Orkla</span></div>
					<div class="item" data-id="16"><div class="image"><img src="resources/images/clients/mobile/orthex.jpg" alt=""></div><span>Orthex</span></div>
					<div class="item" data-id="17"><div class="image"><img src="resources/images/clients/mobile/perheyritysten_liitto.jpg" alt=""></div><span>Perheyritysten liitto</span></div>
					<div class="item" data-id="18"><div class="image"><img src="resources/images/clients/mobile/sony_mobile.jpg" alt=""></div><span>Sony Mobile</span></div>
					<div class="item" data-id="19"><div class="image"><img src="resources/images/clients/mobile/suoma.jpg" alt=""></div><span>Suoma ry.</span></div>
					<div class="item" data-id="20"><div class="image"><img src="resources/images/clients/mobile/varma.jpg" alt=""></div><span>Varma</span></div>
					<div class="item" data-id="21"><div class="image"><img src="resources/images/clients/mobile/visit_turku.jpg" alt=""></div><span>Visit Turku</span></div>
					<div class="item" data-id="22"><div class="image"><img src="resources/images/clients/mobile/ytk.jpg" alt=""></div><span>Yleinen työttömyyskassa YTK</span></div>
				</div>
				<div class="content">
					<div class="images">
						<img src="resources/images/clients/7-eleven.jpg" alt="" data-id="1">
						<img src="resources/images/clients/absolut.jpg" alt="" data-id="2">
						<img src="resources/images/clients/admares.jpg" alt="" data-id="3">
						<img src="resources/images/clients/tillander.jpg" alt="" data-id="4">
						<img src="resources/images/clients/cadmatic.jpg" alt="" data-id="5">
						<img src="resources/images/clients/carlsberg.jpg" alt="" data-id="6">
						<img src="resources/images/clients/turun_kaupunki.jpg" alt="" data-id="7">
						<img src="resources/images/clients/hesburger.jpg" alt="" data-id="8">
						<img src="resources/images/clients/ido.jpg" alt="" data-id="9">
						<img src="resources/images/clients/keiju.jpg" alt="" data-id="10">
						<img src="resources/images/clients/loreal.jpg" alt="" data-id="11">
						<img src="resources/images/clients/minttu.jpg" alt="" data-id="12">
						<img src="resources/images/clients/muumimaailma.jpg" alt="" data-id="13">
						<img src="resources/images/clients/nestek.jpg" alt="" data-id="14">
						<? /*<img src="resources/images/clients/naantalin_musiikkijuhlat.jpg" alt="" data-id="11"> */ ?>
						<img src="resources/images/clients/orkla.jpg" alt="" data-id="15">
						<img src="resources/images/clients/orthex.jpg" alt="" data-id="16">
						<img src="resources/images/clients/perheyritysten_liitto.jpg" alt="" data-id="17">
						<img src="resources/images/clients/sony_mobile.jpg" alt="" data-id="18">
						<img src="resources/images/clients/suoma.jpg" alt="" data-id="19">
						<img src="resources/images/clients/varma.jpg" alt="" data-id="20">
						<img src="resources/images/clients/visit_turku.jpg" alt="" data-id="21">
						<img src="resources/images/clients/ytk.jpg" alt="" data-id="22">
						<img src="resources/images/clients/handelshogskolan.jpg" alt="" data-id="23">
					</div>
				</div>
			</div>
		</div>
	</div>
</main>

<script>
	app.ready(function() {
		new ClientsList(d("#clients"));
	});
</script>

<?= $M->footer; ?>