<?= $M->head; ?>
<div id="all">
	
	<div id="start-content">
	<?=$M->header;?>
	<!--<main class="container">
		<video autoplay muted loop playsinline>
			<source src="/resources/videos/video.mp4" type="video/mp4">
		</video>
		<p>More drama coming soon!</p>
	</main>-->
	
	<main>
		<?= $M->share; ?>
		<div class="c-content m-no-padding m-center">
            <!--Carousel-->
           
            <div class="content-slider">
                <div class="slider">
                    <div class="mask">
                        <ul>
                            <li class="anim1">
                                <div class="quote">“True professionals who have their finger on the pulse.”</div>
                                <div class="source">Vuoden Toimisto (Agency of the Year) - research</div>
                            
                            </li>
                            <li class="anim2">
                                <div class="quote">“Always bringing fresh ideas to the table unprompted, providing real benefits to the customer.”</div>
                                    <div class="source">Vuoden Toimisto (Agency of the Year) - research</div>
                            </li>
                            <li class="anim3">
                                <div class="quote">“Taking a broad view of customer business, considering both the nature of the industry, and competition within the field.”</div>
                                    <div class="source">Vuoden Toimisto (Agency of the Year) - research</div>
                            </li>
                            <li class="anim4">
                                <div class="quote">“Finding the hidden gems and creative perspectives which the customer might not notice.”</div>
                                    <div class="source">Vuoden Toimisto (Agency of the Year) - research</div>
                            </li>
                            <li class="anim5">
                                <div class="quote">“Keeping hold of the reigns, ensuring projects success without breaking the bank.”</div>
                                 <div class="source">Vuoden Toimisto (Agency of the Year) - research</div>
                            </li>
                           <li class="anim6">
                                <div class="quote">”Professionals who know their business. They see the whole picture and manage the scope. Excellent service, allowing the client to purr with satisfaction.”</div>
                                 <div class="source">Vuoden Toimisto (Agency of the Year) - research</div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="c-logo">
                <img src="/resources/images/image_black_fix.png" alt="">
            </div>
            <!--
			<div class="c-intro-text">
				<h1>We are a Nordic Business Partner with a sense of drama. We combine marketing, communications and technology with creativity. That’s how we turn flux to fortune.</h1>
			</div>
            -->
			<div class="c-intro row">
				<div class="col">
					<a href="/daily-drama/sfb-agency-becomes-drama-queen">
					<img src="/resources/images/pattern.svg" alt="">
					</a>	
				</div>
				<div class="col">
					<a href="/daily-drama/sfb-agency-becomes-drama-queen">
						<h5>daily drama</h5>
						<h2>SFB Agency is now<br>Drama Queen ›</h2>
					</a>
				</div>
			</div>
			
			<div id="canvas-holder">
			<canvas id="canvas" width="1200" height="2750"></canvas>
			<div id="nosto-pl" class="canvas-content"><a href="daily-drama/drama-queen-is-redesigning-the-visual-identity-of-the-finnish-family-firms-association"><h5>daily drama</h5><h3>Visual identity for the<br>Finnish Family Firms<br>Association ›</h3></a></div>
			
			<div id="nosto-tukes" class="canvas-content"><a href="/daily-drama/at-your-own-risk-campaign"><h5>daily drama</h5><h3>At Your Own Risk ›</h3></a></div>
			
			
			<div id="nosto-clients" class="canvas-content"><a href="/clients"><h5>clients</h5><h3>Some<br>of our<br>beloved<br>clients ›</h3></a></div>
			
			
			<div id="nosto-contacts" class="canvas-content"><a href="/contact"><h5>contact us</h5><h3>Let's make<br>some drama ›</h3></a></div>
			
			
		</div>
		</div>
		
	</main>
</div>
		

</div> 

<?= $M->footer; ?>

<script>
app.ready(function(){
	
	start_canvas();
});
</script>
	