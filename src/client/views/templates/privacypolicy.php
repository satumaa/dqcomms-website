<?= $M->head; ?>
<?= $M->header; ?>

<main>
	<?= $M->share; ?>
	<div class="c-content privacy-content">
		<div class="inner">
			<div class="c-text">
			<!--<h1>A Need for a story is in our genes. Stories make our life worth living. Without drama we are nothing. Attention sleeps. It needs to be woken up.</h1>-->
			   <h1>PRIVACY POLICY 2018</h1>

<p style="font-size: 20px;">At Drama Queen Communications Oy (referred to as “DQ” or “we” in this document), we are committed to protecting privacy.
  DQ has adopted privacy principles which guide how we protect and manage personal data and demonstrate how information practices are embedded in our culture.
  We aim to protect the privacy of visitors to the sites that we operate (collectively, the “Site”).</p>

<p style="font-size: 20px;">You can navigate most of the Site without giving us any personal information which identifies you. However, <strong>sometimes</strong> we might need additional information about you in order to provide you with information or services you want to receive from us.</p>

<h1>The Information We Collect</h1>

<h3 style="font-size: 30px;">The types of information</h3>

<p style="font-size: 20px;">1.   Personally identifiable information: This is information that personally identifies one individual from another or enables you to be individually contacted (for example, names, e-mail addresses and other contact information). This information is voluntarily provided to us by Site visitors and is used by us for responding to user submissions and fulfilling requests for further information about our services.</p>

<p style="font-size: 20px;">2.   Aggregate user and tracking information: This information gives us insights on how Site visitors use our site. This data is anonymous and does not contain any personally identifiable information. We use this information to analyse the performance of our website, e-mails and marketing efforts.</p>

<h1>Information you voluntarily provide to DQ</h1>

<p style="font-size: 20px;">DQ only collects personally identifiable information that you voluntarily provide. Examples of such personally identifiable information that you provide to us may include, among other information, your name, physical address and email address.</p>

<h2>Other information DQ collects</h2>

<p style="font-size: 20px;">When you visit our Site, we may automatically collect some information about your visit using cookies, log analysis software and other tracking technologies.</p>

<p style="font-size: 20px;">If you wish, you may set your browser to refuse cookies. However, by refusing to accept a cookie, you may not be able to use some of the features or functions of the Site. DQ cookie files do not contain personally identifiable information.</p>

<h1>3rd Parties on Our Sites</h1>

<p style="font-size: 20px;">In addition to data we directly collect, and data collected on our behalf through 3rd party service providers operating on our behalf, our sites may contain additional 3rd party content from companies over whom we do not have direct control. They will collect data like what we described earlier in “The Information We Collect” and may have their own cookies. These parties can change from time to time, but may currently include:

<p style="font-size: 20px;">Google</p>
<p style="font-size: 20px;">Facebook</p>
<p style="font-size: 20px;">Twitter</p>
</p>
<p style="font-size: 20px;">These 3rd parties may use cookies for advertising purposes including targeting and measurement.</p>

<h2 style="font-size: 35px;">How DQ uses Information</h2>

<p style="font-size: 20px;">We store the information you provide in a secure database to provide you with the information, products, and/or services you request and may be used to provide you with additional information about our services we believe may be of interest to you. The information is stored for a period and in a manner relevant to responding to your request unless you request that it be removed. The information you provide us will be shared with DQ employees and service providers to the extent necessary to administrate our systems and/or accommodate your request.</p>

<p style="font-size: 20px;">If you provide your name, mailing address, telephone and/or email address and request more information about DQ services, this information will be shared with appropriate DQ personnel to fulfil your request. This information will not be shared with non-affiliated third parties without your consent.</p>

<p style="font-size: 20px;">We use non-identifying and aggregate information about the use of the Site to improve the navigation, content and design of the Site. This information may include, for example, the most and least requested pages, analyzing traffic regarding specific features, and the number of users from different countries, among other things.</p>


<h2 style="font-size: 35px;">Promotions, surveys and voting</h2>

<p style="font-size: 20px;">We may operate contests, sweepstakes or other promotions through the Site, which may require registration in order to enter. Your personally identifiable information may be used by us to contact you for winner notification, prize delivery confirmation or other promotional purposes. Your entry in the contest or sweepstakes may also result in your being added to our mailing lists as well as those of our promotional partners or clients associated with the contest or sweepstakes.  Acceptance of a prize may require you (unless prohibited by law) to allow us to post publicly some of your information on the Site, such as in social media or on a winner’s page, in the event you win a contest or sweepstakes.</p>

<p style="font-size: 20px;">We may also request personally identifiable information from you via surveys or voting polls. Participation is voluntary and you will have the opportunity to decide whether or not to disclose information.  At times, you may have to register to vote or to take part in a survey.</p>

<h2 style="font-size: 35px;">Exceptions</h2>

<p style="font-size: 20px;">DQ may preserve and has the right to disclose any information about you or your use of the Site without your prior permission if DQ has a good faith belief that such action is necessary to: (a) protect and defend the rights, property or safety of DQ or its clients or their respective partners, employees, affiliates, other users of the Site, or the public; (b) enforce the Terms of Use for the Site; or (c) respond to claims related to your use of the Site. We may also disclose information as we deem necessary to satisfy any applicable law, regulation, legal process or lawful governmental request or in the event of a sale or potential sale of DQ.</p>

<h2 style="font-size: 35px;">Data Security</h2>

<p style="font-size: 20px;">DQ understands that the safety of your personal information is extremely important to you. Accordingly, DQ uses reasonable physical, technological and other measures to keep your information protected from loss, misuse and unauthorized access, disclosure, alteration and destruction, taking into due account the risks involved in the processing and the nature of the personal information. Due to the nature of the Internet and related technology, we cannot guarantee the security of your personal information and DQ expressly disclaims any such obligation.</p>

<h2 style="font-size: 35px;">Children’s Privacy</h2>

<p style="font-size: 20px;">DQ understands the need to protect the privacy of children. The Site is not intended for use by individuals under 15 years of age. DQC does not knowingly collect or maintain personal information from children under the age of 15 at our Site.</p>

<h2 style="font-size: 35px;">How You Can Access or Correct Your Information</h2>

<p style="font-size: 20px;">You may access, update or remove the information you provided to us by contacting us – see <a href="#contact">“How to Contact Us”</a> below. To protect your privacy and security, we will also take reasonable steps (including seeking additional information or documentation) to verify your identity before updating or removing your information. Upon receiving your email with the request to update or remove your information, we will handle and process your request. The information you provide us may be archived or stored periodically by us according to backup processes conducted in the ordinary course of business. Information stored as part of this backup process will be deleted in due course on a regular schedule.</p>

<h2 style="font-size: 35px;">Links to Other Websites</h2>

<p style="font-size: 20px;">Our Site may contain links to other websites. DQ is not responsible for any websites that it does not own or operate.  You should carefully review the privacy policies and practices of other websites that you link to from the Site, as we cannot control or be responsible for their privacy practices.</p>

<h2 style="font-size: 35px;">International Users</h2>

<p style="font-size: 20px;">Information provided through this Site may be uploaded to a cloud and transferred to or accessed globally. By providing information to this Site, you consent to the transfer, processing and/or use of the information globally throughout DQ. Wherever the information is transferred or accessed within DQ it will be processed and used in accordance with this privacy policy.</p>

<h2 style="font-size: 35px;">Changes to this Privacy Policy</h2>

<p style="font-size: 20px;">We reserve the right to alter this Privacy Policy in our sole discretion from time to time or as our business changes. Amendments to this Privacy Policy take effect when they are notified by us, e.g. on our website. Using our services or websites after the Privacy Policy have been amended shall constitute acceptance of said amendments. We encourage you to periodically check back and review this Privacy Policy.</p>

<p style="font-size: 20px;">Accessing and updating your information; Limiting or Opting out</p>

<p style="font-size: 20px;">Visitors who would like to access, correct, update or delete any personally identifiable information may contact us with a request to do so at <address><a href="mailto:rami.kangas@dqcomms.com" target="_blank">rami.kangas@dqcomms.com</a></address></p>

<p style="font-size: 20px;">Similarly, Site users who wish to subscribe or unsubscribe to our marketing and informational communications or limit our use of any personally identifiable information in any way can do so by using the subscribe/unsubscribe options contained in our emails or by sending an email to <address><a href="mailto:rami.kangas@dqcomms.com" target="_blank">rami.kangas@dqcomms.com</a></address></p>

<p style="font-size: 20px;">We may take a reasonable period of time to respond to any request to update or modify any information, limit use of information or to opt out of or unsubscribe from any communications.  If you request the deletion, modification or limitation of use of your personal information maintained by us, such information may be retained for a period of time in our backup systems as a precaution against system failures.  Some information may be retained for longer periods as required by law, contract or auditing requirements. In some circumstances, we will not be able to limit use of your personal information without unsubscribing you from communications or deleting your information.</p>

<h2 style="font-size: 35px;">How to Contact Us</h2>

<p style="font-size: 20px;" id="contact">If you ever have any questions about this Privacy Policy, please contact us at rami.kangas@dqcomms.com</p>

<p style="font-size: 20px;">This privacy policy was last updated on October 1, 2018.</p>

<p style="font-size: 20px;">Copyright © <?=date("Y",time());?> Drama Queen Communications. All rights reserved.</p>

				</div>
		</div>
	</div>
</main>

<?= $M->footer; ?>