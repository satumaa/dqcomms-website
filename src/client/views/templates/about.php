<?= $M->head; ?>
<?= $M->header; ?>

<main>
	<?= $M->share; ?>
	<div class="c-content">
		<div class="inner">
			<!--<h1>A Need for a story is in our genes. Stories make our life worth living. Without drama we are nothing. Attention sleeps. It needs to be woken up.</h1>-->
			<p>Drama Queen’s history dates back to 1996 when Advertising Agency Satumaa was founded in Turku, Finland. Since those days we have always been driven by curiosity, constant development and creativity. Even so that we wanted to enlarge our family to Sweden by buying a talented agency named Family Business, dedicated to retail and design. Moreover, we extended the story by merging DQ Communications, specializing in communications, social media and content marketing.</p>
			<p>Despite the proudness of our past, our eyes are now looking firmly at the future: Drama Queen is a versatile Nordic Marketing Communications agency. We employ nearly 90 professionals in Finland, Sweden and Denmark.</p>
			<p>We deliver measurable results to our beloved clients by combining marketing, communications and technology with creativity. That’s how we turn flux to fortune.</p>
			<p>And more importantly – Drama makes us human.</p>
		</div>
	</div>
</main>

<?= $M->footer; ?>