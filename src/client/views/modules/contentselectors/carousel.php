<?php $modules = $M->modules; ?>

<?php if (0 == count((array) $modules)): ?>
	<div style='text-align:center;' class='mellow-no-content' data-uri='<?= $M->uri; ?>'>Add Slide</div>
<?php else: ?>
	<div class="carousel" id="my-carousel" data-count="<?=count($modules); ?>" data-uri="<?=$M->uri; ?>">
		<div class="swipe">
			<div class="swipe-wrap">
				<?php
					foreach ($modules as $key => $module) {
						echo $module;
					}
				?>
			</div>
		</div>
	<div class="carousel-dots row"></div>
</div>
<?php endif; ?>	