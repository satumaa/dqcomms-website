<div class="images" data-uri="<?=$M->uri; ?>">
<?php
	$modules = $M->modules;

?>

			<div class="image">


						<div class="carousel">
							<div class="swipe" >
								<div class="swipe-wrap">

                                   <?php

									if (0 == count((array) $modules)) {
										if (\mellow\App::getUser()->canSeeAdminHtml()) {
											echo '<div><img itemprop="image" src="resources/images/holder-image.png" alt=""></div>';
										}
									} else {
										?>

										<?php
										for ($i = 0; $i < count($modules); ++$i) {
											echo $modules[$i]->htmlView('div-img');
										}
									}
									?>

								</div>
							</div>
						</div>
					</div>
					<div class="thumbs">
						<ul>
							<?php

									if (0 == count((array) $modules)) {
										if (\mellow\App::getUser()->canSeeAdminHtml()) {
											echo '<li data-id="1"><img src="resources/images/holder-image.png" alt=""></li>';
										}
									} else {
										for ($i = 0; $i < count($modules); ++$i) {
											echo $modules[$i]->htmlView('li-img');
										}
									}
										?>

						</ul>
					</div>
    </div>
