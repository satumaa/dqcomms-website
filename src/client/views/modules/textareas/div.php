<?php if (\mellow\App::getUser()->canSeeAdminHtml() || false == $M->values->isDefault): ?>
<?php $classes = !empty(@$M->settings['classes']) ? 'class='.@$M->settings['classes'] : ''; ?>
<div data-uri='<?=$M->uri; ?>' <?= $classes ?>><?=$M->values->value; ?></div>
<?php endif; ?>
