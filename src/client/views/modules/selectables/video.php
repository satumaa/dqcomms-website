<?php
$video = ($M->video);

$data_youtube = '';
if (!$video->values->isDefault) {
	$data_youtube = " data-youtube='".$video->values->value."' ";
}

$video_src = $M->image->values->src;

?><div data-uri="<?=$M->uri; ?>">
	<section class="Video-block container">
		<div class="youtube-video" <?=$data_youtube; ?>>
			<div class="image">
				<button class="play">
					<span class="inner">
						<span class="icon"></span>
					</span>
				</button>
				<img src="<?=$video_src; ?>" alt="" data-uri="<?=$M->image->uri; ?>">
			</div>
			<div class="embed test">
				<iframe width="560" height="315" src="https://www.youtube.com/embed/" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
			</div>
		</div>
	</section>
</div>