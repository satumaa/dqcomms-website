<?php if (\mellow\App::getUser()->canSeeAdminHtml() || (false == $M->picture->values->isDefault)): ?>
<?php	
	$btn = '';
	if ($M->buttontext->values->isDefault) {
		$btn = $M->buttontext->values->value;
	} else {
		$btn = $M->buttontext->values->raw;
	}
?>
<div data-title="<?=$btn; ?>" data-uri="<?=$M->uri; ?>">
	<?=$M->picture; ?>
	<div class="content">
		<?=$M->smalltitle; ?>
		<?=$M->h2; ?>
		<p><?=$M->button; ?></p>
	</div>
</div>
<?php endif; ?>