<div class="c-share-button" id="share-button"><button class="open">Share</button><button class="close">Close</button></div>
<div class="c-overlay m-share" data-overlay="share-overlay">
	<div class="inner">
		<button class="close"></button>
		
		
		<?php
		$currentPage=$M->getCurrentTemplate();
		
		$jakourl=urlencode($currentPage->values->url);
		
		if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == "on") {
   
			$jakourl="https://".$_SERVER["HTTP_HOST"].$jakourl;
		
		}else{
			
			$jakourl="http://".$_SERVER["HTTP_HOST"].$jakourl;
		}
		?>
		<div>
			<div class="link"><a target="_blank" rel="noopener" href="https://twitter.com/intent/tweet?text=<?=urlencode($currentPage->values->name);?>&amp;url=<?=$jakourl;?>" class="twitter">Twitter</a></div>
			<div class="link"><a target="_blank" rel="noopener" href="http://www.facebook.com/sharer.php?u=<?=$jakourl;?>" class="facebook">Facebook</a></div>
			<div class="link"><a target="_blank" rel="noopener" href="https://www.linkedin.com/cws/share?url=<?=$jakourl;?>" class="linkedin">Linkedin</a></div>
			<div class="link  no-desktop"><a target="_blank" rel="noopener" href="https://api.whatsapp.com/send?text=<?=$jakourl;?>">WhatsApp</a></div>
	</div>
</div>
</div>