<?php
$products = ($M->getProductsToShow());

?><div class="Product-list" data-uri="<?=$M->uri; ?>">
	<ul>

		<?php

			for ($i = 0; $i < count($products); ++$i) {
				echo $products[$i]->getListView();
			}
		?>

	</ul>
</div>
