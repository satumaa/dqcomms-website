<?php
$class = '';
if (isset($M->fontcolor->values->payload) && isset($M->fontcolor->values->payload) && isset($M->fontcolor->values->payload->cssclass)) {
	$class = $M->fontcolor->values->payload->cssclass;
}

?><div class="Page-header <?=$class; ?>" data-uri="<?=$M->uri; ?>" style="background-image: url(<?=$M->picture->values->src; ?>);">
		<?php
		$isdefault = ($M->picture->values->isDefault);
		?>
	<picture>

	<?php
		$widths = [1920, 1600, 1200, 900, 640];

		$last = 480;

		for ($i = 0; $i < count($widths); ++$i) {
			$key = 's'.$widths[$i];
			$data = $M->picture->values->$key;

			$min_width = 480;
			if (isset($widths[$i + 1])) {
				$min_width = $widths[$i + 1];
			}

			if ($isdefault) {
				echo '<source srcset="//placehold.it/'.$data->width.'x'.$data->height.'" media="(min-width: '.$min_width.'px)">';
			} else {
				echo '<source srcset="'.$data->src.'" media="(min-width: '.$min_width.'px)">';
			}
		}

		?>
		<img src="<?=$M->picture->values->src; ?>" data-uri="<?=$M->picture->uri; ?>" "alt="">
	</picture>
	<div class="inner">
		<div class="container">
			<div class="Breadcrumb">
				<a href="/"><span><?=$M->translate('home'); ?></span></a>

				<?php
					$path = ($M->getCurrentTemplate()->getPath());
					 for ($i = 0; $i < count($path); ++$i) {
					 	if ($i + 1 < count($path)) {
					 		echo '<a href="'.$path[$i]->values->url.'"><span class="'.$path[$i]->values->classes.'" data-uri="'.$path[$i]->uri.'">'.$path[$i]->values->name.'</span></a>';
					 	} else {
					 		echo '<span class="'.$path[$i]->values->classes.'" data-uri="'.$path[$i]->uri.'">'.$path[$i]->values->name.'</span>';
					 	}
					 }

				?>


			</div>
			<?=$M->h1; ?>
			<?=$M->p; ?>

		</div>
	</div>
</div>
