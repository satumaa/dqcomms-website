<?php if (\mellow\App::getUser()->canSeeAdminHtml() || false == $M->link->values->isDefault): ?>
    <a href="<?=$M->link->values->value; ?>" <?= !empty($M->link->values->target) ? ' target="_blank" ' : ''; ?> class=" <?= $M->link->values->classes; ?>" data-uri="<?=$M->uri; ?>"><?=$M->text->values->value; ?></a>
<?php endif; ?>