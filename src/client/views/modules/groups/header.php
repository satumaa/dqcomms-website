	<header class="c-header" id="header">
		<div class="c-nav-close" id="nav-close"></div>
		<nav class="c-main-nav" id="main-nav">
			<ul>
				
				<?php
				$menu=($M->getCurrentTemplate()->getRoot()->getSiblings());
				
				$current_id=$M->getCurrentTemplate()->getPath()[0]->values->ID;
				
				
				for($i=0;$i<count($menu);$i++){
					
					if($menu[$i]->showOnNavigation()){
						
						$cl="";
						if($current_id==$menu[$i]->values->ID){
							$cl=" m-active";
							
						}
						echo '<li  class="'.$cl.'" data-uri="'.$menu[$i]->uri.'"><a href="'.$menu[$i]->values->urlname.'">'.$menu[$i]->values->name.'</a></li>';
						
						
					}
				}
				
				?>
				
				
			</ul>
		</nav>
		<a href="" class="logo"><img src="resources/images/logo.svg" alt=""></a>
		<button class="c-nav-toggle" id="nav-toggle">
			<span class="lines">
				<span class="line"></span>
				<span class="line"></span>
				<span class="line"></span>
			</span>
		</button>
	</header>