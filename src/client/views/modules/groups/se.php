<section class="c-office row">
	<div class="col">
		<img src="resources/images/offices/sweden.jpg" alt="">
	</div>
	<div class="col">
		<h1>Sweden</h1>
		<h2>Stockholm</h2>
		<p>
			Bolinders plan 2<br>
			112 24 Stockholm<br>
			+46 8580 80 900
		</p>
	</div>
</section>

<section class="c-contact">
	<h2>Contacts</h2>
	<div class="row">
		<div class="item">
			<div class="image">
				<img src="resources/images/contacts/john.jpg" alt="John Lagerqvist">
			</div>
			<p>
				John Lagerqvist<br>
				Executive Creative Director, Partner<br>
				+46 738 18 00 00<br>
				<a href="mailto:john.lagerqvist@dqcomms.com">Mail</a>
			</p>
		</div>	
		<div class="item">
			<div class="image">
				<img src="resources/images/contacts/marten.jpg" alt="Mårten Knutsson">
			</div>
			<p>
				Mårten Knutsson<br>
				Chief Creative Officer, Partner<br>
				+46 70 441 92 00<br>
				<a href="mailto:marten.knutsson@dqcomms.com">Mail</a>
			</p>
		</div>															
	</div>
</section>