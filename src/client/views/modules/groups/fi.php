<section class="c-office row">
	<div class="col">
		<img src="resources/images/offices/finland.jpg" alt="">
	</div>
	<div class="col">
		<h1>Finland</h1>
		<h2>Turku</h2>
		<p>
			Itäinen pitkäkatu 21 A<br>
			20700 TURKU<br>
			+358 400 295850<br>
		</p>
		<h2>Helsinki</h2>
		<p>
			Pursimiehenkatu 26 C 3030<br>
			00150 Helsinki<br>
			+358 400 295850
		</p>
	</div>
</section>

<section class="c-contact">
				<h2>Contacts</h2>
				<div class="row">
					<div class="item">
						<div class="image">
							<img src="resources/images/contacts/matias.jpg" alt="Matias Mero">
						</div>
						<p>
							Matias Mero<br>
							CEO<br>
							+358 400 504 701<br>
							<a href="mailto:matias.mero@dqcomms.com">Mail</a>
						</p>
					</div>
					<div class="item">
						<div class="image">
							<img src="resources/images/contacts/ilona.jpg" alt="Ilona Kangas">
						</div>
						<p>
							Ilona Kangas<br>
							Senior Advisor<br>
							+358 50 590 7853<br>
							<a href="mailto:ilona.kangas@dqcomms.com">Mail</a>
						</p>
					</div>
					<div class="item">
						<div class="image">
							<img src="resources/images/contacts/rami.jpg" alt="Rami Kangas">
						</div>
						<p>
							Rami Kangas<br>
							Commercial Director<br>
							+358 40 578 4319<br>
							<a href="mailto:rami.kangas@dqcomms.com">Mail</a>
						</p>
					</div>
					<div class="item">
						<div class="image">
							<img src="resources/images/contacts/minna.jpg" alt="Minna Koivurinta">
						</div>
						<p>
							Minna Koivurinta<br>
							Account Director, Partner<br>
							+358 50 590 7822<br>
							<a href="mailto:minna.koivurinta@dqcomms.com">Mail</a>
						</p>
					</div>
					<div class="item">
						<div class="image">
							<img src="resources/images/contacts/netta_a.jpg" alt="Netta Alaranta">
						</div>
						<p>
							Netta Alaranta<br>
							Account Director<br>
							+358 40 545 6551<br>
							<a href="mailto:netta.alaranta@dqcomms.com">Mail</a>
						</p>
					</div>
				</div>
			</section>
			
			<section class="c-contact">
				<div class="row">
					<div class="item">
						<div class="image">
							<img src="resources/images/contacts/krista.jpg" alt="Krista Suomi-Myllykoski">
						</div>
						<p>
							Krista Suomi-Myllykoski<br>
							Head of Project Management<br>
							+358 50 598 5115<br>
							<a href="mailto:krista.myllykoski@dqcomms.com">Mail</a>
						</p>
					</div>
					<div class="item">
						<div class="image">
							<img src="resources/images/contacts/laura.jpg" alt="Laura Silvonen">
						</div>
						<p>
							Laura Silvonen<br>
							Head of Communications<br>
							+358 400 578 796<br>
							<a href="mailto:laura.silvonen@dqcomms.com">Mail</a>
						</p>
					</div>
					<div class="item">
						<div class="image">
							<img src="resources/images/contacts/taru.jpg" alt="Taru Lehtinen">
						</div>
						<p>
							Taru Lehtinen<br>
							Head of Media & Analysis<br>
							+358 4430 70181<br>
							<a href="mailto:taru.lehtinen@dqcomms.com">Mail</a>
						</p>
					</div>
					<div class="item">
						<div class="image">
							<img src="resources/images/contacts/marko.jpg" alt="Marko Edfelt">
						</div>
						<p>
							Marko Edfelt<br>
							Partner<br>
							+358 40 861 9317<br>
							<a href="mailto:marko.edfelt@dqcomms.com">Mail</a>
						</p>
					</div>
					<div class="item">
						<div class="image">
							<img src="resources/images/contacts/mikko.jpg" alt="Mikko Hed">
						</div>
						<p>
							Mikko Hed<br>
							Project Director<br>
							+358 50 303 7431<br>
							<a href="mailto:mikko.hed@dqcomms.com">Mail</a>
						</p>
					</div>
					<div class="item">
						<div class="image">
							<img src="resources/images/contacts/placeholder.jpg" alt="Liam Manderson">
						</div>
						<p>
							Lauri Suoperä<br>
							Head of Developers<br>
							+358 40 776 0362<br>
							<a href="mailto:lauri.suopera@dqcomms.com">Mail</a>
						</p>
					</div>
					<div class="item">
						<div class="image">
							<img src="resources/images/contacts/riikka_k.jpg" alt="Riikka King">
						</div>
						<p>
							Riikka King<br>
							Senior Project Manager <br>
							+45 53 70 88 08<br>
							<a href="mailto:riikka.king@dqcomms.com">Mail</a><br>
							(on maternity leave)
						</p>
					</div>
					<div class="item">
						<div class="image">
							<img src="resources/images/contacts/mirkka.jpg" alt="Mirkka Varho">
						</div>
						<p>
							Mirkka Varho<br>
							Senior Project Manager <br>
							+358 40 543 1154<br>
							<a href="mailto:mirkka.varho@dqcomms.com">Mail</a>
						</p>
					</div>
					<div class="item">
						<div class="image">
							<img src="resources/images/contacts/netta_p.jpg" alt="Netta Palmeranta">
						</div>
						<p>
							Netta Palmeranta<br>
							Project Manager<br>
							+358 40 776 8175<br>
							<a href="mailto:netta.palmeranta@dqcomms.com">Mail</a>
						</p>
					</div>
					<div class="item">
						<div class="image">
							<img src="resources/images/contacts/ruusu.jpg" alt="Ruusu Saarinen">
						</div>
						<p>
							Ruusu Saarinen<br>
							Project Manager<br>
							+358 40 145 8833<br>
							<a href="mailto:ruusu.saarinen@dqcomms.com">Mail</a>
						</p>
					</div>
					<div class="item">
						<div class="image">
							<img src="resources/images/contacts/veera.jpg" alt="Veera Silvendoin">
						</div>
						<p>
							Veera Silvendoin<br>
							Project Manager<br>
							+358 50 4364 502<br>
							<a href="mailto:veera.silvendoin@dqcomms.com">Mail</a>
						</p>
					</div>
					<div class="item">
						<div class="image">
							<img src="resources/images/contacts/jenna.jpg" alt="Jenna Virtanen">
						</div>
						<p>
							Jenna Virtanen<br>
							Project Manager<br>
							+358 40 5554 230<br>
							<a href="mailto:jenna.virtanen@dqcomms.com">Mail</a>
						</p>
					</div>
					<div class="item">
						<div class="image">
							<img src="resources/images/contacts/elina.jpg" alt="Elina Sihvola">
						</div>
						<p>
							Elina Sihvola<br>
							Project assistant<br>
							+358 405 410 520<br>
							<a href="mailto:elina.Sihvola@dqcomms.com">Mail</a>
						</p>
					</div>
					<div class="item">
						<div class="image">
							<img src="resources/images/contacts/placeholder.jpg" alt="Sanni Immonen">
						</div>
						<p>
							Sanni Immonen<br>
							Project Assistant<br>
							+358 40 528 5893<br>
							<a href="mailto:sanni.immonen@dqcomms.com">Mail</a>
						</p>
					</div>
					
				</div>
			</section>	