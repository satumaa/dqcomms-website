<section class="c-office row">
	<div class="col">
		<img src="resources/images/offices/denmark.jpg" alt="">
	</div>
	<div class="col">
		<h1>Denmark</h1>
		<h2>Copenhagen</h2>
		<p>
			Soho<br>
Flæsketorvet 68<br>
1711 København
		</p>
	</div>
</section>

<section class="c-contact">
	<h2>Contacts</h2>
	<div class="row">
		<div class="item">
			<div class="image">
				<img src="resources/images/contacts/netta_a.jpg" alt="Netta Alaranta">
			</div>
			<p>
				Netta Alaranta<br>
				Account Director<br>
				+358 40 545 6551<br>
				<a href="mailto:netta.alaranta@dqcomms.com">Mail</a>
			</p>
		</div>
		<div class="item">
			<div class="image">
				<img src="resources/images/contacts/riikka_k.jpg" alt="Riikka King">
			</div>
			<p>
				Riikka King<br>
				Senior Project Manager <br>
				+45 53 70 88 08<br>
				<a href="mailto:riikka.king@dqcomms.com">Mail</a><br>
				(on maternity leave)
			</p>
		</div>
	</div>
</section>	