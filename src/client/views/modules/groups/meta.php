<?php
$title = str_replace('<br />', ' ', $M->title->values->value);
$desc = str_replace('<br />', ' ', $M->description->values->value);

$title = str_replace('"', ' ', $title);
$desc = str_replace('"', ' ', $desc);


$img_fb=$M->getShareImgageFb();


?>


<title><?=$title; ?></title>
<meta name="description" content="<?=$desc; ?>">

<meta name="og:title" content="<?=$title; ?>">
<meta name="og:description" content="<?=$desc; ?>">


<?

$scheme="http://";
if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == "on") {
	
	$scheme="https://";
	
	
}
?>
<meta property="og:image" content="<?=$scheme;?><?=$_SERVER['HTTP_HOST']; ?><?=$img_fb;?>">
<meta property="og:image:width" content="1200" />
<meta property="og:image:height" content="628" />

<meta name="twitter:card" content="summary_large_image">
<meta name="twitter:url" content="<?=$scheme;?><?=$_SERVER['HTTP_HOST']; ?><?=$M->getCurrentTemplate()->values->url; ?>">
<meta name="twitter:title" content="<?=$title; ?>">
<meta name="twitter:description" content="<?=$desc; ?>">
<meta name="twitter:image" content="<?=$scheme;?><?=$_SERVER['HTTP_HOST']; ?>/resources/images/share_twitter.png"><!-- 1024x512 -->
