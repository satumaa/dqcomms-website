<!doctype html>
<html>
<head> 
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<base href="/">
	<?=$M->meta; ?>
	<!--[if lt IE 9]>
		<script src="resources/js/vendor/html5shiv.min.js"></script>
		<script src="resources/js/vendor/selectivizr.js"></script>
		<script src="resources/js/vendor/respond.min.js"></script>
	<![endif]-->
	<link href="resources/css/styles.css" rel="stylesheet">
	<!--<link href="resources/css/carousel.css" rel="stylesheet">-->
	<?php if (\mellow\App::getUser()->getLevel() >= 1): ?>
	<link href="/mellow/resources/css/mellow.css" rel="stylesheet">
	<?php endif; ?>
	<script>var app={queue:[],ready:function(a){this.isReady?a():this.queue.push(a)}}</script>
	<?php
		// include_once MELLOW_BASE_PATH.'client/views/static/ga.php';
	?>
	<link rel="apple-touch-icon" sizes="180x180" href="/resources/images/favicon/apple-touch-icon.png">
	<link rel="icon" type="image/png" sizes="32x32" href="/resources/images/favicon/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="16x16" href="/resources/images/favicon/favicon-16x16.png">
	<link rel="manifest" href="/resources/images/favicon/site.webmanifest">
	<link rel="mask-icon" href="/resources/images/favicon/safari-pinned-tab.svg" color="#5bbad5">
	<meta name="msapplication-TileColor" content="#ffffff">
	<meta name="theme-color" content="#ffffff">
	
	<? if (LIVE) : ?>
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-60387522-1"></script>
	<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());

		gtag('config', 'UA-60387522-1');
	</script>
	<?php else : ?>
	<script> function ga() {console.log(arguments); } </script>
	<?php endif; ?>
</head>
<?php
	$root = $M->getCurrentTemplate()->getRoot();
	$classit = '';
	$classit .= \mellow\App::getUser()->canSeeAdminHtml() ? 'mellow-has-bottom-bar ' : '';
	$classit = ((isset($root->is_admin) && true == $root->is_admin)) ? 'mellow-admin-page '.$classit : $classit;
	
	$classit.=" page-".$M->getCurrentTemplate()->values->ID;
?>
<body class="<?=trim($classit); ?>">  