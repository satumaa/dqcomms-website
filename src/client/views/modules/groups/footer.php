<footer class="c-footer container">
	<img class="logo" src="resources/images/logo2.svg" alt="">
	<div class="social">
		<a href="https://www.facebook.com/DramaQueenComms/" class="facebook" target="_blank" rel="noopener">Facebook</a>
		<a href="https://twitter.com/DramaQueenComms" class="twitter" target="_blank" rel="noopener">Twitter</a>
		<a href="https://www.instagram.com/dramaqueencomms/" class="instagram" target="_blank" rel="noopener">Instagram</a>
		<a href="" style="display:none;" class="linkedin" target="_blank" rel="noopener">Linkedin</a>
		<a href="https://vimeo.com/dramaqueencomms" class="vimeo" target="_blank" rel="noopener">Vimeo</a>
	</div>
	<div class="copyright">
	<p>Copyright &copy; <?=date("Y", time());?> Drama Queen Communications Oy / Ab. All Rights Reserved.</p>
	</div>
</footer>

<script>
	app.ready(function() {
		cookieConsent({
			text: "This website uses cookies. By continuing to browse it, you agree to their use. ",
			linkText: "Read more",
			linkUrl: "/privacy-policy",
			buttonText: "OK"
		});
	});
</script>

<!-- build:js resources/js/script.js async -->
<script src="resources/js/babel-helpers.js"></script>
<script src="resources/js/polyfills.js"></script>
<script src="resources/js/vendor/isMobile.min.js"></script>
<script src="resources/js/vendor/swipe.min.js"></script>
<script src="resources/js/vendor/delfin.js"></script>
<script src="resources/js/vendor/cookieconsent.js"></script>
<script src="resources/js/Header.js"></script>
<script src="resources/js/Navigation.js"></script>
<script src="resources/js/ClientsList.js"></script>
<!-- Utils. Remove ones not needed -->
<script src="resources/js/utils/Utils.js"></script>
<script src="resources/js/utils/Overlay.js"></script>
<script src="resources/js/utils/Scroller.js"></script>
<script src="resources/js/utils/VideoEmbed.js"></script>
<script src="resources/js/components/Form.js"></script>

<script src="resources/js/ImageLoader.js"></script>
<script src="resources/js/Shape.js"></script>
<script src="resources/js/Screen.js"></script>
<script src="resources/js/Pl.js"></script>
<script src="resources/js/Tukes.js"></script>
<script src="resources/js/DQCclients.js"></script>
<script src="resources/js/Contacts.js"></script>
<script src="resources/js/DQC.js"></script>
<script src="resources/js/App.js"></script>
<script src="resources/js/start.js"></script>
<!-- endbuild -->


<?=$M->mellowfooter; ?>