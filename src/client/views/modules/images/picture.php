<?php if (\mellow\App::getUser()->canSeeAdminHtml() || false == $M->values->isDefault) {
	?>


<picture>


	<?php


	$widths = [1920, 1600, 1200, 900, 640];

	for ($i = 0; $i < count($widths); ++$i) {
		$key = 's'.$widths[$i];
		$data = $M->values->$key;

		if ($M->values->isDefault) {
			echo '<source srcset="//placehold.it/'.$data->width.'x500" media="(min-width: '.$data->width.'px)">';
		} else {
			echo '<source srcset="'.$data->src.'" media="(min-width: '.$data->width.'px)">';
		}
	} ?>


	<?php /*<source srcset="resources/images/temp/top-content-image-1920.jpg" media="(min-width: 1600px)">
	<source srcset="resources/images/temp/top-content-image-1600.jpg" media="(min-width: 1200px)">
	<source srcset="resources/images/temp/top-content-image-1200.jpg" media="(min-width: 900px)">
	<source srcset="resources/images/temp/top-content-image-900.jpg" media="(min-width: 640px)">
	<source srcset="resources/images/temp/top-content-image-640.jpg" media="(min-width: 480px)">
	<source srcset="resources/images/temp/top-content-image-mobile.jpg"> */?>
	<?php

	if (isset($M->values->mobile)) {
		if ($M->values->isDefault) {
			echo '<source srcset="//placehold.it/'.$M->values->mobile->width.'x'.$M->values->mobile->height.'">';
		} else {
			echo '<source srcset="'.$M->values->mobile->src.'">';
		}
	} ?>

	<img data-uri='<?=$M->uri; ?>' src='<?=$M->values->src; ?>'>

</picture>


<?php
} ?>
