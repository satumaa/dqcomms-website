<?php

define('BASE_PATH', __DIR__, true);
define('MELLOW_CORE_PATH', __DIR__.'/mellow/');
define('MELLOW_CLIENT_PATH', __DIR__.'/client/');

require_once MELLOW_CORE_PATH.'mellow_init.php'; //autoloads etc

require_once __DIR__.'/config.php'; //client confs
