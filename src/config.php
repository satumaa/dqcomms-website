<?php

define('DISABLE_SEARCH', true, true); //TODO: remove
define('ROOT_PATH', MELLOW_CORE_PATH);
define('GOOGLE_ANALYTICS', 'UA-101408306-XXX', true); //TODO: remove
define('MELLOW_BASE_PATH', __DIR__.'/', true);
define('MELLOW_CKEDITOR_VERSION', 'ck');
define('MELLOW_RUN_EXPERIMENTAL', true);

require 'vendor/vlucas/phpdotenv/src/Dotenv.php';
require 'vendor/vlucas/phpdotenv/src/Loader.php';
require 'vendor/vlucas/phpdotenv/src/Validator.php';

$dotEnv = new \Dotenv\Dotenv(__DIR__.'/../configuration/', '.environment.env');
$dotEnv->load();

$DEVS = (json_decode(getenv('DEV_SITE_URLS')));
$LIVES = (json_decode(getenv('LIVE_SITE_URLS')));

if (is_array($DEVS) && is_array($LIVES)) {
	$site = mellow\App::getSite($_SERVER['HTTP_HOST'], $LIVES);

	if (!empty($site)) {
		define('LIVE', true, true);
	} else {
		define('LIVE', false, true);
		$site = mellow\App::getSite($_SERVER['HTTP_HOST'], $DEVS);
		if (empty($site)) {
			$site = reset($DEVS);
		}
	}
	$lang = !empty($site) ? $site[1] : null;
}

if (empty($site) || empty($lang)) {
	die('Error reading configuration');
}

// TODO: use something like the following instead:
/*
$defaultSite = getenv("LIVE_SITE_URL");
$redirectRules = [
	"www." . getenv("LIVE_SITE_URL") => $defaultSite
];

$protocol = isset($_SERVER['HTTPS']) ? 'https' : 'http';

if (array_key_exists($_SERVER["HTTP_HOST"], $redirectRules)) {
	http_response_code(301);
	header('Location: ' . $protocol . '://' . $redirectRules[$_SERVER["HTTP_HOST"]] . $_SERVER["REQUEST_URI"]);
}

$site = new \mellow\Site();

$domains = [
	[getenv("CLIENT_FOLDER_NAME") . ".atk.sfbagency.com:81", "fi"],
	[getenv("CLIENT_FOLDER_NAME") . ".atk.sfbagency.com", "fi"],
	[$defaultSite, "fi"]
];

for ($i = sizeof($domains) - 1; $i >= 0; $i--) {
	$site->addDomain($domains[$i][0], $domains[$i][1]);
	preg_match("/^(?:[a-zA-Z0-9-]*.)?(" . preg_quote($domains[$i][0]) . ")$/", $_SERVER["HTTP_HOST"], $matches);
	if (isset($matches[1])) {
		$site->addDomain($_SERVER["HTTP_HOST"], $domains[$i][1]);
	}
}


\mellow\SiteSelector::add($site);
$active_site = \mellow\SiteSelector::getSite($_SERVER["HTTP_HOST"]);

if ($active_site == false || !is_object($active_site)) {
	die("UNKNOWN SITE");
}

$siteni = $active_site->getDomain($_SERVER["HTTP_HOST"]);
if ($siteni == false) {
	die("No lang");
}

$isLive = $siteni->host === $defaultSite;*/

$root_path = MELLOW_CORE_PATH; // absolute server root path
$site_offset = 0;
$use_log = true;

// database access
define('DB_TRANSLATION_NAME', getenv('DEFAULT:DATABASE_NAME_COMMON'));
define('DB_TRANSLATION_USER', getenv('DEFAULT:DATABASE_USER_COMMON'));
define('DB_TRANSLATION_PWD', getenv('DEFAULT:DATABASE_PASSWORD_COMMON'));

switch ($lang) {
	case 'en':
		define('DB_USER', getenv('DEFAULT:DATABASE_USER_EN'), true);
		define('DB_PWD', getenv('DEFAULT:DATABASE_PASSWORD_EN'), true);
		define('DB_NAME', getenv('DEFAULT:DATABASE_NAME_EN'), true);
		define('META_TITLE', '[Mellow]');
		define('META_DESCRIPTION', '[Mellow]');
		define('MELLOW_LANG', $lang);
	break;
	case 'sv':
	break;
	case 'fi':
	break;
	default:
	die('Unsupported language');
	break;
}

\mellow\Translations::setLanguage(MELLOW_LANG);

$PDO = null;
try {
	$PDO = new \PDO('mysql:host=localhost;dbname='.DB_NAME.';charset=utf8', DB_USER, DB_PWD);
	$PDO->exec('SET CHARACTER SET utf8');
	$PDO->exec('SET NAMES utf8');
	$PDO->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
} catch (PDOException $e) {
	die('Database connection failed');
}

\mellow\App::setPDO($PDO);

if (defined('DB_TRANSLATION_NAME') && defined('DB_TRANSLATION_USER') && defined('DB_TRANSLATION_PWD')) {
	try {
		$PDOT = new \PDO('mysql:host=localhost;dbname='.DB_TRANSLATION_NAME.';charset=utf8', DB_TRANSLATION_USER, DB_TRANSLATION_PWD);
		$PDOT->exec('SET CHARACTER SET utf8');
		$PDOT->exec('SET NAMES utf8');
		$PDOT->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
	} catch (PDOException $e) {
		die('Database connection for translations failed');
	}
} else {
	die('Missing Common/Translation Database configuration');
}
\mellow\App::$PDO_COMMON = $PDOT;

$USER_LEVEL = 0;
if (isset($_SESSION['goin']) && is_numeric($_SESSION['goin'])) {
	$USER_LEVEL = (int) $_SESSION['goin'];
}

\mellow\App::setUser(new \mellow\User($USER_LEVEL));
\mellow\Image::$FOLDER = __DIR__.'/mellow_internal/images/'.MELLOW_LANG.'/';
\mellow\Cache::$CACHE_FOLDER = __DIR__.'/mellow_internal/cache/'.MELLOW_LANG.'/';
\mellow\Image::$FOLDER_URL = '/mellow_internal/images/'.MELLOW_LANG.'/generated/';
\mellow\App::$api_routes['filebank'] = new \mellow\Filebank(__DIR__.'/mellow_internal/filebank/common/', __DIR__.'/mellow_internal/filebank/common/thumbs/');
if (!LIVE) {
	\mellow\Cache::$IS_CACHE_ON = false;
}

mellow\App::login();
