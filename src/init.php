<?php

//\mellow\App::$api_routes["variables"]=new \mellow\Variables();
\mellow\App::$api_routes['schema'] = new \mellow\Schema();
\mellow\App::$api_routes['adminbar'] = new \mellow\AdminBar();
\mellow\App::$api_routes['sitemap'] = new \mellow\Sitemap();
//\mellow\App::$api_routes["searchsettings"]=new \mellow\SearchSettings();

\mellow\App::$public_routes['redirect'] = new \mellow\RedirectToPage();
\mellow\App::$public_routes['file'] = new \mellow\Donwload();

$PDOT = \mellow\App::$PDO_COMMON;

\mellow\Filebank::init($PDOT);
\mellow\Translations::init($PDOT);

$STRUCTURE = new \stdClass();

$STRUCTURE->admin = static function () {
	return new \client\templates\Admin();
};
// $STRUCTURE->variables = static function () {
// 	return new \client\templates\Variables();
// };
// $STRUCTURE->adminsitemap = static function () {
//     return new \client\templates\AdminSitemap();
// };
// $STRUCTURE->adminmetatiedot = static function () {
//     return new \client\templates\AdminMetatiedot();
// };
$STRUCTURE->pagenotfound = static function () {
	return new \client\templates\PageNotFound();
};

$STRUCTURE->home = static function () {
	return new \client\templates\Home();
};

$STRUCTURE->dailydrama = static function () {
	return new \client\templates\DailyDrama();
};


$STRUCTURE->article = static function () {
	return new \client\templates\Article();
};

$STRUCTURE->country = static function () {
	return new \client\templates\Country();
};



$STRUCTURE->ourclients = static function () {
	return new \client\templates\OurClients();
};
$STRUCTURE->about = static function () {
	return new \client\templates\About();
};

$STRUCTURE->contact = static function () {
	return new \client\templates\Contact();
};
$STRUCTURE->privacypolicy = static function () {
	return new \client\templates\PrivacyPolicy();
};

$STRUCTURE->draamiainen=static function () {
	return new \client\templates\Draamiainen();
};
$STRUCTURE->sansdrama=static function () {
	return new \client\templates\SansDrama();
};
$STRUCTURE->namu=static function () {
	return new \client\templates\Namu();
};
$STRUCTURE->gaupgrade=static function () {
	return new \client\templates\GAUpgrade(['view' => 'ga-upgrade']);
};

$STRUCTURE->finnkinoda=static function () {
	return new \client\templates\FinnkinoDa(['view' => 'finnkino-da']);
};


/*$STRUCTURE->kitchen = static function () {
	return new \client\templates\Kitchen();
};

$STRUCTURE->maincategory = static function () {
	return new \client\templates\MainCategory();
};

$STRUCTURE->subcategory = static function () {
	return new \client\templates\SubCategory();
};
$STRUCTURE->simplecategory = static function () {
	return new \client\templates\SimpleCategory();
};

$STRUCTURE->product = static function () {
	return new \client\templates\Product();
};

$STRUCTURE->hidden = static function () {
	return new \client\templates\Hidden();
};

$STRUCTURE->simpleproduct = static function () {
	return new \client\templates\SimpleProduct();
};

$STRUCTURE->smallbanner = static function () {
	return new \client\templates\SmallBanner();
};

$STRUCTURE->news = static function () {
	return new \client\templates\News();
};

$STRUCTURE->article = static function () {
	return new \client\templates\Article();
};*/

\mellow\App::addStructure($STRUCTURE);
