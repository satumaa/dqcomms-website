#!/usr/bin/env bash
# Based on code from http://tech.zumba.com/2014/04/14/control-code-quality/
 
PROJECT=$(php -r "echo dirname(dirname(dirname(realpath('$0'))));")
STAGED_FILES_CMD=$(git diff --cached --name-only --diff-filter=ACMR HEAD | grep \\.php$ | grep -v vendor\/)
 
# Determine if a file list is passed
if [ "$#" -eq 1 ]
then
    oIFS=$IFS
    IFS='
    '
    SFILES="$1"
    IFS=$oIFS
fi
SFILES=${SFILES:-$STAGED_FILES_CMD}
 
# echo "Skipping PHP Lint..."
echo "Checking PHP Lint..."
for FILE in $SFILES
do
    php -l -d display_errors=0 $PROJECT/$FILE
    if [ $? != 0 ]
    then
        echo "Fix the error before commit."
        exit 1
    fi
    FILES="$FILES $PROJECT/$FILE"
done
 
if [ "$FILES" != "" ]
then
    echo "Running PHP CS Fixer..."

    # IFS='
    # '
    # CHANGED_FILES=$(git diff --name-only --diff-filter=ACMRTUXB "${COMMIT_RANGE}")
    # if ! echo "${CHANGED_FILES}" | grep -qE "^(\\.php_cs(\\.dist)?|composer\\.lock)$"; then EXTRA_ARGS=$(printf -- '--path-mode=intersection\n--\n%s' "${CHANGED_FILES}"); else EXTRA_ARGS=''; fi
    # vendor/bin/php-cs-fixer fix --config=.php_cs.dist -v --dry-run --stop-on-violation --using-cache=no ${EXTRA_ARGS}

    # TMP_DIR=tmp-php-cs-fixer/$(date +%Y%m%d%H%M%S)
    # mkdir -p $TMP_DIR
    # for FILE in $SFILES
    # do
    #     mkdir -p $TMP_DIR/$(dirname $FILE)
    #     git show :$FILE > $TMP_DIR/$FILE
    # done
    # ./src/vendor/bin/php-cs-fixer fix --config=./src/.php_cs.dist -v --dry-run --stop-on-violation --using-cache=no $TMP_DIR
    # --dry-run --stop-on-violation 
    git diff --cached --name-only --diff-filter=ACMR HEAD | grep \\.php$ | grep -v vendor\/ | xargs -n1 ./src/vendor/bin/php-cs-fixer fix --config=./src/.php_cs.dist --using-cache=no --show-progress=dots
    
    PHPCS_ERROR=$?
    rm -rf $TMP_DIR
    if [ $PHPCS_ERROR == 0 ] 
    then
        for FILE in $SFILES
        do
            echo "Readding $FILE";
            git add $FILE
        done
        #git diff --cached --name-only --diff-filter=ACMR HEAD | grep \\.php$ | grep -v vendor\/ | xargs git add
    else
        echo 'PHP CS Fixer found errors, please fix & re-add files before recommitting.'
    fi

    if [ $PHPCS_ERROR != 0 ]
    then
        echo $PHPCS_ERROR
        echo "Fix the CS error before commit."
        exit 1
    fi
fi
 
# if [ "$FILES" != "" ]
# then
#     echo "Running Code Sniffer..."

#     #command -v uuidgen >/dev/null 2>&1 || { echo >&2 "You require uuidgen but it's not installed. Install with 'apt install uuid-runtime' to correct. Aborting."; exit 1; }
#     TMP_DIR=tmp-phpcs/$(date +%Y%m%d%H%M%S)
#     mkdir -p $TMP_DIR
#     for FILE in $SFILES
#     do
#         mkdir -p $TMP_DIR/$(dirname $FILE)
#         git show :$FILE > $TMP_DIR/$FILE
#     done
#     ./src/vendor/bin/phpcs --ignore=*/src/vendor/*,*/vendor/*,vendor\/ --standard=PSR2 --encoding=utf-8 -n -p $TMP_DIR
#     PHPCS_ERROR=$?
#     rm -rf $TMP_DIR
#     if [ $PHPCS_ERROR != 0 ]
#     then
#         echo $PHPCS_ERROR
#         echo "Fix the Code Sniffer errors before commit."
#         exit 1
#     fi
# fi
 
exit $?