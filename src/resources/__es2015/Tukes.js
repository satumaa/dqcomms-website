class Tukes extends Screen{
	
	
	
	constructor(options){
		
		
		
		
		super(options);
		this.onDisplay={from:.4,to:.9};
		this.color="yellow";
		this.setOptions();
		this.debug=true;
	}
	
	setOptions(){
		let zoom=1.6;
		
		this.options.shapes=[
			new Shape({
				view_port_width:1200,
				width:120,
				height:462,	
				x:0,
				y:150,
				
				partChange:400,
				mask:new ImageLoader("/resources/svgs/3.png"),
				img:new ImageLoader("/resources/imgs/tukes1.jpg"),
			
			}),
			new Shape({
				view_port_width:1200,
				width:456*zoom,
				height:333*zoom,
				x:1200-456*zoom-100,
				partChange:400,
				y:+75,
				mask:new ImageLoader("/resources/svgs/4.png"),
				img:new ImageLoader("/resources/imgs/tukes2.jpg"),
			
			})
			
		];
		
		
		
	}
	

	
	
}