class TabView extends Utils {

	constructor(el) {
		super(el);

		this.settings = {
			hash: false
		}
		
		this.nav = this.el.querySelector(".tab-nav");
		this.build();
	}

	static setup(arr) {
		return super.setup(arr, TabView, "[data-tabview]");
	}

	build() {
		// Set click event
		$$("li", this.nav).forEach(i => {
			i.addEventListener("click", (e) => {
				this.onClickNav(i);
			});
		});

		// Open from hash
		let hash = window.location.hash.substring(1, window.location.hash.length);

		// if(this.settings.hash && hash && hash != "!") {
		if(this.settings.hash && hash) {
			const el = this.nav.querySelector("li[data-id='" + hash + "']");

			if(el) {
				this.select(el);
			}	
		}
		else {
			this.select(this.nav.querySelector("li"));
		}
		// }
	}

	onClickNav(el) {
		this.select(el);
	}

	select(el) {
		// Clear all
		$$(".tab-nav li", this.el).forEach(i => {
			i.classList.remove("m-active");
		});

		$$(".tab-content", this.el).forEach(i => {
			i.classList.remove("m-active");
		});

		// Select current nav item
		el.classList.add("m-active");

		// Select current content
		const id = el.getAttribute("data-id");

		const content = this.el.querySelector(".tab-content[data-id='" + id + "']");
		content.classList.add("m-active");

		// Set hash
		if(this.settings.hash) {
			location.hash = id;
		}

		// Dispatch event
		this.dispatchEvent("open", {detail: {id: id}});
	}
}