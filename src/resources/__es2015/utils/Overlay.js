class Overlay extends Utils {

	constructor(el) {
		super(el);

		this.btn = d("[data-overlay-button='" + this.el.getAttribute("data-overlay") + "']");
		this.build();
	}

	static setup(arr) {
		return super.setup(arr, Overlay, "[data-overlay]");
	}

	build() {
		const background = document.createElement("div");
		background.classList.add("background");
		this.el.appendChild(background);

		background.addEventListener("click", (e) => {
			this.close();
		});

		const close = this.el.querySelector(".close");

		close.addEventListener("click", (e) => {
			this.close();
		});

		if(this.btn) {
			this.btn.addEventListener("click", (e) => {
				e.preventDefault();

				this.open();
			});
		}
	}

	open() {
		d("html").classList.add("m-overlay-open");
		this.el.classList.add("m-active");

		this.dispatchEvent("open");
	}

	close() {
		d("html").classList.remove("m-overlay-open");
		this.el.classList.remove("m-active");

		this.dispatchEvent("close");
	}
}