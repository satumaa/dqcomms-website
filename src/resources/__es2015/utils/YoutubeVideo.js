class YoutubeVideo extends Utils {

	constructor(el) {
		super();

		this.el = el;
		
		this.build();
	}

	static setup(arr) {
		return super.setup(arr, YoutubeVideo, "[data-youtube]");
	}

	build() {
		const id = this.el.getAttribute("data-youtube");

		if(id) {
			const iframe = this.el.querySelector("iframe");

			iframe.src += id + "?rel=0";

			this.el.querySelector(".play").addEventListener("click", e => {
				this.el.classList.add("active");

				iframe.src += "&autoplay=1";
			});	
		}
	}
}