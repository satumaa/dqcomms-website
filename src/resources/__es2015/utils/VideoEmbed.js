class VideoEmbed extends Utils {

	constructor(el) {
		super(el);

		this.build();
	}

	static setup(arr) {
		return super.setup(arr, VideoEmbed, "[data-video-id]");
	}

	build() {
		const id = this.el.getAttribute("data-video-id");

		if(id) {
			const iframe = this.el.querySelector("iframe");

			iframe.src += id + "?rel=0";

			this.el.querySelector(".play").addEventListener("click", e => {
				this.el.classList.add("m-active");

				iframe.src += "&autoplay=1";
			});	
		}
	}
}