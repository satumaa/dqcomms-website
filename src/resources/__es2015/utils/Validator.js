class Validator {

	constructor(el, options) {
		this.options = options || {};
		this.form = el;
		this.errors = [];

		const submitEvent = options.submitEvent === undefined ? true : options.submitEvent;
		
		if(submitEvent) {
			this.form.addEventListener("submit", e => {
				this.validate(e);
			}, false);
		}
	}

	validate(e) {
		this.errors.length = 0;

		$$("[required]", this.form).forEach(i => {
			// Skip fields hat are not visible
			if((i.offsetParent !== null) == false) {
				return;
			}

			// Radio and checkbox
			if(i.getAttribute("type") == "radio" || i.getAttribute("type") == "checkbox") {
				let error = true;

				$$("input[name=" + i.getAttribute("name") + "]", this.form).forEach(function(i) {
					if(i.checked) {
						error = false;
					}
				});

				if(error) {
					this.addError(i, "data-required");
				}
			}
			// Default
			else if(i.value == "") {
				this.addError(i, "data-required");
			}

			// Select
			else if(i.tagName == "SELECT") {
				if(i.value == 0) {
					this.addError(i, "data-required");
				}
			}

			// Email
			if(i.hasAttribute("data-required-email")) {
				if(!/\S+@\S+\.\S+/.test(i.value)) {
					this.addError(i, "data-required-email");
				}
			}

			// Number
			if(i.hasAttribute("data-required-number")) {
				if(isNaN(i.value)) {
					this.addError(i, "data-required-number");
				}
			}
		});

		if(this.errors.length > 0) {
			if(e) {
				e.preventDefault();
			}

			if(typeof this.options.callback === "function") {
				this.options.callback(this.errors);
			}

			return false;
		}

		return true;
	}

	addError(target, id) {
		const obj = {
			name: target.getAttribute("name"),
			error: id,
			element: target,
			message: target.getAttribute("data-message")
		};

		this.errors.push(obj);
	}
}