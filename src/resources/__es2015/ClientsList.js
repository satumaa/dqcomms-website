class ClientsList {

	constructor(el) {
		this.el = el;
		this.nav = this.el.querySelector(".nav");
		this.build();
	}

	build() {
		$$(".nav .item", this.el).forEach(i => {
			i.addEventListener("mouseenter", e => {
				this.select(i);
			});

			i.addEventListener("touchstart", e => {
				this.select(i);
			});
		});

		this.nav.addEventListener("mouseleave", e => {
			this.clear();
		});
	}

	select(i) {
		let id = i.getAttribute("data-id");

		this.clear();
		console.log(this.el.querySelector("img[data-id='" + id + "']"));
		this.el.querySelector("img[data-id='" + id + "']").classList.add("m-active");
	}

	clear() {
		$$(".images img", this.el).forEach(j => {
			j.classList.remove("m-active");
		});
	}
}