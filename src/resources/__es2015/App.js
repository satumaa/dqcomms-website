class App {
	constructor(queue) {
		this.isReady = false;
		this.readyQueue = queue;
		this.mobile = false;
		this.windowWidth = window.innerWidth;
	}

	init() {
		d.ready(() => {
			// document.addEventListener("DOMContentLoaded", () => {
			this.isReady = true;
			this.start();
			this.processQueue();
		});
	}

	ready(fn) {
		if (this.isReady) {
			fn();
		} else {
			this.readyQueue.push(fn);
		}
	}

	processQueue() {
		while (this.readyQueue.length) {
			this.readyQueue.shift()();
		}
	}

	start() {
		// Check for mobile
		if (isMobile.phone || isMobile.tablet) {
			this.mobile = true;
			d("html").classList.add("mobile-device");
		}

		// Header
		// new Header();

		// Navigation
		new Navigation();

		// Scroller
		Scroller.setup();

		// VideoEmbed
		VideoEmbed.setup();

		// Share
		this.shareButton = d("#share-button");
		this.updateShareButton();

		window.addEventListener("resize", e => {
			const innerWidth = window.innerWidth;

			if (innerWidth != this.windowWidth) {
				this.updateShareButton();
				this.windowWidth = innerWidth;
			}
		});

		if (this.shareButton) {
			const shareOverlay = new Overlay(d("[data-overlay='share-overlay']"));

			if (shareOverlay) {
				this.shareButton.querySelector(".open").addEventListener("click", e => {
					shareOverlay.open();
					e.target.parentNode.classList.add("m-active");
				});

				this.shareButton
					.querySelector(".close")
					.addEventListener("click", e => {
						shareOverlay.close();
						e.target.parentNode.classList.remove("m-active");
					});
			}
		}
	}

	updateShareButton() {
		if (this.shareButton) {
			var height = window.innerHeight;
			this.shareButton.style.top = height * 0.5 + "px";
		}
	}
}

const d = Delfin;

app = new App(app.queue);
app.init();
