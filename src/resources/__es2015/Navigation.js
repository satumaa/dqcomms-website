class Navigation {

	constructor() {
		this.parentIsLink = true;
		this.showActiveMenu = true;
		
		this.build();
	}

	build() {
		d("#nav-toggle").addEventListener("click", function(e) {
			d("html").classList.toggle("m-nav-open");
			d("#main-nav").scrollTo(0,0);
		});

		d("#nav-close").addEventListener("click", function(e) {
			d("html").classList.remove("m-nav-open");
			d("#main-nav").scrollTo(0,0);
		});

		// Adds toggle buttons (+/-)
		this.toggleNavigation();

		// Adds clickable overlay
		// this.overlay();

		// Double tab
		// this.doubleTapToGo();
		// jQuery('#site-header .book .inner').doubleTapToGo();
	}

	toggleNavigation() {
		$$(".m-parent", this.el).forEach((i) => {
			d("a", i).insertAdjacentHTML("beforebegin", '<span class="toggle"><span>+</span><span>-</span></span>');
			
			// Open active menu
			if(i.classList.contains("m-active") && this.showActiveMenu) {
				d("ul", i).classList.add("m-visible");
				d(".toggle", i).classList.add("m-active");
			}

			// Parent is link?
			if(this.parentIsLink) {
				d(".toggle", i).addEventListener("click", function(e) {
					e.preventDefault();
					toggle(i, this);
				});
			}
			else {
				i.addEventListener("click", function(e) {
					e.preventDefault();
					toggle(i, this);
				});
			}

			var toggle = function(item, button) {
				d("ul", item).classList.toggle("m-visible");
				button.classList.toggle("m-active");

				// Close children
				$$(".m-parent", item).forEach(function(j) {
					d("ul", j).classList.remove("m-visible");
					d("toggle", j).classList.remove("m-active");
				});
			};
		});
	}

	overlay() {
		$$("#main-nav > ul > li.m-has-overlay > a").forEach(function(i) {
			const el = i.parentNode;

			// Close
			const close = document.createElement("div");
			close.classList.add("overlay-close");
			el.insertBefore(close, el.firstChild);
			
			close.addEventListener("click", function(e) {
				el.classList.remove("m-overlay-open");
				header.classList.remove("m-overlay-open");
			});

			// Nav clicked
			i.addEventListener("click", function(e) {
				if(!window.matchMedia("(min-width: " + $.breakpoints.medium + ")").matches) {
					return false;
				}

				e.preventDefault();
				e.stopPropagation();

				// Clear all opened navs
				if(!el.classList.contains("m-overlay-open")) {
					$$("#main-nav > ul > li.m-has-overlay").forEach(function(j) {
						j.classList.remove("m-overlay-open");
					});
				}

				// Toggle
				el.classList.toggle("m-overlay-open");
			});
		});
	}

	// doubleTapToGo(breakpoint) {
	// 	jQuery.fn.doubleTapToGo = function( breakpoint ) {
	// 		var breakpoint = breakpoint || 0;
			
	// 		if(!('ontouchstart' in window) && !navigator.msMaxTouchPoints && !navigator.userAgent.toLowerCase().match( /windows phone os 7/i )) {
	// 			return false;
	// 		}

	// 		this.each(function() {
	// 			var curItem = false;
				
	// 			jQuery(this).on("click", function(e) {
	// 				if(window.matchMedia("(min-width: " + breakpoint + ")").matches) {
	// 					var item = jQuery(this);
	// 					if(item[ 0 ] != curItem[ 0 ]) {
	// 						e.preventDefault();
	// 						curItem = item;
	// 					}
	// 				}
	// 			});

	// 			jQuery(document).on("click touchstart MSPointerDown", function(e) {
	// 				if(window.matchMedia("(min-width: " + breakpoint + ")").matches) {
	// 					var resetItem = true,
	// 						parents = jQuery(e.target).parents();

	// 					for(var i = 0; i < parents.length; i++) {
	// 						if(parents[ i ] == curItem[ 0 ]) {
	// 							resetItem = false;
	// 						}
	// 					}

	// 					if(resetItem) {
	// 						curItem = false;
	// 					}
	// 				}
	// 			});
	// 		});
			
	// 		return this;
	// 	};
	// }
}