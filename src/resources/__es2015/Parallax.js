class Parallax {

	constructor(el) {
		this.el = el;
		this.y = 0;
		this.current = 0;

		this.build();
	}

	build() {
		this.img = d(".c-hero img");

		window.addEventListener("scroll", e => {
			this.y = Math.floor((d.scrollTop() / 3));
		});

		this.update();
	}

	update() {
		// console.log("calc", this.y);
		if(this.y != this.current) {
			this.img.style.transform = "translateY(" + this.y + "px)";
			this.current = this.y;
			// console.log("update", this.y);
		}

		requestAnimationFrame(this.update.bind(this));
	}
}