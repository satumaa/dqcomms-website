class Cheerios {

	constructor() {
		
		this.container=document.querySelector('.cheerios')
		this.cheerios=document.querySelectorAll('.cheerio')

		this.width=window.innerWidth
		this.height=window.innerHeight
		this.update()
	
		window.addEventListener('resize',this.setWithHeight)
	}
	
	update(){
		this.cheerios.forEach((cheerio,index)=>{
			cheerio.loop=0
			if(index>=0 && index<4){
				this.animateFromTop(cheerio)
			}else if(index>=4 && index<8){
				this.animateFromLeft(cheerio)
			}else if(index>=8 && index<12){
				this.animateFromRight(cheerio)
			}else{
				this.animateFromButtom(cheerio)
			}
		})
	}

	animateFromTop(cheerio){

		if(cheerio.loop>10){
			return
		}

		const self=this
	
		// 	//x,y,speed,size
		const randoms=this.getRandoms(cheerio)
		cheerio.style.width=randoms[3]+'px'
		cheerio.style.position='abosolute'
		cheerio.style.left=randoms[0]+'px'
		
		if(cheerio.loop>0){
			cheerio.loop=0
			cheerio.style.left=randoms[0]+'px'
			cheerio.style.transform='translate('+randoms[0]+'px,'+ (-cheerio.clientHeight) + 'px)'
		}
		
		Core.animate(cheerio,randoms[2],{
			//transform:'translate('+randoms[0]+'px, '+ (cheerio.clientHeight+ this.height)+'px) rotate(100deg)',
			transform:'translate('+randoms[0]+'px, '+ (this.height+cheerio.clientHeight)+'px) rotate(100deg)',
			
			complete:function(){
				cheerio.loop=1
				self.animateFromTop(cheerio)
			}
		})
		
	}


	animateFromLeft(cheerio){
		
		if(cheerio.loop>10){
			return
		}

		const self=this

		// 	//x,y,speed,size
		const randoms=this.getRandoms(cheerio)
		cheerio.style.width=randoms[3]+'px'
		cheerio.style.position='abosolute'
		cheerio.style.left='0px'
		cheerio.style.top=randoms[1]+'px'

		if(cheerio.loop>0){
			cheerio.loop=0
			cheerio.style.top=randoms[1]+'px'
			cheerio.style.left=-cheerio.clientWidth+'px'
			cheerio.style.transform='translate('+(-cheerio.clientWidth)+'px,'+ randoms[1] + 'px)'
		}
		
		Core.animate(cheerio,randoms[2],{
			//transform:'translate('+randoms[0]+'px, '+ (cheerio.clientHeight+ this.height)+'px) rotate(100deg)',
			transform:'translate('+(this.width+cheerio.clientWidth)+'px, '+ randoms[1]+'px) rotate(100deg)',
			complete:function(){
				cheerio.loop=1
				self.animateFromLeft(cheerio)
			}
		})
	}

	animateFromRight(cheerio){
		
		if(cheerio.loop>10){
			return
		}

		const self=this
	
		// 	//x,y,speed,size
		const randoms=this.getRandoms(cheerio)
		cheerio.style.width=randoms[3]+'px'
		cheerio.style.position='abosolute'
		cheerio.style.right='0px'
		cheerio.style.top=randoms[1]+'px'

		if(cheerio.loop>0){
			cheerio.loop=0
			cheerio.style.top=randoms[1]+'px'
			cheerio.style.transform='translate('+(-this.width )+'px,'+ randoms[1] + 'px)'
		}
		
		Core.animate(cheerio,randoms[2],{
			transform:'translate('+(-this.width-cheerio.clientWidth)+'px, '+ (randoms[1])+'px) rotate(100deg)',
			complete:function(){
				cheerio.loop=1
				self.animateFromRight(cheerio)
			}
		})
		
	}
	animateFromButtom(cheerio){
		
		if(cheerio.loop>10){
			return
		}

		const self=this
	
		// 	//x,y,speed,size
		const randoms=this.getRandoms(cheerio)
		cheerio.style.width=randoms[3]+'px'
		cheerio.style.position='abosolute'
		cheerio.style.bottom='0px'
		cheerio.style.left=randoms[0]+'px'

		if(cheerio.loop>0){
			
			cheerio.loop=0
			cheerio.style.top=randoms[0]+'px'
			cheerio.style.bottom=-cheerio.clientHeight+'px'
			cheerio.style.transform='translate('+(randoms[0])+'px,'+ (this.height+cheerio.clientHeight) + 'px)'
		}
		
		Core.animate(cheerio,randoms[2],{
			transform:'translate('+randoms[0]+'px, '+ (-cheerio.clientHeight - this.height)+'px) rotate(100deg)',
			complete:function(){
				cheerio.loop=1
				self.animateFromButtom(cheerio)
			}
		})
		
	}

	getRandoms(){

		const randomXPosition=Math.floor(Math.random()*(this.width-0))+ 0
		const randomYPosition=Math.floor(Math.random()*(this.height-0))+ 0
		const randomSpeed=Math.floor(Math.random()*(80-20))+20
		const randomSize=Math.floor(Math.random()*(200-100))+100

		return [randomXPosition,randomYPosition,randomSpeed,randomSize]
	
	}

	setWithHeight(){
		this.width=window.innerWidth
		this.height=window.innerHeight
	}


}

new Cheerios();