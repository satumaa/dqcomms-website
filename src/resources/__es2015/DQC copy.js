class DQC{
	
		
	
	
		constructor(context){
			
			this.context=context;
			//this.video=new Video("video.mp4");
			var zoom=1.6;
			
			this.LAYOUT_WIDTH=1200;
			var zoom=(381/261);
			var op1={
				x:74,
				y:0,
				mask:new ImageLoader("/resources/svgs/1.svg"),
				img:new ImageLoader("/resources/imgs/headeri.jpg"),
				context:this.context,
				width:261*zoom,
				height:304*zoom,
				from:100,
				to:0, 
				div:{
					x:450,
					y:90,
					width:500,
					dom:document.getElementById("pl")
					
				},
				view_port_width:this.LAYOUT_WIDTH,
				
				reverse:true,
				debug:true,
			 	cy:0
			}; 
			
			var op2={ 
				debug:true,
				x:670,
					y:291,
				mask:new ImageLoader("/resources/svgs/2.svg"),
				img:new ImageLoader("/resources/imgs/auli.jpg"),
				context:this.context,
				width:231*zoom,
				height:191*zoom,
				from:100,
				to:100,
				view_port_width:this.LAYOUT_WIDTH,
				cy:100,
				reverse:true
			};
			
			var op3={
				debug:true,
				x:375,
					y:804,
				mask:new ImageLoader("/resources/svgs/3.svg"),
				img:new ImageLoader("/resources/imgs/tukes1.jpg"),
				context:this.context,
				width:75*zoom,
				height:289*zoom,
				from:200,
				to:300,
				cy:150,
				view_port_width:this.LAYOUT_WIDTH,
				div:{
					x:-200,
					y:123,
					width:200,
					dom:document.getElementById("tukes")
					
				},
			};
			
			var op4={
				debug:true,
				x:753,
					y:774,
				mask:new ImageLoader("/resources/svgs/1.svg"),
				img:new ImageLoader("/resources/imgs/tukes2.jpg"),
				context:this.context,
				width:261*zoom,
				height:304*zoom,
				from:200,
				to:400,
				cy:90,
				view_port_width:this.LAYOUT_WIDTH
			
			};
			
			var op5={
				x:74,
					y:1446,
				mask:new ImageLoader("/resources/svgs/3.svg"),
				img:new ImageLoader("/resources/imgs/absolut.jpg"),
				context:this.context,
				width:75*zoom,
				height:289*zoom,
				from:.41,
				to:.8,
				cy:190,
				view_port_width:this.LAYOUT_WIDTH
			};
			
			var op6={
				x:410,
				y:1446,
				mask:new ImageLoader("/resources/svgs/4.svg"),
				img:new ImageLoader("/resources/imgs/absolut2.jpg"),
				context:this.context,
				width:456*zoom,
				height:333*zoom,
				from:.39,
				to:.85,
				cy:160,
				view_port_width:this.LAYOUT_WIDTH,
				div:{
					x:-100,
					y:263,
					width:400,
					dom:document.getElementById("absolut")
					
				}
			};
			
			var op7={
				x:229,
				mask:new ImageLoader("/resources/svgs/3.svg"),
				context:this.context,
				width:75*zoom,
				height:289*zoom,
				from:.65,
				to:1,
				view_port_width:this.LAYOUT_WIDTH
			};
			
			var op8={
				x:74,
				mask:new ImageLoader("/resources/svgs/5.svg"),
				context:this.context,
				width:332,
				height:328,
				from:.8,
				to:1.2,
				view_port_width:this.LAYOUT_WIDTH
			};
			
			
			/*vvar op9={
				x:490,
				mask:new ImageLoader("svgs/6.svg"),
				context:this.context,
				width:260,
				height:221,
				from:.55,
				to:.65,
				view_port_width:this.LAYOUT_WIDTH
			};
			var op10={
				x:100,
				mask:new ImageLoader("svgs/3.svg"),
				context:this.context,
				width:75,
				height:289,
				from:.65,
				to:.85,
				view_port_width:this.LAYOUT_WIDTH
			};
			var op11={
				x:500,
				mask:new ImageLoader("svgs/7.svg"),
				context:this.context,
				width:174,
				height:169,
				from:.65,
				to:.85,
				view_port_width:this.LAYOUT_WIDTH
			};*/
			this.shapes=[
				new Shape(op1),
				new Shape(op2),
				new Shape(op3),
				new Shape(op4),
				new Shape(op5),
				new Shape(op6),
				new Shape(op7),
				new Shape(op8)
				/*new Shape(op9),
				new Shape(op10),
				new Shape(op11)*/
				//new Shape(0,context.canvas.height-380*zoom,context,"shape-1.svg",97,375,{from:0,to:1})
			/*	
				new Shape(1060-471*zoom,140,context,"shape-2.svg",471,344,{from:.11,to:.35}),
				
				new Shape(80,this.context.canvas.height-306*zoom,context,"shape-4.svg",244,306,{from:.38,to:.61})
				
				,new Shape(this.context.canvas.width-244*zoom,0,context,"shape-3.svg",244,306,{from:.32,to:.51})
				
				,new Shape(680,0,context,"shape-5.svg", 262,330,{from:.45,to:.56})*/
			
				
			];
			
			
			
			//console.log(this.context.canvas.width,244*zoom);
			
		}
	
		draw(position){
				
			
			
			this.context.clearRect(0,0,this.context.canvas.width,this.context.canvas.height);
			//this.context.save();
			
				//this.context.save();
				//
			if(position<0){
				
				position=0;
			}
			var h=1446+200;
			
			h*=position;
			
			
			
			
			//console.log(h);
			
			 for(var i in this.shapes){
				this.shapes[i].draw(h);
			}
			
			
			/*
			
			this.context.globalCompositeOperation = 'source-in';
			
				this.video.draw(this.context,position);
			
			this.context.restore();
			
			*/
			//this.shapes[1].draw(position);
			
			/*this.context.globalCompositeOperation = 'source-in';
			
			
			
			
			this.video.draw(this.context,position);
			this.context.restore();*/
			//this.shapes[0].draw();
			//this.shapes[1].draw();
		}
}