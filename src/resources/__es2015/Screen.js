class Screen{
	
	
	
	constructor(options){
		
		
		
		this.options=options;
		this.orginal_width=1200;
		this.orginal_height=695;
		this.color="blue";
		this.TARGET_TEST=1;
		this.CUR_TEST=1;
		this.allHeight=0;
	}
	
	getHeight(){
		
		//return this.getDrawAreaHeight();
		let ratio=this.options.context.canvas.width/this.orginal_width;
		return this.orginal_height*ratio;
	}
	
	setAllHeight(h){
		
		this.allHeight=h;
		
	}
	getBottom(){
		let scalar=this.options.context.canvas.width/this.orginal_width;
		
		let bottom=0;
		for(var i=0;i<this.options.shapes.length;i++){
			
			let shapeOptions=this.options.shapes[i].options;
			let shapeY=shapeOptions.y*scalar;
			let img_height=this.options.shapes[i].getHeight(scalar);
			
			bottom=Math.max(bottom,img_height+shapeY);
			
		}
		return bottom;
		
		
	}
	getDrawAreaHeight(){
		return this.options.context.canvas.height-100;
	}
	draw(y,canvas_rect){
		//console.log(this.allHeight);
		
	
		
		//nollasta lähtee...
		
		//console.log(position);
		//console.log(this.allHeight);
		//console.log(canvas_from_top);
		
		var canvas_from_top=canvas_rect.y || canvas_rect.top;
		
		
		var canvas_widthini=(canvas_rect.right-canvas_rect.left); 
		
		
		
		
		let scalar=this.options.context.canvas.width/this.orginal_width;
		
	
		let draw_area=this.getDrawAreaHeight();
		for(var i=0;i<this.options.shapes.length;i++){
			let shapeOptions=this.options.shapes[i].options;
			let shapeY=shapeOptions.y*scalar;
			
			
			
			//let scrollY=-this.allHeight*position;
			var scrollY=0;
			let mypositionY=shapeY+y+scrollY;
			
			
			let img_height=this.options.shapes[i].getHeight(scalar);
			
			
			let my_top_on_sceen=canvas_from_top+mypositionY;
			let my_bottom_on_sceen=my_top_on_sceen+img_height;
			
			
			
			if(my_bottom_on_sceen>=0){
			
			//console.log(mypositionY);
			
			
			
			let mycenterOnScreen=canvas_from_top+mypositionY+img_height/2;
				
				
			
				
				
				
				var ve=this.options.context.canvas.width;
				var screen_scalar=canvas_widthini/ve;
				
				
				var my_CH=img_height/2*screen_scalar;
				var MINUN_TOP=mypositionY*screen_scalar+canvas_from_top;
				
				
				var MINIMI=-img_height*screen_scalar;
				var MAKSIMI=V_HEIGHT;
				
				//var MINUN_TOP=MY_CENTER;
				var DIFF=MAKSIMI-MINIMI;
				
				var TEST=MINUN_TOP-MINIMI;
				TEST=TEST/DIFF;
				if(TEST<0){
					TEST=0;
				}
				if(TEST>1){
					TEST=1;
				}
				
				TEST=-1+2*TEST;
				
				
					
				this.TARGET_TEST=TEST;
				let diff=this.TARGET_TEST-this.CUR_TEST;
				
				
				this.CUR_TEST+=diff*0.08;
				
				
			this.CUR_TEST=this.TARGET_TEST;
				
				
			
				
					if(MINUN_TOP>MAKSIMI){
						
						//return false;
					}
				
					if(MINUN_TOP<MINIMI){
						
						//return false;
					}
			
				
				//console.log(MINUN_TOP,MINIMI,MAKSIMI);
				
				//console.log(shapeY*screen_scalar);
				
				//console.log(ve,canvas_rect.width);
				//console.log(my_top_on_sceen*scalar,window.innerHeight);
			
				
				
				
				
			//console.log(this.allHeight);
		//	console.log(mycenterOnScreen);
			//y-=(this.allHeight*position);
			//console.log(this.allHeight*position);
			
				
				let differense_from_center=mycenterOnScreen-draw_area/2;
				let diff_p=differense_from_center/(draw_area/2);
				//console.log(diff_p);
			
			
			let img=(this.options.shapes[i].get(0,scalar,this.CUR_TEST));
			//console.log(this.allHeight);
			this.options.context.drawImage(img,shapeOptions.x*scalar,shapeY+y+scrollY,img.width,img.height);
			
			
			}
			
			
			
		}
		
		
		return true;
		if(position>=this.onDisplay.from && position<=this.onDisplay.to){
			
			let max=this.onDisplay.to-this.onDisplay.from;
			let pos=this.onDisplay.to-position;
			
			let cur=1-pos/max;
			
			
			cur=-1+2*cur;
			cur*=-1;
			
			
		
		
		//y-=(this.getHeight()+this.allHeight)*position;
		
		//console.log(this.getHeight()+y);
		/*this.options.context.beginPath();
		this.options.context.rect(0,y,this.orginal_width*scalar,this.getDrawAreaHeight());
		this.options.context.fillStyle=this.color;
		
		this.options.context.fill();
		this.options.context.closePath();
		
		*/ 
			
		for(var i=0;i<this.options.shapes.length;i++){
			
			
			let img=(this.options.shapes[i].get(0,scalar));
			
			
			let shapeOptions=this.options.shapes[i].options;
			var shapeY=shapeOptions.y*scalar;
			
			var cy=y+shapeOptions.y*scalar+this.getHeight()/2-shapeOptions.height/2;
			
			
			var bottom=this.getHeight()+shapeOptions.y*scalar;
			var top=-shapeOptions.y*scalar-img.height;
			
			
			//console.log(-img.height);
			//console.log(bottom);
			var bottom=this.getHeight()+shapeY;
			
			var top=shapeOptions.y-(shapeOptions.y+img.height);
			
			//.5 kohdassa
			if(cur<0){
			//	cy=cur*-top;
				let diff=cy-top;
				
				cy=cy+diff*cur;
			}else if(cur>0){
				let diff=bottom-cy;
				cy=cy+diff*cur;
			}
			
			
			
			cy+=shapeOptions.partChange*cur;
			this.options.context.drawImage(img,shapeOptions.x*scalar,cy,img.width,img.height);
			
			
			
			
			
			
		}
		
		}
	}
	
	
	
	
}