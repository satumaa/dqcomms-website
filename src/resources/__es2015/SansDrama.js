var archContainer = document.querySelector(".c-arches");
var overlay = document.querySelector(".c-overlay");
var body = document.querySelector("body");

var attendUpdTimeout = false;

function openOverlay() {
	document.querySelector("html").classList.add("m-overlay-open");
	overlay.classList.add("m-active");
}
function closeOverlay() {
	document.querySelector("html").classList.remove("m-overlay-open");
	overlay.classList.remove("m-active");
}

function updateAttendSection() {
	document.querySelector(".success").classList.add("hidden");
	document.querySelector(".form").classList.add("hidden");
	document.querySelector(".login").classList.add("hidden");
	document.querySelector(".signout").classList.add("hidden");

	if (state.loggedIn == true) {
		document.querySelector(".login").classList.add("hidden");
		document.querySelector(".signout").classList.remove("hidden");

		if (state.hasEntry == true) {
			if (state && state.profile && state.profile.given_name) {
				document.querySelector(".success").innerHTML =
					"See you there, " + state.profile.given_name + "!";
			}

			document.querySelector(".form").classList.add("hidden");
			document.querySelector(".success").classList.remove("hidden");
		} else {
			document.querySelector(".success").classList.add("hidden");
			document.querySelector(".form").classList.remove("hidden");
		}
	} else {
		document.querySelector(".login").classList.remove("hidden");
	}
}

var attend = function(data) {
	Core.fetch("?attend=1", {
		method: "POST",
		data: data,
		callback: function(response) {
			if (response) {
				response = JSON.parse(response);
				if (response.success == true) {
					state.hasEntry = true;
					updateAttendSection();
				}
			}
		}
	});
};

document.querySelector("body").addEventListener("mousemove", function(e) {
	let width = window.innerWidth || document.documentElement.clientWidth;
	let height = window.innerHeight || document.documentElement.clientHeight;

	state.x = e.clientX / width - 0.5;
	state.y = 1 - e.clientY / height - 0.25;

	if (width > 640) {
		var tmp =
			"rotateY(" + state.x * 90 + "deg) rotateX(" + state.y * 45 + "deg)";
		document.querySelector(".sansdrama-logocontainer").style.transform = tmp;
	}
});

window.addEventListener("load", function() {
	var body = document.querySelector("body");
	body.classList.remove("loading");

	document
		.querySelector(".c-overlay .close")
		.addEventListener("click", closeOverlay);
	document
		.querySelector(".sansdrama-logocontainer")
		.addEventListener("click", openOverlay);
	document.querySelector(".info-link").addEventListener("click", openOverlay);
	document
		.querySelector("#googleSignOutButton")
		.addEventListener("click", _gs_signOut);

	document.querySelector(".sansdrama-logocontainer").style.transform =
		"rotateY(0deg) rotateX(0deg)";
	setTimeout(function() {
		document.querySelector(".rotater").classList.add("animate");
	}, 2000);

	var submit = document.querySelector(".submit");
	if (submit) {
		submit.addEventListener("click", attend);
	}
	updateAttendSection();
});
