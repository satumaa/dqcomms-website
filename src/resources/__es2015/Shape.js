class Shape{
	
	
	
	constructor(options){
		
		this.options=options;
		
	
		this.isloaded=true;
		
		
		this.canvas=document.createElement("canvas");
		//document.getElementsByTagName("body")[0].appendChild(this.canvas);
		this.canvas.width=this.options.width;
		this.canvas.height=this.options.height;
		
	}
	
	
	getHeight(scalar){
		return this.options.height*scalar;
	}
	get(position=0,scalar=1,diff_p=0){
		
		
		
		
		var ctx=this.canvas.getContext("2d");
		
		
		
		
		var width=this.options.width*scalar;
		var height=this.options.height*scalar;
		this.canvas.width=width;
		this.canvas.height=height;
		
		
		ctx.clearRect(0,0,width,height);
		
		ctx.globalCompositeOperation = 'source-over';
		
		
		ctx.drawImage(this.options.mask.data,0,0,width,height);
		
		
		var diff=position-this.options.from;
		var max=this.options.to-this.options.from;
		var p=diff/max;

		
		
		if(this.options.img && this.options.img.isloaded && this.options.img.data.height){
				ctx.globalCompositeOperation = 'source-in';
				var w=width;
				var r=this.options.img.data.width/this.options.img.data.height;
				
				var h=w/r;
				
				
				var ero=h-height;
			
				var position_y=-ero*position;
				if(this.options.reverse){
					position_y=-ero+ero*position;
					
				}
				let diff=(1+diff_p)/2;
				if(diff>1){
					
					diff=1;
				}
				if(diff<0){
					
					diff=0;
				}
				//console.log(height,h,ero);
				//position_y=ero*diff;
			
				ctx.drawImage(this.options.img.data,0,-ero*diff,w,h);
				
			}
		if(this.options.text){
			
			
		//	this.options.div.dom.style=this.getPositionStyle(this.options.div.x*scalar,this.options.div.y*scalar);
		}
		
		
		
		return ctx.canvas;
	}
	getPositionStyle(x,y){
		
		return 	"-ms-transform: translate("+x+"px, "+y+"px);-webkit-transform: translate("+x+"px,  "+y+"px);transform: translate("+x+"px,  "+y+"px);";
		
		
		
		
	}
	
	
}