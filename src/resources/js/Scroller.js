"use strict";

var Scroller = function () {
	function Scroller(el) {
		babelHelpers.classCallCheck(this, Scroller);

		this.el = el;

		this.build();
	}

	babelHelpers.createClass(Scroller, [{
		key: "build",
		value: function build() {
			var _this = this;

			this.scroll = this.el.querySelector(".scroller-inner");

			var width = this.el.offsetWidth;
			var scrollWidth = this.el.scrollWidth;

			if (!true) {
				width = -width;
			}

			this.el.classList.add("left");

			this.scroll.addEventListener("scroll", function () {
				_this.updateScrollPosition();
			});

			window.addEventListener("resize", function (e) {
				_this.updateScrollPosition();
			});

			this.updateScrollPosition();
		}
	}, {
		key: "updateScrollPosition",
		value: function updateScrollPosition() {
			var width = this.el.offsetWidth;
			var scrollWidth = this.scroll.scrollWidth;

			// console.log(width, scrollWidth, this.scroll.scrollLeft);

			if (this.scroll.scrollLeft == 0) {
				this.el.classList.add("left");
			} else {
				this.el.classList.remove("left");
			}

			if (this.scroll.scrollLeft >= scrollWidth - width) {
				this.el.classList.add("right");
			} else {
				if (scrollWidth - width > 0) {
					this.el.classList.remove("right");
				}
			}
		}
	}]);
	return Scroller;
}();
//# sourceMappingURL=Scroller.js.map
