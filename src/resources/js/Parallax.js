"use strict";

var Parallax = function () {
	function Parallax(el) {
		babelHelpers.classCallCheck(this, Parallax);

		this.el = el;
		this.y = 0;
		this.current = 0;

		this.build();
	}

	babelHelpers.createClass(Parallax, [{
		key: "build",
		value: function build() {
			var _this = this;

			this.img = d(".c-hero img");

			window.addEventListener("scroll", function (e) {
				_this.y = Math.floor(d.scrollTop() / 3);
			});

			this.update();
		}
	}, {
		key: "update",
		value: function update() {
			// console.log("calc", this.y);
			if (this.y != this.current) {
				this.img.style.transform = "translateY(" + this.y + "px)";
				this.current = this.y;
				// console.log("update", this.y);
			}

			requestAnimationFrame(this.update.bind(this));
		}
	}]);
	return Parallax;
}();
//# sourceMappingURL=Parallax.js.map
