"use strict";

var Component = function () {
	function Component(el) {
		babelHelpers.classCallCheck(this, Component);

		this.el = el;
	}

	babelHelpers.createClass(Component, [{
		key: "init",
		value: function init(settings) {
			if (settings) {
				for (var i in settings) {
					if (settings.hasOwnProperty(i)) {
						this.settings[i] = settings[i];
					}
				}
			}
		}
	}, {
		key: "addEventListener",
		value: function addEventListener(name, fn) {
			var _this = this;

			this.el.addEventListener(name, function (e) {
				fn.call(_this, e);
			});
		}
	}, {
		key: "dispatchEvent",
		value: function dispatchEvent(name, props) {
			this.el.dispatchEvent(new CustomEvent(name, props));
		}
	}], [{
		key: "setup",
		value: function setup(arr, ref, selector, settings) {
			arr = arr || document.querySelectorAll(selector);

			if (arr.length == 0) {
				return false;
			}

			var items = [];

			arr.forEach(function (i) {
				items.push(new ref(i, settings));
			});

			return items;
		}
	}]);
	return Component;
}();
//# sourceMappingURL=Component.js.map
