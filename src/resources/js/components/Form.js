"use strict";

var Form = function (_Component) {
	babelHelpers.inherits(Form, _Component);

	function Form(el, settings) {
		babelHelpers.classCallCheck(this, Form);

		var _this = babelHelpers.possibleConstructorReturn(this, (Form.__proto__ || Object.getPrototypeOf(Form)).call(this, el));

		_this.button = _this.el.querySelector("[data-submit]");
		_this.errorMessage = _this.el.querySelector("[data-fetch-error]");
		_this.successMessage = _this.el.querySelector("[data-success]");
		_this.formContent = _this.el.querySelector("[data-form-content]");
		_this.sending = false;

		_this.settings = {
			ajax: true
		};

		_this.init(settings);

		_this.build();
		return _this;
	}

	babelHelpers.createClass(Form, [{
		key: "build",
		value: function build() {
			var _this2 = this;

			// Validator options
			var options = {
				submitEvent: false,
				callback: function callback(errors) {
					// Loop through validation errors
					errors.forEach(function (i) {
						// console.log(i, i.element.closest("label"));
						i.element.closest(".field").classList.add("m-error");
					});

					// If errors, scroll to and focus first error
					if (errors.length > 0) {
						_this2.showMessage(errors[0].element);
					}
				}
			};

			this.validator = new Validator(this.el, options);

			// Live validation
			this.el.querySelectorAll("[required]").forEach(function (i) {
				// Validate field on change
				var type = i.getAttribute("type");

				if (type == "radio" || type == "checkbox" || type == "file") {
					i.addEventListener("change", function (e) {
						_this2.validateField(i);
					});
				} else {
					i.addEventListener("input", function (e) {
						_this2.validateField(i);
					});
				}
			});

			// Add submit event
			this.el.addEventListener("submit", function (e) {
				_this2.submitted = true;

				if (_this2.validator.validate()) {
					if (_this2.settings.ajax) {
						e.preventDefault();

						if (!_this2.sending) {
							_this2.send();
						}
					}
				} else {
					e.preventDefault();
				}
			}, false);
		}
	}, {
		key: "send",
		value: function send() {
			var _this3 = this;

			var url = this.el.getAttribute("data-action");
			var data = this.serialize();

			// Fetch
			var searchParams = Object.keys(data).map(function (key) {
				return encodeURIComponent(key) + '=' + encodeURIComponent(data[key]);
			}).join('&');

			fetch(url, {
				method: "POST",
				body: searchParams,
				headers: {
					"Content-Type": "application/x-www-form-urlencoded;charset=UTF-8"
				}
			}).then(function (response) {
				return response.json();
			}).then(function (data) {
				_this3.success(data);
			}).catch(function (err) {
				_this3.error(err);
			});

			// XMLHttpRequest
			/*Core.fetch(url, {
   	callback: this.success.bind(this),
   	errorCallback: this.error.bind(this),
   	method: "POST",
   	data: JSON.stringify(data),
   	headers: {"Content-Type":"application/x-www-form-urlencoded"},
   	dataType: "json" // json or text (default)
   });*/

			this.startLoader();
		}
	}, {
		key: "serialize",
		value: function serialize() {
			var elems = this.el.elements;
			var data = {};

			for (var i = 0; i < elems.length; i += 1) {
				var element = elems[i];
				var type = element.type;
				var name = element.name;

				if (!name) {
					continue;
				}

				switch (type) {
					case "checkbox":
						data[name] = element.checked ? element.value : 0;
						break;
					case "radio":
						if (element.checked) {
							data[name] = element.value;
						}
						break;
					default:
						data[name] = element.value;
						break;
				}
			}

			return data;
		}
	}, {
		key: "success",
		value: function success(data) {
			var _this4 = this;

			this.dispatchEvent("success");

			// Hide errors
			if (data.success) {
				if (this.errorMessage) {
					this.el.querySelector("[data-fetch-error]").style.display = "none";
				}

				// Hide form and show success
				if (this.formContent) {
					this.formContent.style.display = "none";
				}

				this.el.reset();

				if (this.successMessage) {
					this.showMessage(this.successMessage, true);
				}
			}
			// Back end validation errors
			else {
					var errors = [];

					// Loop through errors
					data.errors.forEach(function (i) {
						var el = _this4.el.querySelector("[name='" + i + "']");
						el.classList.add("m-error");
						el.closest("label").classList.add("m-error");

						errors.push(el);
					});

					// If errors, scroll to first error
					if (errors.length > 0) {
						this.showMessage(errors[0]);
					}
				}

			this.stopLoader();
		}
	}, {
		key: "error",
		value: function error(_error) {
			console.error(_error);

			if (this.errorMessage) {
				this.showMessage(this.el.querySelector("[data-fetch-error]"), true);
			}

			this.dispatchEvent("error");
			this.stopLoader();
		}
	}, {
		key: "showMessage",
		value: function showMessage(el, show) {
			if (show) {
				el.style.display = "block";
			}

			// Scroll offset
			// let offset = -30;
			var offset = -window.innerHeight / 4;
			var top = el.getBoundingClientRect().top;

			if (top < -offset || top > window.innerHeight) {
				Jump(el, { duration: 500, offset: offset, callback: function callback() {
						el.focus();
					} });
			} else {
				el.focus();
			}
		}
	}, {
		key: "validateField",
		value: function validateField(i) {
			if (this.submitted) {
				if (!this.validator.validateField(i)) {
					i.closest(".field").classList.add("m-error");
				} else {
					i.closest(".field").classList.remove("m-error");
				}
			}
		}
	}, {
		key: "startLoader",
		value: function startLoader() {
			this.sending = true;
			this.el.classList.add("m-sending");
			this.button.querySelector(".c-loader").classList.add("m-active");
		}
	}, {
		key: "stopLoader",
		value: function stopLoader() {
			this.sending = false;
			this.el.classList.remove("m-sending");
			this.button.querySelector(".c-loader").classList.remove("m-active");
		}
	}], [{
		key: "setup",
		value: function setup(arr) {
			return babelHelpers.get(Form.__proto__ || Object.getPrototypeOf(Form), "setup", this).call(this, arr, Form, "[data-form]");
		}
	}]);
	return Form;
}(Component);
//# sourceMappingURL=Form.js.map
