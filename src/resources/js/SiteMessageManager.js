"use strict";

var SiteMessageManager = function () {
	function SiteMessageManager(el) {
		var _this = this;

		babelHelpers.classCallCheck(this, SiteMessageManager);

		this.el = el;
		this.header = d("#site-header");

		this.update();

		window.addEventListener("resize", function (e) {
			_this.update();
		});

		window.onload = function () {
			_this.update();
		};
	}

	babelHelpers.createClass(SiteMessageManager, [{
		key: "update",
		value: function update() {
			var messageHeight = this.el.offsetHeight - 1;
			var headerHeight = this.header.offsetHeight - 1;

			// this.header.style.top = messageHeight + "px";
			// d("body").style.paddingTop = messageHeight + headerHeight + "px";
		}
	}]);
	return SiteMessageManager;
}();
//# sourceMappingURL=SiteMessageManager.js.map
