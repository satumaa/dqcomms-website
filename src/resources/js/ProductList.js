"use strict";

var ProductList = function () {
	function ProductList() {
		babelHelpers.classCallCheck(this, ProductList);

		this.build();
	}

	babelHelpers.createClass(ProductList, [{
		key: "build",
		value: function build() {
			// Color links
			$$(".Product-list .Clickable span").forEach(function (i) {
				i.addEventListener("click", function (e) {
					e.stopPropagation();
					e.preventDefault();

					window.location.href = window.location.protocol + "//" + window.location.host + i.closest("a").getAttribute("href") + "#" + i.getAttribute("data-hash");
				});
			});
		}
	}]);
	return ProductList;
}();
//# sourceMappingURL=ProductList.js.map
