"use strict";

var Screen = function () {
	function Screen(options) {
		babelHelpers.classCallCheck(this, Screen);


		this.options = options;
		this.orginal_width = 1200;
		this.orginal_height = 695;
		this.color = "blue";
		this.TARGET_TEST = 1;
		this.CUR_TEST = 1;
		this.allHeight = 0;
	}

	babelHelpers.createClass(Screen, [{
		key: "getHeight",
		value: function getHeight() {

			//return this.getDrawAreaHeight();
			var ratio = this.options.context.canvas.width / this.orginal_width;
			return this.orginal_height * ratio;
		}
	}, {
		key: "setAllHeight",
		value: function setAllHeight(h) {

			this.allHeight = h;
		}
	}, {
		key: "getBottom",
		value: function getBottom() {
			var scalar = this.options.context.canvas.width / this.orginal_width;

			var bottom = 0;
			for (var i = 0; i < this.options.shapes.length; i++) {

				var shapeOptions = this.options.shapes[i].options;
				var shapeY = shapeOptions.y * scalar;
				var img_height = this.options.shapes[i].getHeight(scalar);

				bottom = Math.max(bottom, img_height + shapeY);
			}
			return bottom;
		}
	}, {
		key: "getDrawAreaHeight",
		value: function getDrawAreaHeight() {
			return this.options.context.canvas.height - 100;
		}
	}, {
		key: "draw",
		value: function draw(y, canvas_rect) {
			//console.log(this.allHeight);


			//nollasta lähtee...

			//console.log(position);
			//console.log(this.allHeight);
			//console.log(canvas_from_top);

			var canvas_from_top = canvas_rect.y || canvas_rect.top;

			var canvas_widthini = canvas_rect.right - canvas_rect.left;

			var scalar = this.options.context.canvas.width / this.orginal_width;

			var draw_area = this.getDrawAreaHeight();
			for (var i = 0; i < this.options.shapes.length; i++) {
				var shapeOptions = this.options.shapes[i].options;
				var _shapeY = shapeOptions.y * scalar;

				//let scrollY=-this.allHeight*position;
				var scrollY = 0;
				var mypositionY = _shapeY + y + scrollY;

				var img_height = this.options.shapes[i].getHeight(scalar);

				var my_top_on_sceen = canvas_from_top + mypositionY;
				var my_bottom_on_sceen = my_top_on_sceen + img_height;

				if (my_bottom_on_sceen >= 0) {

					//console.log(mypositionY);


					var mycenterOnScreen = canvas_from_top + mypositionY + img_height / 2;

					var ve = this.options.context.canvas.width;
					var screen_scalar = canvas_widthini / ve;

					var my_CH = img_height / 2 * screen_scalar;
					var MINUN_TOP = mypositionY * screen_scalar + canvas_from_top;

					var MINIMI = -img_height * screen_scalar;
					var MAKSIMI = V_HEIGHT;

					//var MINUN_TOP=MY_CENTER;
					var DIFF = MAKSIMI - MINIMI;

					var TEST = MINUN_TOP - MINIMI;
					TEST = TEST / DIFF;
					if (TEST < 0) {
						TEST = 0;
					}
					if (TEST > 1) {
						TEST = 1;
					}

					TEST = -1 + 2 * TEST;

					this.TARGET_TEST = TEST;
					var diff = this.TARGET_TEST - this.CUR_TEST;

					this.CUR_TEST += diff * 0.08;

					this.CUR_TEST = this.TARGET_TEST;

					if (MINUN_TOP > MAKSIMI) {

						//return false;
					}

					if (MINUN_TOP < MINIMI) {}

					//return false;


					//console.log(MINUN_TOP,MINIMI,MAKSIMI);

					//console.log(shapeY*screen_scalar);

					//console.log(ve,canvas_rect.width);
					//console.log(my_top_on_sceen*scalar,window.innerHeight);


					//console.log(this.allHeight);
					//	console.log(mycenterOnScreen);
					//y-=(this.allHeight*position);
					//console.log(this.allHeight*position);


					var differense_from_center = mycenterOnScreen - draw_area / 2;
					var diff_p = differense_from_center / (draw_area / 2);
					//console.log(diff_p);


					var img = this.options.shapes[i].get(0, scalar, this.CUR_TEST);
					//console.log(this.allHeight);
					this.options.context.drawImage(img, shapeOptions.x * scalar, _shapeY + y + scrollY, img.width, img.height);
				}
			}

			return true;
			if (position >= this.onDisplay.from && position <= this.onDisplay.to) {

				var max = this.onDisplay.to - this.onDisplay.from;
				var pos = this.onDisplay.to - position;

				var cur = 1 - pos / max;

				cur = -1 + 2 * cur;
				cur *= -1;

				//y-=(this.getHeight()+this.allHeight)*position;

				//console.log(this.getHeight()+y);
				/*this.options.context.beginPath();
    this.options.context.rect(0,y,this.orginal_width*scalar,this.getDrawAreaHeight());
    this.options.context.fillStyle=this.color;
    
    this.options.context.fill();
    this.options.context.closePath();
    
    */

				for (var i = 0; i < this.options.shapes.length; i++) {

					var _img = this.options.shapes[i].get(0, scalar);

					var _shapeOptions = this.options.shapes[i].options;
					var shapeY = _shapeOptions.y * scalar;

					var cy = y + _shapeOptions.y * scalar + this.getHeight() / 2 - _shapeOptions.height / 2;

					var bottom = this.getHeight() + _shapeOptions.y * scalar;
					var top = -_shapeOptions.y * scalar - _img.height;

					//console.log(-img.height);
					//console.log(bottom);
					var bottom = this.getHeight() + shapeY;

					var top = _shapeOptions.y - (_shapeOptions.y + _img.height);

					//.5 kohdassa
					if (cur < 0) {
						//	cy=cur*-top;
						var _diff = cy - top;

						cy = cy + _diff * cur;
					} else if (cur > 0) {
						var _diff2 = bottom - cy;
						cy = cy + _diff2 * cur;
					}

					cy += _shapeOptions.partChange * cur;
					this.options.context.drawImage(_img, _shapeOptions.x * scalar, cy, _img.width, _img.height);
				}
			}
		}
	}]);
	return Screen;
}();
//# sourceMappingURL=Screen.js.map
