"use strict";

var ClientsList = function () {
	function ClientsList(el) {
		babelHelpers.classCallCheck(this, ClientsList);

		this.el = el;
		this.nav = this.el.querySelector(".nav");
		this.build();
	}

	babelHelpers.createClass(ClientsList, [{
		key: "build",
		value: function build() {
			var _this = this;

			$$(".nav .item", this.el).forEach(function (i) {
				i.addEventListener("mouseenter", function (e) {
					_this.select(i);
				});

				i.addEventListener("touchstart", function (e) {
					_this.select(i);
				});
			});

			this.nav.addEventListener("mouseleave", function (e) {
				_this.clear();
			});
		}
	}, {
		key: "select",
		value: function select(i) {
			var id = i.getAttribute("data-id");

			this.clear();
			console.log(this.el.querySelector("img[data-id='" + id + "']"));
			this.el.querySelector("img[data-id='" + id + "']").classList.add("m-active");
		}
	}, {
		key: "clear",
		value: function clear() {
			$$(".images img", this.el).forEach(function (j) {
				j.classList.remove("m-active");
			});
		}
	}]);
	return ClientsList;
}();
//# sourceMappingURL=ClientsList.js.map
