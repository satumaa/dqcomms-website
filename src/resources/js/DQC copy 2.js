"use strict";

var DQC = function () {
	function DQC(context) {
		babelHelpers.classCallCheck(this, DQC);


		this.context = context;
		//this.video=new Video("video.mp4");
		var zoom = 1.6;

		this.LAYOUT_WIDTH = 1200;
		var zoom = 381 / 261;
		var op1 = {
			x: 74,
			mask: new ImageLoader("/resources/svgs/1.svg"),
			img: new ImageLoader("/resources/imgs/headeri.jpg"),
			context: this.context,
			width: 261 * zoom,
			height: 304 * zoom,
			from: -.7,
			to: .35,
			div: {
				x: 450,
				y: 90,
				width: 500,
				dom: document.getElementById("pl")

			},
			view_port_width: this.LAYOUT_WIDTH,

			reverse: true,
			debug: true
		};

		var op2 = {
			x: 670,
			mask: new ImageLoader("/resources/svgs/2.svg"),
			img: new ImageLoader("/resources/imgs/auli.jpg"),
			context: this.context,
			width: 231 * zoom,
			height: 191 * zoom,
			from: -.2,
			to: .39,
			view_port_width: this.LAYOUT_WIDTH,

			reverse: true
		};

		var op3 = {
			x: 375,
			mask: new ImageLoader("/resources/svgs/3.svg"),
			img: new ImageLoader("/resources/imgs/tukes1.jpg"),
			context: this.context,
			width: 75 * zoom,
			height: 289 * zoom,
			from: .22,
			to: .50,
			view_port_width: this.LAYOUT_WIDTH,
			div: {
				x: -200,
				y: 123,
				width: 200,
				dom: document.getElementById("tukes")

			}
		};

		var op4 = {
			x: 753,
			mask: new ImageLoader("/resources/svgs/1.svg"),
			img: new ImageLoader("/resources/imgs/tukes2.jpg"),
			context: this.context,
			width: 261 * zoom,
			height: 304 * zoom,
			from: 0.20,
			to: .62,
			view_port_width: this.LAYOUT_WIDTH

		};

		var op5 = {
			x: 74,
			mask: new ImageLoader("/resources/svgs/3.svg"),
			img: new ImageLoader("/resources/imgs/absolut.jpg"),
			context: this.context,
			width: 75 * zoom,
			height: 289 * zoom,
			from: .41,
			to: 1.9,
			view_port_width: this.LAYOUT_WIDTH
		};

		var op6 = {
			x: 410,
			mask: new ImageLoader("/resources/svgs/4.svg"),
			img: new ImageLoader("/resources/imgs/absolut2.jpg"),
			context: this.context,
			width: 456 * zoom,
			height: 333 * zoom,
			from: .39,
			to: 1.5,
			view_port_width: this.LAYOUT_WIDTH,
			div: {
				x: -100,
				y: 263,
				width: 400,
				dom: document.getElementById("absolut")

			}
		};

		/*	var op7={
  		x:0,
  		mask:new ImageLoader("svgs/3.svg"),
  		context:this.context,
  		width:75*zoom,
  		height:289*zoom,
  		from:.70,
  		to:.1,
  		view_port_width:this.LAYOUT_WIDTH
  	};
  	
  	var op8={
  		x:0,
  		mask:new ImageLoader("svgs/5.svg"),
  		context:this.context,
  		width:332,
  		height:328,
  		from:.55,
  		to:.65,
  		view_port_width:this.LAYOUT_WIDTH
  	};
  	var op9={
  		x:490,
  		mask:new ImageLoader("svgs/6.svg"),
  		context:this.context,
  		width:260,
  		height:221,
  		from:.55,
  		to:.65,
  		view_port_width:this.LAYOUT_WIDTH
  	};
  	var op10={
  		x:100,
  		mask:new ImageLoader("svgs/3.svg"),
  		context:this.context,
  		width:75,
  		height:289,
  		from:.65,
  		to:.85,
  		view_port_width:this.LAYOUT_WIDTH
  	};
  	var op11={
  		x:500,
  		mask:new ImageLoader("svgs/7.svg"),
  		context:this.context,
  		width:174,
  		height:169,
  		from:.65,
  		to:.85,
  		view_port_width:this.LAYOUT_WIDTH
  	};*/
		this.shapes = [new Shape(op1), new Shape(op2), new Shape(op3), new Shape(op4), new Shape(op5), new Shape(op6)];

		//console.log(this.context.canvas.width,244*zoom);
	}

	babelHelpers.createClass(DQC, [{
		key: "draw",
		value: function draw(position) {

			this.context.clearRect(0, 0, this.context.canvas.width, this.context.canvas.height);
			//this.context.save();

			//this.context.save();


			for (var i in this.shapes) {
				this.shapes[i].draw(position);
			}

			/*
   
   this.context.globalCompositeOperation = 'source-in';
   
   	this.video.draw(this.context,position);
   
   this.context.restore();
   
   */
			//this.shapes[1].draw(position);

			/*this.context.globalCompositeOperation = 'source-in';
   
   
   
   
   this.video.draw(this.context,position);
   this.context.restore();*/
			//this.shapes[0].draw();
			//this.shapes[1].draw();
		}
	}]);
	return DQC;
}();
//# sourceMappingURL=DQC copy 2.js.map
