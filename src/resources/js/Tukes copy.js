"use strict";

var Tukes = function (_Screen) {
	babelHelpers.inherits(Tukes, _Screen);

	function Tukes(options) {
		babelHelpers.classCallCheck(this, Tukes);


		var zoom = 1.6;
		options.shapes = [new Shape({
			view_port_width: 1200,
			width: 120,
			height: 462,
			x: 75,
			y: 0,
			partChange: 400,
			mask: new ImageLoader("/resources/svgs/3.svg"),
			img: new ImageLoader("/resources/imgs/tukes1.jpg")

		}), new Shape({
			view_port_width: 1200,
			width: 456 * zoom,
			height: 333 * zoom,
			x: 413,
			partChange: 400,
			y: 200,
			mask: new ImageLoader("/resources/svgs/4.svg"),
			img: new ImageLoader("/resources/imgs/auli.jpg")

		})];

		var _this = babelHelpers.possibleConstructorReturn(this, (Tukes.__proto__ || Object.getPrototypeOf(Tukes)).call(this, options));

		_this.onDisplay = { from: .4, to: .9 };
		_this.color = "yellow";
		return _this;
	}

	return Tukes;
}(Screen);
//# sourceMappingURL=Tukes copy.js.map
