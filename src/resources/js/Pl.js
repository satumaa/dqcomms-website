"use strict";

var Pl = function (_Screen) {
	babelHelpers.inherits(Pl, _Screen);

	function Pl(options) {
		babelHelpers.classCallCheck(this, Pl);


		options.shapes = [new Shape({
			view_port_width: 1200,
			width: 418,
			height: 486,
			x: 0,
			y: 0,
			partChange: 300,
			mask: new ImageLoader("/resources/svgs/1.png"),
			img: new ImageLoader("/resources/imgs/headeri.jpg")

		}), new Shape({
			view_port_width: 1200,
			width: 370,
			height: 306,
			x: 670,
			y: 300,
			partChange: 200,
			mask: new ImageLoader("/resources/svgs/2.png"),
			img: new ImageLoader("/resources/imgs/auli.jpg")

		})];

		var _this = babelHelpers.possibleConstructorReturn(this, (Pl.__proto__ || Object.getPrototypeOf(Pl)).call(this, options));

		_this.onDisplay = { from: -.5, to: .2 };

		return _this;
	}

	return Pl;
}(Screen);
//# sourceMappingURL=Pl.js.map
