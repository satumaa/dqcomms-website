"use strict";

var LazyLoader = function () {
	function LazyLoader() {
		var _this = this;

		babelHelpers.classCallCheck(this, LazyLoader);

		this.assets = [];

		// this.update();

		window.addEventListener("scroll", function (e) {
			_this.update();
		});

		window.addEventListener("resize", function (e) {
			_this.update();
		});
	}

	babelHelpers.createClass(LazyLoader, [{
		key: "update",
		value: function update() {
			this.assets.forEach(function (i) {
				if (i.loaded) {
					return;
				}

				var top = i.element.el.getBoundingClientRect().top;

				if (top + i.offset < window.innerHeight) {
					i.element.loadImage();
					i.loaded = true;
				}
			});
		}
	}, {
		key: "load",
		value: function load(el, options) {
			var _this2 = this;

			var options = options || {};
			var offset = options.offset === undefined ? 0 : options.offset;

			if (Array.isArray(el)) {
				el.forEach(function (i) {
					_this2.addAsset(i, offset);
				});
			} else {
				this.addAsset(el, offset);
			}

			this.update();
		}
	}, {
		key: "addAsset",
		value: function addAsset(el, offset) {
			var obj = {};
			obj.element = el;
			obj.loaded = false;
			obj.offset = offset;

			this.assets.push(obj);
		}
	}]);
	return LazyLoader;
}();
//# sourceMappingURL=LazyLoader.js.map
