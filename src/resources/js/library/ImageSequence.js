"use strict";

var ImageSequence = function () {
	function ImageSequence() {
		babelHelpers.classCallCheck(this, ImageSequence);

		this.frames = 12;
		this.currentFrame = 0;
		this.interval = null;
		this.frameRate = 30;
	}

	babelHelpers.createClass(ImageSequence, [{
		key: "start",
		value: function start() {
			if (!this.interval) {
				this.interval = setInterval(this.update.bind(this), 1000 / this.frameRate);
			}
		}
	}, {
		key: "stop",
		value: function stop() {
			if (this.interval) {
				clearInterval(this.interval);
			}

			this.interval = null;
		}
	}, {
		key: "update",
		value: function update() {
			if (this.currentFrame == this.frames) {
				$$("#loader .frame").forEach(function (i) {
					i.style.display = "none";
				});

				this.currentFrame = 0;
			}

			Delfin("#loader .frame" + ++this.currentFrame).style.display = "block";
		}
	}]);
	return ImageSequence;
}();
//# sourceMappingURL=ImageSequence.js.map
