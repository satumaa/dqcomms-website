"use strict";

var Scroller = function (_Library) {
	babelHelpers.inherits(Scroller, _Library);

	function Scroller(el) {
		babelHelpers.classCallCheck(this, Scroller);

		var _this = babelHelpers.possibleConstructorReturn(this, (Scroller.__proto__ || Object.getPrototypeOf(Scroller)).call(this));

		_this.el = el;
		_this.target = el.querySelector(".inner");
		_this.mouseTarget = el.querySelector(".inner > div");
		_this.internalScrollLeft = 0;
		_this.pressed = false;
		_this.prev = _this.el.querySelector(".prev");
		_this.next = _this.el.querySelector(".next");
		_this.build();
		return _this;
	}

	babelHelpers.createClass(Scroller, [{
		key: "build",
		value: function build() {
			var _this2 = this;

			this.scroll = this.el.querySelector(".inner");

			this.el.classList.add("left");

			this.scroll.addEventListener("scroll", function () {
				_this2.updateScrollPosition();
			});

			window.addEventListener("resize", function (e) {
				_this2.updateScrollPosition();
			});

			this.updateScrollPosition();

			// Dragging
			if (!app.mobile) {
				this.mouseTarget.addEventListener("mousedown", function (e) {
					_this2.mousedown(e);
					e.preventDefault();
					e.stopPropagation();
				});

				document.addEventListener("mousemove", function (e) {
					_this2.mousemove(e);
				});

				document.addEventListener("mouseup", function (e) {
					_this2.mouseup(e);
				});

				// this.animate();

				this.target.addEventListener("mousedown", function (e) {
					_this2.active = false;
				});
			}

			// Navigation
			this.prev.addEventListener("click", function (e) {
				e.preventDefault();
				e.stopPropagation();

				var t = _this2;

				// this.target.scrollLeft -= this.el.offsetWidth;

				jQuery(_this2.target).animate({
					scrollLeft: t.target.scrollLeft - t.el.offsetWidth
				}, 300);
			});

			this.next.addEventListener("click", function (e) {
				e.preventDefault();
				e.stopPropagation();

				var t = _this2;

				jQuery(_this2.target).animate({
					scrollLeft: t.target.scrollLeft + t.el.offsetWidth
				}, 300);

				// this.target.scrollLeft += this.el.offsetWidth;
			});
		}
	}, {
		key: "updateScrollPosition",
		value: function updateScrollPosition() {
			var width = this.el.offsetWidth;
			var scrollWidth = this.scroll.scrollWidth;

			if (this.scroll.scrollLeft == 0) {
				this.el.classList.add("left");
			} else {
				this.el.classList.remove("left");
			}

			if (this.scroll.scrollLeft >= scrollWidth - width) {
				this.el.classList.add("right");
			} else {
				if (scrollWidth - width > 0) {
					this.el.classList.remove("right");
				}
			}
		}
	}, {
		key: "mousedown",
		value: function mousedown(e) {
			this.startX = e.clientX;
			this.xPos = 0;
			this.startScroll = this.target.scrollLeft;
			this.active = true;
			this.pressed = true;
		}
	}, {
		key: "mousemove",
		value: function mousemove(e) {
			if (this.pressed) {
				this.xPos = e.clientX - this.startX;
			}
		}
	}, {
		key: "mouseup",
		value: function mouseup(e) {
			e.preventDefault();
			this.pressed = false;
		}
	}, {
		key: "animate",
		value: function animate() {
			requestAnimationFrame(this.animate.bind(this));

			if (this.active) {
				var targetLeft = this.startScroll - this.xPos;
				var currentLeft = this.target.scrollLeft;
				var temp = (targetLeft - currentLeft) * 0.05;

				//if movement is small enough and mouse is no longer being pressed
				if (Math.abs(temp) < 0.1 && !this.pressed) {
					this.active = false;
				}
				this.target.scrollLeft += temp;
			}
		}
	}], [{
		key: "setup",
		value: function setup(arr) {
			return babelHelpers.get(Scroller.__proto__ || Object.getPrototypeOf(Scroller), "setup", this).call(this, arr, Scroller, "[data-scroller]");
		}
	}]);
	return Scroller;
}(Library);
//# sourceMappingURL=Scroller.js.map
