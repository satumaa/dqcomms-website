"use strict";

var Toggleable = function (_Library) {
	babelHelpers.inherits(Toggleable, _Library);

	function Toggleable(el) {
		babelHelpers.classCallCheck(this, Toggleable);

		var _this = babelHelpers.possibleConstructorReturn(this, (Toggleable.__proto__ || Object.getPrototypeOf(Toggleable)).call(this));

		_this.el = el;

		_this.build();
		return _this;
	}

	babelHelpers.createClass(Toggleable, [{
		key: "build",
		value: function build() {
			$$("li", this.el).forEach(function (j) {
				// if(i.toggleAttached !== true) {
				// i.toggleAttached = true;
				j.querySelector(".title").addEventListener("click", function (e) {
					// this.parentNode.classList.toggle("toggleable__item--active");
					var parent = this.closest("li");
					parent.classList.toggle("active");
					// Toggler.toggle(this.parentNode);
				});

				new Toggleable(j);
				// }
			});
		}
	}], [{
		key: "setup",
		value: function setup(arr) {
			return babelHelpers.get(Toggleable.__proto__ || Object.getPrototypeOf(Toggleable), "setup", this).call(this, arr, Toggleable, "[data-toggleable]");
		}
	}]);
	return Toggleable;
}(Library);
//# sourceMappingURL=Toggleable.js.map
