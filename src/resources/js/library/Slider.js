"use strict";

var Slider = function () {
	function Slider() {
		babelHelpers.classCallCheck(this, Slider);

		this.active = false;
		this.slider = Delfin("#slider");
		this.handle = Delfin(".handle", this.slider);

		this.build();
	}

	babelHelpers.createClass(Slider, [{
		key: "build",
		value: function build() {
			var _this = this;

			this.handle.addEventListener("mousedown", function (e) {
				_this.mousedown(e);
			});

			document.addEventListener("mousemove", function (e) {
				_this.mousemove(e);
			});

			document.addEventListener("mouseup", function (e) {
				_this.mouseup(e);
			});

			this.handle.addEventListener("touchstart", function (e) {
				_this.touchstart(e);
			});

			document.addEventListener("touchmove", function (e) {
				_this.touchmove(e);
			});

			document.addEventListener("touchend", function (e) {
				_this.touchend(e);
			});
		}
	}, {
		key: "mousedown",
		value: function mousedown(e) {
			e.stopPropagation();

			// if(first) {
			// 	tl.kill();
			// 	first = false;
			// }
			this.max = this.slider.offsetWidth - this.handle.offsetWidth;
			this.startX = e.clientX - parseInt(e.target.style.left, 10);
			this.active = true;
		}
	}, {
		key: "mousemove",
		value: function mousemove(e) {
			if (!this.active) {
				return false;
			}

			var xPos = e.clientX - this.startX;

			if (xPos < 0) {
				xPos = 0;
			}
			if (xPos > this.max) {
				xPos = this.max;
			}

			this.handle.style.left = xPos + "px";

			// update(xPos);
		}
	}, {
		key: "mouseup",
		value: function mouseup(e) {
			e.preventDefault();

			if (!this.active) {
				return false;
			}

			this.active = false;
		}
	}, {
		key: "touchstart",
		value: function touchstart(e) {
			// if(first) {
			// 	tl.kill();
			// 	first = false;
			// }

			this.max = this.slider.offsetWidth - this.handle.offsetWidth;
			this.startX = e.touches[0].clientX - parseInt(e.target.style.left, 10);
			this.active = true;
		}
	}, {
		key: "touchmove",
		value: function touchmove(e) {
			if (!this.active) {
				return false;
			}

			var xPos = e.touches[0].clientX - this.startX;

			if (xPos < 0) {
				xPos = 0;
			}
			if (xPos > this.max) {
				xPos = this.max;
			}

			this.handle.style.left = xPos + "px";

			// update(xPos);
		}
	}, {
		key: "touchend",
		value: function touchend(e) {
			// alert("end");
			if (!this.active) {
				return false;
			}

			this.active = false;
		}
	}]);
	return Slider;
}();
//# sourceMappingURL=Slider.js.map
