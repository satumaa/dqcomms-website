"use strict";

var MobileNav = function () {
	function MobileNav(el) {
		babelHelpers.classCallCheck(this, MobileNav);

		this.el = el;
		this.parentIsLink = true;
		this.showActiveMenu = true;
		// this.scrollToActiveMenu = false;

		this.build();
	}

	babelHelpers.createClass(MobileNav, [{
		key: "build",
		value: function build() {
			var _this = this;

			$$(".parent", this.el).forEach(function (i) {
				Delfin("a", i).insertAdjacentHTML("beforebegin", '<span class="mobile-nav-toggle"><span>+</span><span>-</span></span>');

				// Open active menu
				if (i.classList.contains("active") && _this.showActiveMenu) {
					Delfin("ul", i).classList.add("visible");
					Delfin(".mobile-nav-toggle", i).classList.add("active");
				}

				// Parent is link?
				if (_this.parentIsLink) {
					Delfin(".mobile-nav-toggle", i).addEventListener("click", function (e) {
						e.preventDefault();
						toggle(i, this);
					});
				} else {
					i.addEventListener("click", function (e) {
						e.preventDefault();
						toggle(i, this);
					});
				}

				var toggle = function toggle(item, button) {
					Delfin("ul", item).classList.toggle("visible");
					button.classList.toggle("active");

					// Close children
					$$(".parent", item).forEach(function (j) {
						Delfin("ul", j).classList.remove("visible");
						Delfin(".mobile-nav-toggle", j).classList.remove("active");
					});
				};
			});
		}
	}]);
	return MobileNav;
}();
//# sourceMappingURL=MobileNav.js.map
