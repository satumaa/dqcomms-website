"use strict";

var Tooltip = function (_Library) {
	babelHelpers.inherits(Tooltip, _Library);

	function Tooltip(el) {
		babelHelpers.classCallCheck(this, Tooltip);

		var _this = babelHelpers.possibleConstructorReturn(this, (Tooltip.__proto__ || Object.getPrototypeOf(Tooltip)).call(this));

		_this.el = el;
		_this.created = false;

		var o = _this;
		var arr = Delfin.$$("[data-tooltip]");

		["mouseenter", "click"].forEach(function (j) {
			_this.el.addEventListener(j, function (e) {
				e.preventDefault();
				e.stopPropagation();

				if (!o.created) {
					new TooltipView(this, o);
				}
			});
		});
		return _this;
	}

	babelHelpers.createClass(Tooltip, null, [{
		key: "setup",
		value: function setup(arr) {
			return babelHelpers.get(Tooltip.__proto__ || Object.getPrototypeOf(Tooltip), "setup", this).call(this, arr, Tooltip, "[data-tooltip]");
		}
	}]);
	return Tooltip;
}(Library);

var TooltipView = function () {
	function TooltipView(target, parent) {
		babelHelpers.classCallCheck(this, TooltipView);

		this.settings = {
			maxWidth: 250,
			collapsePoint: "640px",
			offset: 20
		};

		this.parent = parent;

		var element = document.createElement("span");
		element.classList.add("tooltip");
		element.setAttribute("role", "tooltip");
		element.innerHTML = target.getAttribute("data-title");

		target.appendChild(element);

		this.target = target;
		this.position = target.getAttribute("data-tooltip");
		this.tooltip = target.querySelector(".tooltip");

		var obj = this;

		this.target.classList.add("tooltip-active");

		this.mouseLeave = function (e) {
			obj.target.classList.remove("tooltip-active");
			e.preventDefault();
			e.stopPropagation();
			obj.remove();
		};

		this.click = function (e) {
			obj.target.classList.remove("tooltip-active");
			e.preventDefault();
			e.stopPropagation();
			obj.remove();
		};

		// Hide on mouseleave
		this.target.addEventListener("mouseleave", this.mouseLeave);

		// Hide on tooltip tap
		this.tooltip.addEventListener("click", this.click);

		this.update();

		this.parent.created = true;
	}

	babelHelpers.createClass(TooltipView, [{
		key: "update",
		value: function update() {

			var offset = this.settings.offset;
			var position = this.position;
			var tooltipWidth = this.tooltip.offsetWidth;

			// Check for max width
			if (tooltipWidth > this.settings.maxWidth) {
				this.tooltip.style.width = n.settings.maxWidth;
				this.tooltip.style.whiteSpace = "normal";
			}

			var tooltipHeight = this.tooltip.offsetHeight;
			var targetWidth = this.target.offsetWidth;
			var targetHeight = this.target.offsetHeight;
			var targetBorderTop = parseFloat(getComputedStyle(this.target)["border-top-width"]);
			var targetBorderLeft = parseFloat(getComputedStyle(this.target)["border-left-width"]);
			var targetBorderRight = parseFloat(getComputedStyle(this.target)["border-right-width"]);

			// No position set
			if (position == "") {
				position = "right";
			}

			// Small screen
			if (window.matchMedia("(max-width: " + this.settings.collapsePoint + ")").matches) {
				if (this.position != "top") {
					position = "bottom";
				} else {
					position = "top";
				}

				var overflow = this.target.getBoundingClientRect().left + document.documentElement.scrollLeft + this.tooltip.offsetWidth;

				if (overflow > window.innerWidth) {
					position = "bottom right";
				}

				this.target.setAttribute("data-tooltip", position);
				this.tooltip.style.maxWidth = window.innerWidth - offset;
			} else {
				position = this.position;
				this.target.setAttribute("data-tooltip", this.position);
			}

			// Position tooltip
			if (position == "right") {
				this.tooltip.style.left = targetWidth + offset - targetBorderLeft + "px";
				this.tooltip.style.top = targetHeight / 2 - tooltipHeight / 2 - targetBorderTop + "px";
			}
			if (position == "left") {
				this.tooltip.style.right = targetWidth + offset - targetBorderRight + "px";
				this.tooltip.style.top = targetHeight / 2 - tooltipHeight / 2 - targetBorderTop + "px";
				this.tooltip.style.left = "auto";
			}
			if (position == "top") {
				this.tooltip.style.top = -tooltipHeight - offset - targetBorderTop + "px";
				this.tooltip.style.left = 0 - targetBorderLeft + "px";
			}
			if (position == "bottom") {
				this.tooltip.style.top = targetHeight + offset - targetBorderTop + "px";
				this.tooltip.style.left = 0 - targetBorderLeft + "px";
			}
			if (position == "bottom right") {
				this.tooltip.style.top = targetHeight + offset - targetBorderTop + "px";
				this.tooltip.style.right = 0 - targetBorderRight + "px";
				this.tooltip.style.left = "auto";
			}
			if (position == "top right") {
				this.tooltip.style.top = -tooltipHeight - offset;
				this.tooltip.style.right = 0;
				this.tooltip.style.left = "auto";
			}
		}
	}, {
		key: "remove",
		value: function remove() {
			this.target.removeEventListener("mouseleave", this.mouseLeave);
			this.tooltip.removeEventListener("click", this.click);
			this.tooltip.parentNode.removeChild(this.tooltip);
			this.target.setAttribute("data-tooltip", this.position);

			this.parent.created = false;
		}
	}]);
	return TooltipView;
}();
//# sourceMappingURL=Tooltip.js.map
