"use strict";

var Views = function () {
	function Views(selector) {
		babelHelpers.classCallCheck(this, Views);

		this.selector = selector || ".view";
		this.currentView = "";
		this.simultaneous = false;
		this.callbackIn = null;
		this.callbackOut = null;
		this.functionIn = null;
		this.functionOut = null;
	}

	babelHelpers.createClass(Views, [{
		key: "load",
		value: function load(id, options) {
			if (this.currentView == id) {
				return false;
			}

			var options = options || {};
			var first = options.first !== undefined ? options.first : false;

			this.callbackIn = options.callbackIn !== undefined ? options.callbackIn : null;
			this.callbackOut = options.callbackOut !== undefined ? options.callbackOut : null;
			this.functionIn = options.functionIn !== undefined ? options.functionIn : null;
			this.functionOut = options.functionOut !== undefined ? options.functionOut : null;

			// Don't fade out on first load
			if (first) {
				this.getPage(id).style.display = "block";
				this.fadeIn(id, this.functionIn);
			} else {
				this.fadeOut(this.currentView, id, this.functionOut);
			}

			this.currentView = id;
		}
	}, {
		key: "fadeOut",
		value: function fadeOut(pageOut, pageIn, fn) {
			var _this = this;

			var page = this.getPage(pageOut);

			var defaultFunction = function defaultFunction(page, callback) {
				TweenLite.to(page, .5, { opacity: 0, onComplete: function onComplete() {
						_this.onFadeOut(page, pageIn);
					} });
			};

			fn = fn || defaultFunction;
			fn(page, this.onFadeOut);

			if (this.simultaneous) {
				this.fadeIn(pageIn, this.functionIn);
			}
		}
	}, {
		key: "onFadeOut",
		value: function onFadeOut(page, pageIn) {
			page.style.display = "none";

			if (!this.simultaneous) {
				this.fadeIn(pageIn, this.functionIn);
			}

			if (typeof this.callbackOut === "function") {
				this.callbackOut(page);
			}
		}
	}, {
		key: "fadeIn",
		value: function fadeIn(id, fn) {
			var _this2 = this;

			var page = this.getPage(id);

			var defaultFunction = function defaultFunction(page, callback) {
				TweenLite.to(page, .5, { opacity: 1, onComplete: function onComplete() {
						_this2.onFadeIn(page);
					} });
			};

			page.style.display = "block";

			fn = fn || defaultFunction;
			fn(page, this.onFadeIn);
		}
	}, {
		key: "onFadeIn",
		value: function onFadeIn(page) {
			if (typeof this.callbackIn === "function") {
				this.callbackIn(page);
			}
		}
	}, {
		key: "getPage",
		value: function getPage(id) {
			return Delfin(this.selector + "[data-id='" + id + "']");
		}
	}]);
	return Views;
}();
//# sourceMappingURL=Views.js.map
