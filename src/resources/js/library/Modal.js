"use strict";

var Modal = function () {
	function Modal(el, click) {
		var _this = this;

		babelHelpers.classCallCheck(this, Modal);

		this.el = el;
		this.settings = {
			lightbox: false,
			history: true
		};

		this.shader = null;
		this.modal = null;
		this.transitions = null;
		this.active = false;
		this.callback = {};

		this.build(click);

		this.onClickCloseFunc = function () {
			for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
				args[_key] = arguments[_key];
			}

			_this.onClickClose.apply(_this, args);
		};

		this.onTransitionEndFunc = function () {
			for (var _len2 = arguments.length, args = Array(_len2), _key2 = 0; _key2 < _len2; _key2++) {
				args[_key2] = arguments[_key2];
			}

			_this.onTransitionEnd.apply(_this, args);
		};

		this.onPopStateFunc = function () {
			for (var _len3 = arguments.length, args = Array(_len3), _key3 = 0; _key3 < _len3; _key3++) {
				args[_key3] = arguments[_key3];
			}

			_this._close.apply(_this, args);
		};

		this.onCloseFunc = function () {
			for (var _len4 = arguments.length, args = Array(_len4), _key4 = 0; _key4 < _len4; _key4++) {
				args[_key4] = arguments[_key4];
			}

			_this.close.apply(_this, args);
		};

		this.onResizeFunc = function () {
			for (var _len5 = arguments.length, args = Array(_len5), _key5 = 0; _key5 < _len5; _key5++) {
				args[_key5] = arguments[_key5];
			}

			_this.onResize.apply(_this, args);
		};

		this.onKeyUpFunc = function () {
			for (var _len6 = arguments.length, args = Array(_len6), _key6 = 0; _key6 < _len6; _key6++) {
				args[_key6] = arguments[_key6];
			}

			_this.onKeyUp.apply(_this, args);
		};
	}

	babelHelpers.createClass(Modal, [{
		key: "build",
		value: function build(click) {
			var _this2 = this;

			this.modal = Delfin("#" + this.el.getAttribute("data-modal-id"));

			if (click) {
				this.el.addEventListener("click", function (e) {
					e.preventDefault();
					_this2.open();
				});
			}
		}
	}, {
		key: "open",
		value: function open(id) {
			// Modal is open
			if (this.active == false) {
				this.shader = document.createElement("div");
				this.shader.setAttribute("id", "modal-shader");

				var s = document.documentElement.style;
				this.transitions = "transition" in s || "WebkitTransition" in s || "MozTransition" in s || "OTransition" in s;

				this.modal.parentNode.insertBefore(this.shader, this.modal);
				getComputedStyle(this.shader)["opacity"];

				// Delfin.animate(this.shader, .5, {opacity: .8});

				this.shader.addEventListener("click", this.onClickCloseFunc);

				this.shader.appendChild(this.modal);

				this.active = true;

				if (this.history()) {
					history.pushState({ modal: "open" }, "");

					window.addEventListener("popstate", this.onPopStateFunc);
				}

				this.shader.classList.add("visible");

				// Resize
				window.addEventListener("resize", this.onResizeFunc);

				// Keyup
				window.addEventListener("keyup", this.onKeyUpFunc);
			}
			// Open new modal
			else {
					if (typeof this.callback === "function") {
						this.callback();
					}

					this.modal.style.display = "none";
				}

			// Common tasks
			var close = Delfin(".modal-close", this.modal);

			if (close != null) {
				close.addEventListener("click", this.onCloseFunc);
			}

			this.modal.style.display = "block";

			// Update position
			this.update();

			Delfin("html").classList.add("modal-open");
		}
	}, {
		key: "update",
		value: function update() {
			var windowHeight = window.innerHeight;
			var layerHeight = this.modal.offsetHeight;
			var yPos = windowHeight / 2 - layerHeight / 2;

			if (layerHeight > windowHeight) {
				yPos = 20;
			}

			this.modal.style.top = yPos + "px";
			Delfin.animate(this.modal, .25, { opacity: 1 });

			if (this.settings.lightbox) {
				//!!!!!!!!!!!!!!!!!!! BUG
				// this.modal.css("width", n.layer.find("img").width());
				this.modal.style.width = n.modal.querySelector("img").offsetWidth;
			}
		}
	}, {
		key: "history",
		value: function (_history) {
			function history() {
				return _history.apply(this, arguments);
			}

			history.toString = function () {
				return _history.toString();
			};

			return history;
		}(function () {
			return !!(window.history && history.pushState) && this.settings.history;
		})
	}, {
		key: "close",
		value: function close(e) {
			if (this.history()) {
				history.back();
			} else {
				this._close();
			}
		}
	}, {
		key: "_close",
		value: function _close() {
			this.shader.removeEventListener("click", this.onClickCloseFunc);
			window.removeEventListener("popstate", this.onPopStateFunc);
			window.removeEventListener("resize", this.onResizeFunc);
			window.removeEventListener("keyup", this.onKeyUpFunc);
			Delfin("html").classList.remove("modal-open");
			// Delfin.animate(n.shader, .5, {opacity: 0, complete: n.onTransitionEnd});

			Delfin.animate(this.modal, .3, { opacity: 0, complete: this.onTransitionEndFunc });

			this.active = false;

			for (var property in this.callback) {
				if (this.callback.hasOwnProperty(property)) {
					if (typeof this.callback[property] === "function") {
						this.callback[property]();
					}
				}
			}

			// if(typeof this.callback === "function") {
			// 	this.callback();
			// }

			this.shader.classList.remove("visible");
		}
	}, {
		key: "onResize",
		value: function onResize() {
			if (this.modal) {
				this.update();
			}
		}
	}, {
		key: "onKeyUp",
		value: function onKeyUp(e) {
			if (e.which == 27) {
				this.close();
			}
		}
	}, {
		key: "onTransitionEnd",
		value: function onTransitionEnd() {
			this.shader.parentNode.insertBefore(this.modal, this.shader);
			this.shader.parentNode.removeChild(this.shader);
			this.modal.style.display = "none";
		}
	}, {
		key: "onClickClose",
		value: function onClickClose(e) {
			if (e.target == this.shader) {
				this.close();
			}
		}
	}, {
		key: "addCloseListener",
		value: function addCloseListener(e, fn) {
			this.callback[e] = fn;
		}
	}, {
		key: "removeCloseListener",
		value: function removeCloseListener(e) {
			delete this.callback[e];
		}
	}], [{
		key: "setup",
		value: function setup(arr) {
			arr = arr || $$("[data-modal-id]");

			var items = [];

			arr.forEach(function (i) {
				items.push(new Modal(i, true));
			});

			return items;
		}
	}]);
	return Modal;
}();
//# sourceMappingURL=Modal.js.map
