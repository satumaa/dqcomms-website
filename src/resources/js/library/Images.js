"use strict";

var Images = function () {
	function Images() {
		babelHelpers.classCallCheck(this, Images);

		this.images = [];

		this.build(Delfin.$$("[data-srcset]"));
	}

	babelHelpers.createClass(Images, [{
		key: "build",
		value: function build(arr) {
			var _this = this;

			arr.forEach(function (i) {
				_this.images.push(new Image(i));
			});
		}
	}, {
		key: "load",
		value: function load(list) {
			list.forEach(function (i) {
				i.setAttribute("data-load", "load");
			});

			for (var i = 0; i < this.images.length; i++) {
				this.images[i].update();
			}
		}
	}]);
	return Images;
}();

var Image = function () {
	function Image(target) {
		babelHelpers.classCallCheck(this, Image);


		this.settings = {
			devicePixelRatio: false
		};

		this.target = target;
		this.images = [];
		this.sizes = {};
		this.useDpr = this.settings.devicePixelRatio && target.getAttribute("data-dpr") != "false" || target.getAttribute("data-dpr") == "true";
		this.currentSize = -1;
		this.currentMq = false;
		this.load = target.getAttribute("data-load");

		var srcset = target.getAttribute("data-srcset");
		var srcsetArr = srcset.split(",");

		for (var i = 0; i < srcsetArr.length; i++) {
			// Single image
			var img = srcsetArr[i].replace(/^\s+|\s+$/g, '');
			// Split url and size
			var imgArr = img.split(" ");
			var unitValue = imgArr[1] ? imgArr[1] : "0w";
			var unit = unitValue.replace(/[0-9.]/g, "");
			var size = unitValue.replace(/[a-zA-Z]/g, "");
			var obj = { id: i, url: imgArr[0], size: size, unit: unit };

			this.images[i] = obj;
		}

		// Sort array
		this.images.sort(function (a, b) {
			return parseFloat(a.size) - parseFloat(b.size);
		});

		// Sizes
		var sizesAttr = target.getAttribute("data-sizes");

		if (sizesAttr) this.sizes = { unit: sizesAttr.replace(/[0-9.]/g, ""), size: sizesAttr.replace(/[a-zA-Z]/g, "") };

		this.update();

		var obj = this;

		window.addEventListener("resize", function () {
			obj.update();
		});
	}

	babelHelpers.createClass(Image, [{
		key: "update",
		value: function update() {
			// Choose largest image as default
			var selectedSize = this.images.length - 1;
			var dpr = window.devicePixelRatio || 1;
			var dprValue = this.useDpr ? dpr : 1;
			var width = this.sizes === true ? this.sizes.size : window.innerWidth;

			for (var i = 0; i < this.images.length; i++) {
				var sizeW = this.images[i].size / dprValue;
				var sizeX = this.images[i].size;
				var unit = this.images[i].unit;

				if (unit == "w") if (sizeW / width >= 1) {
					selectedSize = i;
					break;
				}

				if (unit == "x") if (sizeX > dpr - 0.5) {
					selectedSize = i;
					break;
				}
			}

			// Lazy load
			this.load = this.target.getAttribute("data-load");

			// Check if image should be loaded
			var mq = this.load == "load" || (this.load != undefined ? window.matchMedia("(min-width: " + Delfin.breakpoints[this.load] + ")").matches : true);

			if (mq != this.currentMq) {
				this.currentMq = mq;

				if (mq) {
					this.setImage(selectedSize);
					return false;
				}
			}

			// Switch image only if size has changed
			if (mq && selectedSize != this.currentSize) {
				this.setImage(selectedSize);
				this.currentSize = selectedSize;
			}
		}
	}, {
		key: "setImage",
		value: function setImage(size) {
			if (this.target.tagName == "IMG") this.target.setAttribute("src", this.images[size].url);else this.target.style.backgroundImage = "url(" + this.images[size].url + ")";
		}
	}]);
	return Image;
}();
//# sourceMappingURL=Images.js.map
