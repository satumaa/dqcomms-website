"use strict";

var Carousel2 = function () {
	function Carousel2(el, options) {
		var _this = this;

		babelHelpers.classCallCheck(this, Carousel2);

		this.carousel = el;
		this.control = d(".carousel-dots", this.carousel);
		this.nav = d(".carousel-nav", this.carousel);
		this.options = {};

		for (var i in options) {
			if (options.hasOwnProperty(i)) {
				this.options[i] = options[i];
			}
		}

		this.options.onChange = function () {
			_this.updateControl();

			if (options.callback) {
				options.callback();
			}
		};

		this.swipe = new Siema(this.options);

		var o = this;
		var count = 0;
		var num = this.swipe.innerElements.length;

		// Exit when just 1 slide
		if (num == 1) {
			if (this.nav) {
				this.nav.style.display = "none";
			}

			if (this.control) {
				this.control.style.display = "none";
			}

			return false;
		}

		// Dots
		if (this.control) {
			for (var i = 0; i < num; i++) {
				var item = document.createElement("button");
				item.addEventListener("click", o.onClickControl(i));

				this.control.appendChild(item);
			}

			// Select first
			d(".carousel-dots button:nth-child(1)", this.carousel).classList.add("active");
		}

		// Nav
		var next = d(".carousel-next", this.carousel);
		var prev = d(".carousel-prev", this.carousel);

		if (next) {
			next.addEventListener("click", function () {
				o.swipe.next();
			});
		}

		if (prev) {
			prev.addEventListener("click", function () {
				o.swipe.prev();
			});
		}

		// Stop on mouseenter
		// this.carousel.addEventListener("mouseenter", (e) => {
		// 	this.swipe.stop();
		// });

		// // Restart on mouseleave
		// this.carousel.addEventListener("mouseleave", (e) => {
		// 	this.swipe.restart();
		// });

		// Auto
		setInterval(function () {
			return _this.swipe.next();
		}, 3000);
	}

	babelHelpers.createClass(Carousel2, [{
		key: "updateControl",
		value: function updateControl() {
			this.update(this.swipe.currentSlide + 1);
		}
	}, {
		key: "update",
		value: function update(num) {
			$$(".carousel-dots > button", this.carousel).forEach(function (i) {
				i.className = "";
			});

			if (this.control) {
				d(".carousel-dots button:nth-child(" + num + ")", this.carousel).classList.add("active");
			}
		}
	}, {
		key: "getSwipe",
		value: function getSwipe() {
			return this.swipe;
		}
	}, {
		key: "onClickControl",
		value: function onClickControl(i) {
			var o = this;

			return function (e) {
				o.update(i + 1);
				o.swipe.goTo(i);
			};
		}
	}, {
		key: "destroy",
		value: function destroy() {}
	}]);
	return Carousel2;
}();
//# sourceMappingURL=Carousel2.js.map
