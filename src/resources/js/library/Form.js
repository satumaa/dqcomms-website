"use strict";

var Form = function (_Library) {
	babelHelpers.inherits(Form, _Library);

	function Form(el) {
		babelHelpers.classCallCheck(this, Form);

		var _this = babelHelpers.possibleConstructorReturn(this, (Form.__proto__ || Object.getPrototypeOf(Form)).call(this));

		_this.form = el;

		_this.build();
		return _this;
	}

	babelHelpers.createClass(Form, [{
		key: "build",
		value: function build() {
			var _this2 = this;

			// Clear errors
			$$("[data-required]", this.form).forEach(function (i) {
				i.addEventListener("focus", function (e) {
					this.closest("label").classList.remove("error");
				});
			});

			// Set send event
			this.form.querySelector("[data-submit]").addEventListener("click", function (e) {
				e.preventDefault();

				_this2.send();
			});
		}
	}, {
		key: "send",
		value: function send() {
			var options = {
				submitEvent: false,
				callback: function callback(errors) {
					errors.forEach(function (i) {
						i.element.closest("label").classList.add("error");
					});

					if (errors.length > 0) {
						o.showMessage(errors[0].element);
					}
				}
			};

			// Validator
			var validator = new Validator(this.form, options);

			var o = this;

			// Submit
			function cb() {
				// Hide errors
				o.form.querySelector("[data-fetch-error]").style.display = "none";

				// Hide form and show success
				d("[data-form-content]").style.display = "none";
				o.form.reset();

				o.showMessage(d("[data-success]"), true);
			}

			function ecb() {
				o.form.querySelector("[data-fetch-error]").style.display = "block";
				o.showMessage(o.form.querySelector("[data-fetch-error]"));
			}

			if (validator.validate()) {
				var urltosend = this.form.getAttribute("data-action");

				d.fetch(urltosend, {
					callback: cb,
					errorCallback: ecb,
					method: "POST", // GET or POST
					// data: "test1=test1&test2=test2",
					data: JSON.stringify(this.serialize()),
					headers: { "Content-Type": "application/x-www-form-urlencoded" },
					dataType: "json" // json or text (default)
				});
			}
		}
	}, {
		key: "serialize",
		value: function serialize() {
			var elems = this.form.elements;

			var data = {};

			for (var i = 0; i < elems.length; i += 1) {
				var element = elems[i];
				var type = element.type;
				var name = element.name;

				switch (type) {
					case "checkbox":
						data[name] = element.checked;
						break;
					case "radio":
						if (element.checked) {
							data[name] = element.value;
						}
						break;
					default:
						data[name] = element.value;
						break;
				}
			}

			return data;
		}
	}, {
		key: "showMessage",
		value: function showMessage(el, show) {
			if (show) {
				el.style.display = "block";
			}

			// Scroll offset
			var offset = 20;

			if (window.matchMedia("(min-width: " + d.breakpoints.medium + ")").matches) {
				offset = 20;
			}

			// Scroll only if target not on screen
			var top = el.getBoundingClientRect().top;

			if (top < offset) {
				var pos = el.getBoundingClientRect().top + d.scrollTop() - offset;

				window.scroll({ top: pos, left: 0, behavior: "smooth" });
			}
		}
	}], [{
		key: "setup",
		value: function setup(arr) {
			return babelHelpers.get(Form.__proto__ || Object.getPrototypeOf(Form), "setup", this).call(this, arr, Form, "[data-form]");
		}
	}]);
	return Form;
}(Library);
//# sourceMappingURL=Form.js.map
