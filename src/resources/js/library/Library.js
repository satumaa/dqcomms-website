"use strict";

var Library = function () {
	function Library() {
		babelHelpers.classCallCheck(this, Library);
	}

	babelHelpers.createClass(Library, null, [{
		key: "setup",
		value: function setup(arr, ref, selector) {
			arr = arr || $$(selector);

			if (arr.length == 0) {
				return false;
			}

			var items = [];

			arr.forEach(function (i) {
				items.push(new ref(i));
			});

			return items;
		}
	}]);
	return Library;
}();
//# sourceMappingURL=Library.js.map
