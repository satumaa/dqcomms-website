"use strict";

var Carousel = function () {
	function Carousel(el, options) {
		var _this = this;

		babelHelpers.classCallCheck(this, Carousel);

		if (!el) {
			return false;
		}
		this.carousel = el;
		this.control = d(".carousel-dots", this.carousel);
		this.nav = d(".carousel-nav", this.carousel);
		this.options = {};

		for (var i in options) {
			if (options.hasOwnProperty(i)) {
				this.options[i] = options[i];
			}
		}

		this.options.callback = function () {
			_this.updateControl();

			if (options.callback) {
				options.callback();
			}
		};

		this.swipe = Swipe(el.querySelector(".swipe"), this.options);

		var o = this;
		var count = 0;
		var num = this.swipe.getNumSlides();

		// Exit when just 1 slide
		if (num == 1) {
			if (this.nav) {
				this.nav.style.display = "none";
			}

			if (this.control) {
				this.control.style.display = "none";
			}

			return false;
		}

		// Dots
		if (this.control) {
			for (var i = 0; i < num; i++) {
				var item = document.createElement("button");
				item.addEventListener("click", o.onClickControl(i));
				//item.innerHTML = '<span>' + d(".swipe-wrap > div:nth-child(" + (i + 1) + ")", this.carousel).getAttribute("data-title") + '</span>';
				item.innerHTML = "<span>*</span>";

				this.control.appendChild(item);
			}

			// Select first
			d(".carousel-dots button:nth-child(1)", this.carousel).classList.add("active");
		}

		// Nav
		var next = d(".carousel-next", this.carousel);
		var prev = d(".carousel-prev", this.carousel);

		if (next) {
			next.addEventListener("click", function () {
				o.swipe.next();
			});
		}

		if (prev) {
			prev.addEventListener("click", function () {
				o.swipe.prev();
			});
		}

		// Stop on mouseenter
		this.carousel.addEventListener("mouseenter", function (e) {
			_this.swipe.stop();
		});

		// Restart on mouseleave
		this.carousel.addEventListener("mouseleave", function (e) {
			_this.swipe.restart();
		});
	}

	babelHelpers.createClass(Carousel, [{
		key: "updateControl",
		value: function updateControl() {
			this.update(this.swipe.getPos() + 1);
		}
	}, {
		key: "update",
		value: function update(num) {
			$$(".carousel-dots > button", this.carousel).forEach(function (i) {
				i.className = "";
			});

			if (this.control) {
				d(".carousel-dots button:nth-child(" + num + ")", this.carousel).classList.add("active");
			}
		}
	}, {
		key: "getSwipe",
		value: function getSwipe() {
			return this.swipe;
		}
	}, {
		key: "onClickControl",
		value: function onClickControl(i) {
			var o = this;

			return function (e) {
				o.update(i + 1);
				o.swipe.slide(i);
			};
		}
	}, {
		key: "destroy",
		value: function destroy() {}
	}]);
	return Carousel;
}();
//# sourceMappingURL=Carousel.js.map
