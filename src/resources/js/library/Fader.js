"use strict";

var Fader = function () {
	function Fader() {
		var _this = this;

		babelHelpers.classCallCheck(this, Fader);

		this.settings = {
			repeat: false
		};

		this.assets = [];
		this.windowHeight = window.innerHeight;
		this.onScroll = this.onScroll.bind(this);
		this.onResize = this.onResize.bind(this);

		$$("[class*='fade-']").forEach(function (i) {
			var delay = i.getAttribute("data-delay");
			var scroll = i.getAttribute("data-scroll") ? true : false;
			var obj = {};
			obj.el = i;
			obj.scroll = scroll;
			obj.offset = i.getAttribute("data-offset") ? parseInt(i.getAttribute("data-offset"), 10) : 0;

			if (delay) {
				obj.el.style.transitionDelay = i.getAttribute("data-delay") + "s";
			}

			_this.assets.push(obj);
		});

		this.update(false);

		window.addEventListener("scroll", this.onScroll);
		window.addEventListener("resize", this.onResize);
	}

	babelHelpers.createClass(Fader, [{
		key: "onScroll",
		value: function onScroll() {
			this.update(true);
		}
	}, {
		key: "onResize",
		value: function onResize() {
			this.update(false);
		}
	}, {
		key: "update",
		value: function update(scroll) {
			var _this2 = this;

			this.assets.forEach(function (i) {
				if (i.scroll && !scroll) return;

				var el = i.el;
				var top = el.getBoundingClientRect().top;

				if (top + i.offset < _this2.windowHeight) {
					el.classList.add("fade-in");

					if (!_this2.settings.repeat) {
						var index = _this2.assets.indexOf(i);
						_this2.assets.splice(index, 1);
					}
				} else {
					if (_this2.settings.repeat) {
						el.classList.remove("fade-in");
					}
				}
			});

			if (this.assets.length == 0) {
				this.destroy();
			}
		}
	}, {
		key: "destroy",
		value: function destroy() {
			window.removeEventListener("scroll", this.onScroll);
			window.removeEventListener("resize", this.onResize);
		}
	}]);
	return Fader;
}();
//# sourceMappingURL=Fader.js.map
