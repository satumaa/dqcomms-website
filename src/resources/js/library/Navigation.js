"use strict";

var Navigation = function () {
	function Navigation(el) {
		babelHelpers.classCallCheck(this, Navigation);

		this.parentIsLink = true;
		this.showActiveMenu = true;
		this.el = el;

		this.build();
	}

	babelHelpers.createClass(Navigation, [{
		key: "build",
		value: function build() {
			// Nav toggle
			if (d("#nav-toggle")) {
				d("#nav-toggle").addEventListener("click", function (e) {
					d("html").classList.toggle("nav-open");
					// this.classList.toggle("active");
					d("#main-nav").scrollTo(0, 0);
				});
			}

			// Nav close
			if (d("#nav-close")) {
				d("#nav-close").addEventListener("click", function (e) {
					d("html").classList.remove("nav-open");
					d("#main-nav").scrollTo(0, 0);
				});
			}

			// Overlay
			$$("#main-nav > ul > li.has-overlay > a").forEach(function (i) {
				var el = i.parentNode;

				// Close
				var close = document.createElement("div");
				close.classList.add("overlay-close");
				el.insertBefore(close, el.firstChild);

				close.addEventListener("click", function (e) {
					el.classList.remove("overlay-open");
					d("#site-header").classList.remove("overlay-open");
				});

				// el.querySelector(".close-button button").addEventListener("click", function(e) {
				// 	el.classList.remove("overlay-open");
				// 	d("#site-header").classList.remove("overlay-open");
				// });

				// Nav clicked
				i.addEventListener("click", function (e) {
					if (!window.matchMedia("(min-width: " + d.breakpoints.medium + ")").matches) {
						return false;
					}

					e.preventDefault();
					e.stopPropagation();

					// Clear all opened navs
					if (!el.classList.contains("overlay-open")) {
						$$("#main-nav > ul > li.has-overlay").forEach(function (j) {
							j.classList.remove("overlay-open");
						});
					}

					// Toggle
					el.classList.toggle("overlay-open");

					if (el.classList.contains("overlay-open")) {
						d("#site-header").classList.add("overlay-open");
					} else {
						d("#site-header").classList.remove("overlay-open");
					}
				});
			});

			if (this.el) {
				var deepNavigation = new DeepNavigation(this.el);
			}
		}
	}]);
	return Navigation;
}();
//# sourceMappingURL=Navigation.js.map
