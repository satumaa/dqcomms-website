"use strict";

var Equalizer = function () {
	function Equalizer(el) {
		var _this = this;

		babelHelpers.classCallCheck(this, Equalizer);

		this.el = el;

		this.settings = {
			useOffset: true
		};

		this.build();

		window.addEventListener("resize", function () {
			_this.build();
		});

		// window.onload = (e) => {
		// 	this.update();
	}

	babelHelpers.createClass(Equalizer, [{
		key: "build",
		value: function build() {
			var _this2 = this;

			var cols = $$("[data-equal-target]", this.el);

			if (cols.length == 0) {
				return;
			}

			var maxHeight = 0;
			var filtered = cols.filter(this.visible)[0];

			if (!filtered) {
				return false;
			}

			var offset = filtered.getBoundingClientRect().top + Delfin.scrollTop();
			var arr = new Array();
			var diff = 0;

			cols.forEach(function (item) {
				if (item.offsetParent !== null) {
					item.style.height = "auto";

					if (item.getBoundingClientRect().top + Delfin.scrollTop() != offset && _this2.settings.useOffset) {
						for (var i = 0; i < arr.length; i++) {
							arr[i].style.height = maxHeight + "px";
						}

						maxHeight = 0;
						arr = [];
						offset = item.getBoundingClientRect().top + Delfin.scrollTop();
					} else {
						diff++;
					}

					var columnHeight = item.offsetHeight;
					if (columnHeight > maxHeight) maxHeight = columnHeight;

					arr.push(item);
				}
			});

			if (diff > 1) {
				for (var i = 0; i < arr.length; i++) {
					arr[i].style.height = maxHeight + "px";
				}
			}
		}
	}, {
		key: "visible",
		value: function visible(el) {
			return el.offsetParent !== null;
		}
	}], [{
		key: "setup",
		value: function setup(arr) {
			arr = arr || $$("[data-equal]");

			if (arr.length == 0) {
				return false;
			}

			var items = [];

			arr.forEach(function (i) {
				items.push(new Equalizer(i));
			});

			return items;

			// window.addEventListener("resize", () => {
			// 	items.forEach((i) => {
			// 		i.build();
			// 	});
			// });
		}
	}]);
	return Equalizer;
}();
//# sourceMappingURL=Equalizer.js.map
