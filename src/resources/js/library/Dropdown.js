"use strict";

var Dropdown = function () {
	function Dropdown(el) {
		babelHelpers.classCallCheck(this, Dropdown);

		this.el = el;
		this.build();
	}

	babelHelpers.createClass(Dropdown, [{
		key: "build",
		value: function build() {
			var _this = this;

			if (this.el.getAttribute("data-has-custom-ui")) {
				return false;
			}

			this.el.classList.add("custom-dropdown-ui");

			// Build custom dropdown
			var styleAttr = this.el.getAttribute("style") ? ' style="' + this.el.getAttribute("style") + '"' : "";

			var container = document.createElement("div");
			container.classList.add("dropdown-container");

			this.el.parentNode.insertBefore(container, this.el);
			container.appendChild(this.el);

			var target = document.createElement("div");
			target.innerHTML = '<div class="dropdown" ' + styleAttr + '><div class="dropdown-value"><span>Option 1</span></div></div>';
			target = target.firstChild;
			this.el.parentNode.insertBefore(target, this.el);

			var list = Delfin("ul", target);
			var item = Delfin.$$("li", target);

			// Set initial value
			if (this.el.options[this.el.selectedIndex].value) {
				this.selectOption(target, this.el.options[this.el.selectedIndex].text);
			}

			this.el.addEventListener("mouseenter", function () {
				target.classList.add("hover");
			});

			this.el.addEventListener("mouseleave", function () {
				target.classList.remove("hover");
			});

			// Value changed externally
			this.el.addEventListener("change", function (e) {
				var value = e.target.value;
				_this.selectOption(target, e.target.options[e.target.selectedIndex].text);
				target.classList.remove("hover");
			});

			this.el.setAttribute("data-has-custom-ui", true);
		}
	}, {
		key: "refresh",
		value: function refresh() {
			if ("createEvent" in document) {
				var evt = document.createEvent("HTMLEvents");
				evt.initEvent("change", false, true);
				i.dispatchEvent(evt);
			} else {
				this.el.fireEvent("onchange");
			}
		}
	}, {
		key: "selectOption",
		value: function selectOption(target, value) {
			target.querySelector(".dropdown-value").innerHTML = value;
		}
	}], [{
		key: "setup",
		value: function setup(arr) {
			arr = arr || $$("select");

			if (arr.length == 0) {
				return false;
			}

			var items = [];

			arr.forEach(function (i) {
				items.push(new Dropdown(i));
			});

			return items;
		}
	}]);
	return Dropdown;
}();
//# sourceMappingURL=Dropdown.js.map
