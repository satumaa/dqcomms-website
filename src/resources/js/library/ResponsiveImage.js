"use strict";

// class Images {

// 	constructor() {
// 		this.images = [];

// 		this.build(Delfin.$$("[data-srcset]"));
// 	}

// 	build(arr) {
// 		arr.forEach((i) => {
// 			this.images.push(new Image(i));
// 		});
// 	}

// 	load(list) {
// 		list.forEach(function(i) {
// 			i.setAttribute("data-load", "load");
// 		});

// 		for(var i = 0; i < this.images.length; i++) {
// 			this.images[i].update();
// 		}
// 	}
// }

var ResponsiveImage = function (_Library) {
	babelHelpers.inherits(ResponsiveImage, _Library);

	function ResponsiveImage(el) {
		babelHelpers.classCallCheck(this, ResponsiveImage);

		var _this = babelHelpers.possibleConstructorReturn(this, (ResponsiveImage.__proto__ || Object.getPrototypeOf(ResponsiveImage)).call(this));

		_this.el = el;

		_this.settings = {
			devicePixelRatio: false
		};

		_this.images = [];
		_this.sizes = {};
		_this.useDpr = _this.settings.devicePixelRatio && _this.el.getAttribute("data-dpr") != "false" || _this.el.getAttribute("data-dpr") == "true";
		_this.currentSize = -1;
		_this.currentMq = false;
		_this.load = _this.el.getAttribute("data-load");

		_this.build();
		return _this;
	}

	babelHelpers.createClass(ResponsiveImage, [{
		key: "build",
		value: function build() {
			var srcset = this.el.getAttribute("data-srcset");
			var srcsetArr = srcset.split(",");

			for (var i = 0; i < srcsetArr.length; i++) {
				// Single image
				var img = srcsetArr[i].replace(/^\s+|\s+$/g, '');
				// Split url and size
				var imgArr = img.split(" ");
				var unitValue = imgArr[1] ? imgArr[1] : "0w";
				var unit = unitValue.replace(/[0-9.]/g, "");
				var size = unitValue.replace(/[a-zA-Z]/g, "");
				var obj = { id: i, url: imgArr[0], size: size, unit: unit };

				this.images[i] = obj;
			}

			// Sort array
			this.images.sort(function (a, b) {
				return parseFloat(a.size) - parseFloat(b.size);
			});

			// Sizes
			var sizesAttr = this.el.getAttribute("data-sizes");

			if (sizesAttr) {
				this.sizes = { unit: sizesAttr.replace(/[0-9.]/g, ""), size: sizesAttr.replace(/[a-zA-Z]/g, "") };
			}

			this.update();

			var obj = this;

			window.addEventListener("resize", function () {
				obj.update();
			});
		}
	}, {
		key: "update",
		value: function update() {
			// Choose largest image as default
			var selectedSize = this.images.length - 1;
			var dpr = window.devicePixelRatio || 1;
			var dprValue = this.useDpr ? dpr : 1;
			var width = this.sizes === true ? this.sizes.size : window.innerWidth;

			for (var i = 0; i < this.images.length; i++) {
				var sizeW = this.images[i].size / dprValue;
				var sizeX = this.images[i].size;
				var unit = this.images[i].unit;

				if (unit == "w") {
					if (sizeW / width >= 1) {
						selectedSize = i;
						break;
					}
				}

				if (unit == "x") {
					if (sizeX > dpr - 0.5) {
						selectedSize = i;
						break;
					}
				}
			}

			// Lazy load
			this.load = this.el.getAttribute("data-load");

			// Check if image should be loaded
			var mq = this.load == "load" || (this.load != undefined ? window.matchMedia("(min-width: " + Delfin.breakpoints[this.load] + ")").matches : true);

			if (mq != this.currentMq) {
				this.currentMq = mq;

				if (mq) {
					this.setImage(selectedSize);
					return false;
				}
			}

			// Switch image only if size has changed
			if (mq && selectedSize != this.currentSize) {
				this.setImage(selectedSize);
				this.currentSize = selectedSize;
			}
		}
	}, {
		key: "setImage",
		value: function setImage(size) {
			if (this.el.tagName == "IMG") {
				this.el.setAttribute("src", this.images[size].url);
			} else {
				this.el.style.backgroundImage = "url(" + this.images[size].url + ")";
			}
		}
	}, {
		key: "loadImage",
		value: function loadImage() {
			this.el.setAttribute("data-load", "load");
			this.update();
		}
	}], [{
		key: "setup",
		value: function setup(arr) {
			return babelHelpers.get(ResponsiveImage.__proto__ || Object.getPrototypeOf(ResponsiveImage), "setup", this).call(this, arr, ResponsiveImage, "[data-srcset]");
		}
	}]);
	return ResponsiveImage;
}(Library);
//# sourceMappingURL=ResponsiveImage.js.map
