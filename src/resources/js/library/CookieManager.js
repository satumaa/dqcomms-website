"use strict";

var CookieManager = function () {
	function CookieManager() {
		babelHelpers.classCallCheck(this, CookieManager);
	}

	babelHelpers.createClass(CookieManager, [{
		key: "createCookie",
		value: function createCookie(name, value, days) {
			var expires = "";
			if (days) {
				var date = new Date();
				date.setTime(date.getTime() + days * 24 * 60 * 60 * 1000);
				expires = "; expires=" + date.toUTCString();
			}

			document.cookie = name + "=" + value + expires + "; path=/";
		}
	}, {
		key: "readCookie",
		value: function readCookie(name) {
			var nameEQ = name + "=";
			var ca = document.cookie.split(';');

			for (var i = 0; i < ca.length; i++) {
				var c = ca[i];
				while (c.charAt(0) == ' ') {
					c = c.substring(1, c.length);
				}if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
			}

			return null;
		}
	}, {
		key: "eraseCookie",
		value: function eraseCookie(name) {
			this.createCookie(name, "", -1);
		}
	}]);
	return CookieManager;
}();
//# sourceMappingURL=CookieManager.js.map
