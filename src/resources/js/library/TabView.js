"use strict";

var TabView = function (_Library) {
	babelHelpers.inherits(TabView, _Library);

	function TabView(el) {
		babelHelpers.classCallCheck(this, TabView);

		var _this = babelHelpers.possibleConstructorReturn(this, (TabView.__proto__ || Object.getPrototypeOf(TabView)).call(this));

		_this.view = el;

		_this.build();
		return _this;
	}

	babelHelpers.createClass(TabView, [{
		key: "build",
		value: function build() {
			var _this2 = this;

			$$(".tab-nav li", this.el).forEach(function (i) {
				i.addEventListener("click", function (e) {
					_this2.onClickNav(i);
				});
			});
		}
	}, {
		key: "onClickNav",
		value: function onClickNav(el) {
			// Clear all
			$$(".tab-nav li", this.view).forEach(function (i) {
				i.classList.remove("active");
			});

			$$(".tab-content", this.view).forEach(function (i) {
				i.classList.remove("active");
			});

			// Select current nav item
			el.classList.add("active");

			// Select current content
			var id = el.getAttribute("data-id");

			var content = this.view.querySelector(".tab-content[data-id='" + id + "']");
			content.classList.add("active");
		}
	}], [{
		key: "setup",
		value: function setup(arr) {
			return babelHelpers.get(TabView.__proto__ || Object.getPrototypeOf(TabView), "setup", this).call(this, arr, TabView, "[data-tabview]");
		}
	}]);
	return TabView;
}(Library);
//# sourceMappingURL=TabView.js.map
