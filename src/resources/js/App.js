"use strict";

var App = function () {
	function App(queue) {
		babelHelpers.classCallCheck(this, App);

		this.isReady = false;
		this.readyQueue = queue;
		this.mobile = false;
		this.windowWidth = window.innerWidth;
	}

	babelHelpers.createClass(App, [{
		key: "init",
		value: function init() {
			var _this = this;

			d.ready(function () {
				// document.addEventListener("DOMContentLoaded", () => {
				_this.isReady = true;
				_this.start();
				_this.processQueue();
			});
		}
	}, {
		key: "ready",
		value: function ready(fn) {
			if (this.isReady) {
				fn();
			} else {
				this.readyQueue.push(fn);
			}
		}
	}, {
		key: "processQueue",
		value: function processQueue() {
			while (this.readyQueue.length) {
				this.readyQueue.shift()();
			}
		}
	}, {
		key: "start",
		value: function start() {
			var _this2 = this;

			// Check for mobile
			if (isMobile.phone || isMobile.tablet) {
				this.mobile = true;
				d("html").classList.add("mobile-device");
			}

			// Header
			// new Header();

			// Navigation
			new Navigation();

			// Scroller
			Scroller.setup();

			// VideoEmbed
			VideoEmbed.setup();

			// Share
			this.shareButton = d("#share-button");
			this.updateShareButton();

			window.addEventListener("resize", function (e) {
				var innerWidth = window.innerWidth;

				if (innerWidth != _this2.windowWidth) {
					_this2.updateShareButton();
					_this2.windowWidth = innerWidth;
				}
			});

			if (this.shareButton) {
				var shareOverlay = new Overlay(d("[data-overlay='share-overlay']"));

				if (shareOverlay) {
					this.shareButton.querySelector(".open").addEventListener("click", function (e) {
						shareOverlay.open();
						e.target.parentNode.classList.add("m-active");
					});

					this.shareButton.querySelector(".close").addEventListener("click", function (e) {
						shareOverlay.close();
						e.target.parentNode.classList.remove("m-active");
					});
				}
			}
		}
	}, {
		key: "updateShareButton",
		value: function updateShareButton() {
			if (this.shareButton) {
				var height = window.innerHeight;
				this.shareButton.style.top = height * 0.5 + "px";
			}
		}
	}]);
	return App;
}();

var d = Delfin;

app = new App(app.queue);
app.init();
//# sourceMappingURL=App.js.map
