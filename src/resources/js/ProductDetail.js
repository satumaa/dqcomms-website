"use strict";

var ProductDetail = function () {
	function ProductDetail(el) {
		babelHelpers.classCallCheck(this, ProductDetail);

		this.el = el;
		this.build();
	}

	babelHelpers.createClass(ProductDetail, [{
		key: "build",
		value: function build() {
			var _this = this;

			this.carousel = new Carousel(d(".carousel", this.el), {
				callback: function callback() {}
			});

			$$(".thumbs li", this.el).forEach(function (i) {
				i.addEventListener("click", function (e) {
					// carousel.getSwipe().slide(i.getAttribute("data-id"));
					_this.carousel.getSwipe().slide(d.index(i));

					$$(".thumbs li", _this.el).forEach(function (j) {
						j.classList.remove("active");
					});

					i.classList.add("active");
				});
			});
		}
	}, {
		key: "getElement",
		value: function getElement() {
			return this.el;
		}
	}, {
		key: "getCarousel",
		value: function getCarousel() {
			return this.carousel;
		}
	}]);
	return ProductDetail;
}();
//# sourceMappingURL=ProductDetail.js.map
