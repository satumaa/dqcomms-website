"use strict";

var Clients = function (_Screen) {
	babelHelpers.inherits(Clients, _Screen);

	function Clients(options) {
		babelHelpers.classCallCheck(this, Clients);

		var _this = babelHelpers.possibleConstructorReturn(this, (Clients.__proto__ || Object.getPrototypeOf(Clients)).call(this, options));

		_this.onDisplay = { from: .4, to: .9 };
		_this.color = "yellow";
		_this.setOptions();
		return _this;
	}

	babelHelpers.createClass(Clients, [{
		key: "setOptions",
		value: function setOptions() {
			var zoom = 1.6;

			this.options.shapes = [new Shape({
				view_port_width: 1200,
				width: 120,
				height: 462,
				x: 365,
				y: 90,
				partChange: 400,
				mask: new ImageLoader("/resources/svgs/3.svg"),
				img: new ImageLoader("/resources/imgs/absolut1.jpg")

			}), new Shape({
				view_port_width: 1200,
				width: 418,
				height: 486,
				x: 725,
				y: 90,
				mask: new ImageLoader("/resources/svgs/1.svg"),
				img: new ImageLoader("/resources/imgs/absolut2.jpg")

			})];
		}
	}]);
	return Clients;
}(Screen);
//# sourceMappingURL=Clients.js.map
