"use strict";

var Shape = function () {
	function Shape(options) {
		babelHelpers.classCallCheck(this, Shape);

		/*this.scalar=1;
  this.x=x;
  this.y=y;
  this.index=0;
  this.movement=movement;
  this.offset={x:0,y:0};
  this.width=width;
  this.height=height;*/

		this.options = options;
		//this.context=context;
		this.file = "svg/" + this.options.mask_src;

		this.img = null;

		this.isloaded = true;

		this.canvas = document.createElement("canvas");
		if (this.options.debug) {
			console.log("OI");
			//document.getElementsByTagName("body")[0].appendChild(this.canvas);
		}

		this.canvas.width = this.options.width;
		this.canvas.height = this.options.height;
	}

	babelHelpers.createClass(Shape, [{
		key: "setOffset",
		value: function setOffset(offset) {
			this.offset = offset;
		}
	}, {
		key: "setScalar",
		value: function setScalar(scalar) {

			this.scalar = scalar;
		}
	}, {
		key: "setIndex",
		value: function setIndex(index) {
			this.index = index;
		}
	}, {
		key: "drawVideo",
		value: function drawVideo(position) {
			var video = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;


			if (position > this.movement.from && position < this.movement.to) {

				//console.log(this.movement.from,position);


				//	this.context.save();

				var diff = position - this.movement.from;
				var max = this.movement.to - this.movement.from;
				var p = diff / max;

				var y = this.context.canvas.height - (this.context.canvas.height + this.height) * p;

				//this.context.drawImage(this.img,this.x,y,this.width,this.height);

				//this.context.globalCompositeOperation = 'source-in';

				video.drawVideo(this.context, { x: this.x, y: y, width: this.width, height: this.height }, position);

				//	this.context.restore();
			}
		}
	}, {
		key: "drawToCanvas",
		value: function drawToCanvas(position) {

			var scalar = this.options.context.canvas.width / this.options.view_port_width;
			var ctx = this.canvas.getContext("2d");

			var width = this.options.width * scalar;
			var height = this.options.height * scalar;
			this.canvas.width = width;
			this.canvas.height = height;
			ctx.clearRect(0, 0, width, height);

			ctx.globalCompositeOperation = 'source-over';

			ctx.drawImage(this.options.mask.data, 0, 0, width, height);

			var diff = position - this.options.from;
			var max = this.options.to - this.options.from;
			var p = diff / max;

			if (this.options.img && this.options.img.isloaded && this.options.img.data.height) {
				ctx.globalCompositeOperation = 'source-in';
				var w = width;
				var r = this.options.img.data.width / this.options.img.data.height;

				var h = w / r;

				var ero = h - height;

				var position_y = -ero * p;
				if (this.options.reverse) {
					position_y = -ero + ero * p;
				}
				ctx.drawImage(this.options.img.data, 0, position_y, w, h);
			}
		}
	}, {
		key: "getPositionStyle",
		value: function getPositionStyle(x, y) {

			return "-ms-transform: translate(" + x + "px, " + y + "px);-webkit-transform: translate(" + x + "px,  " + y + "px);transform: translate(" + x + "px,  " + y + "px);";
		}
	}, {
		key: "draw",
		value: function draw(position) {

			if (position > this.options.from && position < this.options.to) {

				//console.log(this.movement.from,position);

				var scalar = this.options.context.canvas.width / this.options.view_port_width;
				this.drawToCanvas(position);

				var diff = position - this.options.from;
				var max = this.options.to - this.options.from;
				var p = diff / max;

				var x = this.options.x * scalar;
				var width = this.options.width * scalar;
				var height = this.options.height * scalar;

				var y = this.options.context.canvas.height - (this.options.context.canvas.height + this.options.height) * p;

				this.options.context.drawImage(this.canvas, x, y, width, height);

				if (this.options.div) {

					//this.options.div.dom.style.left=x+this.options.div.x*scalar+"px";
					//this.options.div.dom.style.top=scalar*this.options.div.y+y+"px";
					//this.options.div.dom.style.width=(scalar*this.options.div.width)+"px";

					var transform_style = this.getPositionStyle(x + this.options.div.x * scalar, scalar * this.options.div.y + y);

					this.options.div.dom.style = transform_style;
				}

				//this.context.save();

				/*	var diff=position-this.options.from;
    	var max=this.options.to-this.options.from;
    	var p=diff/max;
    	
    	
    	var y=this.options.context.canvas.height-(this.options.context.canvas.height+this.options.height)*p;
    	
    	
    	
    
    	
    	
    	
    	var x=this.options.x*scalar;
    	var width=this.options.width*scalar;
    	var height=this.options.height*scalar;
    	
    	this.options.context.save();
    	
    
    	
    	if(this.options.img && this.options.img.isloaded && this.options.img.data.height){
    		
    		var w=width;
    		var r=this.options.img.data.width/this.options.img.data.height;
    		
    		var h=w/r;
    		
    		
    		var ero=h-height;
    		
    		
    		
    		this.options.context.drawImage(this.options.img.data,this.options.x*scalar,y-ero*p,w,h);
    		
    	}
    	this.options.context.drawImage(this.options.mask.data,x,y,width,height);
    	
    	
    	
    	
    	
    this.options.context.restore();
    	*/

				//this.context.globalCompositeOperation = 'source-in';

				//video.drawVideo(this.context,{x:this.x,y:y,width:this.width,height:this.height},position);


				//
			}

			//console.log(this.real_width,this.real_height);
		}
	}]);
	return Shape;
}();
//# sourceMappingURL=Shape copy 2.js.map
