"use strict";

var Utils = function () {
	function Utils(el) {
		babelHelpers.classCallCheck(this, Utils);

		this.el = el;
	}

	babelHelpers.createClass(Utils, [{
		key: "addEventListener",
		value: function addEventListener(name, fn) {
			var _this = this;

			this.el.addEventListener(name, function (e) {
				fn.call(_this, e);
			});
		}
	}, {
		key: "dispatchEvent",
		value: function dispatchEvent(name, props) {
			this.el.dispatchEvent(new CustomEvent(name, props));
		}
	}], [{
		key: "setup",
		value: function setup(arr, ref, selector) {
			arr = arr || $$(selector);

			if (arr.length == 0) {
				return false;
			}

			var items = [];

			arr.forEach(function (i) {
				items.push(new ref(i));
			});

			return items;
		}
	}]);
	return Utils;
}();
//# sourceMappingURL=Utils.js.map
