"use strict";

var TabView = function (_Utils) {
	babelHelpers.inherits(TabView, _Utils);

	function TabView(el) {
		babelHelpers.classCallCheck(this, TabView);

		var _this = babelHelpers.possibleConstructorReturn(this, (TabView.__proto__ || Object.getPrototypeOf(TabView)).call(this, el));

		_this.settings = {
			hash: false
		};

		_this.nav = _this.el.querySelector(".tab-nav");
		_this.build();
		return _this;
	}

	babelHelpers.createClass(TabView, [{
		key: "build",
		value: function build() {
			var _this2 = this;

			// Set click event
			$$("li", this.nav).forEach(function (i) {
				i.addEventListener("click", function (e) {
					_this2.onClickNav(i);
				});
			});

			// Open from hash
			var hash = window.location.hash.substring(1, window.location.hash.length);

			// if(this.settings.hash && hash && hash != "!") {
			if (this.settings.hash && hash) {
				var el = this.nav.querySelector("li[data-id='" + hash + "']");

				if (el) {
					this.select(el);
				}
			} else {
				this.select(this.nav.querySelector("li"));
			}
			// }
		}
	}, {
		key: "onClickNav",
		value: function onClickNav(el) {
			this.select(el);
		}
	}, {
		key: "select",
		value: function select(el) {
			// Clear all
			$$(".tab-nav li", this.el).forEach(function (i) {
				i.classList.remove("m-active");
			});

			$$(".tab-content", this.el).forEach(function (i) {
				i.classList.remove("m-active");
			});

			// Select current nav item
			el.classList.add("m-active");

			// Select current content
			var id = el.getAttribute("data-id");

			var content = this.el.querySelector(".tab-content[data-id='" + id + "']");
			content.classList.add("m-active");

			// Set hash
			if (this.settings.hash) {
				location.hash = id;
			}

			// Dispatch event
			this.dispatchEvent("open", { detail: { id: id } });
		}
	}], [{
		key: "setup",
		value: function setup(arr) {
			return babelHelpers.get(TabView.__proto__ || Object.getPrototypeOf(TabView), "setup", this).call(this, arr, TabView, "[data-tabview]");
		}
	}]);
	return TabView;
}(Utils);
//# sourceMappingURL=TabView.js.map
