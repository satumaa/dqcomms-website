"use strict";

var YoutubeVideo = function (_Utils) {
	babelHelpers.inherits(YoutubeVideo, _Utils);

	function YoutubeVideo(el) {
		babelHelpers.classCallCheck(this, YoutubeVideo);

		var _this = babelHelpers.possibleConstructorReturn(this, (YoutubeVideo.__proto__ || Object.getPrototypeOf(YoutubeVideo)).call(this));

		_this.el = el;

		_this.build();
		return _this;
	}

	babelHelpers.createClass(YoutubeVideo, [{
		key: "build",
		value: function build() {
			var _this2 = this;

			var id = this.el.getAttribute("data-youtube");

			if (id) {
				var iframe = this.el.querySelector("iframe");

				iframe.src += id + "?rel=0";

				this.el.querySelector(".play").addEventListener("click", function (e) {
					_this2.el.classList.add("active");

					iframe.src += "&autoplay=1";
				});
			}
		}
	}], [{
		key: "setup",
		value: function setup(arr) {
			return babelHelpers.get(YoutubeVideo.__proto__ || Object.getPrototypeOf(YoutubeVideo), "setup", this).call(this, arr, YoutubeVideo, "[data-youtube]");
		}
	}]);
	return YoutubeVideo;
}(Utils);
//# sourceMappingURL=YoutubeVideo.js.map
