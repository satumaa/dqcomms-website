"use strict";

var VideoEmbed = function (_Utils) {
	babelHelpers.inherits(VideoEmbed, _Utils);

	function VideoEmbed(el) {
		babelHelpers.classCallCheck(this, VideoEmbed);

		var _this = babelHelpers.possibleConstructorReturn(this, (VideoEmbed.__proto__ || Object.getPrototypeOf(VideoEmbed)).call(this, el));

		_this.build();
		return _this;
	}

	babelHelpers.createClass(VideoEmbed, [{
		key: "build",
		value: function build() {
			var _this2 = this;

			var id = this.el.getAttribute("data-video-id");

			if (id) {
				var iframe = this.el.querySelector("iframe");

				iframe.src += id + "?rel=0";

				this.el.querySelector(".play").addEventListener("click", function (e) {
					_this2.el.classList.add("m-active");

					iframe.src += "&autoplay=1";
				});
			}
		}
	}], [{
		key: "setup",
		value: function setup(arr) {
			return babelHelpers.get(VideoEmbed.__proto__ || Object.getPrototypeOf(VideoEmbed), "setup", this).call(this, arr, VideoEmbed, "[data-video-id]");
		}
	}]);
	return VideoEmbed;
}(Utils);
//# sourceMappingURL=VideoEmbed.js.map
