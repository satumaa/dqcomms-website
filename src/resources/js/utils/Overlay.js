"use strict";

var Overlay = function (_Utils) {
	babelHelpers.inherits(Overlay, _Utils);

	function Overlay(el) {
		babelHelpers.classCallCheck(this, Overlay);

		var _this = babelHelpers.possibleConstructorReturn(this, (Overlay.__proto__ || Object.getPrototypeOf(Overlay)).call(this, el));

		_this.btn = d("[data-overlay-button='" + _this.el.getAttribute("data-overlay") + "']");
		_this.build();
		return _this;
	}

	babelHelpers.createClass(Overlay, [{
		key: "build",
		value: function build() {
			var _this2 = this;

			var background = document.createElement("div");
			background.classList.add("background");
			this.el.appendChild(background);

			background.addEventListener("click", function (e) {
				_this2.close();
			});

			var close = this.el.querySelector(".close");

			close.addEventListener("click", function (e) {
				_this2.close();
			});

			if (this.btn) {
				this.btn.addEventListener("click", function (e) {
					e.preventDefault();

					_this2.open();
				});
			}
		}
	}, {
		key: "open",
		value: function open() {
			d("html").classList.add("m-overlay-open");
			this.el.classList.add("m-active");

			this.dispatchEvent("open");
		}
	}, {
		key: "close",
		value: function close() {
			d("html").classList.remove("m-overlay-open");
			this.el.classList.remove("m-active");

			this.dispatchEvent("close");
		}
	}], [{
		key: "setup",
		value: function setup(arr) {
			return babelHelpers.get(Overlay.__proto__ || Object.getPrototypeOf(Overlay), "setup", this).call(this, arr, Overlay, "[data-overlay]");
		}
	}]);
	return Overlay;
}(Utils);
//# sourceMappingURL=Overlay.js.map
