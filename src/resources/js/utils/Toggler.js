"use strict";

var Toggler = function () {
	function Toggler() {
		babelHelpers.classCallCheck(this, Toggler);
	}

	babelHelpers.createClass(Toggler, null, [{
		key: "toggle",
		value: function toggle(el) {
			var height = el.offsetHeight;
			// var padding = parseInt(getComputedStyle(el)["padding-top"], 10) + parseInt(getComputedStyle(el)["padding-bottom"], 10);
			// console.log("toggle", height);
			if (height > 0) {
				el.style.height = "0";
			} else {
				el.style.height = "auto";
				height = el.offsetHeight;
				el.style.height = "0";

				// getComputedStyle(el)["height"];
				void el.offsetWidth;

				el.style.height = height + "px";
			}
		}
	}]);
	return Toggler;
}();
//# sourceMappingURL=Toggler.js.map
