"use strict";

var Filter = function (_Utils) {
	babelHelpers.inherits(Filter, _Utils);

	function Filter(el, target) {
		babelHelpers.classCallCheck(this, Filter);

		var _this = babelHelpers.possibleConstructorReturn(this, (Filter.__proto__ || Object.getPrototypeOf(Filter)).call(this, el));

		_this.target = d("[data-filter-target='" + el.getAttribute("data-filter") + "']");

		_this.build();
		return _this;
	}

	babelHelpers.createClass(Filter, [{
		key: "build",
		value: function build() {
			var _this2 = this;

			$$("button", this.el).forEach(function (i) {
				i.addEventListener("click", function (e) {
					$$("button", _this2.el).forEach(function (j) {
						j.classList.remove("m-active");
					});

					i.classList.toggle("m-active");
					_this2.update(i.getAttribute("data-id"));
				});
			});
		}
	}, {
		key: "update",
		value: function update(id) {
			// Hide all
			$$(".category", this.target).forEach(function (i) {
				i.classList.add("m-hidden");
			});

			// Show category
			if (id == "all") {
				$$(".category", this.target).forEach(function (i) {
					i.classList.remove("m-hidden");
				});
			} else {
				$$("[data-id='" + id + "']", this.target).forEach(function (i) {
					i.classList.remove("m-hidden");
				});
			}
		}
	}], [{
		key: "setup",
		value: function setup(arr) {
			return babelHelpers.get(Filter.__proto__ || Object.getPrototypeOf(Filter), "setup", this).call(this, arr, Filter, "[data-filter]");
		}
	}]);
	return Filter;
}(Utils);
//# sourceMappingURL=Filter.js.map
