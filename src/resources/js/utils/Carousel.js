"use strict";

var Carousel = function () {
	function Carousel(el, options) {
		var _this = this;

		babelHelpers.classCallCheck(this, Carousel);

		this.carousel = el;
		this.dots = d(".carousel-dots", this.carousel);
		this.nav = d(".carousel-nav", this.carousel);
		this.progress = d(".carousel-progress", this.carousel);
		this.options = {};

		for (var i in options) {
			if (options.hasOwnProperty(i)) {
				this.options[i] = options[i];
			}
		}

		// Runs when slide transition begins
		this.options.callback = function () {
			_this.updateControl();

			if (options.callback) {
				options.callback();
			}
		};

		this.options.transitionEnd = function () {
			_this.setProgressInterval();

			if (options.transitionEnd) {
				options.transitionEnd();
			}
		};

		this.swipe = Swipe(el.querySelector(".swipe"), this.options);

		var num = this.swipe.getNumSlides();

		// Exit when just 1 slide
		if (num == 1) {
			if (this.nav) {
				this.nav.style.display = "none";
			}

			if (this.dots) {
				this.dots.style.display = "none";
			}

			return false;
		}

		// Dots
		if (this.dots) {
			for (var _i = 0; _i < num; _i++) {
				var item = document.createElement("button");
				item.addEventListener("click", this.onClickControl(_i));

				this.dots.appendChild(item);
			}
		}

		// Nav
		var next = this.carousel.querySelector(".carousel-next");
		var prev = this.carousel.querySelector(".carousel-prev");

		if (next) {
			next.addEventListener("click", function (e) {
				_this.swipe.next();
			});
		}

		if (prev) {
			prev.addEventListener("click", function (e) {
				_this.swipe.prev();
			});
		}

		if (this.options.stopOnMouseEnter && !this.progress) {
			// Stop on mouseenter
			this.carousel.addEventListener("mouseenter", function (e) {
				_this.swipe.stop();
				// this.clearProgressInterval(true);
			});

			// Restart on mouseleave
			this.carousel.addEventListener("mouseleave", function (e) {
				_this.swipe.restart();
				// this.setProgressInterval();
			});
		}

		this.update(1, true);
	}

	babelHelpers.createClass(Carousel, [{
		key: "updateControl",
		value: function updateControl() {
			this.update(this.swipe.getPos() + 1);
		}
	}, {
		key: "update",
		value: function update(num, first) {
			// Video
			$$("video", this.carousel).forEach(function (i) {
				i.pause();
				i.currentTime = 0;
			});

			var el = this.carousel.querySelector(".swipe-wrap").children[num - 1];
			var video = el.querySelector("video");

			if (video) {
				video.play();
			}

			// Set active dot
			$$(".carousel-dots > button", this.carousel).forEach(function (i) {
				i.className = "";
			});

			if (this.dots) {
				d(".carousel-dots > button:nth-child(" + num + ")", this.carousel).classList.add("m-active");
			}

			// Clear interval
			this.clearProgressInterval();

			// Set interval for first slide
			if (first) {
				this.setProgressInterval();
			}
		}
	}, {
		key: "clearProgressInterval",
		value: function clearProgressInterval(reset) {
			if (this.progress && this.interval) {
				clearInterval(this.interval);
				this.progress.style.width = "100%";
			}
		}
	}, {
		key: "setProgressInterval",
		value: function setProgressInterval() {
			var _this2 = this;

			if (this.progress) {
				var time = 0;
				var interval = 20;
				var refDate = new Date().getTime();

				this.interval = setInterval(function () {
					var date = new Date().getTime();
					var elapsed = date - refDate;
					var pct = Math.min(elapsed / _this2.options.auto, 1);

					// Update progress
					_this2.progress.style.width = pct * _this2.carousel.offsetWidth + "px";
				}, interval);
			}
		}
	}, {
		key: "getSwipe",
		value: function getSwipe() {
			return this.swipe;
		}
	}, {
		key: "onClickControl",
		value: function onClickControl(i) {
			var _this3 = this;

			return function (e) {
				_this3.update(i + 1);
				_this3.swipe.slide(i);
			};
		}
	}]);
	return Carousel;
}();
//# sourceMappingURL=Carousel.js.map
