"use strict";

var Form = function (_Utils) {
	babelHelpers.inherits(Form, _Utils);

	function Form(el) {
		babelHelpers.classCallCheck(this, Form);

		var _this = babelHelpers.possibleConstructorReturn(this, (Form.__proto__ || Object.getPrototypeOf(Form)).call(this, el));

		_this.button = el.querySelector("[data-submit]");
		_this.sending = false;

		_this.build();
		return _this;
	}

	babelHelpers.createClass(Form, [{
		key: "build",
		value: function build() {
			var _this2 = this;

			// Clear errors on click
			$$("[required]", this.el).forEach(function (i) {
				i.addEventListener("change", function (e) {
					i.closest(".field").classList.remove("error");
				});
			});

			// Set send event
			this.el.querySelector("[data-submit]").addEventListener("click", function (e) {
				e.preventDefault();

				if (!_this2.sending) {
					_this2.send();
				}
			});
		}
	}, {
		key: "send",
		value: function send() {
			var _this3 = this;

			var options = {
				submitEvent: false,
				callback: function callback(errors) {
					// Loop through validation errors
					errors.forEach(function (i) {
						// console.log(i, i.element.closest("label"));
						i.element.closest(".field").classList.add("error");
					});

					// If errors, scroll to first error
					if (errors.length > 0) {
						o.showMessage(errors[0].element);
					}
				}

				// Validator
			};var validator = new Validator(this.el, options);

			var o = this;

			if (validator.validate()) {
				var url = this.el.getAttribute("data-action");
				var data = this.serialize();
				var fd = new FormData();

				for (var i in data) {
					if (data.hasOwnProperty(i)) {
						fd.append(i, data[i]);
					}
				}

				fetch(url, {
					method: "POST",
					body: fd
				}).then(function (response) {
					return response.json();
				}).then(function (data) {
					_this3.success(data);
				}).catch(function (err) {
					_this3.error(err);
				});

				this.startLoader();
			}
		}
	}, {
		key: "serialize",
		value: function serialize() {
			var elems = this.el.elements;
			var data = {};

			for (var i = 0; i < elems.length; i += 1) {
				var element = elems[i];
				var type = element.type;
				var name = element.name;

				switch (type) {
					case 'checkbox':
						data[name] = element.checked ? element.value : 0;
						break;
					case 'radio':
						if (element.checked) {
							data[name] = element.value;
						}
						break;
					default:
						data[name] = element.value;
						break;
				}
			}

			return data;
		}
	}, {
		key: "success",
		value: function success(data) {
			var _this4 = this;

			// Hide errors
			if (data.success) {
				console.log("success");
				this.el.querySelector("[data-fetch-error]").style.display = "none";

				// Hide form and show success
				document.querySelector("[data-form-content]").style.display = "none";
				this.el.reset();

				this.showMessage(document.querySelector("[data-success]"), true);
			}
			// Back end validation errors
			else {
					var errors = [];

					// Loop through errors
					data.errors.forEach(function (i) {
						var el = _this4.el.querySelector("[name='" + i + "']");
						el.classList.add("error");
						el.closest("label").classList.add("error");

						errors.push(el);
					});

					// If errors, scroll to first error
					if (errors.length > 0) {
						this.showMessage(errors[0]);
					}
				}

			this.stopLoader();
		}
	}, {
		key: "error",
		value: function error(data) {
			console.log("error", data);
			this.el.querySelector("[data-fetch-error]").style.display = "block";
			this.stopLoader();
		}
	}, {
		key: "showMessage",
		value: function showMessage(el, show) {
			if (show) {
				el.style.display = "block";
			}

			// Scroll offset
			var offset = 20;

			if (window.matchMedia("(min-width: " + Core.breakpoints.medium + ")").matches) {
				offset = 20;
			}

			// Scroll only if target not on screen
			var top = el.getBoundingClientRect().top;

			if (top < offset) {
				var pos = el.getBoundingClientRect().top + Core.scrollTop() - offset;

				window.scroll({ top: pos, left: 0, behavior: "smooth" });
			}
		}
	}, {
		key: "startLoader",
		value: function startLoader() {
			this.sending = true;
			this.button.querySelector(".c-loader").classList.add("m-active");
		}
	}, {
		key: "stopLoader",
		value: function stopLoader() {
			this.sending = false;
			this.button.querySelector(".c-loader").classList.remove("m-active");
		}
	}], [{
		key: "setup",
		value: function setup(arr) {
			return babelHelpers.get(Form.__proto__ || Object.getPrototypeOf(Form), "setup", this).call(this, arr, Form, "[data-form]");
		}
	}]);
	return Form;
}(Utils);
//# sourceMappingURL=Form.js.map
