"use strict";

var Accordion = function (_Utils) {
	babelHelpers.inherits(Accordion, _Utils);

	function Accordion(el) {
		babelHelpers.classCallCheck(this, Accordion);

		var _this = babelHelpers.possibleConstructorReturn(this, (Accordion.__proto__ || Object.getPrototypeOf(Accordion)).call(this, el));

		_this.settings = {
			hash: false
		};

		_this.build();
		return _this;
	}

	babelHelpers.createClass(Accordion, [{
		key: "build",
		value: function build() {
			var _this2 = this;

			// Scroll to anchor
			var hash = window.location.hash.substring(1, window.location.hash.length);

			if (this.settings.hash && hash && hash != "!") {
				var el = this.el.querySelector("li[data-hash='" + hash + "']");

				if (el) {
					el.classList.add("m-active");

					var pos = el.getBoundingClientRect().top + d.scrollTop();
					window.scroll({ top: pos, behavior: "smooth" });
				}
			}

			// Setup click event
			$$(".item", this.el).forEach(function (i) {
				i.querySelector(".title button").addEventListener("click", function (e) {
					var parent = i.closest("li");
					parent.classList.toggle("m-active");
					// Toggler.toggle(i.parentNode.querySelector(".content"));

					// Set ARIA
					e.target.setAttribute("aria-expanded", i.classList.contains("m-active"));

					// Close others
					if (_this2.settings.hash) {
						$$("li", _this2.el).forEach(function (j) {
							if (i != j) {
								j.classList.remove("m-active");
							}
						});
					}

					// Scroll to
					if (_this2.settings.hash && i.classList.contains("m-active")) {
						var _pos = i.getBoundingClientRect().top + d.scrollTop();
						window.scroll({ top: _pos, behavior: "smooth" });
					}

					hash = i.getAttribute("data-hash");

					if (_this2.settings.hash && hash) {
						// Hash
						if (i.classList.contains("m-active")) {
							location.hash = hash;
						} else {
							location.hash = '#!';
						}
					}
				});
			});
		}
	}], [{
		key: "setup",
		value: function setup(arr) {
			return babelHelpers.get(Accordion.__proto__ || Object.getPrototypeOf(Accordion), "setup", this).call(this, arr, Accordion, "[data-accordion]");
		}
	}]);
	return Accordion;
}(Utils);
//# sourceMappingURL=Accordion.js.map
