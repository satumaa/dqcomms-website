"use strict";

var GoogleMap = function (_Utils) {
	babelHelpers.inherits(GoogleMap, _Utils);

	function GoogleMap(el) {
		babelHelpers.classCallCheck(this, GoogleMap);

		var _this = babelHelpers.possibleConstructorReturn(this, (GoogleMap.__proto__ || Object.getPrototypeOf(GoogleMap)).call(this, el));

		_this.styles = [{
			"featureType": "administrative",
			"elementType": "labels",
			"stylers": [{
				"visibility": "off"
			}]
		}, {
			"featureType": "administrative.country",
			"elementType": "geometry.stroke",
			"stylers": [{
				"visibility": "off"
			}]
		}, {
			"featureType": "administrative.province",
			"elementType": "geometry.stroke",
			"stylers": [{
				"visibility": "off"
			}]
		}, {
			"featureType": "landscape",
			"elementType": "geometry",
			"stylers": [{
				"visibility": "on"
			}, {
				"color": "#e3e3e3"
			}]
		}, {
			"featureType": "landscape.natural",
			"elementType": "labels",
			"stylers": [{
				"visibility": "off"
			}]
		}, {
			"featureType": "poi",
			"elementType": "all",
			"stylers": [{
				"visibility": "off"
			}]
		}, {
			"featureType": "road",
			"elementType": "all",
			"stylers": [{
				"color": "#cccccc"
			}]
		}, {
			"featureType": "road",
			"elementType": "labels",
			"stylers": [{
				"visibility": "off"
			}]
		}, {
			"featureType": "transit",
			"elementType": "labels.icon",
			"stylers": [{
				"visibility": "off"
			}]
		}, {
			"featureType": "transit.line",
			"elementType": "geometry",
			"stylers": [{
				"visibility": "off"
			}]
		}, {
			"featureType": "transit.line",
			"elementType": "labels.text",
			"stylers": [{
				"visibility": "off"
			}]
		}, {
			"featureType": "transit.station.airport",
			"elementType": "geometry",
			"stylers": [{
				"visibility": "off"
			}]
		}, {
			"featureType": "transit.station.airport",
			"elementType": "labels",
			"stylers": [{
				"visibility": "off"
			}]
		}, {
			"featureType": "water",
			"elementType": "geometry",
			"stylers": [{
				"color": "#FFFFFF"
			}]
		}, {
			"featureType": "water",
			"elementType": "labels",
			"stylers": [{
				"visibility": "off"
			}]
		}];

		google.maps.event.addDomListener(window, "load", _this.init.bind(_this));
		return _this;
	}

	babelHelpers.createClass(GoogleMap, [{
		key: "init",
		value: function init() {
			// Basic options for a simple Google Map
			// For more options see: https://developers.google.com/maps/documentation/javascript/reference#MapOptions
			var mapOptions = {
				// How zoomed in you want the map to start at (always required)
				zoom: 8,

				// The latitude and longitude to center the map (always required)
				center: new google.maps.LatLng(60.8695821, 21.8581337),

				// How you would like to style the map. 
				// This is where you would paste any style found on Snazzy Maps.
				styles: this.styles
			};

			// Get the HTML DOM element that will contain your map 
			// We are using a div with id="map" seen below in the <body>
			var mapElement = this.el;

			// Create the Google Map using our element and options defined above
			var map = new google.maps.Map(mapElement, mapOptions);
			var markers = [];

			// Turku
			var marker = new google.maps.Marker({
				position: new google.maps.LatLng(60.451812, 22.266631),
				map: map,
				title: 'Turku',
				label: {
					text: 'Turku'
				},
				icon: {
					url: '/resources/images/map-marker.svg',
					labelOrigin: new google.maps.Point(11, 40)
				}
			});

			markers.push(marker);
		}
	}], [{
		key: "setup",
		value: function setup(arr) {
			return babelHelpers.get(GoogleMap.__proto__ || Object.getPrototypeOf(GoogleMap), "setup", this).call(this, arr, GoogleMap, "[data-google-map]");
		}
	}]);
	return GoogleMap;
}(Utils);
//# sourceMappingURL=GoogleMap.js.map
