"use strict";

var Validator = function () {
	function Validator(el, options) {
		var _this = this;

		babelHelpers.classCallCheck(this, Validator);

		this.options = options || {};
		this.form = el;
		this.errors = [];

		var submitEvent = options.submitEvent === undefined ? true : options.submitEvent;

		if (submitEvent) {
			this.form.addEventListener("submit", function (e) {
				_this.validate(e);
			}, false);
		}
	}

	babelHelpers.createClass(Validator, [{
		key: "validate",
		value: function validate(e) {
			var _this2 = this;

			this.errors.length = 0;

			$$("[required]", this.form).forEach(function (i) {
				// Skip fields hat are not visible
				if (i.offsetParent !== null == false) {
					return;
				}

				// Radio and checkbox
				if (i.getAttribute("type") == "radio" || i.getAttribute("type") == "checkbox") {
					var error = true;

					$$("input[name=" + i.getAttribute("name") + "]", _this2.form).forEach(function (i) {
						if (i.checked) {
							error = false;
						}
					});

					if (error) {
						_this2.addError(i, "data-required");
					}
				}
				// Default
				else if (i.value == "") {
						_this2.addError(i, "data-required");
					}

					// Select
					else if (i.tagName == "SELECT") {
							if (i.value == 0) {
								_this2.addError(i, "data-required");
							}
						}

				// Email
				if (i.hasAttribute("data-required-email")) {
					if (!/\S+@\S+\.\S+/.test(i.value)) {
						_this2.addError(i, "data-required-email");
					}
				}

				// Number
				if (i.hasAttribute("data-required-number")) {
					if (isNaN(i.value)) {
						_this2.addError(i, "data-required-number");
					}
				}
			});

			if (this.errors.length > 0) {
				if (e) {
					e.preventDefault();
				}

				if (typeof this.options.callback === "function") {
					this.options.callback(this.errors);
				}

				return false;
			}

			return true;
		}
	}, {
		key: "addError",
		value: function addError(target, id) {
			var obj = {
				name: target.getAttribute("name"),
				error: id,
				element: target,
				message: target.getAttribute("data-message")
			};

			this.errors.push(obj);
		}
	}]);
	return Validator;
}();
//# sourceMappingURL=Validator.js.map
