"use strict";

var Contacts = function (_Screen) {
	babelHelpers.inherits(Contacts, _Screen);

	function Contacts(options) {
		babelHelpers.classCallCheck(this, Contacts);

		var _this = babelHelpers.possibleConstructorReturn(this, (Contacts.__proto__ || Object.getPrototypeOf(Contacts)).call(this, options));

		_this.onDisplay = { from: .4, to: .9 };
		_this.color = "yellow";
		_this.setOptions();
		return _this;
	}

	babelHelpers.createClass(Contacts, [{
		key: "setOptions",
		value: function setOptions() {
			var zoom = 1.6;

			this.options.shapes = [new Shape({
				view_port_width: 1200,
				width: 325 * zoom,
				height: 343 * zoom,
				x: 0,
				y: 90,

				mask: new ImageLoader("/resources/svgs/8.png"),
				img: new ImageLoader("/resources/imgs/us.jpg")

			})];
		}
	}]);
	return Contacts;
}(Screen);
//# sourceMappingURL=Contacts.js.map
