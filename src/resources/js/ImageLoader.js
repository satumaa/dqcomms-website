"use strict";

var ImageLoader = function ImageLoader(src) {
	babelHelpers.classCallCheck(this, ImageLoader);


	this.data = new Image();
	this.data.src = src;
	this.isloaded = false;
	this.data.onload = function () {

		this.isloaded = true;
	}.bind(this);
};
//# sourceMappingURL=ImageLoader.js.map
