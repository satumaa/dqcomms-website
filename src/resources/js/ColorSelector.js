"use strict";

var ColorSelector = function () {
	function ColorSelector(el, parent, productDetails) {
		babelHelpers.classCallCheck(this, ColorSelector);

		this.el = el;
		this.parent = parent;
		this.productDetails = productDetails;
		this.build();
	}

	babelHelpers.createClass(ColorSelector, [{
		key: "build",
		value: function build() {
			var _this = this;

			$$(".palette span", this.el).forEach(function (i) {
				i.addEventListener("click", function (e) {
					_this.setColor(i);
				});
			});
		}
	}, {
		key: "setColor",
		value: function setColor(el) {
			var index = d.index(el);
			var elems = $$(".color-images", this.parent);

			// Add active classes
			elems.forEach(function (j) {
				j.classList.remove("active");
			});

			$$(".palette span", this.el).forEach(function (j) {
				j.classList.remove("active");
			});

			el.classList.add("active");

			elems[index].classList.add("active");

			// Refresh carousel
			this.productDetails[index].getCarousel().getSwipe().setup({});

			// Update text and color
			this.el.querySelector(".output p").textContent = el.getAttribute("data-title");
			this.el.querySelector(".output span").style.backgroundColor = el.style.backgroundColor;

			// Set hash
			window.location.hash = el.getAttribute("data-hash");

			// Update image urls
			$$(".thumbs img", this.productDetails[index].getElement()).forEach(function (i) {
				if (!i.getAttribute("src")) {
					i.setAttribute("src", i.getAttribute("data-src"));
				}
			});

			$$(".carousel img", this.productDetails[index].getElement()).forEach(function (i) {
				if (!i.getAttribute("src")) {
					i.setAttribute("src", i.getAttribute("data-src"));
				}
			});
		}
	}]);
	return ColorSelector;
}();
//# sourceMappingURL=ColorSelector.js.map
