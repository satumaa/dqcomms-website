// Delfin front-end framework | MIT license | delfincss.com

var Delfin;

(function () {
"use strict";

	var n = Delfin = function(selector, container) {
		return typeof selector === "string"? (container || document).querySelector(selector) : selector || null;
	};

	n.$$ = function(selector, container) {
		// return [].slice.call((container || document).querySelectorAll(selector));
		return [].slice.call((container === undefined ? document : container).querySelectorAll(selector));
	};

	n.ready = function(fn) {
		// if(n.jq()) {
		// 	jQuery(document).ready(fn);
		// }
		// else {
		// 	if(document.readyState !== "loading") {
		// 		fn();
		// 	}
		// 	else {
		// 		document.addEventListener("DOMContentLoaded", fn);
		// 	}
		// }

		if(document.readyState !== "loading") {
			fn();
		}
		else {
			document.addEventListener("DOMContentLoaded", fn);
		}
	};

	n.initVariable = function(name) {
		var el = n("head").appendChild(document.createElement("meta"));
		el.classList.add(name);
	};

	n.getVariable = function(name) {
        if(n("meta." + name) == null) {
            this.initVariable(name);
        }

        var getCompStyle = getComputedStyle(n("meta." + name));

        if(getCompStyle && getCompStyle["font-family"]) {
            return getCompStyle["font-family"].replace(/["']/g, "");
        }
    };

	n.jq = function() {
		return typeof jQuery != "undefined";
	};

	n.scrollTop = function() {
		return document.documentElement.scrollTop || document.body.scrollTop;
	};

	n.index = function(el) {
		var index = 0;

		while(el = el.previousElementSibling) {
			index++;
	    }
		
		return index;
	};

	n.trigger = function(el, name) {
		var event = document.createEvent("HTMLEvents");
		event.initEvent(name, true, false);
		el.dispatchEvent(event);
	};

	n.animate = function(el, duration, props) {
		var old = el.style.transition;
		var oldWebkit = el.style.webkitTransition;
		var transition = "";
		var webkitTransition = "";
		var count = 0;
		var counter = 0;
		var easing = props.ease || "linear";
		var delay = props.delay || 0;
		var complete = props.complete || null;

		for(var key in props) {
			if(key == "ease" || key == "delay" || key == "complete") {
				delete props[key];
				continue;
			}

			var str = key.replace(/([a-z])([A-Z])/g, "$1-$2");
			var tmp = str.toLowerCase() + " " + duration + "s " + easing + " " + delay + "s, ";

			transition += tmp;
			count++;

			if(str.slice(0, 9) == "transform") {
				str = "-webkit-" + str;
				webkitTransition += str.toLowerCase() + " " + duration + "s " + easing + " " + delay + "s, ";
			}
			else {
				webkitTransition += tmp;
			}
		}

		el.style.webkitTransition = webkitTransition.substring(0, webkitTransition.length - 2);
		el.style.transition = transition.substring(0, transition.length - 2);

		for(var key in props) {
			var str = key.replace(/([a-z])([A-Z])/g, "$1-$2");

			el.style[key] = props[key];

			if(str.slice(0, 9) == "transform") {
				el.style["-webkit-" + key] = props[key];
			}
		}

		el.addEventListener("transitionend", end);
		el.addEventListener("webkitTransitionEnd", end);

		var t = setTimeout(end2, duration * 1000 + delay * 1000 + 50);
		
		function end(e) {
			if(++counter < count || e.target != e.currentTarget) {
				return false;
			}

			clearTimeout(t);
			counter = 0;
			end2();
		}

		function end2() {
			el.removeEventListener("transitionend", end);
			el.removeEventListener("webkitTransitionEnd", end);
			el.style.transition = old;
			el.style.webkitTransition = oldWebkit;

			if(complete) {
				complete.call();
			}
		}
	};

	// n.fetch = function(url, props) {
	// 	if(!url) {
	// 		return false;
	// 	}

	// 	var data = props.data || "";
	// 	var cb = props.callback || function() {};
	// 	var ecb = props.errorCallback || function() {};
	// 	var method = props.method || "GET";
	// 	var headers = props.headers || {};
	// 	var dataType = props.dataType || "text";

	// 	var xhr = new XMLHttpRequest();

	// 	if(!xhr) {
	// 		return false;
	// 	}

	// 	xhr.onreadystatechange = function() {
	// 		if(xhr.readyState === XMLHttpRequest.DONE)
	// 			if(xhr.status === 200) {
	// 				var response = xhr.responseText;

	// 				if(dataType == "json")
	// 					cb(JSON.parse(response));
	// 				else
	// 					cb(response);
	// 			} else
	// 				ecb(xhr.status, xhr.responseText);
	// 	};

	// 	xhr.open(method, url);
		
	// 	if(!headers["Content-Type"]) {
	// 		xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	// 	}

	// 	for(var property in headers) {
	// 		if(headers.hasOwnProperty(property)) {
	// 			xhr.setRequestHeader(property, headers[property]);
	// 		}
	// 	}

	// 	xhr.send(data);
	// };

	n.ready(function() {
		n.breakpoints = {
			smallMax: n.getVariable("breakpoints-small-max"),
			medium: n.getVariable("breakpoints-medium"),
			mediumMax: n.getVariable("breakpoints-medium-max"),
			large: n.getVariable("breakpoints-large"),
			largeMax: n.getVariable("breakpoints-large-max"),
			xlarge: n.getVariable("breakpoints-xlarge")
		}
	});

})();

var $$ = Delfin.$$;

if(!Delfin.jq()) {
	var $ = Delfin;
}
