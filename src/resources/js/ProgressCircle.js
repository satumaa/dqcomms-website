"use strict";

var ProgressCircle = function () {
	function ProgressCircle(el, color, barometer) {
		var _this = this;

		babelHelpers.classCallCheck(this, ProgressCircle);

		this.el = el;
		this.el.appendChild(document.createElement('canvas'));
		this.label = this.el.parentNode.querySelector(".skeleton span");
		this.color = color || this.el.getAttribute("data-color");
		this.isSmall = this.el.getAttribute("data-small") === 'true';
		this.intervalId = null;
		this.options = {
			percent: 0,
			size: 360,
			lineWidth: 22,
			rotate: 0
		};
		this.barometer = barometer ? true : false;

		this.build();

		window.addEventListener("resize", function (e) {
			_this.build();
			_this.draw(_this.pct);
		});
	}

	babelHelpers.createClass(ProgressCircle, [{
		key: "build",
		value: function build() {
			// var canvas = document.createElement('canvas');
			this.canvas = Delfin("canvas", this.el);
			// this.options.size = this.el.parentNode.querySelector(".skeleton").offsetWidth * 2.12;
			var size = getComputedStyle(this.el.parentNode.querySelector(".skeleton"))["width"];
			this.options.size = parseInt(size, 10) * 2.12;

			this.ctx = this.canvas.getContext("2d");
			this.canvas.width = this.canvas.height = this.options.size;
			this.canvas.style.width = this.canvas.width / 2 + "px";
			this.canvas.style.height = this.canvas.width / 2 + "px";
			this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);

			this.ctx.translate(this.options.size / 2, this.options.size / 2); // change center
			this.ctx.rotate((-1 / 2 + this.options.rotate / 180) * Math.PI); // rotate -90 deg

			// this.draw('transparent', this.options.lineWidth, 100 / 100);
			//this.draw(this.options.percent / 100);
		}
	}, {
		key: "draw",
		value: function draw(percent) {
			var progress = percent / 100;
			var radius = (this.options.size - this.options.lineWidth) / 2;
			var color = this.color;

			progress = Math.min(Math.max(0, progress), 1);

			this.ctx.clearRect(-this.canvas.width / 2, -this.canvas.height / 2, this.canvas.width, this.canvas.height);

			this.ctx.beginPath();
			this.ctx.arc(0, 0, radius, 0, Math.PI * 2, false);
			this.ctx.strokeStyle = color;
			this.ctx.lineCap = "square"; // butt, round or square
			this.ctx.lineWidth = 2;
			this.ctx.stroke();

			this.ctx.beginPath();
			this.ctx.arc(0, 0, radius, 0, Math.PI * 2 * progress, false);
			this.ctx.strokeStyle = color;
			this.ctx.lineCap = "square"; // butt, round or square
			this.ctx.lineWidth = this.options.lineWidth * (this.isSmall ? 0.7 : 1);
			this.ctx.stroke();
		}
	}, {
		key: "update",
		value: function update(pct, label) {
			var _this2 = this;

			this.pct = pct;
			// pct = parseFloat(pct);

			var currentPct = this.options.percent;
			this.options.percent = pct;

			if (this.intervalId !== null) {
				clearInterval(this.intervalId);
			}

			this.intervalId = setInterval(function () {
				var diff = _this2.options.percent - currentPct;

				if (Math.abs(diff) > 0.1) {
					currentPct += diff * 0.03;
				} else {
					clearInterval(_this2.intervalId);
					_this2.intervalId = null;

					currentPct = pct;
				}

				_this2.draw(currentPct);

				if (_this2.barometer) {
					var value = currentPct * 100;
					var stringValue = (parseFloat(Math.round(value * 100) / 100) / 100).toFixed(1);
					_this2.updateLabel(stringValue + "%");
				}
			}, 10);

			// Update label
			// this.label.innerHTML = label;
			if (!this.barometer) {
				this.updateLabel(label);
			}
		}
	}, {
		key: "updateLabel",
		value: function updateLabel(label) {
			var strArr = [];

			if (this.barometer) {
				strArr = label.split(".");
				// this.label.innerText = strArr[0];
				this.label.querySelector(".big-number").innerHTML = strArr[0];
				this.label.querySelector(".small-number").innerHTML = "." + strArr[1];
			} else {
				this.label.innerHTML = label;
			}
		}
	}]);
	return ProgressCircle;
}();
//# sourceMappingURL=ProgressCircle.js.map
