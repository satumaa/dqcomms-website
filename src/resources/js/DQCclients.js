"use strict";

var DQCclients = function (_Screen) {
	babelHelpers.inherits(DQCclients, _Screen);

	function DQCclients(options) {
		babelHelpers.classCallCheck(this, DQCclients);

		var _this = babelHelpers.possibleConstructorReturn(this, (DQCclients.__proto__ || Object.getPrototypeOf(DQCclients)).call(this, options));

		_this.onDisplay = { from: .4, to: .9 };
		_this.color = "yellow";
		_this.setOptions();
		return _this;
	}

	babelHelpers.createClass(DQCclients, [{
		key: "setOptions",
		value: function setOptions() {
			var zoom = 1.6;

			this.options.shapes = [new Shape({
				view_port_width: 1200,
				width: 120,
				height: 486,
				x: 365,
				y: 90,
				partChange: 400,
				mask: new ImageLoader("/resources/svgs/3.png"),
				img: new ImageLoader("/resources/imgs/absolut1.jpg")

			}), new Shape({
				view_port_width: 1200,
				width: 418,
				height: 486,
				x: 1200 - 418 - 100,
				y: 90,
				mask: new ImageLoader("/resources/svgs/1.png"),
				img: new ImageLoader("/resources/imgs/absolut2.jpg")

			})];
		}
	}]);
	return DQCclients;
}(Screen);
//# sourceMappingURL=DQCclients.js.map
