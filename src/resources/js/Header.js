"use strict";

var Header = function () {
	function Header() {
		var _this = this;

		babelHelpers.classCallCheck(this, Header);

		window.addEventListener("scroll", function (e) {
			_this.update();
		});

		this.update();
	}

	babelHelpers.createClass(Header, [{
		key: "update",
		value: function update() {
			var top = d.scrollTop();
			var header = d("#header");
			// var body = d("body");

			if (top > 200) {
				header.classList.add("m-fixed");
				// body.classList.add("fixed");
			} else {
				header.classList.remove("m-fixed");
				// body.classList.remove("fixed");
			}
		}
	}]);
	return Header;
}();
//# sourceMappingURL=Header.js.map
