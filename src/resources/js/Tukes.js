"use strict";

var Tukes = function (_Screen) {
	babelHelpers.inherits(Tukes, _Screen);

	function Tukes(options) {
		babelHelpers.classCallCheck(this, Tukes);

		var _this = babelHelpers.possibleConstructorReturn(this, (Tukes.__proto__ || Object.getPrototypeOf(Tukes)).call(this, options));

		_this.onDisplay = { from: .4, to: .9 };
		_this.color = "yellow";
		_this.setOptions();
		_this.debug = true;
		return _this;
	}

	babelHelpers.createClass(Tukes, [{
		key: "setOptions",
		value: function setOptions() {
			var zoom = 1.6;

			this.options.shapes = [new Shape({
				view_port_width: 1200,
				width: 120,
				height: 462,
				x: 0,
				y: 150,

				partChange: 400,
				mask: new ImageLoader("/resources/svgs/3.png"),
				img: new ImageLoader("/resources/imgs/tukes1.jpg")

			}), new Shape({
				view_port_width: 1200,
				width: 456 * zoom,
				height: 333 * zoom,
				x: 1200 - 456 * zoom - 100,
				partChange: 400,
				y: +75,
				mask: new ImageLoader("/resources/svgs/4.png"),
				img: new ImageLoader("/resources/imgs/tukes2.jpg")

			})];
		}
	}]);
	return Tukes;
}(Screen);
//# sourceMappingURL=Tukes.js.map
