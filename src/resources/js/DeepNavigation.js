"use strict";

var DeepNavigation = function () {
	function DeepNavigation(el, props) {
		babelHelpers.classCallCheck(this, DeepNavigation);

		props = props ? props : {};

		this.parentIsLink = props.parentIslink !== undefined ? props.parentIslink : true;
		this.showActiveMenu = props.showActiveMenu !== undefined ? props.showActiveMenu : true;
		this.el = el;

		this.build();
	}

	babelHelpers.createClass(DeepNavigation, [{
		key: "build",
		value: function build() {
			var _this = this;

			$$(".parent", this.el).forEach(function (i) {
				d("a", i).insertAdjacentHTML("beforebegin", '<span class="toggle"><span>+</span><span>-</span></span>');

				// Open active menu
				if (i.classList.contains("active") && _this.showActiveMenu) {
					d("ul", i).classList.add("visible");
					d(".toggle", i).classList.add("active");
				}

				// Parent is link?
				if (_this.parentIsLink) {
					d(".toggle", i).addEventListener("click", function (e) {
						e.preventDefault();
						toggle(i, this);
					});
				} else {
					i.addEventListener("click", function (e) {
						e.preventDefault();
						toggle(i, this);
					});
				}

				var toggle = function toggle(item, button) {
					d("ul", item).classList.toggle("visible");
					button.classList.toggle("active");

					// Close children
					$$(".parent", item).forEach(function (j) {
						d("ul", j).classList.remove("visible");
						d(".toggle", j).classList.remove("active");
					});
				};
			});
		}
	}]);
	return DeepNavigation;
}();
//# sourceMappingURL=DeepNavigation.js.map
