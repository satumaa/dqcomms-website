'use strict';

var Cheerios = function () {
	function Cheerios() {
		babelHelpers.classCallCheck(this, Cheerios);


		this.container = document.querySelector('.cheerios');
		this.cheerios = document.querySelectorAll('.cheerio');

		this.width = window.innerWidth;
		this.height = window.innerHeight;
		this.update();

		window.addEventListener('resize', this.setWithHeight);
	}

	babelHelpers.createClass(Cheerios, [{
		key: 'update',
		value: function update() {
			var _this = this;

			this.cheerios.forEach(function (cheerio, index) {
				cheerio.loop = 0;
				if (index >= 0 && index < 4) {
					_this.animateFromTop(cheerio);
				} else if (index >= 4 && index < 8) {
					_this.animateFromLeft(cheerio);
				} else if (index >= 8 && index < 12) {
					_this.animateFromRight(cheerio);
				} else {
					_this.animateFromButtom(cheerio);
				}
			});
		}
	}, {
		key: 'animateFromTop',
		value: function animateFromTop(cheerio) {

			if (cheerio.loop > 10) {
				return;
			}

			var self = this;

			// 	//x,y,speed,size
			var randoms = this.getRandoms(cheerio);
			cheerio.style.width = randoms[3] + 'px';
			cheerio.style.position = 'abosolute';
			cheerio.style.left = randoms[0] + 'px';

			if (cheerio.loop > 0) {
				cheerio.loop = 0;
				cheerio.style.left = randoms[0] + 'px';
				cheerio.style.transform = 'translate(' + randoms[0] + 'px,' + -cheerio.clientHeight + 'px)';
			}

			Core.animate(cheerio, randoms[2], {
				//transform:'translate('+randoms[0]+'px, '+ (cheerio.clientHeight+ this.height)+'px) rotate(100deg)',
				transform: 'translate(' + randoms[0] + 'px, ' + (this.height + cheerio.clientHeight) + 'px) rotate(100deg)',

				complete: function complete() {
					cheerio.loop = 1;
					self.animateFromTop(cheerio);
				}
			});
		}
	}, {
		key: 'animateFromLeft',
		value: function animateFromLeft(cheerio) {

			if (cheerio.loop > 10) {
				return;
			}

			var self = this;

			// 	//x,y,speed,size
			var randoms = this.getRandoms(cheerio);
			cheerio.style.width = randoms[3] + 'px';
			cheerio.style.position = 'abosolute';
			cheerio.style.left = '0px';
			cheerio.style.top = randoms[1] + 'px';

			if (cheerio.loop > 0) {
				cheerio.loop = 0;
				cheerio.style.top = randoms[1] + 'px';
				cheerio.style.left = -cheerio.clientWidth + 'px';
				cheerio.style.transform = 'translate(' + -cheerio.clientWidth + 'px,' + randoms[1] + 'px)';
			}

			Core.animate(cheerio, randoms[2], {
				//transform:'translate('+randoms[0]+'px, '+ (cheerio.clientHeight+ this.height)+'px) rotate(100deg)',
				transform: 'translate(' + (this.width + cheerio.clientWidth) + 'px, ' + randoms[1] + 'px) rotate(100deg)',
				complete: function complete() {
					cheerio.loop = 1;
					self.animateFromLeft(cheerio);
				}
			});
		}
	}, {
		key: 'animateFromRight',
		value: function animateFromRight(cheerio) {

			if (cheerio.loop > 10) {
				return;
			}

			var self = this;

			// 	//x,y,speed,size
			var randoms = this.getRandoms(cheerio);
			cheerio.style.width = randoms[3] + 'px';
			cheerio.style.position = 'abosolute';
			cheerio.style.right = '0px';
			cheerio.style.top = randoms[1] + 'px';

			if (cheerio.loop > 0) {
				cheerio.loop = 0;
				cheerio.style.top = randoms[1] + 'px';
				cheerio.style.transform = 'translate(' + -this.width + 'px,' + randoms[1] + 'px)';
			}

			Core.animate(cheerio, randoms[2], {
				transform: 'translate(' + (-this.width - cheerio.clientWidth) + 'px, ' + randoms[1] + 'px) rotate(100deg)',
				complete: function complete() {
					cheerio.loop = 1;
					self.animateFromRight(cheerio);
				}
			});
		}
	}, {
		key: 'animateFromButtom',
		value: function animateFromButtom(cheerio) {

			if (cheerio.loop > 10) {
				return;
			}

			var self = this;

			// 	//x,y,speed,size
			var randoms = this.getRandoms(cheerio);
			cheerio.style.width = randoms[3] + 'px';
			cheerio.style.position = 'abosolute';
			cheerio.style.bottom = '0px';
			cheerio.style.left = randoms[0] + 'px';

			if (cheerio.loop > 0) {

				cheerio.loop = 0;
				cheerio.style.top = randoms[0] + 'px';
				cheerio.style.bottom = -cheerio.clientHeight + 'px';
				cheerio.style.transform = 'translate(' + randoms[0] + 'px,' + (this.height + cheerio.clientHeight) + 'px)';
			}

			Core.animate(cheerio, randoms[2], {
				transform: 'translate(' + randoms[0] + 'px, ' + (-cheerio.clientHeight - this.height) + 'px) rotate(100deg)',
				complete: function complete() {
					cheerio.loop = 1;
					self.animateFromButtom(cheerio);
				}
			});
		}
	}, {
		key: 'getRandoms',
		value: function getRandoms() {

			var randomXPosition = Math.floor(Math.random() * (this.width - 0)) + 0;
			var randomYPosition = Math.floor(Math.random() * (this.height - 0)) + 0;
			var randomSpeed = Math.floor(Math.random() * (80 - 20)) + 20;
			var randomSize = Math.floor(Math.random() * (200 - 100)) + 100;

			return [randomXPosition, randomYPosition, randomSpeed, randomSize];
		}
	}, {
		key: 'setWithHeight',
		value: function setWithHeight() {
			this.width = window.innerWidth;
			this.height = window.innerHeight;
		}
	}]);
	return Cheerios;
}();

new Cheerios();
//# sourceMappingURL=Cheerios.js.map
