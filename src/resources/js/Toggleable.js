"use strict";

var Toggleable = function () {
	function Toggleable(el) {
		babelHelpers.classCallCheck(this, Toggleable);

		$$(".title", el).forEach(function (i) {
			i.addEventListener("click", function (e) {
				this.parentNode.classList.toggle("active");
			});
		});
	}

	babelHelpers.createClass(Toggleable, null, [{
		key: "setup",
		value: function setup() {
			$$(".toggleable li").forEach(function (i) {

				if (i.toggleAttached !== true) {

					i.toggleAttached = true;
					i.addEventListener("click", function (e) {
						this.parentNode.classList.toggle("active");
						// Toggler.toggle(this.parentNode);
					});
					new Toggleable(i);
				}
			});
		}
	}]);
	return Toggleable;
}();
//# sourceMappingURL=Toggleable.js.map
