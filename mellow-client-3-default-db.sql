# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: sfb1.satumaa.kone.io (MySQL 5.5.5-10.0.26-MariaDB-1~jessie)
# Database: mellow-client-3-default
# Generation Time: 2018-07-13 09:19:07 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table mellow_bind
# ------------------------------------------------------------

DROP TABLE IF EXISTS `mellow_bind`;

CREATE TABLE `mellow_bind` (
  `ID` int(11) NOT NULL,
  `subcontent_id` int(11) NOT NULL DEFAULT '0',
  `page_id` int(11) NOT NULL DEFAULT '0',
  `value` int(11) NOT NULL DEFAULT '0',
  `timer_id` int(11) NOT NULL DEFAULT '0',
  `element` varchar(255) NOT NULL DEFAULT '',
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table mellow_booleans
# ------------------------------------------------------------

DROP TABLE IF EXISTS `mellow_booleans`;

CREATE TABLE `mellow_booleans` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `page_id` int(11) NOT NULL DEFAULT '0',
  `group_id` int(11) NOT NULL,
  `subcontent_id` int(11) NOT NULL DEFAULT '0',
  `uri` varchar(30) NOT NULL,
  `value` text NOT NULL,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `mellow_booleans` WRITE;
/*!40000 ALTER TABLE `mellow_booleans` DISABLE KEYS */;

INSERT INTO `mellow_booleans` (`ID`, `page_id`, `group_id`, `subcontent_id`, `uri`, `value`, `modified`)
VALUES
	(25,2779,0,0,'newin','1','2018-07-13 11:53:35'),
	(24,2786,0,0,'bestseller','1','2018-07-13 11:53:22'),
	(23,2699,0,0,'showatfrontpage','1','2018-07-13 11:46:06');

/*!40000 ALTER TABLE `mellow_booleans` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table mellow_collections
# ------------------------------------------------------------

DROP TABLE IF EXISTS `mellow_collections`;

CREATE TABLE `mellow_collections` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `pages_id` int(11) NOT NULL DEFAULT '0',
  `uri` varchar(255) NOT NULL,
  `something` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table mellow_folders
# ------------------------------------------------------------

DROP TABLE IF EXISTS `mellow_folders`;

CREATE TABLE `mellow_folders` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table mellow_forms
# ------------------------------------------------------------

DROP TABLE IF EXISTS `mellow_forms`;

CREATE TABLE `mellow_forms` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `form` varchar(255) NOT NULL,
  `data` text NOT NULL,
  `added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table mellow_groups
# ------------------------------------------------------------

DROP TABLE IF EXISTS `mellow_groups`;

CREATE TABLE `mellow_groups` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `page_id` int(11) NOT NULL DEFAULT '0',
  `subcontent_id` int(11) NOT NULL DEFAULT '0',
  `group_id` int(11) NOT NULL DEFAULT '0',
  `uri` varchar(40) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table mellow_imagebank
# ------------------------------------------------------------

DROP TABLE IF EXISTS `mellow_imagebank`;

CREATE TABLE `mellow_imagebank` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `mellow_folders_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL DEFAULT '',
  `type` varchar(255) NOT NULL DEFAULT '',
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `added` datetime NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


# Dump of table mellow_images
# ------------------------------------------------------------

DROP TABLE IF EXISTS `mellow_images`;

CREATE TABLE `mellow_images` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `value` int(255) NOT NULL,
  `subcontent_id` int(11) NOT NULL DEFAULT '0',
  `group_id` int(11) NOT NULL DEFAULT '0',
  `page_id` int(11) NOT NULL DEFAULT '0',
  `uri` varchar(255) NOT NULL DEFAULT '',
  `timer_id` int(11) NOT NULL DEFAULT '0',
  `type` varchar(255) NOT NULL,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


# Dump of table mellow_links
# ------------------------------------------------------------

DROP TABLE IF EXISTS `mellow_links`;

CREATE TABLE `mellow_links` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `page_id` int(11) NOT NULL DEFAULT '0',
  `group_id` int(11) NOT NULL,
  `subcontent_id` int(11) NOT NULL DEFAULT '0',
  `uri` varchar(30) NOT NULL,
  `value` text NOT NULL,
  `target` int(11) NOT NULL DEFAULT '0',
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table mellow_options
# ------------------------------------------------------------

DROP TABLE IF EXISTS `mellow_options`;

CREATE TABLE `mellow_options` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `subcontent_id` int(11) NOT NULL DEFAULT '0',
  `page_id` int(11) NOT NULL DEFAULT '0',
  `value` int(11) NOT NULL DEFAULT '0',
  `timer_id` int(11) NOT NULL DEFAULT '0',
  `element` varchar(255) NOT NULL DEFAULT '',
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table mellow_pages
# ------------------------------------------------------------

DROP TABLE IF EXISTS `mellow_pages`;

CREATE TABLE `mellow_pages` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `foreign_key` varchar(255) DEFAULT NULL,
  `myclass` varchar(255) NOT NULL DEFAULT '',
  `parentID` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL,
  `urlname` varchar(255) NOT NULL,
  `layout_ID` int(11) NOT NULL,
  `default_page` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `title` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `order_index` int(11) NOT NULL DEFAULT '0',
  `active` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `inmenu` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `start` datetime NOT NULL,
  `end` datetime NOT NULL,
  `added` date NOT NULL,
  `clock` time NOT NULL,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `foreign_key` (`foreign_key`),
  KEY `parentID` (`parentID`),
  KEY `active` (`active`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `mellow_pages` WRITE;
/*!40000 ALTER TABLE `mellow_pages` DISABLE KEYS */;

/*
-- (2,NULL,'Variables',1,'Variables','variables',0,0,'','',0,1,1,'0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00','00:00:00','2017-06-16 09:54:23'),
-- (3,NULL,'AdminSitemap',1,'Sitemap','sitemap',0,0,'','',0,1,1,'0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00','00:00:00','2017-06-16 09:54:23'),
-- (4,NULL,'AdminMetatiedot',1,'Metatiedot','metatiedot',0,0,'','',4,1,1,'0000-00-00 00:00:00','0000-00-00 00:00:00','2018-04-03','00:00:00','2018-06-07 16:21:45'),*/
INSERT INTO `mellow_pages` (`ID`, `foreign_key`, `myclass`, `parentID`, `name`, `urlname`, `layout_ID`, `default_page`, `title`, `description`, `order_index`, `active`, `inmenu`, `start`, `end`, `added`, `clock`, `modified`)
VALUES
	(1,NULL,'Admin',0,'Admin','admin',0,0,'','',0,1,1,'0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00','00:00:00','2018-06-07 16:21:45'),
	(5,NULL,'Home',0,'Home','home',0,1,'','',0,1,1,'0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00','00:00:00','2018-06-13 13:59:14'),
	(6,NULL,'PageNotFound',1,'404','404',0,0,'','',2,1,1,'0000-00-00 00:00:00','0000-00-00 00:00:00','2017-06-20','00:00:00','2018-07-12 12:11:52'),
	(7,NULL,'Hidden',0,'Terms & Conditions','terms--conditions',0,0,'','',10,1,1,'0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00','00:00:00','2018-06-07 16:17:53'),
	(8,NULL,'Hidden',0,'Privacy Policy','privacy-policy',0,0,'','',11,1,1,'0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00','00:00:00','2018-06-07 16:18:07');

/*!40000 ALTER TABLE `mellow_pages` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table mellow_redirects
# ------------------------------------------------------------

DROP TABLE IF EXISTS `mellow_redirects`;

CREATE TABLE `mellow_redirects` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `pages_id` int(11) NOT NULL DEFAULT '0',
  `urltogo` text NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table mellow_select
# ------------------------------------------------------------

DROP TABLE IF EXISTS `mellow_select`;

CREATE TABLE `mellow_select` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `page_id` int(11) NOT NULL DEFAULT '0',
  `group_id` int(11) NOT NULL,
  `subcontent_id` int(11) NOT NULL DEFAULT '0',
  `uri` varchar(30) NOT NULL,
  `value` text NOT NULL,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table mellow_selectableitems
# ------------------------------------------------------------

DROP TABLE IF EXISTS `mellow_selectableitems`;

CREATE TABLE `mellow_selectableitems` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `page_id` int(11) NOT NULL DEFAULT '0',
  `group_id` int(11) NOT NULL,
  `subcontent_id` int(11) NOT NULL DEFAULT '0',
  `uri` varchar(30) NOT NULL,
  `value` int(11) NOT NULL,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table mellow_selected_content
# ------------------------------------------------------------

DROP TABLE IF EXISTS `mellow_selected_content`;

CREATE TABLE `mellow_selected_content` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `module_id` int(11) NOT NULL,
  `uri` varchar(255) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table mellow_subcontent
# ------------------------------------------------------------

DROP TABLE IF EXISTS `mellow_subcontent`;

CREATE TABLE `mellow_subcontent` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `subcontent_id` int(11) NOT NULL DEFAULT '0',
  `uri` varchar(255) NOT NULL DEFAULT '',
  `order_index` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `mellow_subcontent` WRITE;
/*!40000 ALTER TABLE `mellow_subcontent` DISABLE KEYS */;

INSERT INTO `mellow_subcontent` (`ID`, `subcontent_id`, `uri`, `order_index`)
VALUES
	(1,1,'block',1);

/*!40000 ALTER TABLE `mellow_subcontent` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table mellow_subcontents
# ------------------------------------------------------------

DROP TABLE IF EXISTS `mellow_subcontents`;

CREATE TABLE `mellow_subcontents` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `page_id` int(11) NOT NULL DEFAULT '0',
  `subcontent_id` int(11) NOT NULL DEFAULT '0',
  `group_id` int(11) NOT NULL DEFAULT '0',
  `uri` varchar(40) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


# Dump of table mellow_tables
# ------------------------------------------------------------

DROP TABLE IF EXISTS `mellow_tables`;

CREATE TABLE `mellow_tables` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `mellow_subcontents_id` int(11) NOT NULL,
  `cellcount` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table mellow_text
# ------------------------------------------------------------

DROP TABLE IF EXISTS `mellow_text`;

CREATE TABLE `mellow_text` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `page_id` int(11) NOT NULL DEFAULT '0',
  `group_id` int(11) NOT NULL,
  `subcontent_id` int(11) NOT NULL DEFAULT '0',
  `uri` varchar(30) NOT NULL,
  `value` text NOT NULL,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`),
  KEY `page_id` (`page_id`),
  KEY `group_id` (`group_id`),
  KEY `subcontent_id` (`subcontent_id`),
  KEY `uri` (`uri`),
  FULLTEXT KEY `value` (`value`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table mellow_timer
# ------------------------------------------------------------

DROP TABLE IF EXISTS `mellow_timer`;

CREATE TABLE `mellow_timer` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `pages_id` int(11) NOT NULL DEFAULT '0',
  `subcontent_id` int(11) NOT NULL DEFAULT '0',
  `element` varchar(255) NOT NULL DEFAULT '',
  `name` varchar(255) NOT NULL DEFAULT '',
  `start` date NOT NULL,
  `end` date NOT NULL,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table mellow_timers
# ------------------------------------------------------------

DROP TABLE IF EXISTS `mellow_timers`;

CREATE TABLE `mellow_timers` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `page_id` int(11) NOT NULL DEFAULT '0',
  `group_id` int(11) NOT NULL,
  `subcontent_id` int(11) NOT NULL DEFAULT '0',
  `uri` varchar(30) NOT NULL,
  `start` datetime NOT NULL,
  `end` datetime NOT NULL,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table mellow_url
# ------------------------------------------------------------

DROP TABLE IF EXISTS `mellow_url`;

CREATE TABLE `mellow_url` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `subcontent_id` int(11) NOT NULL DEFAULT '0',
  `page_id` int(11) NOT NULL DEFAULT '0',
  `timer_id` int(11) NOT NULL DEFAULT '0',
  `value` text NOT NULL,
  `element` varchar(255) NOT NULL DEFAULT '',
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`),
  FULLTEXT KEY `value` (`value`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table mellow_users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `mellow_users`;

CREATE TABLE `mellow_users` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL DEFAULT '',
  `langCode` varchar(15) NOT NULL DEFAULT 'fi',
  `pwd` varchar(255) NOT NULL DEFAULT '',
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `mellow_users` WRITE;
/*!40000 ALTER TABLE `mellow_users` DISABLE KEYS */;


INSERT INTO `mellow_users` (`ID`, `username`, `langCode`, `pwd`, `modified`)
VALUES
	(1, 'test', 'en', '098f6bcd4621d373cade4e832627b4f6','2018-07-09 11:25:05'),
	(2, 'jukka.kokko@sfbagency.com', 'en', 'dfa5be2e7aabe71732a4a7a6e38f81d4', '2016-06-21 15:50:36'),
	(3, 'lauri.suopera@sfbagency.com', 'en', '5069524eddfda3f74fcbec2a4ded2e1e', '2016-06-21 15:50:36'),
	(4, 'liam.manderson@sfbagency.com', 'en', '0d107d09f5bbe40cade3de5c71e9e9b7', '2016-06-21 15:50:36'),
	(5, 'juuso.saarinen@sfbagency.com', 'en', 'b0a78582d6f9fcaf0d0c5b191e56a79d', '2016-06-21 15:50:36'),
	(6, 'noora.fredman@sfbagency.com', 'en', 'd9e6185617665edcdcc61d6bb0bf07ef', '2016-06-21 15:50:36'),
  (7, 'hunter.rafuse@sfbagency.com', 'en', '95e2dbfa2899593c0f167058ac3f3929', '2018-07-18 12:30:02'),
  (8, 'shovit.thapa@sfbagency.com', 'en', '9edfc23ac482d09f7f7beef957153b08', '2018-07-18 12:35:21');


/*!40000 ALTER TABLE `mellow_users` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table mellow_youtube
# ------------------------------------------------------------

DROP TABLE IF EXISTS `mellow_youtube`;

CREATE TABLE `mellow_youtube` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `page_id` int(11) NOT NULL DEFAULT '0',
  `group_id` int(11) NOT NULL,
  `subcontent_id` int(11) NOT NULL DEFAULT '0',
  `uri` varchar(30) NOT NULL,
  `value` text NOT NULL,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`),
  FULLTEXT KEY `value` (`value`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table modules
# ------------------------------------------------------------

DROP TABLE IF EXISTS `modules`;

CREATE TABLE `modules` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `page_id` int(11) NOT NULL DEFAULT '0',
  `collection_id` int(11) NOT NULL DEFAULT '0',
  `subcontent_id` int(11) NOT NULL DEFAULT '0',
  `modules_parent_id` int(11) NOT NULL DEFAULT '0',
  `class` varchar(255) NOT NULL,
  `uri` varchar(255) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `page_id` (`page_id`,`collection_id`,`subcontent_id`,`uri`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table translations
# ------------------------------------------------------------

DROP TABLE IF EXISTS `translations`;

CREATE TABLE `translations` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `translation_key` varchar(255) NOT NULL,
  `lang_fi` text NOT NULL,
  `lang_en` text NOT NULL,
  `lang_sv` text NOT NULL,
  `admin_translation` tinyint(4) NOT NULL DEFAULT '0',
  `added` datetime NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `translations` WRITE;
/*!40000 ALTER TABLE `translations` DISABLE KEYS */;

INSERT INTO `translations` (`ID`, `translation_key`, `lang_fi`, `lang_en`, `lang_sv`, `admin_translation`, `added`)
VALUES
	(1,'template.context.edit','Rename','','',1,'2018-06-05 15:14:43'),
	(2,'template.context.group.add','Add new','','',1,'2018-06-05 15:14:43'),
	(3,'template.context.group.move','Move','','',1,'2018-06-05 15:14:43'),
	(4,'template.context.remove','Delete','','',1,'2018-06-05 15:14:43'),
	(5,'template.context.move.down','Down','','',1,'2018-06-05 15:14:43'),
	(6,'template.context.unpublish','Unpublish','','',1,'2018-06-05 15:14:43'),
	(7,'meta.default.title','','','',0,'2018-06-05 15:14:43'),
	(8,'metatitle.context.edit','','','',0,'2018-06-05 15:14:43'),
	(9,'meta.default.description','','','',0,'2018-06-05 15:14:43'),
	(10,'metadesc.context.edit','','','',0,'2018-06-05 15:14:43'),
	(11,'template.context.add.metatiedot','','','',0,'2018-06-05 15:14:43'),
	(12,'template.context.publish','Publish','','',1,'2018-06-05 15:14:43'),
	(13,'template.context.add.subcategory','Category','','',1,'2018-06-05 15:14:43'),
	(14,'template.context.move.up','Up','','',1,'2018-06-05 15:14:43'),
	(15,'template.context.add.product','Product with colors','','',1,'2018-06-05 15:14:43'),
	(16,'template.context.add.article','Article','','',1,'2018-06-05 15:14:43'),
	(17,'subcategory.context.edit','Rename category','','',1,'2018-06-05 15:14:43'),
	(18,'subcategory.context.group.add','Add new','','',1,'2018-06-05 15:14:43'),
	(19,'subcategory.context.add.subcategory','Category','','',1,'2018-06-05 15:14:43'),
	(20,'subcategory.context.group.move','Move Category','','',1,'2018-06-05 15:14:43'),
	(21,'subcategory.context.remove','Delete category','','',1,'2018-06-05 15:14:43'),
	(22,'subcategory.context.move.down','Down','','',1,'2018-06-05 15:14:43'),
	(23,'subcategory.context.unpublish','Unpublish Category','','',1,'2018-06-05 15:14:43'),
	(24,'subcategory.context.move.up','Up','','',1,'2018-06-05 15:14:43'),
	(25,'subcategory.context.publish','Publish Category','','',1,'2018-06-05 15:14:43'),
	(26,'categoryimage.context.edit','Select category image','','',1,'2018-06-05 15:14:43'),
	(27,'subcategory.context.add.product','Product','','',1,'2018-06-05 15:14:43'),
	(28,'template.showallproducts','Show all products','Show all products','Show all products',0,'2018-06-05 15:14:43'),
	(29,'product.context.edit','Edit product name and short description','','',1,'2018-06-05 15:14:43'),
	(30,'product.context.group.add','Add new','','',1,'2018-06-05 15:14:43'),
	(31,'product.context.add.color','Color variation','','',1,'2018-06-05 15:14:43'),
	(32,'product.context.group.move','Move product','','',1,'2018-06-05 15:14:43'),
	(33,'product.context.remove','Delete product','','',1,'2018-06-05 15:14:43'),
	(34,'product.context.move.down','Down','','',1,'2018-06-05 15:14:43'),
	(35,'product.context.publish','Publish product','','',1,'2018-06-05 15:14:43'),
	(36,'product.context.move.up','Up','','',1,'2018-06-05 15:14:43'),
	(37,'image.context.edit','Select image','','',1,'2018-06-05 15:14:43'),
	(38,'shorttext.context.edit','Edit products short description','','',1,'2018-06-05 15:14:43'),
	(39,'text.context.edit','Edit text','','',1,'2018-06-05 15:14:43'),
	(40,'buttonlink.context.edit','Edit button link','','',1,'2018-06-05 15:14:43'),
	(41,'buttontext.context.edit','Edit button text','','',1,'2018-06-05 15:14:43'),
	(42,'carousel.context.add.block','Add new slide','','',1,'2018-06-05 15:14:43'),
	(43,'carousel.context.remove.block','Delete slide','','',1,'2018-06-05 15:14:43'),
	(44,'carousel.context.group.move','Move slide','','',1,'2018-06-05 15:14:43'),
	(45,'carousel.context.move.down','Down','','',1,'2018-06-05 15:14:43'),
	(46,'carousel.context.move.up','Up','','',1,'2018-06-05 15:14:43'),
	(47,'smalltitle.context.edit','Edit subtopic','','',1,'2018-06-05 15:14:43'),
	(48,'title.context.edit','Edit topic','','',1,'2018-06-05 15:14:43'),
	(49,'template.form.topic.create','Create new page','','',1,'2018-06-05 15:14:44'),
	(50,'template.form.topic.edit','Edit page','','',1,'2018-06-05 15:14:44'),
	(51,'template.form.send.edit','Save','','',1,'2018-06-05 15:14:44'),
	(52,'template.form.send.create','Save','','',1,'2018-06-05 15:14:44'),
	(53,'template.form.label.create.name','Name of the page','','',1,'2018-06-05 15:14:44'),
	(54,'template.form.label.edit.name','Name of the page','','',1,'2018-06-05 15:14:44'),
	(55,'title.form.label.edit.name','Edit topic','','',1,'2018-06-05 15:14:44'),
	(56,'title.form.label.create.name','Edit topic','','',1,'2018-06-05 15:14:44'),
	(57,'smalltitle.form.label.edit.name','Edit subtopic','','',1,'2018-06-05 15:14:44'),
	(58,'smalltitle.form.label.create.name','Edit subtopic','','',1,'2018-06-05 15:14:44'),
	(59,'buttontext.form.label.edit.name','Edit Button Text','','',1,'2018-06-05 15:14:44'),
	(60,'buttontext.form.label.create.name','Edit subtopic','','',1,'2018-06-05 15:14:44'),
	(61,'buttonlink.form.label.edit.name','Select Button link','','',1,'2018-06-05 15:14:44'),
	(62,'buttonlink.form.label.create.name','Select Button link','','',1,'2018-06-05 15:14:44'),
	(63,'metatitle.form.label.edit.name','','','',0,'2018-06-05 15:14:44'),
	(64,'metatitle.form.label.create.name','','','',0,'2018-06-05 15:14:44'),
	(65,'metadesc.form.label.edit.name','','','',0,'2018-06-05 15:14:44'),
	(66,'metadesc.form.label.create.name','','','',0,'2018-06-05 15:14:44'),
	(67,'text.form.label.edit.name','Edit text','','',1,'2018-06-05 15:14:44'),
	(68,'text.form.label.create.name','Edit text','','',1,'2018-06-05 15:14:44'),
	(69,'ingressi.context.edit','Edit ingres','','',1,'2018-06-05 15:15:00'),
	(70,'ingressi.form.label.edit.name','Edit ingres','','',1,'2018-06-05 15:15:00'),
	(71,'ingressi.form.label.create.name','Edit ingres','','',1,'2018-06-05 15:15:00'),
	(72,'subcategory.form.topic.create','Add new category','','',1,'2018-06-05 15:17:13'),
	(73,'subcategory.form.topic.edit','Rename category','','',1,'2018-06-05 15:17:13'),
	(74,'subcategory.form.send.edit','Save','','',1,'2018-06-05 15:17:13'),
	(75,'subcategory.form.send.create','Save','','',1,'2018-06-05 15:17:13'),
	(78,'product.context.unpublish','Unpublish product','','',1,'2018-06-05 15:18:47'),
	(79,'hero.home','Home','Home','Home',0,'2018-06-05 15:18:47'),
	(80,'template.products','Products','Products','Products',0,'2018-06-05 15:18:47'),
	(81,'color.context.edit','Edit color name and rgb- value','','',1,'2018-06-05 15:18:47'),
	(82,'color.context.group.add','Add Group','','',0,'2018-06-05 15:18:47'),
	(83,'color.context.group.move','Move Color','','',1,'2018-06-05 15:18:47'),
	(84,'color.context.remove','Delete color','','',1,'2018-06-05 15:18:47'),
	(85,'color.context.move.down','Down','','',1,'2018-06-05 15:18:47'),
	(86,'color.context.unpublish','Unpublish color','','',1,'2018-06-05 15:18:47'),
	(87,'color.context.move.up','UP','','',1,'2018-06-05 15:18:47'),
	(88,'images.context.add.productimage','Add new product image','','',1,'2018-06-05 15:18:47'),
	(89,'productimage.context.remove.block','Remove product image','','',1,'2018-06-05 15:18:47'),
	(90,'productimage.context.group.move','Move product image','','',1,'2018-06-05 15:18:47'),
	(91,'productimage.context.move.down','Down','','',1,'2018-06-05 15:18:47'),
	(92,'productimage.context.move.up','Up','','',1,'2018-06-05 15:18:47'),
	(93,'product.discover','Discover','Discover','Discover',0,'2018-06-05 15:18:47'),
	(94,'product.form.topic.create','Add new Product','','',1,'2018-06-05 15:26:24'),
	(95,'product.form.topic.edit','Edit Product','','',1,'2018-06-05 15:26:24'),
	(96,'product.form.send.edit','Save','','',1,'2018-06-05 15:26:24'),
	(97,'product.form.send.create','Save','','',1,'2018-06-05 15:26:24'),
	(98,'product.form.label.create.name','Product name','','',1,'2018-06-05 15:26:24'),
	(99,'product.form.label.edit.name','Product name','','',1,'2018-06-05 15:26:24'),
	(100,'topic.context.edit','Edit topic','','',1,'2018-06-05 15:26:24'),
	(101,'features.context.add.feature','Add New Feature','','',1,'2018-06-05 15:26:24'),
	(102,'longtext.context.edit','Edit long description','','',1,'2018-06-05 15:26:24'),
	(103,'topic.form.label.edit.name','Edit topic','','',1,'2018-06-05 15:26:24'),
	(104,'topic.form.label.create.name','Edit topic','','',1,'2018-06-05 15:26:24'),
	(105,'link.context.edit','Select link','','',1,'2018-06-05 15:26:24'),
	(106,'link.form.label.edit.name','Select link','','',1,'2018-06-05 15:26:24'),
	(107,'link.form.label.create.name','Select link','','',1,'2018-06-05 15:26:24'),
	(108,'feature.context.remove.block','Remove Feature','','',1,'2018-06-05 15:26:24'),
	(109,'feature.context.group.move','Move','','',1,'2018-06-05 15:26:24'),
	(110,'feature.context.move.down','Down','','',1,'2018-06-05 15:26:24'),
	(111,'feature.context.move.up','Up','','',1,'2018-06-05 15:26:24'),
	(112,'featurecontent.Lisää','Add Content','','',1,'2018-06-05 15:26:24'),
	(113,'featurecontent.context.add.text','Text','','',1,'2018-06-05 15:26:24'),
	(114,'featurecontent.context.add.topic','Topic','','',1,'2018-06-05 15:26:24'),
	(115,'topic.context.remove.block','Remove topic','','',1,'2018-06-05 15:26:24'),
	(116,'topic.context.group.move','Move topic','','',1,'2018-06-05 15:26:24'),
	(117,'topic.context.move.down','Down','','',1,'2018-06-05 15:26:24'),
	(118,'text.context.remove.block','Delete text','','',1,'2018-06-05 15:26:24'),
	(119,'text.context.group.move','Move Text','','',1,'2018-06-05 15:26:24'),
	(120,'text.context.move.up','Up','','',1,'2018-06-05 15:26:24'),
	(121,'text.context.move.down','Down','','',1,'2018-06-05 15:26:24'),
	(122,'topic.context.move.up','Up','','',1,'2018-06-05 15:26:24'),
	(123,'shorttext.form.label.edit.name','Short Description','','',1,'2018-06-05 15:26:24'),
	(124,'shorttext.form.label.create.name','Short Description','','',1,'2018-06-05 15:26:24'),
	(125,'longtext.form.label.edit.name','Product description','','',1,'2018-06-05 15:26:24'),
	(126,'longtext.form.label.create.name','Product description','','',1,'2018-06-05 15:26:24'),
	(127,'product.home','Home','Home','',0,'2018-06-05 15:29:45'),
	(128,'product.colors','Colors','Colors','',0,'2018-06-05 15:29:45'),
	(129,'color.form.topic.create','Add Color variation','','',1,'2018-06-05 15:29:46'),
	(130,'color.form.topic.edit','Edit Color Variation','','',1,'2018-06-05 15:29:46'),
	(131,'color.form.send.edit','Save','','',1,'2018-06-05 15:29:46'),
	(132,'color.form.send.create','Save','','',1,'2018-06-05 15:29:46'),
	(133,'color.form.label.create.name','Name of the color','','',0,'2018-06-05 15:29:46'),
	(134,'color.form.label.edit.name','Name of the color','','',0,'2018-06-05 15:29:46'),
	(135,'productimage.context.edit','Select product image','','',1,'2018-06-05 15:30:34'),
	(136,'subcategory.products','Products','Products','',0,'2018-06-05 15:31:14'),
	(137,'shortdesc.form.label.edit.name','Short Description','','',1,'2018-06-05 15:39:39'),
	(138,'shortdesc.form.label.create.name','Short Description','','',1,'2018-06-05 15:39:39'),
	(139,'shortdesc.context.edit','Edit Short Description','','',1,'2018-06-05 15:39:48'),
	(140,'template.discover','Discover','Discover','Discover',0,'2018-06-05 15:39:48'),
	(141,'subcategory.form.label.name.create','Category name','','',1,'2018-06-05 16:02:16'),
	(142,'subcategory.form.label.name.edit','Category name','','',1,'2018-06-05 16:02:16'),
	(143,'product.form.label.name.create','Product name','','',1,'2018-06-05 16:02:19'),
	(144,'product.form.label.name.edit','Product name','','',1,'2018-06-05 16:02:19'),
	(145,'color.form.label.name.create','Color name','','',1,'2018-06-05 16:10:40'),
	(146,'color.form.label.name.edit','Color name','','',1,'2018-06-05 16:10:40'),
	(147,'template.form.label.name.create','Name of the page','','',1,'2018-06-05 16:12:54'),
	(148,'template.form.label.name.edit','Name of the page','','',1,'2018-06-05 16:12:54'),
	(149,'rgb.context.edit','Edit RGB- value','','',1,'2018-06-05 16:16:44'),
	(150,'rgb.form.label.edit.name','Color RGB- value','','',1,'2018-06-05 16:16:48'),
	(151,'rgb.form.label.create.name','Color RGB- value','','',1,'2018-06-05 16:16:48'),
	(152,'template.context.add.simplecategory','Add Category','','',1,'2018-06-05 16:48:41'),
	(153,'subcategory.context.add.simpleproduct','Product','','',1,'2018-06-05 16:55:59'),
	(154,'simpleproduct.form.topic.create','Name of the product','','',1,'2018-06-05 17:01:00'),
	(155,'simpleproduct.form.topic.edit','Name of the product','','',1,'2018-06-05 17:01:00'),
	(156,'simpleproduct.form.send.edit','Save','','',1,'2018-06-05 17:01:00'),
	(157,'simpleproduct.form.send.create','Save','','',1,'2018-06-05 17:01:00'),
	(158,'simpleproduct.form.label.name.create','Name of the product','','',1,'2018-06-05 17:01:00'),
	(159,'simpleproduct.form.label.name.edit','Name of the product','','',1,'2018-06-05 17:01:00'),
	(160,'simpleproduct.context.edit','Rename product','','',1,'2018-06-05 17:01:00'),
	(161,'simpleproduct.context.group.add','Add new','','',0,'2018-06-05 17:02:13'),
	(162,'simpleproduct.context.add.color','Color variation','','',0,'2018-06-05 17:02:13'),
	(163,'simpleproduct.context.group.move','Move Product','','',1,'2018-06-05 17:02:13'),
	(164,'simpleproduct.context.remove','Delete product','','',1,'2018-06-05 17:02:13'),
	(165,'simpleproduct.context.publish','Publish product','','',1,'2018-06-05 17:02:13'),
	(166,'simpleproduct.home','Home','Home','',0,'2018-06-05 17:02:47'),
	(167,'simpleproduct.colors','Colors','Colors','',0,'2018-06-05 17:02:47'),
	(168,'simpleproduct.discover','Discover','Discover','',0,'2018-06-05 17:06:51'),
	(169,'color.context.publish','Publish color','','',1,'2018-06-05 17:34:28'),
	(175,'topicandtext.context.remove.block','Remove topic and text','','',1,'2018-06-05 17:42:12'),
	(176,'topicandtext.context.group.move','Move topic and text','','',1,'2018-06-05 17:42:12'),
	(177,'topicandtext.context.move.down','Down','','',1,'2018-06-05 17:42:20'),
	(178,'large-image.context.remove.block','Remove banner','','',1,'2018-06-05 17:42:20'),
	(179,'large-image.context.group.move','Move Banner','','',1,'2018-06-05 17:42:20'),
	(180,'large-image.context.move.up','Up','','',1,'2018-06-05 17:42:20'),
	(181,'topicandingres.context.remove.block','Remove topic and ingres','','',1,'2018-06-06 15:32:33'),
	(182,'topicandingres.context.group.move','Move topic and ingres','','',1,'2018-06-06 15:32:33'),
	(183,'topicandingres.context.move.up','Up','','',1,'2018-06-06 15:32:33'),
	(184,'topicandingres.context.move.down','Down','','',1,'2018-06-06 15:32:33'),
	(185,'topicandtext.context.move.up','Up','','',1,'2018-06-06 15:32:33'),
	(186,'nosto.context.remove.block','Remove feature banner','','',1,'2018-06-06 15:32:33'),
	(187,'nosto.context.group.move','Move feature banner','','',1,'2018-06-06 15:32:33'),
	(188,'nosto.context.move.up','Up','','',1,'2018-06-06 15:32:33'),
	(189,'nosto.context.move.down','Down','','',1,'2018-06-06 15:32:33'),
	(190,'large-image.context.move.down','Down','','',1,'2018-06-06 15:32:33'),
	(195,'carouselbutton.context.edit','Edit Slide navigation text','','',1,'2018-06-07 09:56:14'),
	(196,'carouselbutton.form.label.edit.name','Slide navigation text','','',1,'2018-06-07 09:56:15'),
	(197,'carouselbutton.form.label.create.name','Slide navigation text','','',1,'2018-06-07 09:56:15'),
	(199,'subcategory.context.add.smallbanner','Small banner','','',1,'2018-06-07 12:38:17'),
	(200,'smallbanner.form.topic.create','Topic','','',1,'2018-06-07 12:45:21'),
	(201,'smallbanner.form.topic.edit','Edit topic','','',1,'2018-06-07 12:45:21'),
	(202,'smallbanner.form.send.edit','Save','','',1,'2018-06-07 12:45:21'),
	(203,'smallbanner.form.send.create','Save','','',1,'2018-06-07 12:45:21'),
	(204,'smallbanner.form.label.name.create','Name of banner','','',1,'2018-06-07 12:45:21'),
	(205,'smallbanner.form.label.name.edit','Name of banner','','',1,'2018-06-07 12:45:21'),
	(206,'smallbanner.context.edit','Edit name of banner','','',1,'2018-06-07 12:45:21'),
	(207,'smallbanner.context.group.add','Add','','',1,'2018-06-07 12:46:05'),
	(208,'smallbanner.context.group.move','Move banner','','',1,'2018-06-07 12:46:05'),
	(209,'smallbanner.context.remove','Delete banner','','',1,'2018-06-07 12:46:05'),
	(210,'smallbanner.context.move.up','Up','','',1,'2018-06-07 12:46:05'),
	(211,'smallbanner.context.publish','Publish banner','','',1,'2018-06-07 12:46:05'),
	(212,'bannerimage.context.edit','Select banner image','','',1,'2018-06-07 12:53:03'),
	(213,'smallbannerimage.context.edit','Select banner image','','',1,'2018-06-07 12:53:05'),
	(214,'smallbanner.context.move.down','Down','','',1,'2018-06-07 13:11:49'),
	(215,'smallbanner.context.unpublish','Unpublish banner','','',1,'2018-06-07 13:12:02'),
	(216,'title.form.topic.edit','','','',0,'2018-06-07 14:48:31'),
	(217,'title.form.topic.create','','','',0,'2018-06-07 14:48:31'),
	(218,'ingressi.form.topic.edit','Edit Ingres','','',1,'2018-06-07 14:48:31'),
	(219,'ingressi.form.topic.create','Edit Ingres','','',1,'2018-06-07 14:48:31'),
	(220,'metatitle.form.topic.edit','','','',0,'2018-06-07 14:48:31'),
	(221,'metatitle.form.topic.create','','','',0,'2018-06-07 14:48:31'),
	(222,'metadesc.form.topic.edit','','','',0,'2018-06-07 14:48:31'),
	(223,'metadesc.form.topic.create','','','',0,'2018-06-07 14:48:31'),
	(224,'text.form.topic.edit','Edit text','','',1,'2018-06-07 14:48:31'),
	(225,'text.form.topic.create','Edit text','','',1,'2018-06-07 14:48:31'),
	(226,'buttontext.form.topic.edit','','','',0,'2018-06-07 14:48:31'),
	(227,'buttontext.form.topic.create','','','',0,'2018-06-07 14:48:31'),
	(228,'topic.form.topic.edit','Edit topic','','',1,'2018-06-07 14:48:34'),
	(229,'topic.form.topic.create','Edit topic','','',1,'2018-06-07 14:48:34'),
	(230,'rgb.form.topic.edit','Edit RGB- color','','',1,'2018-06-07 14:49:03'),
	(231,'rgb.form.topic.create','Edit RGB- color','','',1,'2018-06-07 14:49:03'),
	(232,'shorttext.form.topic.edit','Short Description','','',1,'2018-06-07 14:49:28'),
	(233,'shorttext.form.topic.create','Short Description','','',1,'2018-06-07 14:49:28'),
	(234,'longtext.form.topic.edit','Long Description','','',1,'2018-06-07 14:49:28'),
	(235,'longtext.form.topic.create','Long Description','','',1,'2018-06-07 14:49:28'),
	(236,'topic.form.send.edit','Save','','',1,'2018-06-07 14:51:51'),
	(237,'topic.form.send.create','Save','','',1,'2018-06-07 14:51:51'),
	(238,'text.form.send.edit','Save','','',1,'2018-06-07 14:51:51'),
	(239,'text.form.send.create','Save','','',1,'2018-06-07 14:51:51'),
	(240,'title.form.send.edit','Save','','',1,'2018-06-07 14:51:51'),
	(241,'title.form.send.create','Save','','',1,'2018-06-07 14:51:51'),
	(242,'shorttext.form.send.edit','Save','','',1,'2018-06-07 14:51:51'),
	(243,'shorttext.form.send.create','Save','','',1,'2018-06-07 14:51:51'),
	(244,'longtext.form.send.edit','Save','','',1,'2018-06-07 14:51:51'),
	(245,'longtext.form.send.create','Save','','',1,'2018-06-07 14:51:51'),
	(246,'metatitle.form.send.edit','Save','','',1,'2018-06-07 14:51:51'),
	(247,'metatitle.form.send.create','Save','','',1,'2018-06-07 14:51:51'),
	(248,'metadesc.form.send.edit','Save','','',1,'2018-06-07 14:51:51'),
	(249,'metadesc.form.send.create','Save','','',1,'2018-06-07 14:51:51'),
	(250,'buttontext.form.send.edit','Save','','',1,'2018-06-07 14:51:51'),
	(251,'buttontext.form.send.create','Save','','',1,'2018-06-07 14:51:51'),
	(252,'rgb.form.send.edit','Save','','',1,'2018-06-07 14:51:59'),
	(253,'rgb.form.send.create','Save','','',1,'2018-06-07 14:51:59'),
	(254,'smalltitle.form.topic.edit','Edit text','','',1,'2018-06-07 14:52:57'),
	(255,'smalltitle.form.topic.create','Edit text','','',1,'2018-06-07 14:52:57'),
	(256,'smalltitle.form.send.edit','Save','','',1,'2018-06-07 14:52:57'),
	(257,'smalltitle.form.send.create','Save','','',1,'2018-06-07 14:52:57'),
	(258,'carouselbutton.form.topic.edit','Edit Slide navigation text','','',1,'2018-06-07 14:52:57'),
	(259,'carouselbutton.form.topic.create','Edit Slide navigation text','','',1,'2018-06-07 14:52:57'),
	(260,'carouselbutton.form.send.edit','Save','','',1,'2018-06-07 14:52:57'),
	(261,'carouselbutton.form.send.create','Save','','',1,'2018-06-07 14:52:57'),
	(262,'ingressi.form.send.edit','Save','','',1,'2018-06-07 14:53:12'),
	(263,'ingressi.form.send.create','Save','','',1,'2018-06-07 14:53:12'),
	(264,'shortdesc.form.topic.edit','Short description','','',1,'2018-06-07 14:57:20'),
	(265,'shortdesc.form.topic.create','Short description','','',1,'2018-06-07 14:57:20'),
	(266,'shortdesc.form.send.edit','Save','','',1,'2018-06-07 14:57:20'),
	(267,'shortdesc.form.send.create','Save','','',1,'2018-06-07 14:57:20'),
	(268,'selectableitems.form.label.edit','Select category','','',1,'2018-06-07 15:19:42'),
	(269,'selectableitems.form.label.create','Select category','','',1,'2018-06-07 15:19:42'),
	(270,'listimg.context.edit','Select Listview- image','','',1,'2018-06-07 15:25:42'),
	(271,'listimg.context.remove','Remove Listview- image','','',1,'2018-06-07 15:27:18'),
	(272,'listimg.context.clear','Remove Listview- image','','',1,'2018-06-07 15:34:07'),
	(273,'simpleproduct.context.unpublish','Unpublish','','',1,'2018-06-07 16:21:51'),
	(279,'variablesrow.context.remove.block','Remove Block','','',0,'2018-06-08 08:06:41'),
	(280,'variablesrow.context.group.move','Move Group','','',0,'2018-06-08 08:06:41'),
	(281,'selectable.context.remove.block','Remove','','',1,'2018-06-08 08:16:34'),
	(282,'selectable.context.group.move','Move','','',1,'2018-06-08 08:16:35'),
	(283,'nameofcolor.context.edit','Edit name of the color','','',1,'2018-06-08 08:18:11'),
	(284,'nameofcolor.form.label.edit.name','Edit name','','',1,'2018-06-08 08:18:11'),
	(285,'nameofcolor.form.label.create.name','Edit name','','',1,'2018-06-08 08:18:11'),
	(286,'nameofcolor.form.topic.edit','Name of the color','','',1,'2018-06-08 08:18:11'),
	(287,'nameofcolor.form.topic.create','Name of the color','','',1,'2018-06-08 08:18:11'),
	(288,'nameofcolor.form.send.edit','Save','','',1,'2018-06-08 08:18:11'),
	(289,'nameofcolor.form.send.create','Save','','',1,'2018-06-08 08:18:11'),
	(290,'selectable.context.move.down','Down','','',1,'2018-06-08 08:23:57'),
	(291,'selectable.context.move.up','Up','','',1,'2018-06-08 08:23:57'),
	(293,'video.context.remove.block','Remove video','','',1,'2018-06-11 13:56:34'),
	(294,'video.context.group.move','Move video','','',1,'2018-06-11 13:56:34'),
	(295,'video.context.move.up','Up','','',1,'2018-06-11 13:56:34'),
	(296,'simpleproduct.context.move.down','Down','','',1,'2018-06-12 11:17:27'),
	(297,'youtube.context.edit','Select Youtube video','','',1,'2018-06-12 13:30:55'),
	(298,'videocoverimage.context.edit','Select Poster- image','','',1,'2018-06-12 13:30:55'),
	(299,'video.context.move.down','Down','','',1,'2018-06-12 13:54:50'),
	(300,'youtube.form.label.edit.name','Youtube ID','','',1,'2018-06-12 14:00:19'),
	(301,'youtube.form.label.create.name','Youtube ID','','',1,'2018-06-12 14:00:19'),
	(302,'youtube.form.topic.edit','Select Youtube Video','','',1,'2018-06-12 14:00:19'),
	(303,'youtube.form.topic.create','Select Youtube Video','','',1,'2018-06-12 14:00:19'),
	(304,'youtube.form.send.edit','Save','','',1,'2018-06-12 14:00:19'),
	(305,'youtube.form.send.create','Save','','',1,'2018-06-12 14:00:19'),
	(306,'simpleproduct.context.move.up','Move Up','','',0,'2018-06-13 13:30:28'),
	(307,'template.filter.by','Filter By','','',0,'2018-06-13 14:13:33'),
	(308,'template.filter.category','Filter category','','',0,'2018-06-13 14:14:19'),
	(309,'lancomecontent.Add content','Add Content','','',0,'2018-07-12 16:36:16'),
	(310,'lancomecontent.context.add.topicandingres','Add Topic and Ingres','','',0,'2018-07-12 16:36:16'),
	(311,'lancomecontent.context.add.video','Add Video','','',0,'2018-07-12 16:36:16'),
	(312,'lancomecontent.context.add.kolmenostoa','Add kolmenostoa','','',0,'2018-07-12 16:36:16'),
	(313,'lancomecontent.context.add.largeimage','Add Large Image','','',0,'2018-07-12 16:36:16'),
	(314,'lancomecontent.context.add.nosto','Add Nost','','',0,'2018-07-12 16:36:16'),
	(315,'lancomecontent.context.add.topicandtext','Add Topic And Text','','',0,'2018-07-12 16:36:16'),
	(316,'examplecontent.Add content','Add Content','','',0,'2018-07-13 11:42:05'),
	(317,'examplecontent.context.add.topicandingres','Add Topic and Ingres','','',0,'2018-07-13 11:42:05'),
	(318,'examplecontent.context.add.video','Add ','','',0,'2018-07-13 11:42:05'),
	(319,'examplecontent.context.add.kolmenostoa','Add Kolmenosta','','',0,'2018-07-13 11:42:05'),
	(320,'examplecontent.context.add.largeimage','Add Large Image','','',0,'2018-07-13 11:42:05'),
	(321,'examplecontent.context.add.nosto','Add Nosto','','',0,'2018-07-13 11:42:05'),
	(322,'examplecontent.context.add.topicandtext','Add Topic and Text','','',0,'2018-07-13 11:42:05'),
	(323,'template.read more','Read More','','',0,'2018-07-13 11:44:17');

/*!40000 ALTER TABLE `translations` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
